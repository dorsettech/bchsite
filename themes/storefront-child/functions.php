<?php
/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
include_once 'api_store_vendors.php';
require_once 'hooks-functions.php';

function sf_child_theme_dequeue_style() {
    wp_dequeue_style('storefront-style');
    wp_dequeue_style('storefront-woocommerce-style');
}

/* * ***** CORS ****** */

function add_custom_headers() {

    remove_filter('rest_pre_serve_request', 'rest_send_cors_headers');

    add_filter('rest_pre_serve_request', function ($value) {
        header('Access-Control-Allow-Headers: Authorization, X-WP-Nonce,Content-Type, X-Requested-With, Accept, x-xsrf-token');
        // header('Access-Control-Allow-Origin: https://m.stripe.com/', TRUE);
		//header('Access-Control-Allow-Origin: https://connect.stripe.com/', TRUE);
        header('Access-Control-Allow-Origin: *');
        // header('Access-Control-Allow-Origin: http://localhost:8100');
//        header_remove('Access-Control-Allow-Origin');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Credentials: true');
        return $value;
    });
}

add_action('rest_api_init', 'add_custom_headers', 15);

// add_filter('allowed_http_origins', 'add_allowed_origins');

function add_allowed_origins($origins) {
    $origins[] = 'https://emceaz9wmwe.exactdn.com';
    $origins[] = 'https://thebritishcrafthouse.co.uk';
    $origins[] = 'https://m.stripe.com';
    return $origins;
}
//
//add_action('rest_pre_serve_request', function() {
//    $list = headers_list();
//    update_option('list_header_set', $list);
////    delete_option('list_header_set');
////    header_remove('Access-Control-Allow-Origin');
//}, 999);




/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */
//** this code is used to fix the cores issues on IPA
add_filter('kses_allowed_protocols', function ($protocols) {
    $protocols[] = 'ionic';
    return $protocols;
});

function shs_remove_actions_parent_theme() {
    remove_action('storefront_page', 'storefront_page_header', 10);
}

add_action('init', 'shs_remove_actions_parent_theme', 1);

add_action('init', 'z_remove_wc_breadcrumbs');

function z_remove_wc_breadcrumbs() {
    remove_action('storefront_before_content', 'woocommerce_breadcrumb', 10);
}

function x_my_custom_widgets_init() {

    register_sidebar(array(
        'name' => __('Shop Sidebar', '__x__'),
        'id' => 'sidebar-shop',
        'description' => __('Shop filters.', '__x__'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));
}

add_action('widgets_init', 'x_my_custom_widgets_init');

add_filter('wcfmmp_store_tabs', function ($store_tabs, $store_id) {
    if (isset($store_tabs['about'])) {
        unset($store_tabs['about']);
    }

    return $store_tabs;
}, 50, 2);

function prefix_category_title($title) {
    if (is_category()) {
        $title = single_cat_title('', false);
    }
    return $title;
}

add_filter('get_the_archive_title', 'prefix_category_title');

add_action('init', 'jk_customise_storefront');

function jk_customise_storefront() {
    // Remove the storefromt post content function
    remove_action('storefront_loop_post', 'storefront_post_content', 30);

    // Add our own custom function
    add_action('storefront_loop_post', 'jk_custom_storefront_post_content', 30);
}

function jk_custom_storefront_post_content() {
    ?>
    <div class="entry-content" itemprop="articleBody">
        <?php
        if (has_post_thumbnail()) {
            the_post_thumbnail('full', array('itemprop' => 'image'));
        }
        ?>
        <?php the_excerpt(); ?>
    </div><!-- .entry-content -->
    <?php
}

/**
 * Remove related products output
 */
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

/* add articles tab */
add_action(
        'wcfmmp_after_store_article_loop_start', function ($store_id, $store_info) {
    echo
    '<div id="primary" class="content-area"> <main id="main" class="site-main" role="main">';
}, 50, 2);
add_action('wcfmmp_store_article_template', function () {
    get_template_part('content', 'post');
});
add_action('wcfmmp_store_article_template_none', function () {
    get_template_part('content', 'none');
});
add_action('wcfmmp_before_store_article_loop_end', function ($store_id, $store_info) {
    echo '</div></div>';
}, 50, 2);
add_filter( 'wcfmmp_store_tabs', 'new_wcfmmp_store_tabs',10,2);
function new_wcfmmp_store_tabs($store_tabs, $id) {
   $store_tabs['articles'] =  __( 'Blogs', 'wc-multivendor-marketplace' );
   return $store_tabs;
}
add_filter('wcfm_is_allow_store_articles', '__return_true');


// Move Product tabs underneath the product images
// =============================================================================
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
add_action('woocommerce_product_thumbnails', 'woocommerce_output_product_data_tabs', 10);
// =============================================================================
//change description tab name
add_filter('woocommerce_product_tabs', 'woo_rename_tabs', 98);

function woo_rename_tabs($tabs) {

    $tabs['description']['title'] = __('Details'); // Rename the description tab
    return $tabs;
}

/**
 * Reorder product data tabs
 */
add_filter('woocommerce_product_tabs', 'woo_reorder_tabs', 98);

function woo_reorder_tabs($tabs) {

    $tabs['description']['priority'] = 1; // Description second
    return $tabs;
}

function wcfm_custom_2008_translate_text($translated) {
    $translated = str_ireplace('store', 'Shop', $translated);
    return $translated;
}

/*add_filter('gettext', 'wcfm_custom_2008_translate_text');
add_filter('ngettext', 'wcfm_custom_2008_translate_text');*/
add_filter('wcfm_is_allow_store_inquiry_custom_button_label', '__return_true');
add_filter('wcfm_is_allow_store_visibility', '__return_false');

function storefront_credit() {
    ?>
    <div class="site-info">
        <?php echo esc_html(apply_filters('storefront_copyright_text', $content = '&copy; ' . get_bloginfo('name') . ' ' . date('Y'))); ?>
    </div><!-- .site-info -->
    <?php
}

function hs_image_editor_default_to_gd($editors) {
    $gd_editor = 'WP_Image_Editor_GD';
    $editors = array_diff($editors, array($gd_editor));
    array_unshift($editors, $gd_editor);
    return $editors;
}

add_filter('wp_image_editors', 'hs_image_editor_default_to_gd');

/**
 * Add Continue Shopping Button on Cart Page
 */
add_action('woocommerce_after_cart_totals', 'tl_continue_shopping_button');

function tl_continue_shopping_button() {
    $shop_page_url = get_permalink(woocommerce_get_page_id('shop'));

    echo '<div class="">';
    echo ' <a href="' . $shop_page_url . '" class="button wc-forward">Continue Shopping</a>';
    echo '</div>';
}

add_filter('wcfmmp_shipping_processing_times', function ($processing_times) {
    $processing_times[10] = __('5-7 business days', 'wcfm');
    return $processing_times;
}, 50);

function
wcfmmp_custom_hide_local_pickup_vendor($methods) {
    unset($methods['local_pickup']);
    return
            $methods;
}

/**
 * These next few bits hide local pick up and free shipping from the vendors shipping method options - keeping it super simple for them as they can only now pick flat rate.
 */
add_filter('vendor_shipping_methods', 'wcfmmp_custom_hide_local_pickup_vendor');

function
wcfmmp_custom_hide_free_shipping_vendor($methods) {
    unset($methods['free_shipping']);
    return
            $methods;
}

add_filter(
        'vendor_shipping_methods', 'wcfmmp_custom_hide_free_shipping_vendor');
/**
 * This next bit forces twelve products per page for vendors shops as the setting in the back end didn't seem to do it.
 */
add_filter(
        'wcfmmp_store_ppp', function ($post_per_page) {
    $post_per_page = '12';
    return $post_per_page;
}, 50);
/**
 * This next bit removes the seo fields relating to social for things like open graph as this was just confusing vendors. It's in the Shop social and SEO settings.
 */
add_filter(
        'wcfm_is_allow_vendor_seo_facebook', '__return_false');
add_filter('wcfm_is_allow_vendor_seo_twitter', '__return_false');

/**
 * This next bit switches the word articles for blog in peoples shops.
 */
/*function wcfm_custom_1010_translate_text($translated) {
    $translated = str_ireplace('Articles', 'Blog', $translated);
    return $translated;
}

add_filter('gettext', 'wcfm_custom_1010_translate_text');
add_filter('ngettext', 'wcfm_custom_1010_translate_text');*/
/**
 * This next bit hides the video tutorial links for the admin as they only take them to the WCFM ones which are not suitable.
 */
add_filter('wcfm_is_allow_video_tutorial', '__return_false');

function register_image_sizes() {
    add_theme_support('post-thumbnails');
    add_image_size('carousel-twofifty', 250, 250, true);
}

add_action('after_setup_theme', 'register_image_sizes');

/*function wcfm_custom_1410_translate_text($translated) {
    $translated = str_ireplace('Inquiry', 'Enquiry', $translated);
    return $translated;
}

add_filter('gettext', 'wcfm_custom_1410_translate_text');
add_filter('ngettext', 'wcfm_custom_1410_translate_text');*/
/**
 * This next bit hides social media form from vendors
 */
add_filter('wcfm_profile_fields_social', function ($social_fields) {
    $social_fields = wcfm_hide_field('linkdin', $social_fields);
    $social_fields = wcfm_hide_field('google_plus', $social_fields);
    $social_fields = wcfm_hide_field('snapchat', $social_fields);
    $social_fields = wcfm_hide_field('youtube', $social_fields);
    return $social_fields;
}, 50);
/**
 * This forces single shop checkout
 */
add_action('woocommerce_add_to_cart_validation', function ($is_allow, $product_id, $quantity) {
    $product = get_post($product_id);
    $product_author = $product->post_author;
    //Iterating through each cart item
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        $cart_product_id = $cart_item['product_id'];
        $cart_product = get_post($cart_product_id);
        $cart_product_author = $cart_product->post_author;
        if ($cart_product_author != $product_author) {
            $is_allow = false;
            break;
        }
    }
    if (!$is_allow) {
        // We display an error message
        wc_clear_notices();
        wc_add_notice(__("We noticed you have products from another seller in your basket. Please visit your basket and checkout with their items first and then come back for this one. Thanks so much", "wcfm"), 'error');
    }
    return $is_allow;
}, 99, 3);

/**
 * Hide becone a vendor link on registration and login page
 */
add_filter('wcfm_is_allow_my_account_become_vendor', '__return_false');

/**
 * Resolve analytics inaccuracies when GEO locate isn't enabled
  add_filter( 'wcfm_is_allow_wc_geolocate', '__return_false' );
 */
/**
 * Hide shipping settings for vendors
 */
add_filter('wcfm_is_allow_vshipping_settings', '__return_false');

/**
 * Stops Next payment notifications being sent out
  add_filter( 'wcfm_is_allow_vendor_membership_renewal_notification', '__return_false' );
 */
add_filter('wcfmmp_is_allow_store_list_country_filter', '__return_false');
add_filter('wcfmmp_is_allow_store_list_state_filter', '__return_false');

/* add_action( 'wcfmmp_after_store_list_serach_form', function($WCFM) {

  ?>
  <select id="wcfmmp_store_country" class="country_select wcfm-select" name="wcfmmp_store_country">
  <option value="GB"><?php _e("United Kingdom (UK)"); ?></option>
  </select>
  <?php

  } ); */

add_filter('woocommerce_add_success', 'matched_zone_per_product_shpping_message', 10, 1);

function matched_zone_per_product_shpping_message($message) {
    if ($message == 'Customer matched zone "Per-Product Shipping"') {
        if (is_page('basket') || is_page('checkout')) {
            return '';
        }
    }
    return $message;
}

/* set per product shipping to true default */
add_filter('wcfm_per_product_shipping_fields', 'wcfm_per_product_shipping_fields_set_default', 10, 3);

function wcfm_per_product_shipping_fields_set_default($options, $shipping_fields, $product_id) {
    if ($product_id == 0) {
        $options['_per_product_shipping']['dfvalue'] = 'yes';
    }
    return $options;
}

// Convert pdf logo url into image path
add_filter("wcfmmp_store_logo", "wcfmmarketplace_logo_url_to_src", 10, 2);

function wcfmmarketplace_logo_url_to_src($store_logo, $vendor_id) {
    global $WCFM, $wpdb, $WCMp;
    $marketplece = wcfm_is_marketplace();
    if ($marketplece == 'wcfmmarketplace') {
        $vendor_data = get_user_meta($vendor_id, 'wcfmmp_profile_settings', true);
        $gravatar = isset($vendor_data['gravatar']) ? absint($vendor_data['gravatar']) : 0;
        $gravatar_url = $gravatar ? get_attached_file($gravatar) : '';

        if (!empty($gravatar_url)) {
            return $gravatar_url;
        }
    }
    return $store_logo;
}

function hb_filter_vendor_email_shipping_value($order) {
    $order_id = $order->get_id();
    // if ( ( $refunded = $order->get_total_shipping_refunded() ) > 0 ) {
    //     $shipping = '<del>' . strip_tags( wc_price( $order->get_total_shipping(), array( 'currency' => $order->get_currency() ) ) ) . '</del> <ins>' . wc_price( $order->get_total_shipping() - $refunded, array( 'currency' => $order->get_currency() ) ) . '</ins>';
    // } else {
    //     $shipping = wc_price( $order->get_total_shipping(), array( 'currency' => $order->get_currency() ) );
    // }
    $shipping = wc_price($order->get_total_shipping(), array('currency' => $order->get_currency()));

    $pps_shipping_charges = get_post_meta($order_id, 'pps_shipping_charges', true);
    if (!empty($pps_shipping_charges)) {
        $items = $order->get_items();
        $pps_shipping_cost_html = '';
        $pps_shipping_cost = $pps_shipping_charges['shipping_charges'];
        $single_item_line_cost = $pps_shipping_charges['single_item_line_cost'];
        $single_item_item_cost = $pps_shipping_charges['single_item_item_cost'];

        foreach ($items as $item_id => $item_data) {
            $item_quantity = $item_data->get_quantity();
            if (!empty($pps_shipping_cost) && count($items) > 1) {
                foreach ($pps_shipping_cost as $shipping_cost) {
                    $pps_shipping_cost_html .= wc_price($shipping_cost);
                    $pps_shipping_cost_html .= ($shipping_cost != end($pps_shipping_cost)) ? ' + ' : ' = ';
                }
            } elseif (!empty($single_item_line_cost) && count($items) == 1 && $item_quantity > 1) {
                $pps_shipping_cost_html .= wc_price($single_item_line_cost[0]);
            }
            if ($single_item_item_cost[0] != 0 && count($items) == 1 && $item_quantity > 1) {
                $pps_shipping_cost_html .= ' + ' . wc_price($single_item_item_cost[0]) . ' = ';
            }
            $pps_shipping_total_cost = array_sum($pps_shipping_charges['shipping_charges']);
            $pps_shipping_cost_html .= wc_price($pps_shipping_total_cost);
            break;
        }
        $pps_shipping_method = get_post_meta($order_id, 'pps_shipping_method', true);
        $method_data = get_option('woocommerce_per_product_' . $pps_shipping_method . '_settings');
        if ($method_data['title']) {
            $pps_shipping_cost_html .= ' <small class="shipped_via">via ' . $method_data['title'] . '</small>';
        }
        $shipping = $pps_shipping_cost_html;
    }
    return $shipping;
}

// upon shipped mark order as completed
add_filter('wcfm_is_force_shipping_address', '__return_true');
add_action('wcfm_after_order_mark_shipped', 'bch_mark_order_complted_on_ship', 50, 6);

function bch_mark_order_complted_on_ship($order_id, $order_item_id, $tracking_code, $tracking_url, $product_id, $wcfm_tracking_data) {
    $traking_added = false;
    $order = wc_get_order($order_id);
    $order->update_status('completed', '', true);
    do_action('woocommerce_order_edit_status', $order_id, 'completed');
}

// exclude archive post in the count
add_filter('wcfm_limit_check_status', 'bch_set_product_filter_exclude_archive', 10);

function bch_set_product_filter_exclude_archive($post_type) {
    $p_arr = array('publish', 'pending', 'draft', 'future', 'private');
    return $p_arr;
}

//enable full editor for excerpt for wcfm
add_filter('wcfm_product_manage_fields_content', 'bch_unset_tiny_editor', 10, 2);

function bch_unset_tiny_editor($field_arr) {
    if (isset($field_arr['excerpt']['teeny'])) {
        unset($field_arr['excerpt']['teeny']);
    }
    return $field_arr;
}

add_filter('wcfmmmp_gross_sales_shipping_cost', 'bch_get_original_shipping', 10, 2);

function bch_get_original_shipping($shipping_aount, $vendor_id) {
    $order_id = $_GET['order_id'];
    $order = new WC_Order($order_id);
    $order_shipping_total = $order->get_total_shipping();
    return $order_shipping_total;
}

add_filter('wcfm_gross_total', 'bch_filter_order_invoice_gross', 10, 3);

function bch_filter_order_invoice_gross($shipping_aount, $vendor_id, $order_id) {
    $order = new WC_Order($order_id);
    $get_total_order_gross = $order->get_total();
    return $get_total_order_gross;
}

function google_analytics_tracking_code() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143472985-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-143472985-1');
    </script>
    <?php
}

// include GA tracking code before the closing head tag
add_action('wp_head', 'google_analytics_tracking_code');

function disable_checkout_button_no_shipping() {
    $package_counts = array();
    // get shipping packages and their rate counts
    $packages = WC()->shipping->get_packages();
    foreach ($packages as $key => $pkg) {
        $package_counts[$key] = count($pkg['rates']);
    }

    // remove button if any packages are missing shipping options
    if (in_array(0, $package_counts)) {
        remove_action('woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20);
    }
}

add_action('woocommerce_proceed_to_checkout', 'disable_checkout_button_no_shipping', 1);
add_action('woocommerce_before_cart', 'auto_select_free_shipping_by_default');

function auto_select_free_shipping_by_default() {
    if (isset(WC()->session) && !WC()->session->has_session()) {
        WC()->session->set_customer_session_cookie(true);
    }

    // Check if "free shipping" is already set
    if (strpos(WC()->session->get('chosen_shipping_methods')[0], 'free_shipping') !== false) {
        return;
    }

    // Loop through shipping methods
    foreach (WC()->session->get('shipping_for_package_0')['rates'] as $key => $rate) {
        if ($rate->method_id === '16') {
            // Set "Free shipping" method
            WC()->session->set('chosen_shipping_methods', array($rate->id));
            return;
        }
    }
}

//add_filter( 'wcfmmp_store_ppp', function( $post_per_page ) {
//$post_per_page = '10';
//return $post_per_page;
//}, 50 );
add_action('wcfm_after_product_archived', function ($product_id) {
    $product = wc_get_product($product_id);
    $product->set_catalog_visibility('hidden');
    $product->save();
});

// Load vendor Featured Categories
function vendor_ftr_cats($atts) {
    global $wp;
    $fetured_cat = '';
    $us = explode("/", $wp->request);
    $user = get_user_by('slug', $us[1]);
    $fetured_cat = get_user_meta($user->ID, 'product_section_cat', true);

    if ($fetured_cat != '') {
        $fetured_cat = explode(',', $fetured_cat);
        // $fetured_cat = array_slice($fetured_cat, 3);
        if (isset($fetured_cat) && $fetured_cat > 0 && !empty($fetured_cat)):
            ob_start();
            ?>
            <div class="sidebar_heading">
                <h4 class="widget-title">Categories</h4>
                <ul>
                    <?php
                    foreach ($fetured_cat as $cat_value):
                        if ($cat_value != '') {
                            $cate_name = get_term_by('id', (int) $cat_value, 'shop_category');
                            $term_link = get_term_link((int) $cat_value);
                            if (!is_wp_error($term_link)) {
                                ?>
                                <li>
                                    <a href="<?php echo $term_link; ?>"><?php echo $cate_name->name; ?></a>
                                </li>
                                <?php
                            }
                        }
                    endforeach;
                    ?>
                </ul>
            </div>
            <?php
        endif;
    }
    $fetured_cats = ob_get_contents();
    ob_end_clean();
    return $fetured_cats;
    // }
}

add_shortcode('featured_catgory', 'vendor_ftr_cats');

// Code for Viraj script goes here
//add_action('wp_head', 'wfcm_select2_dd_script');
//function wfcm_select2_dd_script() {
//echo "<script>jQuery(document).ready(function() {
// jQuery('#dropdown_product_cat').select2();
//});</script>";
//}
// add_action('wp_footer', 'wfcm_select2_dd_script', 99999);

function wfcm_select2_dd_script() {
    ?>
    <script>
        jQuery(document).ready(function () {
            jQuery('#dropdown_product_cat').select2();
        });
    </script>
    <?php
}

// =============================================================

remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');

#########################################################

add_filter('kses_allowed_protocols', function ($protocols) {
    $protocols[] = 'ionic';
    return $protocols;
});

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/get/here_today_gone_tom', array(
      'methods' => 'GET',
        'callback' => 'here_today_gone_tom',
            )
    );
});

function here_today_gone_tom($data) {
    $final_data = [];
    $autumn_home = array(
        'post_type' => 'product',
        //'posts_per_page' => 4,
        'product_cat' => 'here-today-gone-tomorrow',
    );
    $loop = new WP_Query($autumn_home);
    while ($loop->have_posts()): $loop->the_post();
        global $product;
        $product_id = get_the_ID();
        $_product = wc_get_product($product_id);
        $image_id = $_product->get_image_id();
        $image_url = wp_get_attachment_image_url($image_id, 'full');
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => $image_url,
            //'title' => get_the_title(),
            'regular_price' => number_format( $_product->get_regular_price(), 2 ),
            'sale_price' => number_format( $_product->get_sale_price(), 2 ),
            'get_price' => number_format( $_product->get_price(), 2 ),
        );
        array_push($final_data, $data);
    endwhile;
    wp_reset_query();
    $response['status'] = 'success';
    $response['data'] = $final_data;
    return $response;
    exit;
}

// add_action('rest_api_init', function () {
//     register_rest_route('wp/v2', '/get/here_today_gone_tom_using_slider', array(
//         'methods' => 'GET',
//         'callback' => 'here_today_gone_tom_using_slider',
//             )
//     );
// });
//
// function here_today_gone_tom_using_slider($data) {
//     $final_data = [];
//     $post_meta_value = get_post_meta(3546, 'sp_wpcp_upload_options', true);
//     if (!empty($post_meta_value['wpcp_specific_product'])) {
//
//         $product_query = catSlideQueryData(3546);
//         // foreach ($post_meta_value['wpcp_specific_product'] as $key => $valueid) {
//         foreach ($product_query as $key => $valueid) {
//
//             $_product = wc_get_product($valueid->ID);
//
//             if ($_product) {
//                 //$title = wp_trim_words( $_product->get_title(), 4, '');
//                 $image_id = $_product->get_image_id();
//                 $url = get_permalink($valueid->ID);
//                 $image_url = wp_get_attachment_image_url($image_id, 'full');
//                 $data = array(
//                     'id' => $valueid->ID,
//                     'link' => $url,
//                     'img' => $image_url,
//                     //'title' => $title,
//                     'regular_price' => number_format ($_product->get_regular_price(), 2 ),
//                     'sale_price' => number_format ($_product->get_sale_price(), 2 ),
//                     'get_price' => number_format ($_product->get_price(), 2 ),
//                 );
//
//                 array_push($final_data, $data);
//             }
//         }
//         wp_reset_query();
//         $response['status'] = 'success';
//         $response['data'] = $final_data;
//         return $response;
//         exit;
//     } else {
//
//         $response['status'] = 'error';
//         $response['message'] = "No data found";
//         return $response;
//         exit;
//     }
// }

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/get/featured', array(
        'methods' => 'GET',
        'callback' => 'get_featured',
            )
    );
});

function get_featured($data) {
    $final_data = [];
    $autumn_home_arr = array();
    $autumn_home = array(
        'post_type' => 'product',
        //'posts_per_page' => 15,
        'product_cat' => 'autumn-home',
    );
    $loop = new WP_Query($autumn_home);
    while ($loop->have_posts()): $loop->the_post();
        global $product;
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => woocommerce_get_product_thumbnail(),
            'title' => get_the_title(),
        );
        array_push($autumn_home_arr, $data);
    endwhile;
    wp_reset_query();
    $response['status'] = 'success';
    $response['data'] = $autumn_home_arr;
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/post/contact/', array(
        'methods' => 'POST',
        'callback' => 'processForm',
            )
    );
});

function processForm($data) {

    if (isset($data)) {

        $name = $_POST['your-name'];
        $email = $_POST['your-email'];
        $message = $_POST['your-message'];
        $to = get_bloginfo('admin_email'); //sendto@example.com
        //$to = "noman.bin.iqbal@gmail.com";
        $subject = 'User Contact Email';
        $body = '<div class="bodyemail"><p><span>Name:</span>' . $name . '</p><p><span>Email:<span>' . $email . '</p><p><span>Massage:             <span>' . $message . '</p> </div>';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $sent_mail = wp_mail($to, $subject, $body, $headers);
        if ($sent_mail) {
            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
        }
        return $response;
        exit;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-page/', array(
        'methods' => 'POST',
        'callback' => 'home_page',
            )
    );
});

function home_page() {

    $Gift_Ideas_For_You_arr = [];
    $Heading = 'Gift ideas for you…';

    $Home_interiors_image = get_site_url() . '/wp-content/uploads/2020/06/shutterstock_339362615-2-1.jpg';
    $Home_interiors_link = 'home-and-interiors';
    $Home_interiors_title = 'Home & interiors';

    $Cards_and_stationery_image = get_site_url() . '/wp-content/uploads/2020/06/comp-shutterstock_680957929-1-1-500x334.jpg';
    $Cards_and_stationery_link = 'cards-stationery';
    $Cards_and_stationery_title = 'Cards & stationery';

    $Jewellery_image = get_site_url() . '/wp-content/uploads/2020/06/shutterstock_308848862-2-1.jpg';
    $Jewellery_link = 'fashion-and-jewellery';
    $Jewellery_title = 'Handmade Jewellery';

    $Baby_Child_image = get_site_url() . '/wp-content/uploads/2020/06/comp-shutterstock_1114763348-1-1-500x333.jpg';
    $Baby_Child_link = 'baby-gifts-child-gifts';
    $Baby_Child_title = 'Baby & Child';

    $fashion_image = get_site_url() . '/wp-content/uploads/2020/11/shutterstock_705175807-1.jpg';
    $fashion_link = 'fashion';
    $fashion_title = 'Fashion';

    // $Weddings_image = get_site_url().'/wp-content/uploads/2020/06/comp-shutterstock_1182517363-1-1-500x333.jpg';
    // $Weddings_link = 'weddings';
    // $Weddings_title = 'Weddings';

    $Art_image = get_site_url() . '/wp-content/uploads/2020/06/comp-shutterstock_1071475751-1-500x319.jpg';
    $Art_link = 'art';
    $Art_title = 'Art';

    $Gifts_occasions_image = get_site_url() . '/wp-content/uploads/2020/06/comp-shutterstock_184478294-1-1-500x333.jpg';
    $Gifts_occasions_link = 'gifts-occasions';
    $Gifts_occasions_title = 'Gifts & occasions';

    $Home_interiors[] = array("title" => $Home_interiors_title, "image" => $Home_interiors_image, "slug" => $Home_interiors_link);
    $Cards_and_stationery[] = array("title" => $Cards_and_stationery_title, "image" => $Cards_and_stationery_image, "slug" => $Cards_and_stationery_link);

    $Jewellery[] = array("title" => $Jewellery_title, "image" => $Jewellery_image, "slug" => $Jewellery_link);

    $Baby_Child[] = array("title" => $Baby_Child_title, "image" => $Baby_Child_image, "slug" => $Baby_Child_link);

    //$Weddings[] = array("title"=>$Weddings_title,"image"=>$Weddings_image,"slug"=>$Weddings_link);
    $Fashion[] = array("title" => $fashion_title, "image" => $fashion_image, "slug" => $fashion_link);

    $Art[] = array("title" => $Art_title, "image" => $Art_image, "slug" => $Art_link);

    $Gifts_occasions[] = array("title" => $Gifts_occasions_title, "image" => $Gifts_occasions_image, "slug" => $Gifts_occasions_link);

    $gift_Ideas_For_You_marge = array_merge($Home_interiors, $Cards_and_stationery, $Jewellery, $Baby_Child, $Fashion, $Art, $Gifts_occasions);

    $Gift_Ideas_For_You_Data = array(
        'Heading' => $Heading,
        'gift_Ideas_For_You' => $gift_Ideas_For_You_marge,
    );
    $Mainheading = 'Featured collections…';
    /* $letterbox_gift_ideas_image = get_site_url().'/wp-content/uploads/2020/10/shutterstock_1037746972-1.jpg';
      $letterbox_gift_ideas_link = 'letterbox-gifts';
      $letterbox_gift_ideas_title = 'Letterbox Gift Ideas';

      $Christmas_Ideas_image = get_site_url().'/wp-content/uploads/2020/09/shutterstock_484872337-1.jpg';
      $Christmas_Ideas_link ='christmas';
      $Christmas_Ideas_title = 'Christmas Ideas'; */

    $Handpicked_image = get_site_url() . '/wp-content/uploads/2021/03/resized-image-Promo-1.jpeg';
    $Handpicked_title = 'Handpicked';
    $Handpicked_Subtitle = 'BRITISH ARTISANS';
    $Handpicked_description = 'Selected British artisans makers';
    $Handpicked_slug = 'meet-the-maker';

    // $Original_image = get_site_url().'/wp-content/uploads/2021/04/bch-beaut.jpg';
    $Original_image = get_site_url() . '/wp-content/uploads/2021/06/TBCH-love-1-2.jpg';
    $Original_title = 'Original';
    $Original_subtitle = 'HANDMADE CREATIONS';
    $Original_description = 'Beautifully made, for you, in Britain';
    $Original_slug = 'home-and-interiors';
    //$Original_slug = 'commissions';
    // $Gifts_image = get_site_url().'/wp-content/uploads/2021/04/bch-beaut2.jpg';
    $Gifts_image = get_site_url() . '/wp-content/uploads/2021/06/My-Post-2021-06-11T193633.786.jpg';
    $Gifts_title = 'Gifts';
    // $Gifts_subtitle = 'YOU’LL WANT TO KEEP';
    $Gifts_subtitle = 'TO BE TREASURED';
    // $Gifts_description = 'That’s OK though, we won’t tell…';
    $Gifts_description = 'Forever…';
    $Gifts_slug = 'gifts-occasions';

    $letterbox_gift_ideas[] = array("title" => $letterbox_gift_ideas_title, "image" => $letterbox_gift_ideas_image, "slug" => $letterbox_gift_ideas_link);
    $Christmas_Ideas[] = array("title" => $Christmas_Ideas_title, "image" => $Christmas_Ideas_image, "slug" => $Christmas_Ideas_link);
    //$Handpicked[] = array("title"=>$Handpicked_title,"image"=>$Handpicked_image,"subtitle"=>$Handpicked_Subtitle,"description"=>$Handpicked_description, 'slug'=>$Handpicked_slug);
    //$Original[] = array("title"=>$Original_title,"image"=>$Original_image,"subtitle"=>$Original_subtitle,"description"=>$Original_description, 'slug'=> $Original_slug);
    //$Gifts[] = array("title"=>$Gifts_title,"image"=>$Gifts_image,"subtitle"=>$Gifts_subtitle,"description"=>$Gifts_description, 'slug' =>$Gifts_slug);

    $threeSections = array(
        //array("title"=>$Handpicked_title,"image"=>$Handpicked_image,"subtitle"=>$Handpicked_Subtitle,"description"=>$Handpicked_description, 'slug'=>$Handpicked_slug),
        array("title" => $Handpicked_title, "image" => $Handpicked_image, "subtitle" => $Handpicked_Subtitle, "description" => $Handpicked_description, 'slug' => $Handpicked_slug),
        array("title" => $Original_title, "image" => $Original_image, "subtitle" => $Original_subtitle, "description" => $Original_description, 'slug' => $Original_slug),
        array("title" => $Gifts_title, "image" => $Gifts_image, "subtitle" => $Gifts_subtitle, "description" => $Gifts_description, 'slug' => $Gifts_slug),
    );

    $easter_image = get_site_url() . '/wp-content/uploads/2022/09/shutterstock_390281551-1-1.jpg';
    $easter_link = 'home-and-interiors';
    $easter_title = 'IDEAS FOR YOUR HOME';

    $send_hug_image = get_site_url() . '/wp-content/uploads/2022/09/shutterstock_2044773218-1.jpg';
    $send_hug_link = 'gifts-occasions';
    $send_hug_title = 'PERFECT GIFTS';

    $easter[] = array("title" => $easter_title, "image" => $easter_image, "slug" => $easter_link, "subtitle" => '', "description" => '');
    $send_hug[] = array("title" => $send_hug_title, "image" => $send_hug_image, "slug" => $send_hug_link, "subtitle" => '', "description" => '');

    $Featured_collections_marge = array_merge($easter, $send_hug);
    $Featured_collections = array(
        'Heading' => $Mainheading,
        'Featured_collections' => $Featured_collections_marge,
    );
    /*     * *******************Sell at The British Craft House section************ */
    $Newheading = 'Sell at The British Craft House';
    $Simple_title = 'Simple';
    $Simple_subtitle = 'APPLICATION';
    $Simple_description = 'No fuss, complete the on line form and apply to join our handmade creative community';

    $Transparent_title = 'Transparent';
    $Transparent_subtitle = 'FEES';
    $Transparent_description = 'Low set up fee and great value flat-rate commission. No hidden extra costs';

    $Fantastic_title = 'Fantastic';
    $Fantastic_subtitle = 'SERVICE & SUPPORT';
    $Fantastic_description = 'Award winning support and advice from the largest, engaged community of British crafters';

    $Simple[] = array("title" => $Simple_title, "subtitle" => $Simple_subtitle, "description" => $Simple_description);
    $Transparent[] = array("title" => $Transparent_title, "subtitle" => $Transparent_subtitle, "description" => $Transparent_description);
    $Fantastic[] = array("title" => $Fantastic_title, "subtitle" => $Fantastic_subtitle, "description" => $Fantastic_description);

    $Sell_British_Craft_House_marge = array_merge($Simple, $Transparent, $Fantastic);
    $Sell_British_Craft_House = array(
        'heading' => $Newheading,
        'Sell_British_Craft_House' => $Sell_British_Craft_House_marge,
    );

    $response['status'] = 'success';
    $response['gift_ideas_for'] = $Gift_Ideas_For_You_Data;
    $response['featured_collections'] = $Featured_collections;
    $response['Sell_british_craft_house'] = $Sell_British_Craft_House;
    $response['threeSections'] = $threeSections;
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-and-interiors/', array(
        'methods' => 'POST',
        'callback' => 'home_and_interiors',
            )
    );
});

function home_and_interiors() {

    $arr = array('slider_one' => 1641, 'slider_two' => 1653, 'slider_three' => 1662);
    $all_slider_data = commonPages($arr);

    $heading = 'Home & Interiors';
    $description = 'Perfectly handcrafted gifts can be what makes the difference between a house and a home.
					Add some unique personality to your styling by choosing from our art, ceramics, lighting, cushions and decorations.
					All handmade with love by our talented team of artisans makers.';
    $art_image = get_site_url() . '/wp-content/uploads/2020/06/shutterstock_782650141-2-1.jpg';
    $art_link = 'art';
    $art_title = 'Art';
    $textiles_image = get_site_url() . '/wp-content/uploads/2019/08/knitting-1981518_1920.jpg';
    $textiles_link = 'textiles';
    $textiles_title = 'Textiles';
    $personalised_image = get_site_url() . '/wp-content/uploads/2019/10/a.jpg';
    $personalised_link = 'personalised-home';
    $personalised_title = 'Personalised Homeware';

    $art[] = array("title" => $art_title, "image" => $art_image, "slug" => $art_link);
    $textiles[] = array("title" => $textiles_title, "image" => $textiles_image, "slug" => $textiles_link);
    $personalised[] = array("title" => $personalised_title, "image" => $personalised_image, "slug" => $personalised_link);
    $home_and_interiors = array_merge($art, $textiles, $personalised);

    $HomeAndInteriors_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $home_and_interiors,
    );

    if ($HomeAndInteriors_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $HomeAndInteriors_Section_one;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-and-interiors-textiles/', array(
        'methods' => 'POST',
        'callback' => 'home_and_interiors_textiles',
            )
    );
});

function home_and_interiors_textiles() {

    $textiles_arr = array();
    $textiles = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'product_cat' => 'textiles',
    );
    $loop = new WP_Query($textiles);
    while ($loop->have_posts()): $loop->the_post();
        global $product;
        $product_id = get_the_ID();
        $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => $productimage[0],
            'title' => get_the_title(),
        );
        array_push($textiles_arr, $data);
    endwhile;
    wp_reset_query();

    $response['status'] = 'success';
    $response['data'] = $textiles_arr;
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-and-interiors/personalised-home/', array(
        'methods' => 'POST',
        'callback' => 'home_and_interiors_personalised_home',
            )
    );
});

function home_and_interiors_personalised_home() {

    $personalised_home_arr = array();
    $personalised_home = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'product_cat' => 'personalised-home',
    );

    $loop = new WP_Query($personalised_home);
    while ($loop->have_posts()): $loop->the_post();
        global $product;
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => woocommerce_get_product_thumbnail(),
            'title' => get_the_title(),
        );
        array_push($personalised_home_arr, $data);
    endwhile;

    wp_reset_query();
    if ($personalised_home_arr) {
        $response['status'] = 'success';
        $response['data'] = $personalised_home_arr;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-and-interiors/art/wooden-art/', array(
        'methods' => 'POST',
        'callback' => 'home_and_interiors_wooden_art',
            )
    );
});

function home_and_interiors_wooden_art() {
    $wooden_art_arr = array();
    $wooden_art = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'product_cat' => 'wooden-art',
    );

    $loop = new WP_Query($wooden_art);
    while ($loop->have_posts()): $loop->the_post();
        global $product;
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => woocommerce_get_product_thumbnail(),
            'title' => get_the_title(),
        );
        array_push($wooden_art_arr, $data);
    endwhile;
    wp_reset_query();
    $response['status'] = 'success';
    $response['data'] = $wooden_art_arr;
    return $response;
    exit;
    die;
}

// Removed Archived Filter and Button
add_filter('wcfm_is_allow_archive_product', '__return_false');

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/cards-stationery/', array(
        'methods' => 'POST',
        'callback' => 'cards_and_stationery',
            )
    );
});

function cards_and_stationery() {

    $arr = array('slider_one' => 1590, 'slider_two' => 795, 'slider_three' => 70059);
    $all_slider_data = commonPages($arr);

    $heading = 'Cards & Stationery';
    $description = 'Handmade cards and stationery made in the UK,  especially for you by our talented designers.
    Looking for a perfectly thoughtful handmade card? Maybe a new notebook for some list writing or planning.  Or a sketchbook perhaps…
    Stationery makes for a perfect gift idea too!';

    $Birthday_image = get_site_url() . '/wp-content/uploads/2021/01/Cars-50-Turquoise.jpg';
    $Birthday_link = 'birthday-cards-by-occassion';
    $Birthday_title = 'Birthday Cards';

    $thank_you_image = get_site_url() . '/wp-content/uploads/2020/06/DSCF0017.jpg';
    $thank_you_link = 'thank-you-cards';
    $thank_you_title = 'Thank you Cards';

    $just_becaus_image = get_site_url() . '/wp-content/uploads/2020/06/1-17.jpg';
    $just_becaus_link = 'any-occasion';
    $just_because_title = 'Just Because Cards';

    $Birthday[] = array("title" => $Birthday_title, "image" => $Birthday_image, "slug" => $Birthday_link);
    $thank_you[] = array("title" => $thank_you_title, "image" => $thank_you_image, "slug" => $thank_you_link);
    $just_because[] = array("title" => $just_because_title, "image" => $just_becaus_image, "slug" => $just_becaus_link);
    $cards_and_stationery = array_merge($Birthday, $thank_you, $just_because);

    $cards_and_stationery_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $cards_and_stationery,
    );

    if ($cards_and_stationery_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $cards_and_stationery_Section_one;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/fashion-and-jewellery/', array(
        'methods' => 'POST',
        'callback' => 'jewellery',
            )
    );
});

function jewellery() {

    $arr = array('slider_one' => 1091, 'slider_two' => 3550, 'slider_three' => 285622, 'slider_four' => 3560);
    $all_slider_data = commonPages($arr);

    $heading = 'Handmade Jewellery';
    $description = 'Handmade jewellery from British designers just can’t be beaten to look good and feel great!
					If you like to stand out from the crowd and make a style statement then there is no better way than to commission a unique piece that no-one else will be wearing.';

    $unisex_image = get_site_url() . '/wp-content/uploads/2020/06/1-18.jpg';
    $unisex_link = 'unisex-jewellery';
    $unisex_title = 'Unisex Jewellery';

    $women_image = get_site_url() . '/wp-content/uploads/2020/06/Sterling-Silver-Daisy-Bird-Stud-earrings-new-5.jpg';
    $women_link = 'womens-jewellery';
    $women_title = 'Women’s Jewellery';

    $men_image = get_site_url() . '/wp-content/uploads/2020/06/1.jpeg';
    $men_link = 'mens-jewellery';
    $men_title = 'Men’s Jewellery';

    $unisex[] = array("title" => $unisex_title, "image" => $unisex_image, "slug" => $unisex_link);
    $women[] = array("title" => $women_title, "image" => $women_image, "slug" => $women_link);
    $men[] = array("title" => $men_title, "image" => $men_image, "slug" => $men_link);

    $Jewellery = array_merge($unisex, $women, $men);
    $Jewellery_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Jewellery,
    );

    if ($Jewellery_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $Jewellery_Section_one;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/baby-gifts-child-gifts/', array(
        'methods' => 'POST',
        'callback' => 'baby_and_child_gifts',
            )
    );
});

function baby_and_child_gifts() {

    $arr = array('slider_one' => 1677, 'slider_two' => 70064, 'slider_three' => 67580);
    $all_slider_data = commonPages($arr);

    $heading = 'Baby & Child Gifts';
    $description = 'Handmade gift ideas for children and babies…
						From handcrafted socks to prints with their name on, we have so much that will be sure to make their little faces light up!
						Created by the best of creative British small businesses.';

    $first_image = get_site_url() . '/wp-content/uploads/2019/09/baby-gifts.jpg';
    $first_link = 'gifts-for-baby';
    $first_title = 'Baby Gifts';

    $second_image = get_site_url() . '/wp-content/uploads/2019/08/brothers-457234_1920.jpg';
    $second_link = 'blankets';
    $second_title = 'Blankets';

    $third_image = get_site_url() . '/wp-content/uploads/2020/06/1-14.jpg';
    $third_link = 'childrens-room-and-nursery';
    $third_title = 'Baby Room';
    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $baby_and_child_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );

    if ($baby_and_child_gifts_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $baby_and_child_gifts_Section_one;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/weddings/', array(
        'methods' => 'POST',
        'callback' => 'weddings',
            )
    );
});

function weddings() {

    $arr = array('slider_one' => 785, 'slider_two' => 795, 'slider_three' => 796);
    $all_slider_data = commonPages($arr);

    $heading = 'Weddings';
    $description = 'We have everything handmade here to make your Wedding perfect – truly, everything you have thought of and a few things you won’t have! From stationery to accessories – all handmade in the UK by our clever artisans';

    $first_image = get_site_url() . '/wp-content/uploads/2019/09/bridal-headpiece.jpg';
    $first_link = 'bridal-hair-accessories';
    $first_title = 'Bridal hairpieces';

    $second_image = get_site_url() . '/wp-content/uploads/2019/08/brothers-457234_1920.jpg';
    $second_link = 'gifts-for-the-groom';
    $second_title = 'Gifts for the groom';

    $third_image = get_site_url() . '/wp-content/uploads/2019/07/make-it-happen-1024x682.jpeg';
    $third_link = 'invitations';
    $third_title = 'Invitations';
    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);

    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);
    $baby_and_child_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );

    if ($baby_and_child_gifts_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $baby_and_child_gifts_Section_one;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/fashion/', array(
        'methods' => 'POST',
        'callback' => 'fashion',
            )
    );
});

function fashion() {

    $arr = array('slider_one' => 55741, 'slider_two' => 55405, 'slider_three' => 1107, 'slider_four' => 55404);
    $all_slider_data = commonPages($arr);

    $heading = 'Fashion Gifts';
    $description = 'British fashion gifts from British designers. Whether it’s a gloriously luxurious poncho, silks or knitwear our artisans make exquisite handmade clothes and ccessories.Stand out from the crowd and make a style statement with handcrafted British Fashion. From crocheted socks, felt hats, to the most gorgeous knitwear for both inside and outside. You will find it all here.';

    $first_image = get_site_url() . '/wp-content/uploads/2020/06/IMG_1246-scaled-e1591721062503.jpeg';
    $first_link = 'womens-fashion';
    $first_title = 'Women’s Fashion';

    $second_image = get_site_url() . '/wp-content/uploads/2020/11/shutterstock_1554894680-1.jpg';
    $second_link = 'clothing-outfits';
    $second_title = 'Babies & Children';

    $third_image = get_site_url() . '/wp-content/uploads/2020/06/1-1.jpeg';
    $third_link = 'mens-fashion';
    $third_title = 'Men’s Fashion';

    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $fasion_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );

    if ($fasion_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $fasion_Section_one;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/art/', array(
        'methods' => 'POST',
        'callback' => 'art',
            )
    );
});

function art() {

    $arr = array('slider_one' => 1635, 'slider_two' => 226472, 'slider_three' => 1627);
    $all_slider_data = commonPages($arr);

    $heading = 'Art';
    $description = 'Art to add to your home decor or to give as gifts. We have all manner of styles available, all created in the UK my our small independent creatives.';
    $first_image = get_site_url() . '/wp-content/uploads/2020/07/shutterstock_394541995-1-1.jpg';
    $first_link = 'wooden-art';
    $first_title = 'Wooden Art';
    $second_image = get_site_url() . '/wp-content/uploads/2019/08/mosaic-200864_1920.jpg';
    $second_link = 'ceramic-art';
    $second_title = 'Ceramic Art';
    $third_image = get_site_url() . '/wp-content/uploads/2019/08/vases-3880641_1920.jpg';
    $third_link = 'invitations';
    $third_title = 'Glass Art';
    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);
    $baby_and_child_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );

    if ($baby_and_child_gifts_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $baby_and_child_gifts_Section_one;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/gifts-occasions/', array(
        'methods' => 'POST',
        'callback' => 'gifts_occasions',
            )
    );
});

function gifts_occasions() {

    $arr = array('slider_one' => 269560, 'slider_two' => 239548, 'slider_three' => 204794, 'slider_four' => 3552);
    $all_slider_data = commonPages($arr);

    $heading = 'Gifts & Occasions';
    $description = 'Discover our beautiful handmade gifts and become the most thoughtful gift giver in an instant.Personalised, bespoke, with a great range of ideas for all ages.All made in Britain with care and attention to detail by our team of talented artisan makers';

    $first_image = get_site_url() . '/wp-content/uploads/2020/09/1-11.jpg';
    $first_link = 'for-him';
    $first_title = 'Gifts for Him';

    $second_image = get_site_url() . '/wp-content/uploads/2020/06/make-up-jars-dragonfly-tbch.jpg';
    $second_link = 'gifts-for-her';
    $second_title = 'Gifts for Her';

    $third_image = get_site_url() . '/wp-content/uploads/2020/06/IMG_0361.jpg';
    $third_link = 'gifts-for-children';
    $third_title = 'Gifts for Children';
    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);

    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $baby_and_child_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );

    if ($baby_and_child_gifts_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $baby_and_child_gifts_Section_one;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/letterbox-gifts/', array(
        'methods' => 'POST',
        'callback' => 'letterbox_gifts',
            )
    );
});

function letterbox_gifts() {

    $arr = array('slider_one' => 84447, 'slider_two' => 84451, 'slider_three' => 6407);
    $all_slider_data = commonPages($arr);

    $heading = 'Letter Box Gift Ideas';
    $description = 'If you need to know that your purchased item will just pop through the letterbox then this is the place to be.Search through the fabulous ideas by our artisan sellers and put a smile on someone’s face.Think of it like sending a hug in a box.Here is some inspiration or you can shop the whole collection here!';

    $letterbox_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
    );

    if ($letterbox_gifts_Section_one && $all_slider_data) {
        $response['status'] = 'success';
        $response['section_one'] = $letterbox_gifts_Section_one;
        $response['slider_one'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/christmas/', array(
        'methods' => 'POST',
        'callback' => 'christmas',
            )
    );
});

function christmas() {

    $slider_one_id = 201681;
    $slider_two_id = 176456;
    $slider_three_id = 176600;

    $heading = 'Christmas Gifts, Cards and Decorations';
    $description = 'Christmas gifts are so important to get right. Christmas comes but once a year, so go on – make the most of it with gorgeous handmade designs to make the most wonderful time of the year that bit more special. Thoughtfully chosen gifts and a beautifully decorated home can make loved ones feel cherished. Looking for something unique for your mum? A special Christmas stocking to be passed down the generations or some beautiful handcrafted decorations? You are in the right place! Why not add a teeny tiny something to your basket for yourself this Christmas! Whatever you choose may your Christmas be blessed with joy and cheer!';

    $first_image = get_site_url() . '/wp-content/uploads/2020/09/shutterstock_351220193-1.jpg';
    $first_link = 'for-him';
    $first_title = 'GIFTS FOR HIM';

    $second_image = get_site_url() . '/wp-content/uploads/2020/06/make-up-jars-dragonfly-tbch.jpg';
    $second_link = 'gifts-for-her';
    $second_title = 'GIFTS FOR HER';

    $third_image = get_site_url() . '/wp-content/uploads/2020/09/shutterstock_520231807-1.jpg';
    $third_link = 'gifts-for-children';
    $third_title = 'GIFTS FOR CHILDREN';

    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $letterbox_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );
    $Slider_one_Data = array();
    $slider_one = get_post_meta($slider_one_id, 'sp_wpcp_upload_options', true);
    $slider_one_title = get_post_field('post_title', $slider_one_id);
    $slider_one_imageid = $slider_one['wpcp_gallery'];
    $Slider_one_section_heading = "Gifts Galore";

    if (!empty($slider_one_imageid) && !empty($slider_one_title)) {

        $ArrayOfYour_slider_one_imageid = explode(',', $slider_one_imageid);

        foreach ($ArrayOfYour_slider_one_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $pattern = '/"[a-zA-Z0-9.\/\-\?\&]*"/';
                preg_match($pattern, $sliderurl, $matches);
                $stringinlink = strval($matches[0]);
                $slug = trim($stringinlink, '"');
                $trimmedProduct_gifts = str_replace("/product-category/gifts/", '', $slug);
                $trimmedProduct_category = str_replace("/product-category/", '', $trimmedProduct_gifts);
                $trimmedSomeBrightIdeas = str_replace("gifts/some-bright-ideas/", '', $trimmedProduct_category);
                $trimmedHomeInteriorsGifts = str_replace("home-interiors-gifts/", '', $trimmedSomeBrightIdeas);
                $trimmedSomeByRecipient = str_replace("by-recipient/", '', $trimmedHomeInteriorsGifts);
                $trimmedSomeBrightideas = str_replace("some-bright-ideas/", '', $trimmedSomeByRecipient);
                $trimmedHomeInteriorsGiftByRoom = str_replace("home-interiors-gifthome-by-room/", '', $trimmedSomeBrightideas);
                $FinalNameOfCat = str_replace("/", '', $trimmedHomeInteriorsGiftByRoom);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_one_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $slider_one_title,
            );
            array_push($Slider_one_Data, $data);
        }
    }
    $Slider_two = get_post_meta($slider_two_id, 'sp_wpcp_upload_options', true);
    $Slider_two_Title = get_post_field('post_title', $slider_two_id);
    $Slider_two_Data = [];
    $slider_two_imageid = $Slider_two['wpcp_gallery'];
    $Slider_two_section_heading = "Something rather Special";

    if (!empty($slider_two_imageid) && !empty($Slider_two_Title)) {

        $ArrayOfYour_slider_two_imageid = explode(',', $slider_two_imageid);

        foreach ($ArrayOfYour_slider_two_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_two_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Slider_two_Title,
            );
            array_push($Slider_two_Data, $data);
        }
    }
    $Slider_three = get_post_meta($slider_three_id, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $slider_three_id);
    $Slider_three_data = [];
    $Slider_three_section_heading = "Ideas for everyone!";
    $slider_three_imageid = $Slider_three['wpcp_gallery'];

    if (!empty($slider_three_imageid) && !empty($Slider_three_title)) {

        $ArrayOfYour_slider_three_imageid = explode(',', $slider_three_imageid);

        foreach ($ArrayOfYour_slider_three_imageid as $imageID) {
            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $pattern = '/"[a-zA-Z0-9.\/\-\?\&]*"/';
                preg_match($pattern, $sliderurl, $matches);
                $stringinlink = strval($matches[0]);
                $slug = trim($stringinlink, '"');
                $slug_in_array = explode("/", $slug, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_three_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Slider_three_title,
            );
            array_push($Slider_three_data, $data);
        }
    }
    if ($letterbox_gifts_Section_one && $Slider_one_Data && $Slider_two_Data && $Slider_three_data) {
        $response['status'] = 'success';
        $response['section_one'] = $letterbox_gifts_Section_one;
        $response['slider_one'] = $Slider_one_Data;
        $response['slider_two'] = $Slider_two_Data;
        $response['slider_three'] = $Slider_three_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/category-products/', array(
        'methods' => 'GET',
        'callback' => 'category_products',
            )
    );
});

function category_products($request) {

    $cat = $_REQUEST['cat'];
    $page = ($_REQUEST['page'] ? $_REQUEST['page'] : 1);
    $Products_arr = [];
    $get_orderby = $_REQUEST['orderby'];

    switch ($get_orderby) {
        case 'popularity':
            $orderby = 'total_sales';
            $order = 'DESC';
            break;
        case 'rating':
            $orderby = '_wc_average_rating';
            $order = 'DESC';
            break;
        case 'date':
            $orderby = 'post_date';
            $order = 'DESC';
            break;
        case 'price':
            $orderby = '_price';
            $order = 'DESC';
            break;
        case 'price-desc':
            $orderby = '_price';
            $order = 'ASC';
            break;
        default:
            $orderby = 'menu_order title';
            $order = 'ASC';
            break;
    }
    if (!empty($cat)) {

        if ($get_orderby == 'price' || $get_orderby == 'price-desc') {

            $autumn_home = array(
                'posts_per_page' => 10,
                'meta_key' => $orderby,
                'orderby' => 'meta_value_num',
                'order' => $order,
                'product_cat' => $cat,
                'post_type' => 'product',
                'paged' => $page,
            );
        } else {
            $autumn_home = array(
                'posts_per_page' => 10,
                /* 'tax_query'     => array(
                  'relation'     => 'AND',
                  array(
                  'taxonomy'     => 'product_cat',
                  'field'     => 'slug',
                  'terms'     => $cat
                  )
                  ), */
                'product_cat' => $cat,
                'post_type' => 'product',
                'orderby' => $orderby,
                'order' => $order,
                'paged' => $page,
            );
        }
        $loop = new WP_Query($autumn_home);
        $total = 0;
        while ($loop->have_posts()): $loop->the_post();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'img' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => number_format( $product->get_regular_price(), 2 ),
                'sale_price' => number_format( $product->get_sale_price(), 2 ),
                'get_price' => number_format( $product->get_price(), 2 ),
            );
            $total += 1;
            array_push($Products_arr, $data);
        endwhile;
    }
    if ($Products_arr) {
        $response['status'] = 'success';
        $response['products'] = $Products_arr;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

// WooCommerce: Allow editing of all orders regardless of status
//add_filter ( 'wc_order_is_editable', 'majemedia_add_order_statuses_to_editable' );
//function majemedia_add_order_statuses_to_editable () {
//return TRUE;
//}

/*
 * Create a column. And maybe remove some of the default ones
 * @param array $columns Array of all user table columns {column ID} => {column Name}
 */
add_filter('manage_users_columns', 'rudr_modify_user_table');

function rudr_modify_user_table($columns) {

    // unset( $columns['posts'] ); // maybe you would like to remove default columns
    $columns['registration_date'] = 'Registration date'; // add new
    return $columns;
}

/*
 * Fill our new column with the registration dates of the users
 * @param string $row_output text/HTML output of a table cell
 * @param string $column_id_attr column ID
 * @param int $user user ID (in fact - table row ID)
 */
add_filter('manage_users_custom_column', 'rudr_modify_user_table_row', 10, 3);

function rudr_modify_user_table_row($row_output, $column_id_attr, $user) {

    $date_format = 'j M, Y H:i';
    switch ($column_id_attr) {
        case 'registration_date':
            return date($date_format, strtotime(get_the_author_meta('registered', $user)));
            break;
        default:
    }
    return $row_output;
}

/*
 * Make our "Registration date" column sortable
 * @param array $columns Array of all user sortable columns {column ID} => {orderby GET-param}
 */
add_filter('manage_users_sortable_columns', 'rudr_make_registered_column_sortable');

function rudr_make_registered_column_sortable($columns) {
    return wp_parse_args(array('registration_date' => 'registered'), $columns);
}

///////////////////Filter for Auth Request //////////////
add_filter('jwt_auth_whitelist', function ($endpoints) {
    return array(
        '/wp-json/wp/v2/*',
        '/wp-json/yoast/v1/*',
        '/wp-json/wpcom/v2/*',
        '/wp-json/wp/v1/*',
        '/wp-json/jetpack/v4/*',
        '/wp-json/wcfmmp/v1/*',
        '/wp-json/sow/v1/*',
    );
});

/* * ******************* Register ********************* */
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'register', array(
        'methods' => 'POST',
        'callback' => 'create_users',
            )
    );
});

function create_users($data) {
    $response['status'] = 'error';
    //$parts = explode("@", $data['email']);
    $login_name = $parts[0];
    $user_id = wp_insert_user(array(
        'user_login' => $data['username'],
        'user_pass' => $data['password'],
        'user_email' => $data['email'],
    ));
    if (!is_wp_error($user_id)) {

        $user_data = get_userdata($user_id);
        $url = site_url() . "/wp-json/jwt-auth/v1/token";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=" . $data['username'] . "&password=" . $data['password']);
        $output = json_decode(curl_exec($ch));
        curl_close($ch);

        $response['status'] = 'success';
        $response['data'] = $output;
        $response['message'] = "User registered successfully";
    } else {
        $response['data'] = [];
        $response['message'] = $user_id->get_error_message();
    }
    return new WP_REST_Response($response);
    exit;
}

/////////////////////// Login API //////////////////////////////////////////
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'user/login', array(
        'methods' => 'POST',
        'callback' => 'login',
    ));
});

function login($data) {



    $username = sanitize_user($data['username']);
    $password = trim($data['password']);
    $url = "https://thebritishcrafthouse.co.uk/wp-json/jwt-auth/v1/token";
    $data = json_encode(array(
        "username"  => $username,
        "password" => $password
        ));

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/x-www-form-urlencoded"));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if (curl_errno($ch)) {
        return curl_error($ch);
    }
    $output = json_decode(curl_exec($ch));
    // print_r($output);
    // exit;
    curl_close($ch);

    if (isset($output->data->id)) {
        update_user_meta($output->data->id, "fcm_token", $data['fcm_token']);
        update_user_meta($output->data->id, "device_type", $data['device_type']);
    }
    if ($output->code == 'invalid_email' || $output->code == 'invalid_username') {
        $res['status'] = 'error';
        $res['message'] = 'Try again or check your email/username';
        return new WP_REST_Response($res);
        exit;
    }
    if ($output->code == 'incorrect_password') {
        $res['status'] = 'error';
        $res['message'] = 'The password you entered is incorrect';
        return new WP_REST_Response($res);
        exit;
    }
    $output->status = 'success';

//    pk: sk_live_YqeEbhkB0OrmlkA7MxK4aXHH00yDdVG477
//    sk: pk_live_Bj4ZFQJoi3Z3M4eYh5zq6K8000SAuK0tYb
//    Stripe_Test_Secret_Key = sk_test_4A5Us3O1KqJaYBrC0J69pDbn00G4Iq3MKJ
//    Stripe_Test_Publishable_Key = pk_test_FvWubPv9FF6hXHFuTvDVgd2Y00s6f1ONkq
//    if ($username == 'demo09@gmail.com') {
//
    //stripe api keys
    // if ($username == 'aaronhealey23@gmail.com') {
    //     $output->stripe_sk = 'sk_test_4A5Us3O1KqJaYBrC0J69pDbn00G4Iq3MKJ';
    //     $output->stripe_pk = 'pk_test_FvWubPv9FF6hXHFuTvDVgd2Y00s6f1ONkq';
    // } else {
    //     $output->stripe_sk = STRIPE_SECRET_KEY;
    //     $output->stripe_pk = STRIPE_PUBLIC_KEY;
    // }
    $output->stripe_sk = STRIPE_SECRET_KEY;
    $output->stripe_pk = STRIPE_PUBLIC_KEY;
    return $output;
}

/* ============ FORGOT PASSWORD ============ */
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'forgot/password', array(
        'methods' => 'POST',
        'callback' => 'forgot_password',
    ));
});

function forgot_password($data) {
    $login = $data['email'];
    if (empty($login)) {
        $json = array('status' => 'error', 'message' => 'Please enter registered email');
        return $json;
        exit;
    }
    $userdata = get_user_by('email', $login);
    if (empty($userdata)) {
        $userdata = get_user_by('login', $login);
    }
    if (empty($userdata)) {
        $json = array('status' => 'error', 'message' => 'Email not found');
        return $json;
        exit;
    }
    $username = $userdata->user_login;
    $user = new WP_User(intval($userdata->ID));
    $key = get_password_reset_key($user);
    $admin_email = get_option('admin_email');
    $reset_link = site_url() . '/my-account/lost-password/?key=' . $key . '&id=' . $userdata->ID;
    $mail['subject'] = 'Forgot Password';
    $message = '<html>
          <head>
          	<title>Password Reset</title>
			</head>
			<body style="margin:0;">
			<div style="width:700px; margin:0 auto;background:#f7f7f7;padding:20px 0;">
			<p style="text-align:center; margin-bottom:40px;"><img src="https://thebritishcrafthouse.co.uk/wp-content/uploads/2019/07/The-British-Craft-House-full-CMYK-teal-thick.png" alt="Square-Logo-300dpi" style="width:165px;" border="0"></p>
			<div class="main_sec" style="background: #fff;
		    border: 1px solid #fff;
		    margin: 10px 22px;">
        	<p style="background: #4ab6b6;
			    padding: 32px 35px;
			    color: #fff;
			    font-size: 30px;
			    font-family: sans-serif;
			    margin-top: 0;">Password Reset Request</p>
			    <p style="color: #717171; font-family: sans-serif; margin: 25px 35px;">Hi ' . $username . ',</p>
				<p style="color: #717171; font-family: sans-serif; margin: 25px 35px; line-height: 30px;">Someone has requested a new password for the following account on The British Craft House:</p>
				<p style="color: #717171; font-family: sans-serif; margin: 25px 35px;">Username: ' . $username . '</p>
				<p style="color: #717171; font-family: sans-serif; margin: 25px 35px;">If you did not make this request, just ignore this email. If you would like to proceed:</p>
				<p style="color: #4ab6b6; font-family: sans-serif; margin: 25px 35px;"><a href="' . esc_url($reset_link) . '" title="' . __('Reset your password link : ', 'my_slug') . '" style="color: #4ab6b6;">Click here to reset your password</a></p>
			</div>
			<p style="color: #909090; font-family: sans-serif; text-align: center; font-size: 13px; margin-top: 25px;">The British Craft House</p>
			</div>
          </body>
        </html>';
    $headers = "Content-Type: text/html; charset=UTF-8";
    //$headers .= 'From: Purple Office <'.$admin_email.'>';
    wp_mail($login, $mail['subject'], stripslashes($message), $headers);
    $json = array('status' => 'success', 'message' => 'Password reset link has been sent to your registered email');
    return $json;
    exit;
}

/* add_filter( 'woocommerce_api_product_response', 'wc_api_add_custom_data_to_product', 10, 2 );
  function wc_api_add_custom_data_to_product( $product_data, $product ) {
  print_r($product_data); die;
  // retrieve a custom field and add it to API response
  $product_data['vendor_id'] = get_post_field( 'post_author', $product->id);
  $product_data['vendor_name'] = get_the_author_meta( 'display_name', $product_data['vendor_id']);

  return $product_data;
  } */
add_filter('woocommerce_rest_prepare_product', 'wc_rest_api_add_custom_data_to_product', 90, 2);

function wc_rest_api_add_custom_data_to_product($response, $product) {
    $response->data['vendor_id'] = $product->post_author;
    $response->data['vendor_name'] = get_the_author_meta('display_name', $product->post_author);
    return $response;
}

////////////// Hook, To add add extra data in woocommerce product api ///////////////
add_filter('woocommerce_rest_prepare_product_object', 'get_vendor_product_new', 20, 3);

function get_vendor_product_new($response, $object, $request) {
    if (empty($response->data)) {
        return $response;
    }

    //$vendor_id = $object->post_author;
    $response->data['shipping_details'] = getProductShipping($object->id, 2);
    return $response;
}

////////////// END Hook, To add add extra data in woocommerce product api ///////////////

function getProductShipping($product_id, $quantity) {

    $productID = $product_id;
    $product_quantity = $quantity;
    global $woocommerce, $wpdb;
    $shipping_rule_item_cost = 0;
    $shipping_rule_cost = 0;
    $shipping_country = $woocommerce->customer->get_shipping_country();
    //foreach ( $items as $cart_item ) {
    $country_code_query = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->prefix}woocommerce_per_product_shipping_rules WHERE product_id = " . $productID . " ORDER BY rule_order;", $productID), OBJECT);

    if (!empty($country_code_query)) {
        foreach ($country_code_query as $key => $value) {
            $country_codes = $value->rule_country;
            $country_codes = explode(',', $country_codes);
            if (in_array($shipping_country, $country_codes)) {
                $available_shipping_methods[] = $value->rule_postcode;
            }
            if ($product_quantity > 1) {
                $shipping_rule_item_cost = (float) $value->rule_item_cost * (int) $product_quantity;
            }
            $shipping_rule_cost = (float) $value->rule_cost;
        }
    }
    //}
    $available_shipping_methods = array_unique($available_shipping_methods);

    if (!empty($available_shipping_methods)) {

        foreach ($available_shipping_methods as $key => $value) {
            $method_data = get_option('woocommerce_per_product_' . $value . '_settings');
            $method_title = $method_data['title'];

            $item = new WC_Order_Item_Shipping();
            $item->set_method_title($method_title);
            $item->set_method_id($value); //set an existing Shipping method rate ID
            //$item->set_total( $shipping_lines[0]['total'] ); // (optional)
            //$item->calculate_taxes($calculate_tax_for);
            //$order->add_item( $item );

            $shipping_methods = array(
                'value' => $value,
                'method_title' => $method_title,
                'method_data' => $method_data,
                'item' => $item,
                'shipping_rule_item_cost' => $shipping_rule_item_cost,
                'shipping_rule_cost' => $shipping_rule_cost,
            );
        }
        return $shipping_methods;
    } else {
        return 'No shipping method found';
    }
}

//////CURL function///////////
function curlRequest($method, $data = false, $url) {

    $curl = curl_init();
    $secretKey = STRIPE_SECRET_KEY;
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer " . $secretKey,
        ),
    ));
    $curl_response = curl_exec($curl);
    curl_close($curl);
    return $curl_response;
}

////// Stripe Payment for woocommerce Order //////
add_action('rest_api_init', 'stripePayment');

function stripePayment() {
    register_rest_route('/wp/v2/', 'stripe_payment', array(
        'methods' => 'POST',
        'callback' => 'stripe_payment_for_order',
    ));
}

add_action('rest_api_init', 'stripePayment_2');

function stripePayment_2() {
    register_rest_route('/wp/v2/', 'stripe_payment_test', array(
        'methods' => 'POST',
        'callback' => 'stripe_payment_for_order',
    ));
}

//    $auth = apache_request_headers();
//    $authtoken = $auth['Authorization'];
//    $vaildation = validateToken($authtoken);
//    if (!isset($vaildation->id)) {
//        $response['status'] = 'error';
//        $response['message'] = $vaildation->message;
//        $response['data'] = $vaildation;
//        return $response;
//    }


function stripe_payment_for_order_2() {
    return '1';
}

function stripe_payment_for_order($request = null) {
    global $wpdb;
    $response = [];


    $secretKey = STRIPE_SECRET_KEY;
    //$secretKey = 'sk_test_4A5Us3O1KqJaYBrC0J69pDbn00G4Iq3MKJ';
    //if(isset($vaildation->id)){

    $parameters = $request->get_params();



    $amount = sanitize_text_field($parameters['amount']);
    $stripe_card_token = sanitize_text_field($parameters['stripe_card_token']);
    $order_data = $parameters['order_data'];
    $items_data = $order_data['line_items'];
    $product_name = array();
    for ($j = 0; $j < count($items_data); $j++) {
        $product_name[] = $items_data[$j]['name'];
    }
    $product_name = implode(',', $product_name);

    if ($parameters['customer_id'] == 12310 || (isset($parameters['testId']) && $parameters['testId'] == 12310)) {

    }
//    $parameters['valid_user'] = $vaildation;

    $orderPlace = placeOrderTestMode($parameters);

    $console = [];
    $console['date'] = date('d-m-y h:i:s A');
    $console['order'] = $orderPlace;
    update_option('test_place_order_responce', $console);
    if ($orderPlace && !isset($orderPlace['status'])) {
        $response['status'] = 'success';
        $response['message'] = 'Order placed successfully';
    } else {
        $response = $orderPlace;
    }

    return $response;

    /*

      $user_id = $parameters['customer_id'];
      $country = $order_data['shipping_address']['country'];
      $vendor_id = $parameters['vendor_id'];
      $error = new WP_Error();

      // if($country != 'United Kingdom'){
      // $error->add( 404, __( "The item trying to be purchased cannot be dispatched to the selected country ".$country."."), array( 'status' => 400 ) );
      // return $error;
      // }
      // if ( empty( $stripe_card_token ) ) {
      // $error->add( 404, __( "Payment Token 'stripe card token' is required."), array( 'status' => 400 ) );
      // return $error;
      // }
      // if ( empty( $amount ) ) {
      // $error->add( 404, __( "Payment Token 'amount' is required."), array( 'status' => 400 ) );
      // return $error;
      // }

      $stripe_connected_acc = get_user_meta($vendor_id, 'wcfmmp_stripe_split_pay_customer_id', true);
      $stripe_connected_acc = 'cus_JhNfsjX5acWTs8';
      $transferPost = "description=" . $vaildation->email;
      $url = 'https://api.stripe.com/v1/customers';
      $transfer_response = curlRequest('POST', $transferPost, $url); /////Call curl function to generate stripe Customer id

      if (isset(json_decode($transfer_response)->error)) {
      $response['status'] = "error";
      $response['message'] = json_decode($transfer_response)->error->message;
      return $response;
      }

      $customer_id = json_decode($transfer_response)->id;
      $transferPost1 = "source=" . $stripe_card_token;
      $url1 = "https://api.stripe.com/v1/customers/" . $customer_id . "/sources";
      $transfer_response = curlRequest('POST', $transferPost1, $url1); /////Call curl function to Attach Customer with card token

      if (isset(json_decode($transfer_response)->error)) {

      $response['status'] = "error";
      $response['message'] = json_decode($transfer_response)->error->message;
      return $response;
      }

      $card_data = json_decode($transfer_response);
      $card_id = $card_data->id;
      $description = "woocommerce order";
      $stripe_total_price = $amount * 100;
      $admin_comm = ($stripe_total_price * 7.2) / 100;
      $app_fee = "$admin_comm";
      //$post_data = "amount=".$stripe_total_price."&currency=gbp&customer=".$customer_id."&source=".$card_id."&application=".$stripe_connected_acc."&application_fee=".$app_fee."&application_fee_amount=".$app_fee."&payment_intent=".$card_id.'&metadata["product_name"]='.$product_name.'&metadata["billing_address_one"]='.$order_data['billing_address']['address_1'].'&metadata["billing_address_two"]='.$order_data['billing_address']['address_2'].'&metadata["billing_address_one"]='.$order_data['billing_address']['address_2'].'&metadata["billing_city"]='.$order_data['billing_address']['city'].'&metadata["billing_country"]='.$order_data['billing_address']['country'].'&metadata["billing_email"]='.$order_data['billing_address']['email'].'&metadata["billing_first_name"]='.$order_data['billing_address']['first_name'].'&metadata["billing_last_name"]='.$order_data['billing_address']['last_name'].'&metadata["billing_phone"]='.$order_data['billing_address']['phone'].'&metadata["billing_state"]='.$order_data['billing_address']['postcode'].'&metadata["billing_postcode"]='.$order_data['billing_address']['state'];$post_data = "amount=".$stripe_total_price."&currency=gbp&customer=".$customer_id."&source=".$card_id.'&metadata["product_name"]='.$product_name.'&metadata["billing_address_one"]='.$order_data['billing_address']['address_1'].'&metadata["billing_address_two"]='.$order_data['billing_address']['address_2'].'&metadata["billing_address_one"]='.$order_data['billing_address']['address_2'].'&metadata["billing_city"]='.$order_data['billing_address']['city'].'&metadata["billing_country"]='.$order_data['billing_address']['country'].'&metadata["billing_email"]='.$order_data['billing_address']['email'].'&metadata["billing_first_name"]='.$order_data['billing_address']['first_name'].'&metadata["billing_last_name"]='.$order_data['billing_address']['last_name'].'&metadata["billing_phone"]='.$order_data['billing_address']['phone'].'&metadata["billing_state"]='.$order_data['billing_address']['postcode'].'&metadata["billing_postcode"]='.$order_data['billing_address']['state'];
      $post_data = "amount=" . $stripe_total_price . "&currency=gbp&customer=" . $customer_id . "&source=" . $card_id . "&application_fee_amount=" . $app_fee . "&transfer_data[destination]=" . $stripe_connected_acc . '&metadata["product_name"]=' . $product_name . '&metadata["billing_address_one"]=' . $order_data['billing_address']['address_1'] . '&metadata["billing_address_two"]=' . $order_data['billing_address']['address_2'] . '&metadata["billing_address_one"]=' . $order_data['billing_address']['address_2'] . '&metadata["billing_city"]=' . $order_data['billing_address']['city'] . '&metadata["billing_country"]=' . $order_data['billing_address']['country'] . '&metadata["billing_email"]=' . $order_data['billing_address']['email'] . '&metadata["billing_first_name"]=' . $order_data['billing_address']['first_name'] . '&metadata["billing_last_name"]=' . $order_data['billing_address']['last_name'] . '&metadata["billing_phone"]=' . $order_data['billing_address']['phone'] . '&metadata["billing_state"]=' . $order_data['billing_address']['postcode'] . '&metadata["billing_postcode"]=' . $order_data['billing_address']['state'];
      $post_data = "amount=" . $stripe_total_price . "&currency=gbp&customer=" . $customer_id . "&source=" . $card_id . '&metadata["product_name"]=' . $product_name . '&metadata["billing_address_one"]=' . $order_data['billing_address']['address_1'] . '&metadata["billing_address_two"]=' . $order_data['billing_address']['address_2'] . '&metadata["billing_address_one"]=' . $order_data['billing_address']['address_2'] . '&metadata["billing_city"]=' . $order_data['billing_address']['city'] . '&metadata["billing_country"]=' . $order_data['billing_address']['country'] . '&metadata["billing_email"]=' . $order_data['billing_address']['email'] . '&metadata["billing_first_name"]=' . $order_data['billing_address']['first_name'] . '&metadata["billing_last_name"]=' . $order_data['billing_address']['last_name'] . '&metadata["billing_phone"]=' . $order_data['billing_address']['phone'] . '&metadata["billing_state"]=' . $order_data['billing_address']['postcode'] . '&metadata["billing_postcode"]=' . $order_data['billing_address']['state'];
      //$url2 =  "https://api.stripe.com/v1/charges";
      $url2 = "https://api.stripe.com/v1/payment_intents";
      $curl_response = curlRequest('POST', $post_data, $url2); //Call curl Function
      return json_decode($curl_response);
      if (isset(json_decode($curl_response)->error)) {
      $response['status'] = 'error';
      $response['message'] = json_decode($curl_response)->error->message;
      return $response;
      } elseif (isset(json_decode($curl_response)->status) && json_decode($curl_response)->status == "succeeded") {

      $admin_comm = ($amount * 7.2) / 100;
      $transfer_total = $amount - $admin_comm;
      $transfer_total_amt = $transfer_total;
      $transfer_post_data = "amount=" . $transfer_total_amt . "&currency=gbp&destination=" . $stripe_connected_acc;
      $transfer_url = "https://api.stripe.com/v1/transfers";
      $curl_res = curlRequest('POST', $transfer_post_data, $transfer_url); //Call curl Function

      if (!empty($order_data) && is_array($order_data) || is_object($order_data)) {

      $address_new = $order_data['billing_address'];
      $shipping_address = $order_data['shipping_address'];
      $line_items = $order_data['line_items'];
      $shipping_lines = $order_data['shipping_lines'];
      $shipping_total = $order_data['shipping_total'];
      //$user_id = $vaildation->id;
      // Now we create the order
      $order = wc_create_order(array('customer_id' => $user_id));
      for ($i = 0; $i < count($line_items); $i++) {
      $product_id = $line_items[$i]['product_id'];
      $product_qty = $line_items[$i]['quantity'];
      $order->add_product(get_product($product_id), $product_qty);
      }
      //$order->add_product( get_product(225929), $line_items[0]['quantity']);
      // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php

      $order->set_address($address_new, 'billing');
      $order->set_address($shipping_address, 'shipping');
      $order->set_payment_method('stripe');
      $order->set_payment_method_title('Credit Card');
      $order->set_transaction_id(json_decode($curl_response)->id);
      $order->set_date_paid(time());

      //$order->add_shipping($shipping_lines);
      // Get a new instance of the WC_Order_Item_Shipping Object
      // Get the customer country code
      $country_code = $order->get_shipping_country();

      // Set the array for tax calculations
      $calculate_tax_for = array(
      'country' => $country_code,
      'state' => '', // Can be set (optional)
      'postcode' => '', // Can be set (optional)
      'city' => '', // Can be set (optional)
      );
      $item = new WC_Order_Item_Shipping();

      $item->set_method_title($shipping_lines['method_title']);
      //$item->set_method_id( $shipping_lines['method_id'] );//set an existing Shipping method rate ID
      $item->set_total($shipping_lines['total']); // (optional)
      //$item->calculate_taxes($calculate_tax_for);

      $order->add_item($item);

      //$item = new WC_Order_Item_Shipping();
      //$item->set_method_title($method_title);
      //$item->set_total( $shipping_total); // (optional)
      //$order->add_item( $item );

      $order->calculate_totals();
      $order_id = trim(str_replace('#', '', $order->get_order_number()));

      $my_post = array(
      'ID' => $order_id,
      'post_status' => 'wc-completed',
      );
      wp_update_post($my_post);
      $order->save();
      $response['status'] = 'success';
      $response['message'] = 'Order placed successfully';
      $response['data']['order_id'] = $order_id;
      $response['data'] = $product_id . "df" . $product_qty;
      return $response;
      } else {
      $response['status'] = 'error';
      $response['message'] = 'Order details are missing Please check!';
      return $response;
      }
      } else {
      $response['status'] = 'error';
      $response['message'] = 'There is some error, please contact admin';
      return $response;
      }
      //}
      // else{
      // $response['status'] = 'error';
      // $response['data'] = $vaildation;
      // return $response;
      // }
     */
}

add_action('rest_api_init', 'bch_token_vaildation_register');

function bch_token_vaildation_register() {
    register_rest_route('/wp/v2/', 'token-vaildation', array(
        'methods' => 'POST',
        'callback' => 'bch_token_vaildation_callback',
    ));
}

function bch_token_vaildation_callback() {
    $response = [];
    $auth = apache_request_headers();
    $authtoken = $auth['Authorization'];
    $vaildation = validateToken($authtoken);
    if (!isset($vaildation->id)) {
        $response['status'] = 'error';
        $response['message'] = $vaildation->message;
        $response['data'] = $vaildation;
    } else {
        $response['status'] = 'success';
        $response['data'] = $vaildation;
    }

    return $response;
}

//////////validate Auth token///////////////////
function validateToken($token) {
    $url = site_url() . "/wp-json/jwt-auth/v1/token/validate";
    $ch = curl_init();
    $headers = array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization:' . $token,
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, true);
    $output = json_decode(curl_exec($ch));

    if ($output->statusCode == 200) { //if validate success get user details from token
        $newurl = site_url() . "/wp-json/wp/v2/users/me";
        $chs = curl_init();
        $header = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization:' . $token,
        );
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_URL, $newurl);
        curl_setopt($chs, CURLOPT_HTTPHEADER, $header);
        curl_setopt($chs, CURLOPT_POST, true);
        if (curl_errno($chs)) {
            return curl_error($chs);
        }
        $res = json_decode(curl_exec($chs));
        return $res;
    } else {
        return $output;
    }
}

//function wcfm_custom_product_manage_fields_pricing( $pricing_fields, $product_id, $product_type ) {
//if( isset( $pricing_fields['sale_price'] ) ) {
//    unset( $pricing_fields['sale_price'] );
//}
//return $pricing_fields;
//}
/**
 * Get Navigation Menu API
 * Transform a navigational menu to it's tree structure
 *
 * @uses  buildTree()
 * @uses  wp_get_nav_menu_items()
 *
 * @param  String     'Menu name' OR $menud_id
 * @return Array|null $tree
 */
function get_my_menu() {
    $items = wp_get_nav_menu_items('Main Nav');
    $data = nav_menu_object_tree($items);
    return $data;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'menu', array(
        'methods' => 'GET',
        'callback' => 'get_my_menu',
    ));
});

/**
 * "Build a tree from a flat array in PHP"
 *
 *
 */
function buildTree(array &$elements, $parentId = 0) {
    $branch = array();
    foreach ($elements as &$element) {
        if ($element->menu_item_parent == $parentId) {
            $children = buildTree($elements, $element->ID);
            if ($children) {
                $element->wpse_children = $children;
            }

            $branch[$element->ID] = $element;
            unset($element);
        }
    }
    return $branch;
}

function nav_menu_object_tree($nav_menu_items_array) {

    foreach ($nav_menu_items_array as $key => $value) {
        $value->children = array();
        $nav_menu_items_array[$key] = $value;
    }

    $nav_menu_levels = array();
    $index = 0;
    if (!empty($nav_menu_items_array)) {
        do {
            if ($index == 0) {
                foreach ($nav_menu_items_array as $key => $obj) {
                    if ($obj->menu_item_parent == 0) {
                        $nav_menu_levels[$index][] = $obj;
                        unset($nav_menu_items_array[$key]);
                    }
                }
            } else {
                foreach ($nav_menu_items_array as $key => $obj) {
                    if (in_array($obj->menu_item_parent, $last_level_ids)) {
                        $nav_menu_levels[$index][] = $obj;
                        unset($nav_menu_items_array[$key]);
                    }
                }
            }
            $last_level_ids = wp_list_pluck($nav_menu_levels[$index], 'db_id');
            $index++;
        } while (!empty($nav_menu_items_array));
    }

    $nav_menu_levels_reverse = array_reverse($nav_menu_levels);

    $nav_menu_tree_build = array();
    $index = 0;
    if (!empty($nav_menu_levels_reverse)) {
        do {
            if (count($nav_menu_levels_reverse) == 1) {
                $nav_menu_tree_build = $nav_menu_levels_reverse;
            }
            $current_level = array_shift($nav_menu_levels_reverse);
            if (isset($nav_menu_levels_reverse[$index])) {
                $next_level = $nav_menu_levels_reverse[$index];
                foreach ($next_level as $nkey => $nval) {
                    foreach ($current_level as $ckey => $cval) {
                        if ($nval->db_id == $cval->menu_item_parent) {
                            $explode_url = explode('product-category/', $cval->url);
                            $site = rtrim($explode_url[1], "/");
                            $cval->slug = $site;
                            $nval->children[] = $cval;
                            if (empty($nval->children)) {
                                $cval->slug = end($explode_url);
                            }
                        }
                    }
                }
            }
        } while (!empty($nav_menu_levels_reverse));
    }

    $nav_menu_object_tree = $nav_menu_tree_build[0];
    return $nav_menu_object_tree;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'search_vendor', array(
        'methods' => 'GET',
        'callback' => 'serachVendor',
    ));
});

function serachVendor($request) {

    $parameters = $request->get_params();
    $search_string = sanitize_text_field($parameters['search_string']);
    $user_query = new WP_User_Query(array('meta_key' => 'store_name', 'meta_value' => $search_string, 'meta_compare' => 'LIKE'));
    $search_result = [];

    for ($i = 0; $i < count($user_query->results); $i++) {

        $store_id = $user_query->results[$i]->data->ID;
        $user_info = get_user_meta($store_id);

        if (isset($user_info['wcfm_subscription_status'][0]) && $user_info['wcfm_subscription_status'][0] == 'active') {

            $store_user = wcfmmp_get_store($store_id);
            $store_info = $store_user->get_shop_info();
            $gravatar = $store_user->get_avatar();
            $banner_type = $store_user->get_list_banner_type();
            if ($banner_type == 'video') {
                $banner_video = $store_user->get_list_banner_video();
            } else {
                $banner = $store_user->get_list_banner();
                if (!$banner) {
                    $banner = isset($WCFMmp->wcfmmp_marketplace_options['store_list_default_banner']) ? $WCFMmp->wcfmmp_marketplace_options['store_list_default_banner'] : $WCFMmp->plugin_url . 'assets/images/default_banner.jpg';
                    $banner = apply_filters('wcfmmp_list_store_default_bannar', $banner);
                }
            }
            $store_name = isset($store_info['store_name']) ? esc_html($store_info['store_name']) : __('N/A', 'wc-multivendor-marketplace');
            $store_name = apply_filters('wcfmmp_store_title', $store_name, $store_id);
            $store_url = wcfmmp_get_store_url($store_id);
            $store_address = $store_user->get_address_string();
            $store_description = $store_user->get_shop_description();
            $search_result[] = array(
                "vendor_id" => $store_id,
                "vendor_display_name" => $user_info['store_name'][0],
                "vendor_shop_name" => $user_info['store_name'][0],
                "vendor_shop_logo" => $gravatar,
                "mobile_banner" => $banner,
                "vendor_description" => isset($user_info['_store_description'][0]) ? $user_info['_store_description'][0] : null,
            );
        }
    }
    return $search_result;
}

//Hide categories from WordPress category widget
function exclude_widget_categories($args) {
    $user = wp_get_current_user();
    $blocked_user_roles = array("customer");
    if (!is_user_logged_in() || is_user_logged_in() && count(array_intersect($blocked_user_roles, $user->roles)) > 0) {
        $exclude = "44802,894";
        $args["exclude"] = $exclude;
        return $args;
    }
}

add_filter("widget_categories_args", "exclude_widget_categories");

function isAnchor($string) {
    return preg_match("/^\<a.*\>.*\<\/a\>/", $string, $m) != 0;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_store_vendor', array(
        'methods' => 'GET',
        'callback' => 'get_store_vendors',
            )
    );
});

function my_sort($a, $b) {
    if ($a == $b) {
        return 0;
    }

    return ($a < $b) ? -1 : 1;
}

function get_store_vendors($request) {
    //echo $_SERVER['HTTP_X_FORWARDED_FOR']; die;
    global $WCFM;
    $_POST["controller"] = 'wcfm-vendors';
    $_POST['length'] = !empty($request['per_page']) ? intval($request['per_page']) : 100;
    $_POST['start'] = !empty($request['page']) ? (intval($request['page']) - 1) * $_POST['length'] : 0;
    $_POST['filter_date_form'] = !empty($request['after']) ? $request['after'] : '';
    $_POST['filter_date_to'] = !empty($request['before']) ? $request['before'] : '';
    $_POST['orderby'] = 'ASC';
    $queries_data = array();
    parse_str($_SERVER['QUERY_STRING'], $queries_data);

    $_POST['search_data'] = array();
    foreach ($queries_data as $query_key => $query_value) {
        if (in_array($query_key, apply_filters('wcfmmp_vendor_list_exclude_search_keys', array('v', 'search_term', 'wcfmmp_store_search', 'wcfmmp_store_category', 'wcfmmp_radius_addr', 'wcfmmp_radius_lat', 'wcfmmp_radius_lng', 'wcfmmp_radius_range', 'excludes', 'orderby', 'lang')))) {
            $_POST['search_data'][$query_key] = $query_value;
        }
    }

    define('WCFM_REST_API_CALL', true);
    $WCFM->init();
    $wcfm_vendors_array = array();
    $wcfm_vendors_json_arr = array();
    $response = array();
    $wcfm_vendors_array = $WCFM->ajax->wcfm_ajax_controller();

    if ($_SERVER['HTTP_X_FORWARDED_FOR'] == '122.160.97.221') {
        //echo $dfgd = count($wcfm_vendors_array);
        echo "<pre>";
        print_r($wcfm_vendors_array);
        die;
    }
    if (!empty($wcfm_vendors_array)) {
        $index = 0;
        foreach ($wcfm_vendors_array as $wcfm_vendors_id => $wcfm_vendors_name) {
            $response[$index] = get_formatted_item_data($wcfm_vendors_id, $wcfm_vendors_name);
            $index++;
        }
        $response = apply_filters("wcfmapi_rest_prepare_store_vendors_objects", $response, $request);
        $response_final = new WP_REST_Response($response, 200);
        $response_final->header('Cache-Control', 'no-cache');
        //usort($response_final->data,"my_sort");
        // if($_SERVER['HTTP_X_FORWARDED_FOR'] == '122.160.97.221'){
        // echo "<pre>"; print_r($response_final->data); die;
        // }
        return $response_final;
        //return rest_ensure_response( apply_filters( "wcfmapi_rest_prepare_store_vendors_objects", $response, $request ) );
    } else {
        $response_final = new WP_REST_Response($response, 200);
        $response_final->header('Cache-Control', 'no-cache');
        return $response_final;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_store_vendor_products', array(
        'methods' => 'GET',
        'callback' => 'get_store_vendor_products',
    ));
});

function get_store_vendor_products($request) {
    $query_args = prepare_objects_query($request);
    $query = new WP_Query();
    $result = $query->query($query_args);
    $data = array();
    //$objects = array_map( array( $request['id'], 'get_object' ), $result );
    $data_objects = array();
    foreach ($objects as $object) {
        $data = prepare_data_for_response($object, $request);
        $data_objects[] = prepare_response_for_collection($data);
    }
    $response_final = new WP_REST_Response($data_objects);
    $response_final->header('Cache-Control', 'no-cache');
    // $response = rest_ensure_response( $data_objects );
    $response_final = format_collection_response($response_final, $request, $query->found_posts);
    return $response_final;
}

######## Get Temp Auth Token #######################
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_temp_auth_token', array(
        'methods' => 'GET',
        'callback' => 'requestForTempAuthToken',
    ));
});

function requestForTempAuthToken($request) {

    $tempToken = $request['token'];
    if (empty($tempToken)) {
        $tempToken = tempUserAuthToken();
        $res['token'] = $tempToken;
        $res['status'] = 'success';
        return $res;
    } else {
        $vaildation = validateToken("Bearer " . $tempToken);
        if (!isset($vaildation->id)) {
            $tempToken = tempUserAuthToken();
            $res['token'] = $tempToken;
            $res['status'] = 'success';
            return $res;
        } else {
            $res['token'] = $tempToken;
            $res['status'] = 'success';
            return $res;
        }
    }
}

function tempUserAuthToken() {
    $url = site_url() . "/wp-json/jwt-auth/v1/token";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/x-www-form-urlencoded"));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=tempauthuser@yopmail.com&password=tempauth@123");
    $output = json_decode(curl_exec($ch));
    curl_close($ch);
    if ($output->success == true) {
        return $output->data->token;
    } else {
        return false;
    }
}

/* function my_rest_prepare_product( $data, $post, $request ) {
  return $post->ID;
  $_data = $data->data;
  $comments =  get_comments_number($post->ID);
  }
  add_filter( 'rest_prepare_product', 'my_rest_prepare_product', 12, 3 ); */

/* add_filter('woocommerce_rest_prepare_product_object', 'so54387226_custom_data', 10, 3);

  function so54387226_custom_data($response, $object, $request) {
  if (empty($response->data))
  return $response;
  $id = $object->get_id(); //it will fetch product id
  $response->data['booking_meta_data'] = $id;
  return $response;
  } */

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_per_product_shipping', array(
        'methods' => 'POST',
        'callback' => 'getPerProductShippings',
    ));
});

function getPerProductShippings($request) {

    $productID = $request['product_id'];
    $product_quantity = $request['quantity'];
    global $woocommerce, $wpdb;
    $shipping_rule_item_cost = 0;
    $shipping_rule_cost = 0;
    $shipping_country = $woocommerce->customer->get_shipping_country();
    //foreach ( $items as $cart_item ) {
    $country_code_query = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->prefix}woocommerce_per_product_shipping_rules WHERE product_id = " . $productID . " ORDER BY rule_order;", $productID), OBJECT);
    if (!empty($country_code_query)) {
        foreach ($country_code_query as $key => $value) {
            $country_codes = $value->rule_country;
            $country_codes = explode(',', $country_codes);
            if (in_array($shipping_country, $country_codes)) {
                $available_shipping_methods[] = $value->rule_postcode;
            }
            if ($product_quantity > 1) {
                $shipping_rule_item_cost = (float) $value->rule_item_cost * (int) $product_quantity;
            }
            $shipping_rule_cost = (float) $value->rule_cost;
        }
    }
    //}
    $available_shipping_methods = array_unique($available_shipping_methods);

    if (!empty($available_shipping_methods)) {

        foreach ($available_shipping_methods as $key => $value) {
            $method_data = get_option('woocommerce_per_product_' . $value . '_settings');
            $method_title = $method_data['title'];
            $item = new WC_Order_Item_Shipping();
            $item->set_method_title($method_title);
            $item->set_method_id($value); //set an existing Shipping method rate ID
            //$item->set_total( $shipping_lines[0]['total'] ); // (optional)
            //$item->calculate_taxes($calculate_tax_for);
            //$order->add_item( $item );
            $shipping_methods = array(
                'value' => $value,
                'method_title' => $method_title,
                'method_data' => $method_data,
                'item' => $item,
                'shipping_rule_item_cost' => $shipping_rule_item_cost,
                'shipping_rule_cost' => $shipping_rule_cost,
            );
        }
        return wp_send_json(['status' => 'success', 'data' => $shipping_methods]);
    } else {
        return wp_send_json(['status' => 'success', 'message' => 'No shipping method found"']);
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'calculate_per_product_shipping_cost', array(
        'methods' => 'GET',
        'callback' => 'calculate_per_product_shipping_cost',
    ));
});
/**
 * Calculate the per product shipping cost if enabled for the product.
 *
 * @param array $product_data The product data form the package array.
 * @param array $package Shipping package array.
 *
 * @return float|bool
 */
/* function calculate_per_product_shipping_cost() {

  global $woocommerce;

  $customerZipCode =  75098;
  $zipResultArr = csd_check_zip_and_state($customerZipCode);
  $package =  $woocommerce->cart->get_shipping_packages();
  $package[0]['destination']['state'] = $zipResultArr['state'];
  $package[0]['destination']['postcode'] = $customerZipCode ;
  $package[0]['destination']['city'] = $zipResultArr['city'];
  $package[0]['destination']['address'] = '';
  $package[0]['destination']['address_2'] = '';
  print_r($package);}
  /*$product_data = array(
  'variation_id' => '',
  'product_id' => 67582,
  'quantity' =>2,

  );

  $rule               = false;
  $item_shipping_cost = 0;

  if ( $product_data['variation_id'] ) {
  $rule = woocommerce_per_product_shipping_get_matching_rule( $product_data['variation_id'], $package );
  }

  if ( false === $rule ) {
  $rule = woocommerce_per_product_shipping_get_matching_rule( $product_data['product_id'], $package );
  }

  if ( $rule ) {
  $item_shipping_cost += (float) $rule->rule_item_cost * (int) $product_data['quantity'];
  $item_shipping_cost += (float) $rule->rule_cost;
  } elseif ( '0' === $this->cost || $this->cost > 0 ) {
  // Use default shipping cost.
  $item_shipping_cost += (float) $this->cost * (int) $product_data['quantity'];
  } else {
  // NO default and nothing found - abort.
  return false;
  }

  // Fee.
  $item_shipping_cost += $this->get_fee( (float) $this->fee, $item_shipping_cost ) * (int) $product_data['quantity'];

  return $item_shipping_cost;
  } */

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_store_vendors_list', array(
        'methods' => 'GET',
        'callback' => 'get_store_vendors_list',
    ));
});

function get_store_vendors_list($request) {

    global $WCFM;
    $_POST["controller"] = 'wcfm-vendors';
    $_POST['length'] = !empty($request['per_page']) ? intval($request['per_page']) : 100;
    $_POST['filter_date_form'] = !empty($request['after']) ? $request['after'] : '';
    $_POST['filter_date_to'] = !empty($request['before']) ? $request['before'] : '';
    $_POST['orderby'] = 'name';
    $_POST['order'] = 'ASC';
    $queries_data = array();
    parse_str($_SERVER['QUERY_STRING'], $queries_data);
    define('WCFM_REST_API_CALL', true);
    $WCFM->init();
    $wcfm_vendors_array = array();
    $wcfm_vendors_json_arr = array();
    $response = array();
    $index = 0;
    for ($i = 0; $i < 10; $i++) {

        $_POST['start'] = $i * $_POST['length'];
        $wcfm_vendors_array = $WCFM->ajax->wcfm_ajax_controller();

        if (count($wcfm_vendors_array) > 0) {
            //$wcfm_vendors_json_arr = array_merge($wcfm_vendors_json_arr, $wcfm_vendors_array);
            foreach ($wcfm_vendors_array as $wcfm_vendors_id => $wcfm_vendors_name) {
                $res = get_vendor_formatted_item_data($wcfm_vendors_id, $wcfm_vendors_name);
                if ($res != null) {
                    $response[$index] = $res;
                    $index++;
                }
            }
        }
    }
    if (!empty($response)) {
        $shop_name = array();
        foreach ($response as $key => $row) {
            $shop_name[$key] = $row['vendor_shop_name'];
        }
        array_multisort($shop_name, SORT_ASC, $response);
        $response_final['status'] = 'success';
        $response_final['data'] = $response;
        return $response_final;
    } else {
        $response_final['status'] = 'success';
        $response_final['message'] = 'No shop found';
        return $response_final;
    }
}

function wcfm_custom_product_manage_fields_advanced($wcmp_advanced_fields) {
    if (isset($wcmp_advanced_fields['enable_reviews'])) {
        $wcmp_advanced_fields['enable_reviews']['custom_attributes'] = array('required' => 1);
    }
    return $wcmp_advanced_fields;
}

add_filter('wcfm_product_manage_fields_advanced', 'wcfm_custom_product_manage_fields_advanced', 50, 2);

// Redirect return to shop to shop page
//add_filter( 'woocommerce_return_to_shop_redirect', function( $redirect_url ) {
//$vendor_id = 0;
//foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
//    $cart_product_id = $cart_item['product_id'];
//$cart_product = get_post( $cart_product_id );
//$cart_product_author = $cart_product->post_author;
//if( function_exists( 'wcfm_is_vendor' ) && wcfm_is_vendor( $cart_product_author ) ) {
//    $vendor_id = $cart_product_author;
//    break;
//}
//}
//if( $vendor_id ) {
//    $redirect_url = wcfmmp_get_store_url( $vendor_id );
//}
//return $redirect_url;
//}, 100 );

/* Shipping per product change only text */
add_filter('woocommerce_order_item_display_meta_value', 'change_order_item_meta_value', 20, 3);

/**
 * Changing a meta value
 * @param  string        $value  The meta value
 * @param  WC_Meta_Data  $meta   The meta object
 * @param  WC_Order_Item $item   The order item object
 * @return string        The title
 */
function change_order_item_meta_value($value, $meta, $item) {

    if ('per_product' === $meta->value) {
        $pps_shipping_method = get_post_meta($item->get_order_id(), 'pps_shipping_method', true);
        $method_data = get_option('woocommerce_per_product_' . $pps_shipping_method . '_settings');
        $value = $method_data['title'];
    }

    return $value;
}

///////////////////////////////////// Editor Choice Menu Page API's //////////////////////////////////
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/featured-gifts-2/', array(
        'methods' => 'GET',
        'callback' => 'featuredGifts',
            )
    );
});

function featuredGifts() {

    $main_arr['heading'] = 'Featured Gifts';
    $main_arr['description'] = 'Fabulous gift ideas for your family, your friends, your bestie or even maybe yourself…';

    $arr = array('slider_one' => 5447, 'slider_two' => 5446, 'slider_three' => 5436);
    $all_slider_data = commonPages($arr);

    if ($main_arr && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $main_arr;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/bright-ideas/', array(
        'methods' => 'GET',
        'callback' => 'brightIdeas',
            )
    );
});

function brightIdeas() {

    $main_arr['heading'] = 'Some Bright Gift Ideas';
    $main_arr['description'] = 'Need bright gift ideas fast? Sometimes you really don’t know what to get for someone, the person that has everything, the teen that wants cash! This is where you will find something unusual, something you haven’t thought of! Enjoy browsing from our talented team of handpicked creatives.';

    $arr = array('slider_one' => 6410, 'slider_two' => 6409, 'slider_three' => 6407);
    $all_slider_data = commonPages($arr);

    if ($main_arr && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $main_arr;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/made-in-england/', array(
        'methods' => 'GET',
        'callback' => 'madeInEngland',
            )
    );
});

function madeInEngland() {

    $main_arr['heading'] = 'Some Bright Gift Ideas';
    $main_arr['description'] = 'Need bright gift ideas fast? Sometimes you really don’t know what to get for someone, the person that has everything, the teen that wants cash! This is where you will find something unusual, something you haven’t thought of! Enjoy browsing from our talented team of handpicked creatives.';

    $arr = array('slider_one' => 4723, 'slider_two' => 4731, 'slider_three' => 4727);
    $all_slider_data = commonPages($arr);

    if ($main_arr && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $main_arr;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/made-in-scotland/', array(
        'methods' => 'GET',
        'callback' => 'madeInScotland',
            )
    );
});

function madeInScotland() {

    $arr = array('slider_one' => 4786, 'slider_two' => 4734, 'slider_three' => 106462);
    $all_slider_data = commonPages($arr);

    $main_arr['heading'] = 'Gifts handcrafted in Scotland & Northern Ireland';
    $main_arr['description'] = 'Gifts made in Scotland and Northern Ireland – from tartan bags to handcrafted jewellery, our talented makers up have been working hard to bring you a wonderful choice of artisan gifts! A few guests too, who are inspired by Scotland!';

    if ($main_arr && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $main_arr;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/made-in-wales/', array(
        'methods' => 'GET',
        'callback' => 'madeInWales',
            )
    );
});

function madeInWales() {

    $arr = array('slider_one' => 4736, 'slider_two' => 4737, 'slider_three' => 4735);
    $all_slider_data = commonPages($arr);

    $main_arr['heading'] = 'Gifts handcrafted in Wales';
    $main_arr['description'] = 'Our gifts handmade in Wales are sure to delight! From ceramic mushrooms to beautiful embroidery our handpicked makers up and down the country have been crafting their hearts out to bring you the very best gift ideas! Also includes Welsh inspired gifts handmade in Britain!';

    if ($main_arr && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $main_arr;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/food-drink/', array(
        'methods' => 'GET',
        'callback' => 'foodDrink',
            )
    );
});

function foodDrink() {

    $arr = array('slider_one' => 188373, 'slider_two' => 188389, 'slider_three' => 188391);
    $all_slider_data = commonPages($arr);

    $heading = 'Food & Drink';
    $description = 'Handmade gifts for your foodie friends and family. Beautifully homemade treats, both big and small, made by our talented independent artisans.';

    $first_image = get_site_url() . '/wp-content/uploads/2020/08/shutterstock_1056582266-1-1.jpg';
    $first_link = 'food-and-drink';
    $first_title = 'FOODIE GIFTS';

    $second_image = get_site_url() . '/wp-content/uploads/2020/08/shutterstock_591566654-1-1.jpg';
    $second_link = 'gin-and-gin-gifts';
    $second_title = 'GIN GIFTS';

    $third_image = get_site_url() . '/wp-content/uploads/2020/08/shutterstock_543635506-1-1.jpg';
    $third_link = 'chocolates-and-sweets';
    $third_title = 'SWEET TREATS';

    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $top_section = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );

    if ($top_section && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $top_section;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/outdoor-living/', array(
        'methods' => 'GET',
        'callback' => 'outdoorLiving',
            )
    );
});

function outdoorLiving() {

    $arr = array('slider_one' => 92226, 'slider_two' => 92227, 'slider_three' => 92225);
    $all_slider_data = commonPages($arr);

    $main_arr['heading'] = 'Outdoor Living';
    $main_arr['description'] = 'Outdoor living ideas for the perfect summer! Embrace the great outdoors with beautiful lighting and planters for your garden.
									Create a relaxed and welcoming environment with a wonderful selection of lanterns and candles.
									Picnic in style with our fabulous ceramics and table wear.
									Here is some inspiration or you can shop the whole collection';

    if ($main_arr && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $main_arr;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/commissions/', array(
        'methods' => 'GET',
        'callback' => 'commissions',
            )
    );
});

function commissions() {

    $arr = array('slider_one' => 270586, 'slider_two' => 270587, 'slider_three' => 270588);
    $all_slider_data = commonPages($arr);

    $heading = 'Special Orders';
    $description = 'We are unique in offering this bespoke service by our talented team of artisans. Here you will find very special designs that you will be the only person to own. You can have a design made to your specifications. Or you can commission a design that is perfect for you.';

    $first_image = get_site_url() . '/wp-content/uploads/2021/04/resized-image-Promo-2.jpeg';
    $first_link = 'one-of-a-kind';
    $first_title = 'ONE OF A KIND';

    $second_image = get_site_url() . '/wp-content/uploads/2021/04/resized-image-Promo-6.jpeg';
    $second_link = 'tbch-exclusive';
    $second_title = 'EXCLUSIVE TO TBCH';

    $third_image = get_site_url() . '/wp-content/uploads/2021/04/resized-image-Promo-3.jpeg';
    $third_link = 'commissions';
    $third_title = 'COMMISSIONS WELCOME';

    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $top_section = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );

    if ($top_section && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $top_section;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/fathers-day-gifts-and-cards/', array(
        'methods' => 'GET',
        'callback' => 'fathersDayGiftsAndCards',
            )
    );
});

function fathersDayGiftsAndCards() {

    $arr = array('slider_one' => 104850, 'slider_two' => 104853, 'slider_three' => 104864, 'slider_four' => 104861);
    $all_slider_data = commonPages($arr);

    $heading = 'Father’s Day Gifts & Cards';
    $description = 'Handmade Father’s Day gifts galore! Browse from our talented artisans for inspiration for thoughtful, original gifts that are sure to delight. Whatever you might be looking for is here, from quirky and funny to traditional ideas, all under one roof.';

    $first_image = get_site_url() . '/wp-content/uploads/2021/03/Mole-photo-main.jpg';
    $first_link = 'fathers-day-special-occasions-gifts';
    $first_title = 'FATHER’S DAY GIFT IDEAS';

    $second_image = get_site_url() . '/wp-content/uploads/2021/03/Dad-Card-scaled-1.jpg';
    $second_link = 'fathers-day-cards';
    $second_title = 'FATHER’S DAY CARDS';

    $third_image = get_site_url() . '/wp-content/uploads/2021/03/scottish-wool-art-printed-card-special-edition-2.jpg';
    $third_link = 'art';
    $third_title = 'ART FOR DADS';

    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $top_section = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );

    if ($top_section && $all_slider_data) {
        $response['status'] = 'success';
        $response['main'] = $top_section;
        $response['all_slider_data'] = $all_slider_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

///////////////// Common Page API's Function //////////////////////////

function commonPages($slider_id_arr) {

    $all_slider_data = [];
    foreach ($slider_id_arr as $k => $id) {

        $slider_value = get_post_meta($id, 'sp_wpcp_upload_options', true);
        $slider_title = get_post_field('post_title', $id);
        $slider_arr = [];

        if (!empty($slider_title)) {
            if ($slider_value['wpcp_display_product_from'] == 'taxonomy') {

                if (!empty($slider_value['wpcp_taxonomy_terms'][0])) {

                    $args = [
                        'post_type' => 'product',
                        'posts_per_page' => $slider_value['number_of_total_posts'],
                        'orderby' => array(
                            'date' => 'ASC',
                            'menu_order' => 'ASC',
                        /* Other params */
                        ),
                        'tax_query' => [
                            'relation' => 'AND',
                            [
                                'taxonomy' => 'product_cat',
                                'field' => 'term_id',
                                'terms' => $slider_value['wpcp_taxonomy_terms'][0],
                            //'include_children' => false // Remove if you need posts from term 7 child terms
                            ],
                        ],
                            // Rest of your arguments
                    ];

                    $loops = get_posts($args);
                    foreach ($loops as $key => $val) {
                        $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($val->ID), 'single-post-thumbnail');
                        $data = array(
                            'id' => $val->ID,
                            'link' => get_permalink($val->ID),
                            'catimage' => $productimage[0],
                            'title' => get_the_title($val->ID),
                            'regular_price' => number_format( get_post_meta($val->ID, '_regular_price', true), 2 ),
                            'price' => number_format( get_post_meta($val->ID, '_price', true), 2 ),
                            'slider_title' => $slider_title,
                        );
                        array_push($slider_arr, $data);
                    }
                }
            }
            if ($slider_value['wpcp_display_product_from'] == 'specific_products') {
                $products_Arr = $slider_value['wpcp_specific_product'];
                foreach ($products_Arr as $pro_id) {
                    $product = wc_get_product($pro_id);
                    $result = (array) json_decode($product);

                    $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($pro_id), 'single-post-thumbnail');
                    if (!empty($result['name']) && !empty($result['regular_price']) && !empty(get_permalink($pro_id))) {
                        $data = array(
                            'id' => $pro_id,
                            'link' => get_permalink($pro_id),
                            'catimage' => $productimage[0],
                            'title' => $result['name'],
                            'regular_price' => number_format( $result['regular_price'], 2 ),
                            'price' => number_format( $result['price'], 2 ),
                            'slider_title' => $slider_title,
                        );

                        array_push($slider_arr, $data);
                    }
                }
            }
            if ($slider_value['wpcp_display_product_from'] == 'latest') {

                $Slider_one_Data = array();
                $slider_one = get_post_meta($id, 'sp_wpcp_upload_options', true);
                $slider_one_imageid = $slider_one['wpcp_gallery'];

                if (!empty($slider_one_imageid)) {

                    $ArrayOfYour_slider_one_imageid = explode(',', $slider_one_imageid);

                    foreach ($ArrayOfYour_slider_one_imageid as $imageID) {

                        $sliderurl = get_post_field('post_excerpt', $imageID);

                        if ($sliderurl) {

                            if ($id == 269560) {

                                $pattern = '/"[a-zA-Z0-9.\/\-\?\&]*"/';
                                preg_match($pattern, $sliderurl, $matches);
                                $stringinlink = strval($matches[0]);
                                $trimmedProduct_category = str_replace("/product-category/", '', $stringinlink);
                                $trimmedSubCat = str_replace("cards-stationery/", '', $trimmedProduct_category);
                                $trimmedart = str_replace("art/", '', $trimmedSubCat);
                                $trimmedfasion_jewellery = str_replace("fashion-jewellery/", '', $trimmedart);
                                $trimmedGiftsByInterest = str_replace("gifts-by-interest/", '', $trimmedfasion_jewellery);
                                $trimmedGifts = str_replace("gifts/", '', $trimmedGiftsByInterest);
                                $trimmedRecipient = str_replace("by-recipient/", '', $trimmedGifts);
                                $trimmedChildren = str_replace("gifts-for-children/", '', $trimmedRecipient);
                                $trimmedClothingType = str_replace("clothing-type/", '', $trimmedChildren);
                                $FinalNameOfCat = str_replace('/', '', $trimmedClothingType);
                                $slug = trim($FinalNameOfCat, '"');

                                preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);

                                $title = $matches[1][0];
                                $catImages = get_post_field('guid', $imageID);
                                $data = array(
                                    'slider_title' => $slider_title,
                                    'catimage' => $catImages,
                                    'title' => $title,
                                    'slug' => str_replace("\"", '', $FinalNameOfCat),
                                    // 'slug' => "fromhere??",
                                );
                                // echo "==> 222 ".$FinalNameOfCat."\n";
                                array_push($slider_arr, $data);
                            } else {
                                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                                $stringinlink = strval($RemoveAnchor['href']);
                                $slug_in_array = explode("/", $stringinlink, 10);
                                $get_existing_values = array_values(array_filter($slug_in_array));
                                $FinalNameOfCat = end($get_existing_values);

                                preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);

                                $title = $matches[1][0];
                                $catImages = get_post_field('guid', $imageID);
                                $data = array(
                                    'slider_title' => $slider_title,
                                    'catimage' => $catImages,
                                    'title' => $title,
                                    // 'slug' => $FinalNameOfCat,
                                    'slug' => str_replace("\"", '', $FinalNameOfCat),
                                );
                                // echo "==> :: ".$FinalNameOfCat."\n";
                                array_push($slider_arr, $data);
                            }

                            // print_r($data);
                        }
                    }
                }
            }
        }

        $all_slider_data[$k] = $slider_arr;
    }
    return $all_slider_data;
}

///////////////////////////////////// END Editor Choice Menu Page API's //////////////////////////////////


/**
 * Render only for rest API wp-carousel pro plugin
 */
function catSlideQueryData($sliderId) {

    // $post_meta_value = get_post_meta( 3546, 'sp_wpcp_upload_options', true );
    $post_meta_value = get_post_meta($sliderId, 'sp_wpcp_upload_options', true);

    // print_r($post_meta_value);
    /* New change by v */
    $wpcp_product_from = $post_meta_value['wpcp_display_product_from'];
    $product_category_terms = isset($post_meta_value['wpcp_taxonomy_terms']) ? $post_meta_value['wpcp_taxonomy_terms'] : '';
    $product_terms_operator = isset($post_meta_value['wpcp_product_category_operator']) ? $post_meta_value['wpcp_product_category_operator'] : '';
    $specific_product_ids = isset($post_meta_value['wpcp_specific_product']) ? $post_meta_value['wpcp_specific_product'] : '';
    $default_args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'ignore_sticky_posts' => 1,
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'meta_query' => array(
            array(
                'key' => '_stock_status',
                'value' => 'outofstock',
                'compare' => 'NOT IN',
            ),
        ),
    );
    if ('latest' === $wpcp_product_from) {
        $default_args['posts_per_page'] = $post_meta_value['wpcp_total_products'];
        $args = $default_args;
    } elseif ('specific_products' === $wpcp_product_from) {
        $args = array(
            'post__in' => $specific_product_ids,
        );
        $args = array_merge($default_args, $args);
    } elseif ('taxonomy' === $wpcp_product_from) {

        $args['tax_query'][] = array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $product_category_terms,
            'operator' => $product_terms_operator,
        );
        $args = array_merge($default_args, $args);
    }
    $product_query = new WP_Query($args);

    return $product_query->posts;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get-single-product-shipping', array(
        'methods' => 'POST',
        'callback' => 'getSingleProductShipping',
    ));
});

function getSingleProductShipping($request) {
    global $woocommerce;

    global $wpdb;
    //echo "<pre>"; print_r($request['product_id']); die;
    //print_r($woocommerce->get('shipping/zones/5')); die;
    $results = $wpdb->get_results("SELECT * FROM wp_woocommerce_per_product_shipping_rules WHERE product_id =" . $request['product_id']);
    $sipping = [];
    $used_m = array();
    foreach ($results as $k => $v) {
        if (!in_array($v->rule_postcode, $used_m)) {
            $zone_detail = get_option('woocommerce_per_product_' . $v->rule_postcode . '_settings');
            $sipping[$k] = $zone_detail;
            $sipping[$k]['id'] = $v->rule_postcode;
            $sipping[$k]['shipping_rule_cost'] += (float) $v->rule_cost;
            $sipping[$k]['shipping_rule_item_cost'] += (float) $v->rule_item_cost;
            array_push($used_m, $v->rule_postcode);
        }

    }
    //echo "<pre>"; print_r($sipping); die;
    return $sipping;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get-product-shipping', array(
        'methods' => 'POST',
        'callback' => 'bchGetProductShipping',
    ));
});

function bchGetProductShipping($request) {
    global $wpdb;
    $shipping_tbl = $wpdb->prefix . 'woocommerce_per_product_shipping_rules';
    $respnse = [];
    $product_ids = [];
    if (strpos($request['product_id'], ',')) {
        $product_ids = explode(',', $request['product_id']);
    } else {
        $product_ids = [$request['product_id']];
    }
    if (!empty($product_ids)) {
        $sippings = [];
        foreach ($product_ids as $product_id) {
            $results = $wpdb->get_results("SELECT * FROM $shipping_tbl WHERE product_id =" . $product_id);
            if ($results) {
                $sipping = [];
                foreach ($results as $k => $v) {
                    $zone_detail = get_option('woocommerce_per_product_' . $v->rule_postcode . '_settings');
                    $sipping[$k] = $zone_detail;
                    $sipping[$k]['id'] = $v->rule_postcode;
                    $sipping[$k]['shipping_rule_cost'] += (float) $v->rule_cost;
                    $sipping[$k]['shipping_rule_item_cost'] += (float) $v->rule_item_cost;
                }
                $sippings[$product_id] = $sipping;
            }
        }
        if (!empty($sippings)) {
            $respnse['status'] = 'success';
            $respnse['data'] = $sippings;
        } else {
            $respnse['status'] = 'error';
            $respnse['message'] = 'No Shipping found';
        }
    } else {
        $respnse['status'] = 'error';
        $respnse['message'] = 'product ids not are empty!';
    }
    return $respnse;
}

/* add_action('rest_api_init', function () {
  //register_rest_route('wp/v2', 'testing_api', array(
  'methods' => 'POST',
  'callback' => 'testingAPI'
  ));
  });

  function testingAPI($request){

  return $_product = get_product(263920);

  return $shipclass = $_product->get_shipping_class();

  die;
  }
 */

add_filter('wcfm_is_allow_change_main_order_status_on_all_item_shipped', '__return_true');

//Testing Order Api

add_action('rest_api_init', 'stripePaymentorder');

function stripePaymentorder() {
    register_rest_route('/wp/v2/', 'stripe_payment_order', array(
        'methods' => 'POST',
        'callback' => 'stripe_payment_for_order_api',
    ));
}

function stripe_payment_for_order_api($request = null) {

    global $wpdb;
    $auth = apache_request_headers();
    $authtoken = $auth['Authorization'];
    $vaildation = validateToken($authtoken);
    $secretKey = STRIPE_SECRET_KEY;

    $response = array();
    $parameters = $request->get_params();

    $amount = sanitize_text_field($parameters['amount']);
    $stripe_card_token = sanitize_text_field($parameters['stripe_card_token']);
    $order_data = $parameters['order_data'];
    $items_data = $order_data['line_items'];
    $product_name = array();
    for ($j = 0; $j < count($items_data); $j++) {
        $product_name[] = $items_data[$j]['name'];
    }
    $product_name = implode(',', $product_name);

    $user_id = $parameters['customer_id'];
    $country = $order_data['shipping_address']['country'];
    $vendor_id = $parameters['vendor_id'];
    $error = new WP_Error();

    // $stripe_connected_acc = get_user_meta( $vendor_id, 'wcfmmp_stripe_split_pay_customer_id', true );
    $stripe_connected_acc = 'cus_JhNfsjX5acWTs8';
    $transferPost = "description=" . $vaildation->email;
    $url = 'https://api.stripe.com/v1/customers';
    $transfer_response = curlRequest('POST', $transferPost, $url); /////Call curl function to generate stripe Customer id

    if (isset(json_decode($transfer_response)->error)) {
        $response['status'] = "error";
        $response['message'] = json_decode($transfer_response)->error->message;
        return $response;
    }
    $customer_id = json_decode($transfer_response)->id;
    $transferPost1 = "source=" . $stripe_card_token;
    $url1 = "https://api.stripe.com/v1/customers/" . $customer_id . "/sources";
    $transfer_response = curlRequest('POST', $transferPost1, $url1); /////Call curl function to Attach Customer with card token

    if (isset(json_decode($transfer_response)->error)) {

        $response['status'] = "error";
        $response['message'] = json_decode($transfer_response)->error->message;
        return $response;
    }

    $card_data = json_decode($transfer_response);
    $card_id = $card_data->id;
    $description = "woocommerce order";
    $stripe_total_price = $amount * 100;
    $post_data = "amount=" . $stripe_total_price . "&currency=gbp&customer=" . $customer_id . "&source=" . $card_id . '&metadata["product_name"]=' . $product_name . '&metadata["billing_address_one"]=' . $order_data['billing_address']['address_1'] . '&metadata["billing_address_two"]=' . $order_data['billing_address']['address_2'] . '&metadata["billing_address_one"]=' . $order_data['billing_address']['address_2'] . '&metadata["billing_city"]=' . $order_data['billing_address']['city'] . '&metadata["billing_country"]=' . $order_data['billing_address']['country'] . '&metadata["billing_email"]=' . $order_data['billing_address']['email'] . '&metadata["billing_first_name"]=' . $order_data['billing_address']['first_name'] . '&metadata["billing_last_name"]=' . $order_data['billing_address']['last_name'] . '&metadata["billing_phone"]=' . $order_data['billing_address']['phone'] . '&metadata["billing_state"]=' . $order_data['billing_address']['postcode'] . '&metadata["billing_postcode"]=' . $order_data['billing_address']['state'];
    $url2 = "https://api.stripe.com/v1/charges";
    $curl_response = curlRequest('POST', $post_data, $url2); //Call curl Function

    if (isset(json_decode($curl_response)->error)) {
        $response['status'] = 'error';
        $response['message'] = json_decode($curl_response)->error->message;
        return $response;
    } elseif (isset(json_decode($curl_response)->status) && json_decode($curl_response)->status == "succeeded") {

        if (!empty($order_data)) {

            $address_new = $order_data['billing_address'];
            $shipping_address = $order_data['shipping_address'];
            $line_items = $order_data['line_items'];
            $shipping_lines = $order_data['shipping_lines'];
            $shipping_total = $order_data['shipping_total'];
            $method_title = $shipping_lines["method_title"];
            $shipping_rate = $shipping_lines["total"];

            $order = wc_create_order(array('customer_id' => $user_id));
            for ($i = 0; $i < count($line_items); $i++) {
                $product_id = $line_items[$i]['product_id'];
                $product_qty = $line_items[$i]['quantity'];
                $order->add_product(get_product($product_id), $product_qty);
            }

            $order->set_address($address_new, 'billing');
            $order->set_address($shipping_address, 'shipping');
            $order->set_payment_method('stripe');
            $order->set_payment_method_title('Credit Card');
            $order->set_transaction_id(json_decode($curl_response)->id);
            $order->set_date_paid(time());

            //$order->add_shipping($shipping_lines);
            // Get a new instance of the WC_Order_Item_Shipping Object
            // Get the customer country code
            $country_code = $order->get_shipping_country();

            // Set the array for tax calculations
            $calculate_tax_for = array(
                'country' => $country_code,
                'state' => '', // Can be set (optional)
                'postcode' => '', // Can be set (optional)
                'city' => '', // Can be set (optional)
            );

            //$order->add_shipping(10);
            $item = new WC_Order_Item_Shipping();
            //$item->set_method_title($method_title);
            $item->set_total($shipping_total); // (optional)
            $order->add_item($item);
            $order->calculate_totals();

            $order_id = trim(str_replace('#', '', $order->get_order_number()));

            $my_post = array(
                'ID' => $order_id,
                'post_status' => 'wc-completed',
            );
            wp_update_post($my_post);
            $order->save();
            $response['status'] = 'success';
            $response['message'] = 'Order placed successfully';
            $response['data']['order_id'] = $order_id;
            $response['data'] = $product_id . "df" . $product_qty;
            return $response;
        }
    } else {
        $response['status'] = 'error';
        $response['message'] = 'Order details are missing Please check!';
        return $response;
    }
}

function bch_vendor_featured_products_callback($atts) {
    $atts = shortcode_atts(
            [
        'title' => __('Featured Products'),
        'number_of_products' => 5,
        'orderby' => 'ID',
        'order' => 'ASC',
            ], $atts, 'bch_vendor_featured_products');

    $wcfm_store_url = wcfm_get_option('wcfm_store_url', 'store');
    $wcfm_store_name = apply_filters('wcfmmp_store_query_var', get_query_var($wcfm_store_url));
    $seller_info = get_user_by('slug', $wcfm_store_name);
    $store_id = $seller_info->data->ID;

    $tax_query[] = [
        'taxonomy' => 'product_visibility',
        'field' => 'name',
        'terms' => 'featured',
        'operator' => 'IN', // or 'NOT IN' to exclude feature products
    ];

    $query_args = [];
    $query_args['post_type'] = 'product';
    $query_args['post_status'] = 'publish';
    $query_args['author'] = $store_id;
    $query_args['posts_per_page'] = $atts['number_of_products'];
    $query_args['orderby'] = $atts['orderby'];
    $query_args['order'] = $atts['order'];
    $query_args['tax_query'] = $tax_query;
    $query_args['fields'] = 'ids';

    $query = new WP_Query($query_args);
    $get_products = $query->get_posts();
    ?>
    <aside id="wcfmmp-store-featured-products-3" class="widget sidebar-box clr wcfmmp-store-featured-products">
        <div class="sidebar_heading">
            <h4 class="widget-title"><?php echo $atts['title']; ?></h4>
        </div>
        <ul class="product_list_widget">
            <?php
            if ($get_products) {
                foreach ($get_products as $product_id) {
                    $product = wc_get_product($product_id);
                    $attach_id = get_post_thumbnail_id($product_id);
                    $pro_image_html = wp_get_attachment_image($attach_id, 'thumbnail');
                    $pro_price = $product->get_price_html();
                    $get_pro_url = $product->get_permalink();
                    $get_pro_title = $product->get_name();
                    ?>
                    <li>
                        <a href="<?php echo $get_pro_url; ?>">
                            <?php echo $pro_image_html; ?>
                            <span class="product-title"><?php echo $get_pro_title; ?></span>
                        </a>
                        <?php echo $pro_price; ?>
                    </li>
                    <?php
                }
            } else {
                ?>
                <li>Feature Products Not available</li>
                <?php
            }
            ?>
        </ul>
    </aside>
    <?php
}

add_shortcode('bch_vendor_featured_products', 'bch_vendor_featured_products_callback');

// [bch_vendor_featured_products title="Featured Products" number_of_products="5" orderby="ID" order="ASC"]

function bch_api_res_callback() {
//    $amount = 7;
//
//    $stripe_total_price = (int) $amount * 100;
//
//    $admin_comm = ($amount * 7.2) / 100;
////    $app_fee_gen = round($admin_comm + 0.20, 2);
//    $app_fee_gen = $admin_comm;
//    $app_fee = $app_fee_gen * 100;
//
//    echo "amount : $amount <br>";
//    echo "stripe_total_price : $stripe_total_price <br>";
//    echo "admin_comm : $admin_comm <br>";
//    echo "app_fee_gen : $app_fee_gen <br>";
//    echo "app_fee : $app_fee <br>";
//    echo '------------------------<br>';
//    echo 'fnc data<br>';
//    echo '------------------------<br>';
//
//    $amount_gen = gt_get_stripe_amount($amount);
//    $app_fees_gen = gt_get_stripe_amount($admin_comm);
//
//    echo "amount : $amount_gen <br>";
//    echo "app_fee : $app_fees_gen <br>";
//    $res = get_option('bch_admin_charges');
    $res = get_option('list_header_set');
//    $res2 = get_option('test_place_order_responce');
    print "<pre>";
    print_r($res);
    print "<br>";
//    print_r($res2);
    print "</pre>";
//    delete_option('bch_admin_charges');
//    delete_option('test_place_order_responce');
}

add_shortcode('bch_api_res', 'bch_api_res_callback');

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_image', array(
        'methods' => 'GET',
        'callback' => 'bch_get_image_callback',
    ));
});

function bch_get_image_callback($request) {
    $parameters = $request->get_params();
    $attach_id = sanitize_text_field($parameters['id']);
    $image_size = 'large';
    if (isset($parameters['size'])) {
        $image_size = sanitize_text_field($parameters['size']);
    }

    $attachment = wp_get_attachment_image_src($attach_id, $image_size);
    if ($attachment) {
        $response['status'] = 'success';
        $response['data'] = [
            'image_id' => $attach_id,
            'url' => $attachment[0],
            'width' => $attachment[1],
            'height' => $attachment[2],
            'is_resized' => $attachment[3],
        ];
    } else {
        $response['status'] = 'error';
        $response['message'] = "No image found with #$attach_id this image id!";
    }

    return $response;
}

$allCharts = [];

add_filter( 'wcfm_is_allow_stripe_express_api', '__return_false' );


require_once('wcfm/wcfm-per-product-shipping-select2.php');

function adjust_action_scheduler_log_retention() {
	return 7 * DAY_IN_SECONDS;
}

add_filter('action_scheduler_retention_period', 'adjust_action_scheduler_log_retention');
