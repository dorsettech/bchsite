<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package storefront
 */
if (!is_active_sidebar('sidebar-1')) {
    return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
    <?php
    global $wp_query;
    $get_taxonomy = $wp_query->get_queried_object()->taxonomy;
    if ($get_taxonomy == 'category') {
        dynamic_sidebar('sidebar-1');
    } else {
        dynamic_sidebar('sidebar-shop');
    }
    ?>
</div><!-- #secondary -->