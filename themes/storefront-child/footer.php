<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">
		
		

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' );
			?>
		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>

<script>
    jQuery(document).ready(function(){
		var urlw      = window.location.href;
		
		if( urlw === 'https://thebritishcrafthouse.co.uk/my-account/?password-reset=true')	
		{
			
			function getMobileOperatingSystem() {

				var userAgent = navigator.userAgent || navigator.vendor || window.opera;
				
				// Windows Phone must come first because its UA also contains "Android"
				if (/windows phone/i.test(userAgent)) {
					return "Windows Phone";
				}

				if (/android/i.test(userAgent)) {
					return "Android";
				}

				// iOS detection from: http://stackoverflow.com/a/9039885/177710
				if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
					return "iOS";
				}

				return "unknown";
			}

			if(getMobileOperatingSystem() == 'Android'){

				window.location = "britishcraft://";
			}

			if(getMobileOperatingSystem() == 'iOS'){
				
				//window.location = ios_app_url;
				window.location = "britishcraft://";
			}

			if(getMobileOperatingSystem() == 'unknown'){
						   
				window.location = 'https://thebritishcrafthouse.co.uk/my-account/edit-account/';
				//window.location = "com.betasoft.barbersnet://?id=britishcraft";
			}
		}
    });
</script>
</body>
</html>