<?php
if( !function_exists( 'wcfm_product_manage_fields_shipping_per_product' ) ) {
	function wcfm_product_manage_fields_shipping_per_product( $shipping_fields, $product_id ) {
		global $wp, $WCFM, $WCFMu, $wpdb;
		
		$per_product_shipping_table = 'wcpv_per_product_shipping_rules';
		if( WCFMu_Dependencies::wcfm_wc_per_peroduct_shipping_active_check() ) {
			$per_product_shipping_table = 'woocommerce_per_product_shipping_rules';
		}
		
		
		$_per_product_shipping = 'no';
		$_per_product_shipping_add_to_all = 'no';
		$per_product_shipping_rules = array();
	
		if( $product_id ) {
			if( WCFMu_Dependencies::wcfm_wc_per_peroduct_shipping_active_check() ) {
				$_per_product_shipping = get_post_meta( $product_id, '_per_product_shipping', true ) ? get_post_meta( $product_id, '_per_product_shipping', true ) : 'no';
				$_per_product_shipping_add_to_all = get_post_meta( $product_id, '_per_product_shipping_add_to_all', true ) ? get_post_meta( $product_id, '_per_product_shipping_add_to_all', true ) : 'no';
			}
			
			$rules = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}{$per_product_shipping_table} WHERE product_id = %d ORDER BY rule_order;", $product_id ) );
	
			if( !empty($rules) ) {
				foreach ( $rules as $rule ) {
					$per_product_shipping_rules[] = array( 'country'   => explode( ',', esc_attr( $rule->rule_country ) ), 
																								 'state'     => esc_attr( $rule->rule_state ),
																								 'postcode'  => esc_attr( $rule->rule_postcode ),
																								 'cost'      => esc_attr( $rule->rule_cost ),
																								 'item_cost' => esc_attr( $rule->rule_item_cost ),
																								 'item_id'   => $rule->rule_id,
																								 );
				}
			}
		}
		
		$countries_obj   = new WC_Countries();
				$woo_continents = $countries_obj->get_continents();
				$woo_countries = $countries_obj->get_countries();
				$optGroupData = [];
				foreach($woo_continents as $key=>$value) {
					foreach( $value['countries'] as $country_code ) {
						 //echo $woo_countries[$country_code];
						$optGroupData[$value['name']][$country_code] =  $woo_countries[$country_code];
					}
				}
				//echo "<pre>";print_r($countries);echo "</pre>";
				//echo "<pre>";print_r($woo_countries);echo "</pre>";
				
                $countries       = $countries_obj->countries;
                foreach ($countries as $code => $value) {
                        $countries[$code] = html_entity_decode($value);
				}
				//echo "<pre>";print_r($woo_continents);echo "</pre>";
				
				$zone = WC_Shipping_Zones::get_zones();
				$zone_id;
				if($zone){
					foreach ($zone as $key => $value) { //dynamically gets value of last available shipping zone 
						$zone_id = $key;	//just for prevent any type of data lost or id change
					}
				}

				$wc_zone = new WC_Shipping_Zone($zone_id);
				$wc_shipping = $wc_zone->get_shipping_methods();

				$wc_shipping_zone = [];
				foreach ($wc_shipping as $key => $value) {
					$wc_shipping_zone[$key] = $value->title;
				}
		
		$per_product_shipping_fields = apply_filters( 'wcfm_per_product_shipping_fields', array( 
			"_per_product_shipping"            => array( 'label' => __( 'Per-product shipping', 'woocommerce-shipping-per-product' ) , 'type' => 'checkbox', 'class' => 'wcfm-checkbox', 'label_class' => 'wcfm_title checkbox_title', 'value' => 'yes', 'dfvalue' => $_per_product_shipping, 'hints' => __( 'Enable per-product shipping cost', 'woocommerce-shipping-per-product' ) ),
			//"_per_product_shipping_add_to_all" => array( 'label' => __( 'Adjust Shipping Costs', 'woocommerce-shipping-per-product' ) , 'type' => 'checkbox', 'class' => 'wcfm-checkbox', 'label_class' => 'wcfm_title checkbox_title', 'value' => 'yes', 'dfvalue' => $_per_product_shipping_add_to_all, 'hints' => __( 'Add per-product shipping cost to all shipping method rates?', 'woocommerce-shipping-per-product' ) ),
			"_per_product_shipping_rules"      => array('label' => __('Shipping Rules', 'wc-frontend-manager-ultimate') , 'type' => 'multiinput', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'value' => $per_product_shipping_rules, 'options' => array(
			
			//"country" => array('label' => __('Country Code', 'woocommerce-shipping-per-product'), 'type' => 'text', 'placeholder' => '*', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'hints' => __( 'A 2 digit country code, e.g. US. Leave blank to apply to all.', 'woocommerce-shipping-per-product' ) ),
			"country" => array('label' => __('Country Code', 'woocommerce-shipping-per-product'), 'attributes' => array( 'multiple' => 'multiple', 'style' => 'width: 60%;' ), 'type' => 'select', 'options-group' => $optGroupData, 'placeholder' => '*', 'class' => 'wcfm-select hb-country-multiselect country', 'label_class' => 'wcfm_title', 'hints' => __( 'The shipping destination.', 'woocommerce-shipping-per-product' ) ),
																									"state" => array('label' => __('State/County Code', 'woocommerce-shipping-per-product'), 'type' => 'text', 'placeholder' => '*', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'hints' => __( 'A state code, e.g. AL. Leave blank to apply to all.', 'woocommerce-shipping-per-product' ) ),
																									//"postcode" => array('label' => __('Zip/Postal Code', 'woocommerce-shipping-per-product'), 'type' => 'number', 'placeholder' => '*', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'hints' => __( 'Postcode for this rule. Wildcards (*) can be used. Leave blank to apply to all areas.', 'woocommerce-shipping-per-product' ) ),
																									"postcode" => array('label' => __('Shipping Method', 'woocommerce-shipping-per-product'), 'attributes' => array( 'style' => 'width: 60%;' ), 'type' => 'select','options' => $wc_shipping_zone, 'placeholder' => '*', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'hints' => __( 'Available Shipping Methods.', 'woocommerce-shipping-per-product' ) ),
																									"cost" => array('label' => __('Cost', 'woocommerce-shipping-per-product'), 'type' => 'number', 'placeholder' => '0.00', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'hints' => __( 'Decimal cost for the line as a whole.', 'woocommerce-shipping-per-product' ), 'attributes' => array( 'min' => '0.01', 'step' => '0.01' ) ),
																									"item_cost" => array('label' => __('Subsequent Item Cost', 'woocommerce-shipping-per-product'), 'type' => 'number', 'placeholder' => '0.00', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'hints' => __( 'Decimal cost for the item (multiplied by qty).', 'woocommerce-shipping-per-product' ), 'attributes' => array( 'min' => '0.01', 'step' => '0.01' ) ),
																									"item_id" => array( 'type' => 'hidden' )
																									)	)								
			), $shipping_fields, $product_id );
		
		if( !WCFMu_Dependencies::wcfm_wc_per_peroduct_shipping_active_check() ) {
			unset( $per_product_shipping_fields['_per_product_shipping'] );
			//unset( $per_product_shipping_fields['_per_product_shipping_add_to_all'] );
		}
		
		$shipping_fields = array_merge( $shipping_fields, $per_product_shipping_fields );
		
		return $shipping_fields;
	}
}

?>