<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Fetch Google APi
 *
 * @package storefront
 */

//get_header(); ?>

<!doctype html>
<html lang="en-GB">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="https://thebritishcrafthouse.co.uk/xmlrpc.php">
	<meta name="p:domain_verify" content="2612508734195" />
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KVFHJP8');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KVFHJP8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-178243004-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-178243004-1');
	
		


    //-------------- Get Auth Token -------------------

    

	</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MJ34SQT');
<!-- End Google Tag Manager -->
</script>

	</head><body data-rsssl=1 class="home page-template page-template-template-fullwidth page-template-template-fullwidth-php page page-id-36 wp-custom-logo wp-embed-responsive theme-storefront kingcomposer kc-css-system woocommerce-no-js mega-menu-primary tinvwl-theme-style group-blog storefront-align-wide left-sidebar woocommerce-active sp-header-active sp-designer sp-shop-alignment-center storefront-blog-excerpt-active storefront-pricing-tables-active wcfm-theme-shopfront-theme">
	
	
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MJ34SQT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<link rel="stylesheet" href="https://thebritishcrafthouse.co.uk/wp-content/cache/min/1/3db3c7c9694f977d77aa12bbc8de9668.css"/>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<!-- Fetch Data Goes Here --->
		

		
		
		
		
		<!-- End Fetch data COde -->
		
		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
