<?php

/**
 * render input field at Memberships > Subscription tab after strip ID
 * store there stripe "default_tax_rates" key generated from stripe account dashboard
 * https://dashboard.stripe.com/test/tax-rates
 *
 * @return $args
 */
add_filter('membership_manager_fields_subscription_stripe_plan', 'default_tax_rate_key');

function default_tax_rate_key($args) {

    global $wp;
    $membership_id = $wp->query_vars['wcfm-memberships-manage'];
    $subscription = (array) get_post_meta($membership_id, 'subscription', true);
    $stripe_def_tax_id = isset($subscription['stripe_def_tax_id']) ? $subscription['stripe_def_tax_id'] : '';
    $args['stripe_def_tax_id'] = [
        'label' => 'Stripe Default tax rate id',
        'name' => 'subscription[stripe_def_tax_id]',
        'type' => 'text',
        'class' => 'wcfm-text wcfm_ele stripe_plan_ele',
        'label_class' => 'wcfm_title stripe_plan_ele',
        'desc_class' => 'stripe_plan_ele',
        'value' => $stripe_def_tax_id,
        'hints' => 'ID of the tax rate that you generated from stripe. Go to Product > Tax rates > New and generate to whatever you wnat TAX rate and  paste here.',
            // 'desc'           => 'How can I have this?'
    ];

    return $args;
}

function placeOrderTestMode($parameters = null) {
    try {
        global $wpdb, $WCFMmp;
        $wc_emails = WC()->mailer()->get_emails();
        $response = [];
        $amount = sanitize_text_field($parameters['amount']);
        $stripe_card_token = sanitize_text_field($parameters['stripe_card_token']);
        $stripe_card_source = sanitize_text_field($parameters['stripe_card_source']);
        $order_data = $parameters['order_data'];
        $items_data = $order_data['line_items'];

        $product_name = [];
        for ($j = 0; $j < count($items_data); $j++) {
            $product_name[] = $items_data[$j]['name'];
        }
        $product_name = implode(',', $product_name);

        $user_id = $parameters['customer_id'];
        $product_price = $parameters['product_price'];
        $shipping_price = $parameters['shipping_price'];
        $order_data['shipping_price'] = $shipping_price;
        $country = $order_data['shipping_address']['country'];
        $vendor_id = $parameters['vendor_id'];
        $cust_email = $order_data['billing_address']['email'];
//        $v_user = $parameters['valid_user'];
//
//        $response_order = gt_create_order($order_data, $user_id);
//        if ($response_order['status'] == 'success') {
//            $order_id = $response_order['order_id'];
//
//            $wcfm_stripe = new WCFMmp_Gateway_Stripe_Split();
//
//            $process_pay = $wcfm_stripe->process_payment($order_id);
//
//            return $process_pay;
//
//            $update_order_args = [
//                'ID' => $order_id,
//                'post_status' => 'wc-processing',
//            ];
//            wp_update_post($update_order_args);
//            // make payment
//        } else {
//            return $response_order;
//        }


        $response_order = gt_create_order($order_data, $user_id);
        if ($response_order['status'] == 'error') {
            return $response_order;
        }
        $order_id = $response_order['order_id'];

        $re_total_commission = $wpdb->get_var("SELECT SUM(total_commission) as total_commission FROM `{$wpdb->prefix}wcfm_marketplace_orders` WHERE order_id =" . $order_id . " AND vendor_id = " . $vendor_id);
        $amount = $amount - $shipping_price;
        $admin_comm = ($amount * 7.2) / 100;

        $stripe_total_price = gt_get_stripe_amount($amount);
        $app_fee = gt_get_stripe_amount($admin_comm);
        //stripe api keys
//        if ($cust_email == 'aaronhealey23@gmail.com') {
        // $secretKey = 'sk_test_4A5Us3O1KqJaYBrC0J69pDbn00G4Iq3MKJ';
//        } else {
           $secretKey = STRIPE_SECRET_KEY;
//        }

        /* NEW CODE GOES HERE... */
        require_once get_home_path() . '/wp-content/plugins/wc-multivendor-marketplace/includes/Stripe/init.php';

        \Stripe\Stripe::setApiKey($secretKey);

        // Create customer
        $stripe_customer_id = get_user_meta($user_id, 'wcfmmp_stripe_split_pay_customer_id', true);
//        $customer = \Stripe\Customer::update( $customer->id, $customer_data );
//        $customer = \Stripe\Customer::retrieve( $stripe_customer_id );
        $customer_data = [
            'source' => $stripe_card_source,
            'email' => sanitize_email($cust_email),
            'phone' => sanitize_text_field($order_data['billing_address']['phone']),
            'name' => sanitize_text_field($order_data['billing_address']['first_name']) . ' ' . sanitize_text_field($order_data['billing_address']['last_name']),
            'description' => 'Customer for from API...!! ' . get_bloginfo(),
        ];
        $customer = \Stripe\Customer::create($customer_data);
        if (!is_object($customer)) {
            $customer = json_encode($customer);
        }

        // Directly Charge the Customer and collect the application_fee
        $connectedKey = get_user_meta($vendor_id, 'stripe_user_id', true);
        // $connectedKey = 'acct_1JSzDn2UHRorp4LE';

        $currency = 'gbp';

        $price_detail = [
            'product_price' => $product_price, // full total
            'shipping_price' => $shipping_price,
        ];
//        $billing_address = $order_data['billing_address'];
        $charge_arr = [
            'amount' => $stripe_total_price, // full total
            'currency' => $currency,
            'source' => $stripe_card_token, // get from APP side
            'application_fee' => $app_fee,
//            'customer' => $customer->id,
            'description' => 'Payment Vendor for order #' . $order_id, // oderId goes here
            'receipt_email' => $order_data['billing_address']['email'],
            'shipping' => [
                'address' => [
                    'line1' => $order_data['billing_address']['address_1'],
                    'line2' => $order_data['billing_address']['address_2'],
                    'city' => $order_data['billing_address']['city'],
                    'state' => $order_data['billing_address']['postcode'],
                    'country' => $order_data['billing_address']['country'],
                    'postal_code' => $order_data['billing_address']['postcode'],
                ],
                'name' => $order_data['billing_address']['first_name'] . ' ' . $order_data['billing_address']['last_name'],
                'phone' => $order_data['billing_address']['phone'],
            ],
            'metadata' => [
                'order_id' => $order_id,
                'product_name' => $product_name,
                'billing_address_one' => $order_data['billing_address']['address_1'],
                'billing_address_two' => $order_data['billing_address']['address_2'],
                'billing_city' => $order_data['billing_address']['city'],
                'billing_state' => $order_data['billing_address']['postcode'],
                'billing_country' => $order_data['billing_address']['country'],
                'billing_email' => $order_data['billing_address']['email'],
                'billing_first_name' => $order_data['billing_address']['first_name'],
                'billing_last_name' => $order_data['billing_address']['last_name'],
                'billing_phone' => $order_data['billing_address']['phone'],
                'billing_postcode' => $order_data['billing_address']['postcode'],
            ],
        ];

        if ($app_fee <= 0) {
            unset($charge_arr['application_fee']);
        }
        $create_charge = \Stripe\Charge::create($charge_arr, ['stripe_account' => $connectedKey]);

        if (!is_object($create_charge)) {
            $create_charge = json_encode($create_charge);
        }
        $transaction_id = $create_charge->id;
        $console = [];
        $console['status'] = 'success';
        $console['status'] = $parameters;
//        $console['customer_charge'] = $create_charge;
//        $console['admin_charge'] = $admin_charge;

        update_option('bch_admin_charges', $console, true);

        // error handle
        if (isset($create_charge->error)) {
            $response['status'] = "error";
            $response['message'] = $create_charge->error->message;
            return $response;
        } elseif (isset($create_charge->status) && $create_charge->status == "succeeded") {
            $order = new WC_Order($order_id);
            $order->set_payment_method('stripe_split');
            $order->set_payment_method_title('Credit or Debit Card (Stripe)');
            $order->set_transaction_id($transaction_id);
            $order->set_date_paid(time());
            $order_id2 = $order->save();

            update_post_meta($order_id2, '_stripe_customer_id', $customer->id);
            update_post_meta($order_id2, 'wcfmmp_stripe_split_pay_charge_id_' . $vendor_id, $transaction_id);
            update_post_meta($order_id2, 'wcfmmp_stripe_split_pay_charge_type_' . $vendor_id, 'direct_charges');
            update_post_meta($order_id2, 'wcfmmp_stripe_split_pay_charge_type', 'direct_charges');

            update_post_meta($order_id2, 'is_ordered_from_APP', $customer->id);
            // Allow resending new order email
            //add_filter('woocommerce_new_order_email_allows_resend', '__return_true');
            // Resend new order email
            $wc_emails['WC_Email_New_Order']->trigger($order_id);
            $wc_emails['WC_Email_Customer_Processing_Order']->trigger($order_id);
            $wc_emails['WCFMmp_Email_Store_new_order']->trigger($order_id);

            // Disable resending new order email
            //add_filter('woocommerce_new_order_email_allows_resend', '__return_false');


            /* WCFM data */

            $commission_id_list = $wpdb->get_col($wpdb->prepare("SELECT ID FROM `{$wpdb->prefix}wcfm_marketplace_orders` WHERE order_id = %d AND vendor_id = %d", $order_id, $vendor_id));

            /* Creating Withdrawal Instance */
            $tbchgross_sales = $order->get_total();
            $withdrawal_id = $WCFMmp->wcfmmp_withdraw->wcfmmp_withdrawal_processed( $vendor_id, $order_id, implode( ',', $commission_id_list ), 'stripe_split', $tbchgross_sales, $re_total_commission, 0, 'pending', 'by_split_pay', 0 );

            /* Withdrawal Processing */
            $WCFMmp->wcfmmp_withdraw->wcfmmp_withdraw_status_update_by_withdrawal( $withdrawal_id, 'completed', __( 'Stripe Split Pay', 'wc-multivendor-marketplace' ) );

            /* Withdrawal Meta */
            $WCFMmp->wcfmmp_withdraw->wcfmmp_update_withdrawal_meta( $withdrawal_id, 'withdraw_amount', $re_total_commission );
            $WCFMmp->wcfmmp_withdraw->wcfmmp_update_withdrawal_meta( $withdrawal_id, 'currency', $order->get_currency() );
            $WCFMmp->wcfmmp_withdraw->wcfmmp_update_withdrawal_meta( $withdrawal_id, 'transaction_id', $transaction_id );
            $WCFMmp->wcfmmp_withdraw->wcfmmp_update_withdrawal_meta( $withdrawal_id, 'transaction_type', 'direct_charges' );

            do_action( 'wcfmmp_withdrawal_request_approved', $withdrawal_id );

            /* END WCFM data */

            $my_post = [
                'ID' => $order_id2,
                'post_status' => 'wc-processing',
            ];
            wp_update_post($my_post);
            return TRUE;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'There is some error, please contact admin';
            return $response;
        }
        /*

          $admin_charge_data = [
          'amount' => $app_fees_gen,
          'currency' => $currency,
          //            'source' => $stripe_card_token, // get from APP side
          'customer' => $customer->id,
          'description' => 'ADMIN PART API - ' . date('Y-m-d h:i:sa'),
          'metadata' => [
          'order_id' => $order_id,
          'email' => $order_data['billing_address']['email'],
          'phone' => $order_data['billing_address']['phone'],
          'billing_email' => $order_data['billing_address']['email'],
          'billing_phone' => $order_data['billing_address']['phone'],
          ],
          ];
          // Amount Pay to Admin
          $admin_charge = \Stripe\Charge::create($admin_charge_data);

          if (empty($stripe_customer_id)) {
          update_user_meta($user_id, 'wcfmmp_stripe_split_pay_customer_id', $customer->id);
          }
          if (!is_object($admin_charge)) {
          $admin_charge = json_encode($admin_charge);
          }


          // error handle
          if (isset($admin_charge->error)) {
          $response['status'] = 'error';
          $response['message'] = $admin_charge->error->message;
          return $response;
          } elseif (isset($admin_charge->status) && $admin_charge->status == "succeeded") {

          $order = new WC_Order($order_id);
          $order->set_payment_method('stripe_split');
          $order->set_payment_method_title('Credit or Debit Card (Stripe)');
          $order->set_transaction_id($transaction_id);
          $order->set_date_paid(time());
          $order_id2 = $order->save();

          $my_post = [
          'ID' => $order_id,
          'post_status' => 'wc-processing',
          ];
          wp_update_post($my_post);
          return true;
          } else {
          $response['status'] = 'error';
          $response['message'] = 'There is some error, please contact admin';
          return $response;
          }

         */
        return true;
        // exit(' END API calls..!! and now can below create order and update rquire meta etc....');
    } catch (Exception $e) {
        echo "Error...";
        print_r($e);
    }
}

function gt_create_order($order_data, $user_id) {
    $response = [];
//    $currency = get_woocommerce_currency();
    $currency = get_woocommerce_currency_symbol();
    if (!empty($order_data)) {

        $address_new = $order_data['billing_address'];
        $shipping_address = $order_data['shipping_address'];
        $line_items = $order_data['line_items'];
        $shipping_lines = $order_data['shipping_lines'];
        $shipping_total = $order_data['shipping_total'];

        $shipping_price = $order_data['shipping_price'];
        //$user_id = $vaildation->id;
        // Now we create the order
        $order = wc_create_order(['customer_id' => $user_id]);

//        for ($i = 0; $i < count($line_items); $i++) {
//            $product_id = $line_items[$i]['product_id'];
//            $product_qty = $line_items[$i]['quantity'];
//            $order->add_product(get_product($product_id), $product_qty);
//        }

        foreach ($line_items as $key => $line_item) {
            $product_id = $line_item['product_id'];
            $product_qty = $line_item['quantity'];
            $product_total = $line_item['total'];
            $item_meta_data = $line_item['meta_data'];
            $item_addons_data = $line_item['addons'];
            $item_addons_meta = [];
            $item_extra_price_a = [];
            if ($item_addons_data) {
                foreach ($item_addons_data as $key => $item_addon_data) {
                    $addon_key = $item_addon_data['key'];
                    $addons_price = isset($item_addon_data['price']) && !empty($item_addon_data['price']) ? $item_addon_data['price'] : 0;
                    if ($addons_price > 0) {
                        $addon_key = $item_addon_data['key'] . " ($currency $addons_price)";
                        $item_extra_price_a[$addon_key] = $addons_price;
                    }
                    $item_addons_meta[] = [
                        'key' => $addon_key,
                        'value' => $item_addon_data['value'],
                    ];
                }
            }
            $item_extra_price = [];
            $item_extra_price['subtotal'] = $product_total;
            $item_extra_price['total'] = $product_total;
            if (!empty($item_extra_price_a)) {
//                $item_extra_price['totals'] = $item_extra_price_a;
            }
            $order_item_id = $order->add_product(get_product($product_id), $product_qty, $item_extra_price);

            if ($order_item_id) {
                if (!empty($item_addons_meta)) {
                    foreach ($item_addons_meta as $item_addon_meta) {
                        $meta_key = '';
                        if (isset($item_addon_meta['key']) && !empty($item_addon_meta['key'])) {
                            $meta_key = $item_addon_meta['key'];
                        }
                        $meta_value = '';
                        if (isset($item_addon_meta['value']) && !empty($item_addon_meta['value'])) {
                            $meta_value = $item_addon_meta['value'];
                        }
                        if (!empty($meta_key) && !empty($meta_value)) {
                            wc_update_order_item_meta($order_item_id, $meta_key, $meta_value);
                        }
                    }
                }

                foreach ($item_meta_data as $item_meta) {
                    $meta_key = '';
                    if (isset($item_meta['key']) && !empty($item_meta['key'])) {
                        $meta_key = $item_meta['key'];
                    }
                    $meta_value = '';
                    if (isset($item_meta['value']) && !empty($item_meta['value'])) {
                        $meta_value = $item_meta['value'];
                    }
                    if (!empty($meta_key) && !empty($meta_value)) {
                        wc_update_order_item_meta($order_item_id, $meta_key, $meta_value);
                    }
                }
            }
        }
        //$order->add_product( get_product(225929), $line_items[0]['quantity']);
        // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php

        $order->set_address($address_new, 'billing');
        $order->set_address($shipping_address, 'shipping');

//        $order->set_payment_method('stripe');
//        $order->set_payment_method_title('Credit Card');
//        $order->set_transaction_id(json_decode($curl_response)->id);
//        $order->set_date_paid(time());
        //$order->add_shipping($shipping_lines);
        // Get a new instance of the WC_Order_Item_Shipping Object
        // Get the customer country code
        $country_code = $order->get_shipping_country();

        // Set the array for tax calculations
        $calculate_tax_for = [
            'country' => $country_code,
            'state' => '', // Can be set (optional)
            'postcode' => '', // Can be set (optional)
            'city' => '', // Can be set (optional)
        ];
//        shipping_lines: {id: "18", title: "Royal Mail 2nd Class", cost: "0.5"}
        $item = new WC_Order_Item_Shipping();
        //$item->set_method_id( $shipping_lines['id'] );//set an existing Shipping method rate ID
        $item->set_method_title($shipping_lines['title']);
        if (isset($shipping_lines['cost'])) {
            $item->set_total($shipping_lines['cost']); // (optional)
        } else {
            $item->set_total($shipping_price); // (optional)
        }
        //$item->calculate_taxes($calculate_tax_for);

        $order->add_item($item);
        $order->calculate_totals();
        $order_id = trim(str_replace('#', '', $order->get_order_number()));



//        $order = new WC_Order($order_id);
//        $order->set_payment_method('stripe');
//        $order->set_payment_method_title('Credit Card');
//        $order->set_transaction_id(json_decode($curl_response)->id);
//        $order->set_date_paid(time());
//        $order_id2 = $order->save();

        /*
         * wc-pending
         * wc-processing
         * wc-on-hold
         * wc-completed
         * wc-cancelled
         * wc-refunded
         * wc-failed
         */

        $my_post = [
            'ID' => $order_id,
            'post_status' => 'wc-pending',
        ];
        wp_update_post($my_post);

        $order_id = $order->save();



        $response['status'] = 'success';
        $response['message'] = 'Order placed successfully';
        $response['order_id'] = $order_id;
        $response['data']['order_id'] = $order_id;
        $response['data'] = $product_id . "df" . $product_qty;
        return $response;
    } else {
        $response['status'] = 'error';
        $response['message'] = 'Order details are missing Please check!';
        return $response;
    }
}

function gt_get_stripe_amount($total, $currency = '', $reverse = false) {
    if (!$currency) {
        $currency = get_woocommerce_currency();
    }

    $total = round($total, 2);

    if (in_array(strtolower($currency), gt_no_decimal_currencies())) {
        return absint($total);
    } else {
        if ($reverse) {
            return absint(wc_format_decimal(( (float) $total / 100), wc_get_price_decimals())); // actual.
        } else {
            return absint(wc_format_decimal(( (float) $total * 100), wc_get_price_decimals())); // In cents.
        }
    }
}

function gt_no_decimal_currencies() {
    return apply_filters('wcfmmp_stripe_split_pay_no_decimal_currencies', array(
        'bif', // Burundian Franc
        'djf', // Djiboutian Franc
        'jpy', // Japanese Yen
        'krw', // South Korean Won
        'pyg', // Paraguayan GuaranÃ­
        'vnd', // Vietnamese Ä�á»“ng
        'xaf', // Central African Cfa Franc
        'xpf', // Cfp Franc
        'clp', // Chilean Peso
        'gnf', // Guinean Franc
        'kmf', // Comorian Franc
        'mga', // Malagasy Ariary
        'rwf', // Rwandan Franc
        'vuv', // Vanuatu Vatu
        'xof', // West African Cfa Franc
    ));
}


/**
 * Override to show empty order item meta data
 */
add_filter('woocommerce_order_item_get_formatted_meta_data','overrideWCItemMeta', 10,2);
function overrideWCItemMeta( $formatted_meta, $that ) {
    $hideprefix = '_';
    $include_all = false;
    $meta_data         = $that->get_meta_data();
    $hideprefix_length = ! empty( $hideprefix ) ? strlen( $hideprefix ) : 0;
    $product           = is_callable( array( $that, 'get_product' ) ) ? $that->get_product() : false;
    $order_item_name   = $that->get_name();

    foreach ( $meta_data as $meta ) {
        if ( empty( $meta->id ) || ( $hideprefix_length && substr( $meta->key, 0, $hideprefix_length ) === $hideprefix ) ) {
            continue;
        }

        $meta->key     = rawurldecode( (string) $meta->key );
        $meta->value   = rawurldecode( (string) $meta->value );
        $attribute_key = str_replace( 'attribute_', '', $meta->key );
        $display_key   = wc_attribute_label( $attribute_key, $product );
        $display_value = wp_kses_post( $meta->value );

        if ( taxonomy_exists( $attribute_key ) ) {
            $term = get_term_by( 'slug', $meta->value, $attribute_key );
            if ( ! is_wp_error( $term ) && is_object( $term ) && $term->name ) {
                $display_value = $term->name;
            }
        }

        // Skip items with values already in the product details area of the product name.
        if ( ! $include_all && $product && $product->is_type( 'variation' ) && wc_is_attribute_in_product_name( $display_value, $order_item_name ) ) {
            continue;
        }

        $formatted_meta[ $meta->id ] = (object) array(
            'key'           => $meta->key,
            'value'         => $meta->value,
            'display_key'   => apply_filters( 'woocommerce_order_item_display_meta_key', $display_key, $meta, $that ),
            'display_value' => wpautop( make_clickable( apply_filters( 'woocommerce_order_item_display_meta_value', $display_value, $meta, $that ) ) ),
        );
    }

    return $formatted_meta;
}


// add_filter( 'woocommerce_available_payment_gateways', 'testing_purpose_only' );
function testing_purpose_only( $available_gateways ) {
    if ( get_current_user_id() == 22097 ) {
    } else {
        unset( $available_gateways['cod'] );
    }
    return $available_gateways;
}

add_action('admin_head', 'css_admin_remove');

function css_admin_remove() {
  echo '<style>
    .sale_price, #sale_price, .sales_schedule {
	display: none;
	}
  </style>';
}

function process_shop_gravtar( $img_path ){
    $process_avtar_url = str_replace('/var/www/vhosts/', '', $img_path);
    $process_avtar_url = str_replace('/httpdocs', '', $process_avtar_url);
    return $process_avtar_url;
}

// if (get_current_user_id() == 3990) {
add_filter( 'wcfmmp_store_logo','process_shop_gravtarss', 999, 2 );
function process_shop_gravtarss(  $store_avtar_url, $id ) {
    return process_shop_gravtar ($store_avtar_url);
}
// }


if (get_current_user_id() == 3990) {
    // add_filter( 'wcfmmp_store_tabs', 'storetabs__', 999, 2 );
    // add_filter( 'wcfmp_store_tabs_url', 'storetabs__', 999, 2 );
    function storetabs__($store_tabs, $thisid) {
        echo "<pre>";
        print_r($store_tabs);
        echo "</pre>";
        // $store_tabs['articles'] = 'Articles';
        return $store_tabs;
    }

    // add_action( 'wp_footer', 'custscript', 999999 );
    function custscript() {
        // global $WCFM;
        // $WCFM->library->load_qtip_lib();
        // echo "<pre>";
        // print_r($WCFM->library->load_qtip_lib());
        // echo "</pre>";
        echo '<script type="text/javascript" src="https://emceaz9wmwe.exactdn.com/wp-content/plugins/wc-frontend-manager/includes/libs/qtip/qtip.js"></script>';
    }
}


add_filter(
    'jwt_auth_valid_credential_response',
    function ( $response, $user ) {

        // Modify the response here.
        $response['data']['stripe_sk'] = STRIPE_SECRET_KEY;
        $response['data']['stripe_pk'] = STRIPE_PUBLIC_KEY;

        return $response;
    },
    10,
    2
);



add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_store_vendors_list_new', array(
        'methods' => 'GET',
        'callback' => 'get_store_vendors_list_new',
    ));
});

function get_store_vendors_list_new( $request ) {

    $per_page = !empty($request['per_page']) ? intval($request['per_page']) : 100;
    $pageno = !empty($request['pageno']) ? intval($request['pageno']) : 1;
    $args = array(
        'role' => 'wcfm_vendor',
        'number' => $per_page,
        'paged' => $pageno,
    );

    $wcfm_vendor = get_users( $args );
    // $wcfm_vendor = new WP_User_Query( $args );
    // print_r($wcfm_vendor);


    $response = array();
    $index = 0;
    if (sizeof($wcfm_vendor) > 0) {
        foreach ($wcfm_vendor as $vendor) {

            $vendorId = $vendor->ID;
            $wcfm_vendors_name = $vendor->user_login;
            $res = get_vendor_formatted_item_data($vendorId, $wcfm_vendors_name);
            // print_r($res);

            if ($res != null) {
                $response[$index] = $res;
                $index++;
            }
        }
    }


    if (!empty($response)) {
        $shop_name = array();
        foreach ($response as $key => $row) {
            $shop_name[$key] = $row['vendor_shop_name'];
        }
        array_multisort($shop_name, SORT_ASC, $response);
        $response_final['status'] = 'success';
        $response_final['data'] = $response;
        // print_r($res);
        return array_unique($response_final);
    } else {
        $response_final['status'] = 'success';
        $response_final['message'] = 'No shop found';
        return $response_final;
    }


}


// add_filter( 'wcfmapi_rest_prepare_store_vendor_object', 'add_vacation_data', 999999999999999, 2 );
add_filter( 'woocommerce_rest_prepare_product_object', 'add_vacation_data', 100, 3 );
function add_vacation_data( $response, $product, $d ) {

    if( isset($response->data) && !empty($response) && !is_object($response->data['store']) && isset( $response->data['store'] ) && !empty($response->data['store']) && isset( $response->data['store']['vendor_id'] ) && !empty($response->data['store']['vendor_id']) ) {
        $vendor_id = $response->data['store']['vendor_id'];
        $vendor_profile_data = get_user_meta( $vendor_id, 'wcfmmp_profile_settings', true );
        $vendor_vacation_mode = array(
            'disable_vacation_purchase' => $vendor_profile_data['wcfm_disable_vacation_purchase'],
            'vacation_mode_type'        => $vendor_profile_data['wcfm_vacation_mode_type'],
            'vacation_mode'             => $vendor_profile_data['wcfm_vacation_mode'],
            'vacation_mode_msg'         => $vendor_profile_data['wcfm_vacation_mode_msg'],
            'vacation_start_date'       => $vendor_profile_data['wcfm_vacation_start_date'],
            'vacation_end_date'         => $vendor_profile_data['wcfm_vacation_end_date'],
        );

        $response->data['vendor_vacation_mode'] = $vendor_vacation_mode;
        // $response['ok'] = "yeyysys";
        // $response['data']['asdas'] = "yeyysys";
        // $ok = array();
    }
    return $response;
}

function wpdocs_dequeue_script() {

    if ( isset($_GET['page']) && $_GET['page'] === 'advanced-cron-manager' ) {
        // code...
        wp_dequeue_script( 'jquery-ui-core' );
    }
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

add_filter( 'woocommerce_countries',  'dt_add_my_country' );
function dt_add_my_country( $countries ) {
  $new_countries = array(
	                    'NIRE'  => __( 'Northern Ireland', 'woocommerce' ),
                			'SCO'  => __( 'Scotland', 'woocommerce' ),
                			'WAL'  => __( 'Wales', 'woocommerce' ),
                			'ENG'  => __( 'England', 'woocommerce' ),
	);

	return array_merge( $countries, $new_countries );
}

add_filter( 'woocommerce_continents', 'dt_add_my_country_to_continents' );
function dt_add_my_country_to_continents( $continents ) {
  $continents['EU']['countries'][] = 'NIRE';
	$continents['EU']['countries'][] = 'SCO';
	$continents['EU']['countries'][] = 'WAL';
	$continents['EU']['countries'][] = 'ENG';
	return $continents;
}
