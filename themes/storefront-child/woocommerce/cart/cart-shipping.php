<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

$formatted_destination    = isset( $formatted_destination ) ? $formatted_destination : WC()->countries->get_formatted_address( $package['destination'], ', ' );
$has_calculated_shipping  = ! empty( $has_calculated_shipping );
$show_shipping_calculator = ! empty( $show_shipping_calculator );
$calculator_text          = '';
global $woocommerce;
$products_ids_array = array();
$items = $woocommerce->cart->get_cart();
$pps_selected_shipping_method = WC()->session->get( 'pps_selected_shipping_method' );


?>
<tr class="woocommerce-shipping-totals shipping">
	<th><?php echo wp_kses_post( $package_name ); ?></th>
	<td data-title="<?php echo esc_attr( $package_name ); ?>">
		<?php if ( $available_methods ) : ?>
			<?php /* <ul id="shipping_method" class="woocommerce-shipping-methods">
				<?php foreach ( $available_methods as $method ) : ?>
					<li>
						<?php
						if ( 1 < count( $available_methods ) ) {
							printf( '<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s />', $index, esc_attr( sanitize_title( $method->id ) ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ) ); // WPCS: XSS ok.
						} else {
							printf( '<input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" />', $index, esc_attr( sanitize_title( $method->id ) ), esc_attr( $method->id ) ); // WPCS: XSS ok.
						}
						printf( '<label for="shipping_method_%1$s_%2$s">%3$s</label>', $index, esc_attr( sanitize_title( $method->id ) ), wc_cart_totals_shipping_method_label( $method ) ); // WPCS: XSS ok.
						do_action( 'woocommerce_after_shipping_rate', $method, $index );
						?>
					</li>
				<?php endforeach; ?>
			</ul> */
			$flag = 0;
			foreach( $items as $cart_item ){
				$countrycode_array = array();
				$custom_rule = false;
				if ( $cart_item['variation_id'] ) {
					$custom_rule = woocommerce_per_product_shipping_get_matching_rule( $cart_item['variation_id'], $package );
				}

				if ( false === $custom_rule ) {
					$custom_rule = woocommerce_per_product_shipping_get_matching_rule( $cart_item['product_id'], $package );
				}
				$countrycode = '';
				if (isset($custom_rule->rule_country)) {
					$countrycode = $custom_rule->rule_country;					
				}
				$countrycode_array = explode(',', $countrycode);
				
				$POdata = array();
				if (isset($_POST['post_data'])) {
					parse_str($_POST['post_data'], $POdata);
				}
				if (isset($POdata['ship_to_different_address']) && $POdata['ship_to_different_address'] == 1) {
					if( isset($POdata['shipping_country']) && $POdata['shipping_country'] != '') {
						if (in_array($POdata['shipping_country'], $countrycode_array)) {
							// echo "fail";
							//parsed
						} else {
							$flag = 1;
						}
					}
				} else {
					if(in_array($woocommerce->customer->get_country( ), $countrycode_array) == false) {
						if( is_cart() ) {
							wc_add_notice( __("Please select a shipping option below"), 'error');
							wc_add_notice( __(""), 'error'); 
						}
						$flag = 1;
					}
				}
				

			}
			if( $flag == 1 ) {
				$pps_shipping_cost_html = 'No shipping available';
				echo $pps_shipping_cost_html;
				remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
			}else {
				$item_quantity = $cart_item['quantity'];
				$pps_shipping_charges = WC()->session->get( 'pps_shipping_charges' );
				// echo "<pre>";print_r($pps_shipping_charges);echo "</pre>";
				$pps_shipping_cost_html = '';

				if ( is_checkout() ) {
					$pps_shipping_method = WC()->session->get( 'pps_selected_shipping_method' );
					$method_data = get_option('woocommerce_per_product_'.$pps_shipping_method.'_settings');
					if ( $method_data['title'] ) {
						$pps_shipping_cost_html .= $method_data['title'].': ';
					}
				}

				$pps_shipping_cost = $pps_shipping_charges['shipping_charges'];
				$single_item_line_cost = $pps_shipping_charges['single_item_line_cost'];
				$single_item_item_cost = $pps_shipping_charges['single_item_item_cost'];

				if( !empty($pps_shipping_cost) && count($items) > 1 ) {
					foreach( $pps_shipping_cost as $shipping_cost ) {
						$pps_shipping_cost_html .= wc_price($shipping_cost);
						$pps_shipping_cost_html .= ( $shipping_cost != end($pps_shipping_cost) ) ? ' + ' : ' = ';
					}
				}elseif( !empty($single_item_line_cost) && count($items) == 1 && $item_quantity > 1 ) {
					$pps_shipping_cost_html .= wc_price($single_item_line_cost[0]);
				}
				if( $single_item_item_cost[0] != 0 && count($items) == 1 && $item_quantity > 1 ) {
					$pps_shipping_cost_html .= ' + ' . wc_price($single_item_item_cost[0]).' = ';
				}
				$pps_shipping_total_cost = array_sum( $pps_shipping_charges['shipping_charges'] );
				$pps_shipping_cost_html .= wc_price($pps_shipping_total_cost);

				echo $pps_shipping_cost_html;
			}
			?>
			<?php if ( is_cart() ) : ?>
				<p class="woocommerce-shipping-destination">
					<?php
					if ( $formatted_destination ) {
						// Translators: $s shipping destination.
						printf( esc_html__( 'Shipping to %s.', 'woocommerce' ) . ' ', '<strong>' . esc_html( $formatted_destination ) . '</strong>' );
						$calculator_text = esc_html__( 'Change address', 'woocommerce' );
					} else {
						echo wp_kses_post( apply_filters( 'woocommerce_shipping_estimate_html', __( 'Shipping options will be updated during checkout.', 'woocommerce' ) ) );
					}
					?>
				</p>
			<?php endif; ?>
			<?php
		elseif ( ! $has_calculated_shipping || ! $formatted_destination ) :
			if ( !empty($pps_selected_shipping_method) ) :
				echo wp_kses_post( apply_filters( 'woocommerce_shipping_may_be_available_html', __( 'Enter your address to view shipping options.', 'woocommerce' ) ) );
			else:
				echo wp_kses_post( apply_filters( 'woocommerce_shipping_may_be_available_html', __( 'Please select shipping options.', 'woocommerce' ) ) );
			endif;
		elseif ( ! is_cart() ) :
			if ( !empty($pps_selected_shipping_method) ) :
				echo wp_kses_post( apply_filters( 'woocommerce_no_shipping_available_html', __( 'There are no shipping options available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
			else:
				echo wp_kses_post( apply_filters( 'woocommerce_no_shipping_available_html', __( 'Please select shipping options.', 'woocommerce' ) ) );
			endif;
		else :
			// Translators: $s shipping destination.
			if ( !empty($pps_selected_shipping_method) ) :
				echo wp_kses_post( apply_filters( 'woocommerce_cart_no_shipping_available_html', sprintf( esc_html__( 'No shipping options were found for %s.', 'woocommerce' ) . ' ', '<strong>' . esc_html( $formatted_destination ) . '</strong>' ) ) );
			else:
				echo wp_kses_post( apply_filters( 'woocommerce_cart_no_shipping_available_html', sprintf( esc_html__( 'Please select shipping options for %s.', 'woocommerce' ) . ' ', '<strong>' . esc_html( $formatted_destination ) . '</strong>' ) ) );
				remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
			endif;
			$calculator_text = esc_html__( 'Enter a different address', 'woocommerce' );
		endif;
		?>

		<?php if ( $show_package_details ) : ?>
			<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?>
		<?php endif; ?>

		<?php if ( $show_shipping_calculator ) : ?>
			<?php woocommerce_shipping_calculator( $calculator_text ); ?>
		<?php endif; ?>
	</td>
</tr>