<?php
/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
include_once('api_store_vendors.php');

function sf_child_theme_dequeue_style() {
    wp_dequeue_style('storefront-style');
    wp_dequeue_style('storefront-woocommerce-style');
}

/* * ***** CORS ****** */

function add_custom_headers() {

    add_filter('rest_pre_serve_request', function($value) {
        header('Access-Control-Allow-Headers: Authorization, X-WP-Nonce,Content-Type, X-Requested-With, Accept, x-xsrf-token');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Credentials: true');
        return $value;
    });
}

add_action('rest_api_init', 'add_custom_headers', 15);

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */
//** this code is used to fix the cores issues on IPA
add_filter('kses_allowed_protocols', function($protocols) {
    $protocols[] = 'ionic';
    return $protocols;
});

function shs_remove_actions_parent_theme() {
    remove_action('storefront_page', 'storefront_page_header', 10);
}

;
add_action('init', 'shs_remove_actions_parent_theme', 1);

add_action('init', 'z_remove_wc_breadcrumbs');

function z_remove_wc_breadcrumbs() {
    remove_action('storefront_before_content', 'woocommerce_breadcrumb', 10);
}

function x_my_custom_widgets_init() {

    register_sidebar(array(
        'name' => __('Shop Sidebar', '__x__'),
        'id' => 'sidebar-shop',
        'description' => __('Shop filters.', '__x__'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));
}

add_action('widgets_init', 'x_my_custom_widgets_init');

add_filter('wcfmmp_store_tabs', function( $store_tabs, $store_id ) {
    if (isset($store_tabs['about']))
        unset($store_tabs['about']);
    return $store_tabs;
}, 50, 2);

function prefix_category_title($title) {
    if (is_category()) {
        $title = single_cat_title('', false);
    }
    return $title;
}

add_filter('get_the_archive_title', 'prefix_category_title');

add_action('init', 'jk_customise_storefront');

function jk_customise_storefront() {
    // Remove the storefromt post content function
    remove_action('storefront_loop_post', 'storefront_post_content', 30);

    // Add our own custom function
    add_action('storefront_loop_post', 'jk_custom_storefront_post_content', 30);
}

function jk_custom_storefront_post_content() {
    ?>
    <div class="entry-content" itemprop="articleBody">
        <?php
        if (has_post_thumbnail()) {
            the_post_thumbnail('full', array('itemprop' => 'image'));
        }
        ?>
        <?php the_excerpt(); ?>
    </div><!-- .entry-content -->
    <?php
}

/**
 * Remove related products output
 */
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

/* add articles tab */
add_action(
        'wcfmmp_after_store_article_loop_start', function( $store_id, $store_info ) {
    echo
    '<div id="primary" class="content-area"> <main id="main" class="site-main" role="main">';
}, 50, 2);
add_action('wcfmmp_store_article_template', function() {
    get_template_part('content', 'post');
});
add_action('wcfmmp_store_article_template_none', function() {
    get_template_part('content', 'none');
});
add_action('wcfmmp_before_store_article_loop_end', function( $store_id, $store_info ) {
    echo '</div></div>';
}, 50, 2);
add_filter('wcfm_is_allow_store_articles', '__return_true');

// Move Product tabs underneath the product images
// =============================================================================
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
add_action('woocommerce_product_thumbnails', 'woocommerce_output_product_data_tabs', 10);
// =============================================================================
//change description tab name
add_filter('woocommerce_product_tabs', 'woo_rename_tabs', 98);

function woo_rename_tabs($tabs) {

    $tabs['description']['title'] = __('Details');        // Rename the description tab
    return $tabs;
}

/**
 * Reorder product data tabs
 */
add_filter('woocommerce_product_tabs', 'woo_reorder_tabs', 98);

function woo_reorder_tabs($tabs) {

    $tabs['description']['priority'] = 1;           // Description second
    return $tabs;
}

function wcfm_custom_2008_translate_text($translated) {
    $translated = str_ireplace('store', 'Shop', $translated);
    return $translated;
}

add_filter('gettext', 'wcfm_custom_2008_translate_text');
add_filter('ngettext', 'wcfm_custom_2008_translate_text');
add_filter('wcfm_is_allow_store_inquiry_custom_button_label', '__return_true');
add_filter('wcfm_is_allow_store_visibility', '__return_false');

function storefront_credit() {
    ?>
    <div class="site-info">
        <?php echo esc_html(apply_filters('storefront_copyright_text', $content = '&copy; ' . get_bloginfo('name') . ' ' . date('Y'))); ?>
    </div><!-- .site-info -->
    <?php
}

function hs_image_editor_default_to_gd($editors) {
    $gd_editor = 'WP_Image_Editor_GD';
    $editors = array_diff($editors, array($gd_editor));
    array_unshift($editors, $gd_editor);
    return $editors;
}

add_filter('wp_image_editors', 'hs_image_editor_default_to_gd');

/**
 * Add Continue Shopping Button on Cart Page
 */
add_action('woocommerce_after_cart_totals', 'tl_continue_shopping_button');

function tl_continue_shopping_button() {
    $shop_page_url = get_permalink(woocommerce_get_page_id('shop'));

    echo '<div class="">';
    echo ' <a href="' . $shop_page_url . '" class="button wc-forward">Continue Shopping</a>';
    echo '</div>';
}

add_filter('wcfmmp_shipping_processing_times', function( $processing_times ) {
    $processing_times[10] = __('5-7 business days', 'wcfm');
    return $processing_times;
}, 50);

function
wcfmmp_custom_hide_local_pickup_vendor($methods) {
    unset($methods['local_pickup']);
    return
            $methods;
}

/**
 * These next few bits hide local pick up and free shipping from the vendors shipping method options - keeping it super simple for them as they can only now pick flat rate.
 */
add_filter('vendor_shipping_methods', 'wcfmmp_custom_hide_local_pickup_vendor');

function
wcfmmp_custom_hide_free_shipping_vendor($methods) {
    unset($methods['free_shipping']);
    return
            $methods;
}

add_filter(
        'vendor_shipping_methods', 'wcfmmp_custom_hide_free_shipping_vendor');
/**
 * This next bit forces twelve products per page for vendors shops as the setting in the back end didn't seem to do it.
 */
add_filter(
        'wcfmmp_store_ppp', function( $post_per_page ) {
    $post_per_page = '12';
    return $post_per_page;
}, 50);
/**
 * This next bit removes the seo fields relating to social for things like open graph as this was just confusing vendors. It's in the Shop social and SEO settings.
 */
add_filter(
        'wcfm_is_allow_vendor_seo_facebook', '__return_false');
add_filter('wcfm_is_allow_vendor_seo_twitter', '__return_false');

/**
 * This next bit switches the word articles for blog in peoples shops.
 */
function wcfm_custom_1010_translate_text($translated) {
    $translated = str_ireplace('Articles', 'Blog', $translated);
    return $translated;
}

add_filter('gettext', 'wcfm_custom_1010_translate_text');
add_filter('ngettext', 'wcfm_custom_1010_translate_text');
/**
 * This next bit hides the video tutorial links for the admin as they only take them to the WCFM ones which are not suitable.
 */
add_filter('wcfm_is_allow_video_tutorial', '__return_false');

function register_image_sizes() {
    add_theme_support('post-thumbnails');
    add_image_size('carousel-twofifty', 250, 250, true);
}

add_action('after_setup_theme', 'register_image_sizes');

function wcfm_custom_1410_translate_text($translated) {
    $translated = str_ireplace('Inquiry', 'Enquiry', $translated);
    return $translated;
}

add_filter('gettext', 'wcfm_custom_1410_translate_text');
add_filter('ngettext', 'wcfm_custom_1410_translate_text');
/**
 * This next bit hides social media form from vendors
 */
add_filter('wcfm_profile_fields_social', function( $social_fields ) {
    $social_fields = wcfm_hide_field('linkdin', $social_fields);
    $social_fields = wcfm_hide_field('google_plus', $social_fields);
    $social_fields = wcfm_hide_field('snapchat', $social_fields);
    $social_fields = wcfm_hide_field('youtube', $social_fields);
    return $social_fields;
}, 50);
/**
 * This forces single shop checkout
 */
add_action('woocommerce_add_to_cart_validation', function( $is_allow, $product_id, $quantity ) {
    $product = get_post($product_id);
    $product_author = $product->post_author;
    //Iterating through each cart item
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        $cart_product_id = $cart_item['product_id'];
        $cart_product = get_post($cart_product_id);
        $cart_product_author = $cart_product->post_author;
        if ($cart_product_author != $product_author) {
            $is_allow = false;
            break;
        }
    }
    if (!$is_allow) {
        // We display an error message
        wc_clear_notices();
        wc_add_notice(__("We noticed you have products from another seller in your basket. Please visit your basket and checkout with their items first and then come back for this one. Thanks so much", "wcfm"), 'error');
    }
    return $is_allow;
}, 99, 3);

/**
 * Hide becone a vendor link on registration and login page
 */
add_filter('wcfm_is_allow_my_account_become_vendor', '__return_false');

/**
 * Resolve analytics inaccuracies when GEO locate isn't enabled
  add_filter( 'wcfm_is_allow_wc_geolocate', '__return_false' );
 */
/**
 * Hide shipping settings for vendors
 */
add_filter('wcfm_is_allow_vshipping_settings', '__return_false');

/**
 * Stops Next payment notifications being sent out
  add_filter( 'wcfm_is_allow_vendor_membership_renewal_notification', '__return_false' );
 */
add_filter('wcfmmp_is_allow_store_list_country_filter', '__return_false');
add_filter('wcfmmp_is_allow_store_list_state_filter', '__return_false');

/* add_action( 'wcfmmp_after_store_list_serach_form', function($WCFM) {

  ?>
  <select id="wcfmmp_store_country" class="country_select wcfm-select" name="wcfmmp_store_country">
  <option value="GB"><?php _e("United Kingdom (UK)"); ?></option>
  </select>
  <?php

  } ); */

add_filter('woocommerce_add_success', 'matched_zone_per_product_shpping_message', 10, 1);

function matched_zone_per_product_shpping_message($message) {
    if ($message == 'Customer matched zone "Per-Product Shipping"') {
        if (is_page('basket') || is_page('checkout')) {
            return '';
        }
    }
    return $message;
}

/* set per product shipping to true default */
add_filter('wcfm_per_product_shipping_fields', 'wcfm_per_product_shipping_fields_set_default', 10, 3);

function wcfm_per_product_shipping_fields_set_default($options, $shipping_fields, $product_id) {
    if ($product_id == 0) {
        $options['_per_product_shipping']['dfvalue'] = 'yes';
    }
    return $options;
}

// Convert pdf logo url into image path
add_filter("wcfmmp_store_logo", "wcfmmarketplace_logo_url_to_src", 10, 2);

function wcfmmarketplace_logo_url_to_src($store_logo, $vendor_id) {
    global $WCFM, $wpdb, $WCMp;
    $marketplece = wcfm_is_marketplace();
    if ($marketplece == 'wcfmmarketplace') {
        $vendor_data = get_user_meta($vendor_id, 'wcfmmp_profile_settings', true);
        $gravatar = isset($vendor_data['gravatar']) ? absint($vendor_data['gravatar']) : 0;
        $gravatar_url = $gravatar ? get_attached_file($gravatar) : '';

        if (!empty($gravatar_url)) {
            return $gravatar_url;
        }
    }
    return $store_logo;
}

function hb_filter_vendor_email_shipping_value($order) {
    $order_id = $order->get_id();
    // if ( ( $refunded = $order->get_total_shipping_refunded() ) > 0 ) {
    //     $shipping = '<del>' . strip_tags( wc_price( $order->get_total_shipping(), array( 'currency' => $order->get_currency() ) ) ) . '</del> <ins>' . wc_price( $order->get_total_shipping() - $refunded, array( 'currency' => $order->get_currency() ) ) . '</ins>';
    // } else {
    //     $shipping = wc_price( $order->get_total_shipping(), array( 'currency' => $order->get_currency() ) );
    // }
    $shipping = wc_price($order->get_total_shipping(), array('currency' => $order->get_currency()));

    $pps_shipping_charges = get_post_meta($order_id, 'pps_shipping_charges', true);
    if (!empty($pps_shipping_charges)) {
        $items = $order->get_items();
        $pps_shipping_cost_html = '';
        $pps_shipping_cost = $pps_shipping_charges['shipping_charges'];
        $single_item_line_cost = $pps_shipping_charges['single_item_line_cost'];
        $single_item_item_cost = $pps_shipping_charges['single_item_item_cost'];

        foreach ($items as $item_id => $item_data) {
            $item_quantity = $item_data->get_quantity();
            if (!empty($pps_shipping_cost) && count($items) > 1) {
                foreach ($pps_shipping_cost as $shipping_cost) {
                    $pps_shipping_cost_html .= wc_price($shipping_cost);
                    $pps_shipping_cost_html .= ( $shipping_cost != end($pps_shipping_cost) ) ? ' + ' : ' = ';
                }
            } elseif (!empty($single_item_line_cost) && count($items) == 1 && $item_quantity > 1) {
                $pps_shipping_cost_html .= wc_price($single_item_line_cost[0]);
            }
            if ($single_item_item_cost[0] != 0 && count($items) == 1 && $item_quantity > 1) {
                $pps_shipping_cost_html .= ' + ' . wc_price($single_item_item_cost[0]) . ' = ';
            }
            $pps_shipping_total_cost = array_sum($pps_shipping_charges['shipping_charges']);
            $pps_shipping_cost_html .= wc_price($pps_shipping_total_cost);
            break;
        }
        $pps_shipping_method = get_post_meta($order_id, 'pps_shipping_method', true);
        $method_data = get_option('woocommerce_per_product_' . $pps_shipping_method . '_settings');
        if ($method_data['title']) {
            $pps_shipping_cost_html .= ' <small class="shipped_via">via ' . $method_data['title'] . '</small>';
        }
        $shipping = $pps_shipping_cost_html;
    }
    return $shipping;
}

// upon shipped mark order as completed
add_filter('wcfm_is_force_shipping_address', '__return_true');
add_action('wcfm_after_order_mark_shipped', 'bch_mark_order_complted_on_ship', 50, 6);

function bch_mark_order_complted_on_ship($order_id, $order_item_id, $tracking_code, $tracking_url, $product_id, $wcfm_tracking_data) {
    $traking_added = false;
    $order = wc_get_order($order_id);
    $order->update_status('completed', '', true);
    do_action('woocommerce_order_edit_status', $order_id, 'completed');
}

// exclude archive post in the count
add_filter('wcfm_limit_check_status', 'bch_set_product_filter_exclude_archive', 10);

function bch_set_product_filter_exclude_archive($post_type) {
    $p_arr = array('publish', 'pending', 'draft', 'future', 'private');
    return $p_arr;
}

//enable full editor for excerpt for wcfm
add_filter('wcfm_product_manage_fields_content', 'bch_unset_tiny_editor', 10, 2);

function bch_unset_tiny_editor($field_arr) {
    if (isset($field_arr['excerpt']['teeny'])) {
        unset($field_arr['excerpt']['teeny']);
    }
    return $field_arr;
}

add_filter('wcfmmmp_gross_sales_shipping_cost', 'bch_get_original_shipping', 10, 2);

function bch_get_original_shipping($shipping_aount, $vendor_id) {
    $order_id = $_GET['order_id'];
    $order = new WC_Order($order_id);
    $order_shipping_total = $order->get_total_shipping();
    return $order_shipping_total;
}

add_filter('wcfm_gross_total', 'bch_filter_order_invoice_gross', 10, 3);

function bch_filter_order_invoice_gross($shipping_aount, $vendor_id, $order_id) {
    $order = new WC_Order($order_id);
    $get_total_order_gross = $order->get_total();
    return $get_total_order_gross;
}

function google_analytics_tracking_code() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143472985-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-143472985-1');
    </script>
    <?php
}

// include GA tracking code before the closing head tag
add_action('wp_head', 'google_analytics_tracking_code');

function disable_checkout_button_no_shipping() {
    $package_counts = array();
    // get shipping packages and their rate counts
    $packages = WC()->shipping->get_packages();
    foreach ($packages as $key => $pkg)
        $package_counts[$key] = count($pkg['rates']);

    // remove button if any packages are missing shipping options
    if (in_array(0, $package_counts))
        remove_action('woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20);
}

add_action('woocommerce_proceed_to_checkout', 'disable_checkout_button_no_shipping', 1);
add_action('woocommerce_before_cart', 'auto_select_free_shipping_by_default');

function auto_select_free_shipping_by_default() {
    if (isset(WC()->session) && !WC()->session->has_session())
        WC()->session->set_customer_session_cookie(true);

    // Check if "free shipping" is already set
    if (strpos(WC()->session->get('chosen_shipping_methods')[0], 'free_shipping') !== false)
        return;

    // Loop through shipping methods
    foreach (WC()->session->get('shipping_for_package_0')['rates'] as $key => $rate) {
        if ($rate->method_id === '16') {
            // Set "Free shipping" method
            WC()->session->set('chosen_shipping_methods', array($rate->id));
            return;
        }
    }
}

//add_filter( 'wcfmmp_store_ppp', function( $post_per_page ) {
//$post_per_page = '10';
//return $post_per_page;
//}, 50 );
add_action('wcfm_after_product_archived', function( $product_id ) {
    $product = wc_get_product($product_id);
    $product->set_catalog_visibility('hidden');
    $product->save();
});

// Load vendor Featured Categories
function vendor_ftr_cats($atts) {
    global $wp;
    $fetured_cat = '';
    $us = explode("/", $wp->request);
    $user = get_user_by('slug', $us[1]);
    $fetured_cat = get_user_meta($user->ID, 'product_section_cat', true);

    if ($fetured_cat != '') {
        $fetured_cat = explode(',', $fetured_cat);
        // $fetured_cat = array_slice($fetured_cat, 3);
        if (isset($fetured_cat) && $fetured_cat > 0 && !empty($fetured_cat)) :
            ob_start();
            ?>
            <div class="sidebar_heading">
                <h4 class="widget-title">Categories</h4>
                <ul>
                    <?php
                    foreach ($fetured_cat as $cat_value) :
                        if ($cat_value != '') {
                            $cate_name = get_term_by('id', (int) $cat_value, 'shop_category');
                            $term_link = get_term_link((int) $cat_value);
                            if (!is_wp_error($term_link)) {
                                ?>
                                <li>
                                    <a href="<?php echo $term_link; ?>"><?php echo $cate_name->name; ?></a>
                                </li>
                                <?php
                            }
                        }
                    endforeach;
                    ?>
                </ul>
            </div>
            <?php
        endif;
    }
    $fetured_cats = ob_get_contents();
    ob_end_clean();
    return $fetured_cats;
    // }   
}

add_shortcode('featured_catgory', 'vendor_ftr_cats');


remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');

#########################################################

add_filter('kses_allowed_protocols', function($protocols) {
    $protocols[] = 'ionic';
    return $protocols;
});

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/get/here_today_gone_tom', array(
        'methods' => 'GET',
        'callback' => 'here_today_gone_tom'
            )
    );
});

function here_today_gone_tom($data) {
    $final_data = [];
    $autumn_home = array(
        'post_type' => 'product',
        //'posts_per_page' => 15,
        'product_cat' => 'here-today-gone-tomorrow'
    );
    $loop = new WP_Query($autumn_home);
    //echo '<pre>';
    //print_r($loop);die;
    while ($loop->have_posts()) : $loop->the_post();
        global $product;
        $product_id = get_the_ID();
        $_product = wc_get_product($product_id);
        $image_id = $_product->get_image_id();
        $image_url = wp_get_attachment_image_url($image_id, 'full');
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => $image_url,
            'title' => get_the_title(),
            'regular_price' => $_product->get_regular_price(),
            'sale_price' => $_product->get_sale_price(),
            'get_price' => $_product->get_price(),
        );
        array_push($final_data, $data);
    endwhile;
    wp_reset_query();
    $response['status'] = 'success';
    $response['data'] = $final_data;
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/get/here_today_gone_tom_using_slider', array(
        'methods' => 'GET',
        'callback' => 'here_today_gone_tom_using_slider'
            )
    );
});

function here_today_gone_tom_using_slider($data) {
    $final_data = [];
    $post_meta_value = get_post_meta(3546, 'sp_wpcp_upload_options', true);

    if (!empty($post_meta_value['wpcp_specific_product'])) {

        foreach ($post_meta_value['wpcp_specific_product'] as $key => $valueid) {

            $_product = wc_get_product($valueid);

            if ($_product) {
                $title = $_product->get_title();
                $image_id = $_product->get_image_id();
                $url = get_permalink($valueid);
                $image_url = wp_get_attachment_image_url($image_id, 'full');
                $data = array(
                    'id' => $valueid,
                    'link' => $url,
                    'img' => $image_url,
                    'title' => $title,
                    'regular_price' => $_product->get_regular_price(),
                    'sale_price' => $_product->get_sale_price(),
                    'get_price' => $_product->get_price(),
                );

                array_push($final_data, $data);
            }
        }
        wp_reset_query();
        $response['status'] = 'success';
        $response['data'] = $final_data;
        return $response;
        exit;
    } else {

        $response['status'] = 'error';
        $response['message'] = "No data found";
        return $response;
        exit;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/get/featured', array(
        'methods' => 'GET',
        'callback' => 'get_featured'
            )
    );
});

function get_featured($data) {
    $final_data = [];
    $autumn_home_arr = array();
    $autumn_home = array(
        'post_type' => 'product',
        //'posts_per_page' => 15,
        'product_cat' => 'autumn-home'
    );
    $loop = new WP_Query($autumn_home);
    while ($loop->have_posts()) : $loop->the_post();
        global $product;
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => woocommerce_get_product_thumbnail(),
            'title' => get_the_title(),
        );
        array_push($autumn_home_arr, $data);
    endwhile;
    wp_reset_query();
    $response['status'] = 'success';
    $response['data'] = $autumn_home_arr;
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/post/contact/', array(
        'methods' => 'POST',
        'callback' => 'processForm'
            )
    );
});

function processForm($data) {

    if (isset($data)) {

        $name = $_POST['your-name'];
        $email = $_POST['your-email'];
        $message = $_POST['your-message'];
        $to = get_bloginfo('admin_email'); //sendto@example.com
        // $to = "abcdfg@yopmail.com"; 
        $subject = 'User Contact Email';
        $body = '<div class="bodyemail"><p><span>Name:</span>' . $name . '</p><p><span>Email:<span>' . $email . '</p><p><span>Massage:             <span>' . $message . '</p> </div>';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $sent_mail = wp_mail($to, $subject, $body, $headers);
        if ($sent_mail) {
            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
        }
        return $response;
        exit;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-page/', array(
        'methods' => 'POST',
        'callback' => 'home_page'
            )
    );
});

function home_page() {

    $Gift_Ideas_For_You_arr = [];
    $Heading = 'Gift ideas for you…';

    $Home_interiors_image = get_site_url() . '/wp-content/uploads/2020/06/shutterstock_339362615-2-1.jpg';
    $Home_interiors_link = 'home-and-interiors';
    $Home_interiors_title = 'Home & interiors';

    $Cards_and_stationery_image = get_site_url() . '/wp-content/uploads/2020/06/comp-shutterstock_680957929-1-1-500x334.jpg';
    $Cards_and_stationery_link = 'cards-stationery';
    $Cards_and_stationery_title = 'Cards & stationery';

    $Jewellery_image = get_site_url() . '/wp-content/uploads/2020/06/shutterstock_308848862-2-1.jpg';
    $Jewellery_link = 'fashion-and-jewellery';
    $Jewellery_title = 'Jewellery';

    $Baby_Child_image = get_site_url() . '/wp-content/uploads/2020/06/comp-shutterstock_1114763348-1-1-500x333.jpg';
    $Baby_Child_link = 'baby-gifts-child-gifts';
    $Baby_Child_title = 'Baby & Child';

    $fashion_image = get_site_url() . '/wp-content/uploads/2020/11/shutterstock_705175807-1.jpg';
    $fashion_link = 'fashion';
    $fashion_title = 'Fashion';

    // $Weddings_image = get_site_url().'/wp-content/uploads/2020/06/comp-shutterstock_1182517363-1-1-500x333.jpg';
    // $Weddings_link = 'weddings';
    // $Weddings_title = 'Weddings';

    $Art_image = get_site_url() . '/wp-content/uploads/2020/06/comp-shutterstock_1071475751-1-500x319.jpg';
    $Art_link = 'art';
    $Art_title = 'Art';

    $Gifts_occasions_image = get_site_url() . '/wp-content/uploads/2020/06/comp-shutterstock_184478294-1-1-500x333.jpg';
    $Gifts_occasions_link = 'gifts-occasions';
    $Gifts_occasions_title = 'Gifts & occasions';

    $Home_interiors[] = array("title" => $Home_interiors_title, "image" => $Home_interiors_image, "slug" => $Home_interiors_link);
    $Cards_and_stationery[] = array("title" => $Cards_and_stationery_title, "image" => $Cards_and_stationery_image, "slug" => $Cards_and_stationery_link);

    $Jewellery[] = array("title" => $Jewellery_title, "image" => $Jewellery_image, "slug" => $Jewellery_link);

    $Baby_Child[] = array("title" => $Baby_Child_title, "image" => $Baby_Child_image, "slug" => $Baby_Child_link);

    //$Weddings[] = array("title"=>$Weddings_title,"image"=>$Weddings_image,"slug"=>$Weddings_link);
    $Fashion[] = array("title" => $fashion_title, "image" => $fashion_image, "slug" => $fashion_link);

    $Art[] = array("title" => $Art_title, "image" => $Art_image, "slug" => $Art_link);

    $Gifts_occasions[] = array("title" => $Gifts_occasions_title, "image" => $Gifts_occasions_image, "slug" => $Gifts_occasions_link);

    $gift_Ideas_For_You_marge = array_merge($Home_interiors, $Cards_and_stationery, $Jewellery, $Baby_Child, $Fashion, $Art, $Gifts_occasions);

    $Gift_Ideas_For_You_Data = array(
        'Heading' => $Heading,
        'gift_Ideas_For_You' => $gift_Ideas_For_You_marge,
    );
    $Mainheading = 'Featured collections…';
    /* $letterbox_gift_ideas_image = get_site_url().'/wp-content/uploads/2020/10/shutterstock_1037746972-1.jpg';
      $letterbox_gift_ideas_link = 'letterbox-gifts';
      $letterbox_gift_ideas_title = 'Letterbox Gift Ideas';

      $Christmas_Ideas_image = get_site_url().'/wp-content/uploads/2020/09/shutterstock_484872337-1.jpg';
      $Christmas_Ideas_link ='christmas';
      $Christmas_Ideas_title = 'Christmas Ideas';

      /*$Handpicked_image = get_site_url().'/wp-content/uploads/2020/06/shutterstock_1410961910-2-1.jpg';
      $Handpicked_title = 'Handpicked';
      $Handpicked_Subtitle = 'BRITISH ARTISANS';
      $Handpicked_description = 'Carefully selected British artisans makers';

      $Original_image = get_site_url().'/wp-content/uploads/2020/06/comp-shutterstock_654791398-1-500x333.jpg';
      $Original_title = 'Original';
      $Original_subtitle = 'HANDMADE CREATIONS';
      $Original_description = 'Beautifully made, for you, in Britain';

      $Gifts_image = get_site_url().'/wp-content/uploads/2020/06/shutterstock_328895351-5-1.jpg';
      $Gifts_title = 'Gifts';
      $Gifts_subtitle = 'YOU’LL WANT TO KEEP';
      $Gifts_description = 'That’s OK though, we won’t tell…';

      $letterbox_gift_ideas[] = array("title"=>$letterbox_gift_ideas_title,"image"=>$letterbox_gift_ideas_image,"slug"=>$letterbox_gift_ideas_link);
      $Christmas_Ideas[] = array("title"=>$Christmas_Ideas_title,"image"=>$Christmas_Ideas_image,"slug"=>$Christmas_Ideas_link);
      /*$Handpicked[] = array("title"=>$Handpicked_title,"image"=>$Handpicked_image,"subtitle"=>$Handpicked_Subtitle,"description"=>$Handpicked_description);*
      $Original[] = array("title"=>$Original_title,"image"=>$Original_image,"subtitle"=>$Original_subtitle,"description"=>$Original_description);
      $Gifts[] = array("title"=>$Gifts_title,"image"=>$Gifts_image,"subtitle"=>$Gifts_subtitle,"description"=>$Gifts_description); */

    $easter_image = get_site_url() . '/wp-content/uploads/2021/04/shutterstock_481964947-2-1.jpg';
    $easter_link = 'garden-exteriors';
    $easter_title = 'OUTDOOR IDEAS';

    $send_hug_image = get_site_url() . '/wp-content/uploads/2021/02/shutterstock_1023285259-2.jpg';
    $send_hug_link = 'letterbox-gifts';
    $send_hug_title = 'SEND A HUG';

    $easter [] = array("title" => $easter_title, "image" => $easter_image, "slug" => $easter_link, "subtitle" => '', "description" => '');
    $send_hug [] = array("title" => $send_hug_title, "image" => $send_hug_image, "slug" => $send_hug_link, "subtitle" => '', "description" => '');

    $Featured_collections_marge = array_merge($easter, $send_hug);
    $Featured_collections = array(
        'Heading' => $Mainheading,
        'Featured_collections' => $Featured_collections_marge,
    );
    /*     * *******************Sell at The British Craft House section************ */
    $Newheading = 'Sell at The British Craft House';
    $Simple_title = 'Simple';
    $Simple_subtitle = 'APPLICATION';
    $Simple_description = 'Beautifully made, for you, in Britain';

    $Transparent_title = 'Transparent';
    $Transparent_subtitle = 'FEES';
    $Transparent_description = 'Low set up fee and great value flat-rate commission. No hidden extra costs';

    $Fantastic_title = 'Fantastic';
    $Fantastic_subtitle = 'SERVICE & SUPPORT';
    $Fantastic_description = 'Award winning support and advice from the largest, engaged community of British crafters';

    $Simple[] = array("title" => $Simple_title, "subtitle" => $Simple_subtitle, "description" => $Simple_description);
    $Transparent[] = array("title" => $Transparent_title, "subtitle" => $Transparent_subtitle, "description" => $Transparent_description);
    $Fantastic[] = array("title" => $Fantastic_title, "subtitle" => $Fantastic_subtitle, "description" => $Fantastic_description);

    $Sell_British_Craft_House_marge = array_merge($Simple, $Transparent, $Fantastic);
    $Sell_British_Craft_House = array(
        'heading' => $Newheading,
        'Sell_British_Craft_House' => $Sell_British_Craft_House_marge,
    );

    $response['status'] = 'success';
    $response['gift_ideas_for'] = $Gift_Ideas_For_You_Data;
    $response['featured_collections'] = $Featured_collections;
    $response['Sell_british_craft_house'] = $Sell_British_Craft_House;
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-and-interiors/', array(
        'methods' => 'POST',
        'callback' => 'home_and_interiors'
            )
    );
});

function home_and_interiors() {
    $heading = 'Home & Interiors';
    $description = 'Perfectly handcrafted gifts can be what makes the difference between a house and a home.
					Add some unique personality to your styling by choosing from our art, ceramics, lighting, cushions and decorations.
					All handmade with love by our talented team of artisans makers.';
    $art_image = get_site_url() . '/wp-content/uploads/2020/06/shutterstock_782650141-2-1.jpg';
    $art_link = 'art';
    $art_title = 'Art';
    $textiles_image = get_site_url() . '/wp-content/uploads/2019/08/knitting-1981518_1920.jpg';
    $textiles_link = 'textiles';
    $textiles_title = 'Textiles';
    $personalised_image = get_site_url() . '/wp-content/uploads/2019/10/a.jpg';
    $personalised_link = 'personalised-home';
    $personalised_title = 'Personalised Homeware';

    $art[] = array("title" => $art_title, "image" => $art_image, "slug" => $art_link);
    $textiles[] = array("title" => $textiles_title, "image" => $textiles_image, "slug" => $textiles_link);
    $personalised[] = array("title" => $personalised_title, "image" => $personalised_image, "slug" => $personalised_link);
    $home_and_interiors = array_merge($art, $textiles, $personalised);

    $HomeAndInteriors_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $home_and_interiors
    );
    $Your_home_your_style_Data = array();
    $Your_home_your_style_slider = get_post_meta(1641, 'sp_wpcp_upload_options', true);
    $Your_home_your_style_Title = get_post_field('post_title', 1641);
    $Your_home_your_style_imageid = $Your_home_your_style_slider['wpcp_gallery'];
    if (!empty($Your_home_your_style_imageid) && !empty($Your_home_your_style_Title)) {
        $ArrayOfYour_home_your_style_imageid = explode(',', $Your_home_your_style_imageid);
        foreach ($ArrayOfYour_home_your_style_imageid as $imageID) {
            $sliderurl = get_post_field('post_excerpt', $imageID);

            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => 'Your home, your style',
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Your_home_your_style_Title,
            );
            array_push($Your_home_your_style_Data, $data);
        }
    }
    $By_Colour_slider = get_post_meta(1653, 'sp_wpcp_upload_options', true);
    $By_Colour_Title = get_post_field('post_title', 1653);
    $By_Colour_imageid = $By_Colour_slider['wpcp_gallery'];
    $By_Colour_data = [];
    if (!empty($By_Colour_imageid) && !empty($By_Colour_Title)) {
        $ArrayOfimageid_By_Colour_imageid = explode(',', $By_Colour_imageid);

        foreach ($ArrayOfimageid_By_Colour_imageid as $imageID) {
            $sliderurl = get_post_field('post_excerpt', $imageID);

            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $By_Colour_Title,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $By_Colour_Title,
            );
            array_push($By_Colour_data, $data);
        }
    }
    $By_Style_data = [];
    $By_Style_slider = get_post_meta(1662, 'sp_wpcp_upload_options', true);
    $By_Style_Title = get_post_field('post_title', 1662);
    $By_Style_imageid = $By_Style_slider['wpcp_gallery'];

    if (!empty($By_Style_imageid) && !empty($By_Style_Title)) {
        $ArrayOfimageid_By_Style_imageid = explode(',', $By_Style_imageid);

        foreach ($ArrayOfimageid_By_Style_imageid as $imageID) {
            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $By_Style_Title,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $By_Style_Title,
            );
            array_push($By_Style_data, $data);
        }
    }
    $response['status'] = 'success';
    $response['section_one'] = $HomeAndInteriors_Section_one;
    $response['slider_one'] = $Your_home_your_style_Data;
    $response['slider_two'] = $By_Colour_data;
    $response['slider_three'] = $By_Style_data;
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-and-interiors-textiles/', array(
        'methods' => 'POST',
        'callback' => 'home_and_interiors_textiles'
            )
    );
});

function home_and_interiors_textiles() {

    $textiles_arr = array();
    $textiles = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'product_cat' => 'textiles'
    );
    $loop = new WP_Query($textiles);
    while ($loop->have_posts()) : $loop->the_post();
        global $product;
        $product_id = get_the_ID();
        $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => $productimage[0],
            'title' => get_the_title(),
        );
        array_push($textiles_arr, $data);
    endwhile;
    wp_reset_query();

    $response['status'] = 'success';
    $response['data'] = $textiles_arr;
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-and-interiors/personalised-home/', array(
        'methods' => 'POST',
        'callback' => 'home_and_interiors_personalised_home'
            )
    );
});

function home_and_interiors_personalised_home() {

    $personalised_home_arr = array();
    $personalised_home = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'product_cat' => 'personalised-home'
    );

    $loop = new WP_Query($personalised_home);
    while ($loop->have_posts()) : $loop->the_post();
        global $product;
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => woocommerce_get_product_thumbnail(),
            'title' => get_the_title(),
        );
        array_push($personalised_home_arr, $data);
    endwhile;

    wp_reset_query();
    if ($personalised_home_arr) {
        $response['status'] = 'success';
        $response['data'] = $personalised_home_arr;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/home-and-interiors/art/wooden-art/', array(
        'methods' => 'POST',
        'callback' => 'home_and_interiors_wooden_art'
            )
    );
});

function home_and_interiors_wooden_art() {
    $wooden_art_arr = array();
    $wooden_art = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'product_cat' => 'wooden-art'
    );

    $loop = new WP_Query($wooden_art);
    while ($loop->have_posts()) : $loop->the_post();
        global $product;
        $data = array(
            'id' => get_the_ID(),
            'link' => get_permalink(),
            'img' => woocommerce_get_product_thumbnail(),
            'title' => get_the_title(),
        );
        array_push($wooden_art_arr, $data);
    endwhile;
    wp_reset_query();
    $response['status'] = 'success';
    $response['data'] = $wooden_art_arr;
    return $response;
    exit;
    die;
}

// Removed Archived Filter and Button
add_filter('wcfm_is_allow_archive_product', '__return_false');
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/cards-stationery/', array(
        'methods' => 'POST',
        'callback' => 'cards_and_stationery'
            )
    );
});

function cards_and_stationery() {

    $stationery_sliderID_one = 1590;
    $stationery_sliderID_two = 795;
    $stationery_sliderID_three = 70059;

    $heading = 'Cards & Stationery';
    $description = 'Handmade cards and stationery made in the UK,  especially for you by our talented designers.
    Looking for a perfectly thoughtful handmade card? Maybe a new notebook for some list writing or planning.  Or a sketchbook perhaps…
    Stationery makes for a perfect gift idea too!';

    $Birthday_image = get_site_url() . '/wp-content/uploads/2021/01/Cars-50-Turquoise.jpg';
    $Birthday_link = 'birthday-cards-by-occassion';
    $Birthday_title = 'Birthday Cards';

    $thank_you_image = get_site_url() . '/wp-content/uploads/2020/06/DSCF0017.jpg';
    $thank_you_link = 'thank-you-cards';
    $thank_you_title = 'Thank you Cards';

    $just_becaus_image = get_site_url() . '/wp-content/uploads/2020/06/1-17.jpg';
    $just_becaus_link = 'any-occasion';
    $just_because_title = 'Just Because Cards';

    $Birthday[] = array("title" => $Birthday_title, "image" => $Birthday_image, "slug" => $Birthday_link);
    $thank_you[] = array("title" => $thank_you_title, "image" => $thank_you_image, "slug" => $thank_you_link);
    $just_because[] = array("title" => $just_because_title, "image" => $just_becaus_image, "slug" => $just_becaus_link);
    $cards_and_stationery = array_merge($Birthday, $thank_you, $just_because);

    $cards_and_stationery_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $cards_and_stationery,
    );
    $Cards_for_occasions_Data = array();
    $Cards_for_occasions_slider = get_post_meta($stationery_sliderID_one, 'sp_wpcp_upload_options', true);
    $Cards_for_occasions_Title = get_post_field('post_title', $stationery_sliderID_one);
    $Cards_for_occasions_imageid = $Cards_for_occasions_slider['wpcp_gallery'];
    $Cards_for_occasions_first_section = "Cards for occasions";

    if (!empty($Cards_for_occasions_imageid) && !empty($Cards_for_occasions_Title)) {

        $ArrayOfYour_Cards_for_occasions_imageid = explode(',', $Cards_for_occasions_imageid);

        foreach ($ArrayOfYour_Cards_for_occasions_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                if ($sliderurl) {
                    $RemoveAnchor = new SimpleXMLElement($sliderurl);
                    $stringinlink = strval($RemoveAnchor['href']);
                    $slug_in_array = explode("/", $stringinlink, 10);
                    $get_existing_values = array_values(array_filter($slug_in_array));
                    $FinalNameOfCat = end($get_existing_values);
                }
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Cards_for_occasions_first_section,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Cards_for_occasions_Title,
            );
            array_push($Cards_for_occasions_Data, $data);
        }
    }

    $Wedding_Stationery_slider = get_post_meta($stationery_sliderID_two, 'sp_wpcp_upload_options', true);
    $Wedding_Stationery_Title = get_post_field('post_title', $stationery_sliderID_two);
    $Wedding_Stationery_imageid = $Wedding_Stationery_slider['wpcp_gallery'];
    $Wedding_Stationery_data = [];
    $Wedding_Stationery_second_section = "Wedding Stationery";

    if (!empty($Wedding_Stationery_imageid) && !empty($Wedding_Stationery_Title)) {

        $ArrayOf_Wedding_Stationery_imageid = explode(',', $Wedding_Stationery_imageid);

        foreach ($ArrayOf_Wedding_Stationery_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Wedding_Stationery_second_section,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Wedding_Stationery_Title,
            );
            array_push($Wedding_Stationery_data, $data);
        }
    }
    $notebooks_and_sketchbooks_value = get_post_meta($stationery_sliderID_three, 'sp_wpcp_upload_options', true);
    $notebooks_and_sketchbooks_Title = get_post_field('post_title', $stationery_sliderID_three);
    $notebooks_and_sketchbooks_data = [];
    $notebooks_and_sketchbooks_third_section = "Notebooks & Sketchbooks";

    if (!empty($notebooks_and_sketchbooks_Title)) {

        $valueid = $notebooks_and_sketchbooks_value['wpcp_taxonomy_terms'][0];
        $term = get_term($valueid, 'product_cat');

        $notebooks_and_sketchbooks = array(
            'post_type' => 'product',
            'posts_per_page' => 10,
            'product_cat' => $term->slug,
        );
        $loop = new WP_Query($notebooks_and_sketchbooks);
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $notebooks_and_sketchbooks_Title,
                'section_heading' => $notebooks_and_sketchbooks_third_section,
            );

            array_push($notebooks_and_sketchbooks_data, $data);
        endwhile;
        wp_reset_query();
    }

    if ($cards_and_stationery_Section_one && $Cards_for_occasions_Data && $Wedding_Stationery_data && $notebooks_and_sketchbooks_data) {
        $response['status'] = 'success';
        $response['section_one'] = $cards_and_stationery_Section_one;
        $response['slider_one'] = $Cards_for_occasions_Data;
        $response['slider_two'] = $Wedding_Stationery_data;
        $response['slider_three'] = $notebooks_and_sketchbooks_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/fashion-and-jewellery/', array(
        'methods' => 'POST',
        'callback' => 'jewellery'
            )
    );
});

function jewellery() {

    $Jewellery_sliderID_one = 1091;
    $Jewellery_sliderID_two = 3550;
    $Jewellery_sliderID_three = 1107;
    $heading = 'jewellery';
    $description = 'Handmade jewellery from British designers just can’t be beaten to look good and feel great!
					If you like to stand out from the crowd and make a style statement then there is no better way than to commission a unique piece that no-one else will be wearing.';

    $unisex_image = get_site_url() . '/wp-content/uploads/2020/06/1-18.jpg';
    $unisex_link = 'unisex-jewellery';
    $unisex_title = 'Unisex Jewellery';

    $women_image = get_site_url() . '/wp-content/uploads/2020/06/Sterling-Silver-Daisy-Bird-Stud-earrings-new-5.jpg';
    $women_link = 'womens-jewellery';
    $women_title = 'Women’s Jewellery';

    $men_image = get_site_url() . '/wp-content/uploads/2020/06/1.jpeg';
    $men_link = 'mens-jewellery';
    $men_title = 'Men’s Jewellery';

    $unisex[] = array("title" => $unisex_title, "image" => $unisex_image, "slug" => $unisex_link);
    $women[] = array("title" => $women_title, "image" => $women_image, "slug" => $women_link);
    $men[] = array("title" => $men_title, "image" => $men_image, "slug" => $men_link);

    $Jewellery = array_merge($unisex, $women, $men);
    $Jewellery_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Jewellery,
    );
    $Jewellery_slider_Data = array();
    $Jewellery_slider = get_post_meta($Jewellery_sliderID_one, 'sp_wpcp_upload_options', true);
    $Jewellery_slider_Title = get_post_field('post_title', $Jewellery_sliderID_one);
    $Jewellery_slider_imageid = $Jewellery_slider['wpcp_gallery'];

    $Slider_one_section_heading = "Jewellery";

    if (!empty($Jewellery_slider_imageid) && !empty($Jewellery_slider_Title)) {

        $ArrayOfYour_Jewellery_slider_imageid = explode(',', $Jewellery_slider_imageid);

        foreach ($ArrayOfYour_Jewellery_slider_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }

            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_one_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Jewellery_slider_Title,
            );
            array_push($Jewellery_slider_Data, $data);
        }
    }

    $Ideas_for_ears = get_post_meta($Jewellery_sliderID_two, 'sp_wpcp_upload_options', true);
    $Ideas_for_ears_Title = get_post_field('post_title', $Jewellery_sliderID_two);
    $slider_two_imageid = $Ideas_for_ears['wpcp_gallery'];
    $Ideas_for_ears_data = [];
    $Slider_two_section_heading = "Ideas for ears!";

    if (!empty($Ideas_for_ears_Title)) {

        $term_id = "";
        $term_id = $Ideas_for_ears['wpcp_taxonomy_terms'][0];
        $term = get_term($term_id, 'product_cat');
        $wooden_art = array(
            'post_type' => 'product',
            'posts_per_page' => 10,
            'orderby' => 'title',
            'order' => 'ASC',
            'product_cat' => $term->slug,
        );

        $loop = new WP_Query($wooden_art);
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $Ideas_for_ears_Title,
                'section_heading' => $Slider_two_section_heading,
            );
            array_push($Ideas_for_ears_data, $data);
        endwhile;
        wp_reset_query();
    }

    $Slider_three = get_post_meta($Jewellery_sliderID_three, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $Jewellery_sliderID_three);
    $Slider_three_data = [];
    $Slider_three_section_heading = "Men’s Jewellery";
    $slider_three_imageid = $Slider_three['wpcp_gallery'];

    if (!empty($slider_three_imageid) && !empty($Slider_three_title)) {

        $ArrayOfYour_slider_three_imageid = explode(',', $slider_three_imageid);

        foreach ($ArrayOfYour_slider_three_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            $data = array();

            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }

            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);

            if (!empty($title) && !empty($catImages)) {

                $data = array(
                    'section_heading' => $Slider_three_section_heading,
                    'catimage' => $catImages,
                    'title' => $title,
                    'slug' => $FinalNameOfCat,
                    'slidername' => $Slider_three_title
                );
            }
            if (count($data) > 0) {
                array_push($Slider_three_data, $data);
            }
        }
    }

    $Slider_four = get_post_meta(3560, 'sp_wpcp_upload_options', true);
    $Slider_four_title = get_post_field('post_title', 3560);
    $Slider_four_data = [];
    $Slider_four_section_heading = "Necklaces & Pendants";

    if (!empty($Slider_four) && !empty($Slider_four_title)) {
        $term_id_Slider_four = "";
        $term_id_Slider_four = $Slider_four['wpcp_taxonomy_terms'][0];
        $term = get_term($term_id_Slider_four, 'product_cat');
        $wooden_art = array(
            'post_type' => 'product',
            'posts_per_page' => 10,
            'product_cat' => $term->slug,
            'orderby' => 'date',
            'order' => 'ASC',
        );
        $loop = new WP_Query($wooden_art);
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $Slider_four_title,
                'section_heading' => $Slider_four_section_heading,
            );

            array_push($Slider_four_data, $data);
        endwhile;
        wp_reset_query();
    }

    if ($Jewellery_Section_one && $Jewellery_slider_Data && $Ideas_for_ears_data && $Slider_three_data && $Slider_four_data) {
        $response['status'] = 'success';
        $response['section_one'] = $Jewellery_Section_one;
        $response['slider_one'] = $Jewellery_slider_Data;
        $response['slider_two'] = $Ideas_for_ears_data;
        $response['slider_three'] = $Slider_three_data;
        $response['slider_four'] = $Slider_four_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/baby-gifts-child-gifts/', array(
        'methods' => 'POST',
        'callback' => 'baby_and_child_gifts'
            )
    );
});

function baby_and_child_gifts() {

    $sliderID_one = 1677;
    $sliderID_two = 70064;
    $sliderID_three = 67580;

    $heading = 'Baby & Child Gifts';
    $description = 'Handmade gift ideas for children and babies…
						From handcrafted socks to prints with their name on, we have so much that will be sure to make their little faces light up!
						Created by the best of creative British small businesses.';

    $first_image = get_site_url() . '/wp-content/uploads/2019/09/baby-gifts.jpg';
    $first_link = 'gifts-for-baby';
    $first_title = 'Baby Gifts';

    $second_image = get_site_url() . '/wp-content/uploads/2019/08/brothers-457234_1920.jpg';
    $second_link = 'blankets';
    $second_title = 'Blankets';

    $third_image = get_site_url() . '/wp-content/uploads/2020/06/1-14.jpg';
    $third_link = 'childrens-room-and-nursery';
    $third_title = 'Baby Room';
    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $baby_and_child_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );
    $Slider_one_Data = array();
    $slider_one = get_post_meta($sliderID_one, 'sp_wpcp_upload_options', true);
    $slider_one_title = get_post_field('post_title', $sliderID_one);
    $slider_one_imageid = $slider_one['wpcp_gallery'];
    $Slider_one_section_heading = "New Born Gifts";

    if (!empty($slider_one_imageid) && !empty($slider_one_title)) {

        $ArrayOfYour_slider_one_imageid = explode(',', $slider_one_imageid);

        foreach ($ArrayOfYour_slider_one_imageid as $imageID) {

            $data = [];
            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_one_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Slider_one_section_heading,
            );
            if (count($data) > 0) {
                array_push($Slider_one_Data, $data);
            }
        }
    }
    $Slider_two = get_post_meta($sliderID_two, 'sp_wpcp_upload_options', true);
    $Slider_two_Title = get_post_field('post_title', $sliderID_two);
    $Slider_two_Data = [];
    $Slider_two_section_heading = "Cards for small & big kids";

    if (!empty($Slider_two_Title)) {

        $valueid = $Slider_two['wpcp_taxonomy_terms'][0];
        $term = get_term($valueid, 'product_cat');

        $Slider_two_arr = array(
            'post_type' => 'product',
            'posts_per_page' => 10,
            'product_cat' => $term->slug,
        );
        $loop = new WP_Query($Slider_two_arr);
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $data = [];
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');

            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $Slider_two_Title,
                'section_heading' => $Slider_two_section_heading,
            );
            if (count($data) > 0) {
                array_push($Slider_two_Data, $data);
            }
        endwhile;
        wp_reset_query();
    }

    $Slider_three = get_post_meta($sliderID_three, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $sliderID_three);
    $Slider_three_data = [];
    $Slider_three_section_heading = "Children & Baby Clothes";

    if (!empty($Slider_three_title)) {

        $wpcp_specific_product = $Slider_three['wpcp_specific_product'];

        foreach ($wpcp_specific_product as $key => $valueid) {
            $data = [];
            $title = get_the_title($valueid);
            if ($title) {
                $regular_price = get_post_meta($valueid, '_regular_price', true);
                $sale_price = get_post_meta($valueid, '_sale_price', true);
                $url = get_permalink($valueid);
                $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($valueid), 'single-post-thumbnail');
                $data = array(
                    'id' => $valueid,
                    'link' => $url,
                    'catimage' => $productimage[0],
                    'title' => $title,
                    'regular_price' => $regular_price,
                    'sale_price' => $sale_price,
                    'slider_title' => $Slider_three_title,
                    'section_heading' => $Slider_three_section_heading,
                );
            }
            if (count($data) > 0) {
                array_push($Slider_three_data, $data);
            }
        }
    }

    if ($baby_and_child_gifts_Section_one && $Slider_one_Data && $Slider_two_Data && $Slider_three_data) {
        $response['status'] = 'success';
        $response['section_one'] = $baby_and_child_gifts_Section_one;
        $response['slider_one'] = $Slider_one_Data;
        $response['slider_two'] = $Slider_two_Data;
        $response['slider_three'] = $Slider_three_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/weddings/', array(
        'methods' => 'POST',
        'callback' => 'weddings'
            )
    );
});

function weddings() {

    $sliderID_one = 785;
    $sliderID_two = 795;
    $sliderID_three = 796;

    $heading = 'Weddings';
    $description = 'We have everything handmade here to make your Wedding perfect – truly, everything you have thought of and a few things you won’t have! From stationery to accessories – all handmade in the UK by our clever artisans';

    $first_image = get_site_url() . '/wp-content/uploads/2019/09/bridal-headpiece.jpg';
    $first_link = 'bridal-hair-accessories';
    $first_title = 'Bridal hairpieces';

    $second_image = get_site_url() . '/wp-content/uploads/2019/08/brothers-457234_1920.jpg';
    $second_link = 'gifts-for-the-groom';
    $second_title = 'Gifts for the groom';

    $third_image = get_site_url() . '/wp-content/uploads/2019/07/make-it-happen-1024x682.jpeg';
    $third_link = 'invitations';
    $third_title = 'Invitations';
    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);

    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);
    $baby_and_child_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );
    $Slider_one_Data = array();
    $slider_one = get_post_meta($sliderID_one, 'sp_wpcp_upload_options', true);
    $slider_one_title = get_post_field('post_title', $sliderID_one);
    $slider_one_imageid = $slider_one['wpcp_gallery'];
    $Slider_one_section_heading = "Your wedding, your style";

    if (!empty($slider_one_imageid) && !empty($slider_one_title)) {

        $ArrayOfYour_slider_one_imageid = explode(',', $slider_one_imageid);

        foreach ($ArrayOfYour_slider_one_imageid as $imageID) {
            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_one_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $slider_one_title,
            );
            array_push($Slider_one_Data, $data);
        }
    }
    $Slider_two = get_post_meta($sliderID_two, 'sp_wpcp_upload_options', true);
    $Slider_two_Title = get_post_field('post_title', $sliderID_two);
    $Slider_two_Data = [];
    $slider_two_imageid = $Slider_two['wpcp_gallery'];
    $Slider_two_section_heading = "Wedding Stationery";

    if (!empty($slider_two_imageid) && !empty($Slider_two_Title)) {

        $ArrayOfYour_slider_two_imageid = explode(',', $slider_two_imageid);

        foreach ($ArrayOfYour_slider_two_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_two_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Slider_two_Title,
            );
            array_push($Slider_two_Data, $data);
        }
    }

    $Slider_three = get_post_meta($sliderID_three, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $sliderID_three);
    $Slider_three_data = [];
    $Slider_three_section_heading = "Wedding gifts";
    $slider_three_imageid = $Slider_three['wpcp_gallery'];

    if (!empty($slider_three_imageid) && !empty($Slider_three_title)) {

        $ArrayOfYour_slider_three_imageid = explode(',', $slider_three_imageid);

        foreach ($ArrayOfYour_slider_three_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_three_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Slider_three_title,
            );
            array_push($Slider_three_data, $data);
        }
    }

    if ($baby_and_child_gifts_Section_one && $Slider_one_Data && $Slider_two_Data && $Slider_three_data) {
        $response['status'] = 'success';
        $response['section_one'] = $baby_and_child_gifts_Section_one;
        $response['slider_one'] = $Slider_one_Data;
        $response['slider_two'] = $Slider_two_Data;
        $response['slider_three'] = $Slider_three_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/fashion/', array(
        'methods' => 'POST',
        'callback' => 'fashion'
            )
    );
});

function fashion() {

    $sliderID_one = 55741;
    $sliderID_two = 55405;
    $sliderID_three = 1107;
    $sliderID_four = 55404;

    $heading = 'Fashion Gifts';
    $description = 'British fashion gifts from British designers. Whether it’s a gloriously luxurious poncho, silks or knitwear our artisans make exquisite handmade clothes and ccessories.Stand out from the crowd and make a style statement with handcrafted British Fashion. From crocheted socks, felt hats, to the most gorgeous knitwear for both inside and outside. You will find it all here.';

    $first_image = get_site_url() . '/wp-content/uploads/2020/06/IMG_1246-scaled-e1591721062503.jpeg';
    $first_link = 'womens-fashion';
    $first_title = 'Women’s Fashion';

    $second_image = get_site_url() . '/wp-content/uploads/2020/11/shutterstock_1554894680-1.jpg';
    $second_link = 'clothing-outfits';
    $second_title = 'Babies & Children';

    $third_image = get_site_url() . '/wp-content/uploads/2020/06/1-1.jpeg';
    $third_link = 'mens-fashion';
    $third_title = 'Men’s Fashion';

    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $fasion_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );
    $Slider_one_Data = array();
    $slider_one = get_post_meta($sliderID_one, 'sp_wpcp_upload_options', true);
    $slider_one_title = get_post_field('post_title', $sliderID_one);
    $slider_one_imageid = $slider_one['wpcp_gallery'];
    $Slider_one_section_heading = "Best of British fashion";

    if (!empty($slider_one_imageid) && !empty($slider_one_title)) {

        $ArrayOfYour_slider_one_imageid = explode(',', $slider_one_imageid);

        foreach ($ArrayOfYour_slider_one_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_one_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $slider_one_title,
            );
            array_push($Slider_one_Data, $data);
        }
    }
    $Slider_two = get_post_meta($sliderID_two, 'sp_wpcp_upload_options', true);
    $Slider_two_Title = get_post_field('post_title', $sliderID_two);
    $Slider_two_Data = [];
    $Slider_two_section_heading = "Headwear with style";

    if (!empty($Slider_two_Title)) {

        $valueid = $Slider_two['wpcp_taxonomy_terms'][0];
        $term = get_term($valueid, 'product_cat');

        $Slider_two_arr = array(
            'post_type' => 'product',
            'posts_per_page' => 10,
            'product_cat' => $term->slug,
        );
        $loop = new WP_Query($Slider_two_arr);
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $Slider_two_Title,
                'section_heading' => $Slider_two_section_heading,
            );
            array_push($Slider_two_Data, $data);
        endwhile;
        wp_reset_query();
    }

    $Slider_three = get_post_meta($sliderID_three, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $sliderID_three);
    $Slider_three_data = [];
    $Slider_three_section_heading = "Men’s Accessories";
    $slider_three_imageid = $Slider_three['wpcp_gallery'];

    if (!empty($slider_three_imageid) && !empty($Slider_three_title)) {

        $ArrayOfYour_slider_three_imageid = explode(',', $slider_three_imageid);

        foreach ($ArrayOfYour_slider_three_imageid as $imageID) {
            $data = array();
            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            if (!empty($title) && !empty($catImages)) {
                $data = array(
                    'section_heading' => $Slider_three_section_heading,
                    'catimage' => $catImages,
                    'title' => $title,
                    'slug' => $FinalNameOfCat,
                    'slidername' => $Slider_three_title,
                );
            }
            if (count($data) > 0) {
                array_push($Slider_three_data, $data);
            }
        }
    }
    $Slider_four = get_post_meta($sliderID_four, 'sp_wpcp_upload_options', true);
    $Slider_four_title = get_post_field('post_title', $sliderID_four);
    $Slider_four_data = [];
    $Slider_four_section_heading = "Bags & Totes";

    if (!empty($Slider_four) && !empty($Slider_four_title)) {

        $valueid = $Slider_four['wpcp_taxonomy_terms'][0];
        $term = get_term($valueid, 'product_cat');

        $Slider_two_arr = array(
            'post_type' => 'product',
            'posts_per_page' => 10,
            'product_cat' => $term->slug,
        );
        $loop = new WP_Query($Slider_two_arr);

        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $product_id = get_the_ID();

            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');

            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $Slider_four_title,
                'section_heading' => $Slider_four_section_heading,
            );
            array_push($Slider_four_data, $data);
        endwhile;
        wp_reset_query();
    }

    if ($fasion_Section_one && $Slider_one_Data && $Slider_two_Data && $Slider_three_data && $Slider_four_data) {
        $response['status'] = 'success';
        $response['section_one'] = $fasion_Section_one;
        $response['slider_one'] = $Slider_one_Data;
        $response['slider_two'] = $Slider_two_Data;
        $response['slider_three'] = $Slider_three_data;
        $response['slider_four'] = $Slider_four_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/art/', array(
        'methods' => 'POST',
        'callback' => 'art'
            )
    );
});

function art() {

    $sliderID_one = 1635;
    $sliderID_two = 226472; //161432;
    $sliderID_three = 1627;

    $heading = 'Art';
    $description = 'Art to add to your home decor or to give as gifts. We have all manner of styles available, all created in the UK my our small independent creatives.';
    $first_image = get_site_url() . '/wp-content/uploads/2020/07/shutterstock_394541995-1-1.jpg';
    $first_link = 'wooden-art';
    $first_title = 'Wooden Art';
    $second_image = get_site_url() . '/wp-content/uploads/2019/08/mosaic-200864_1920.jpg';
    $second_link = 'ceramic-art';
    $second_title = 'Ceramic Art';
    $third_image = get_site_url() . '/wp-content/uploads/2019/08/vases-3880641_1920.jpg';
    $third_link = 'invitations';
    $third_title = 'Glass Art';
    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);
    $baby_and_child_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );
    $Slider_one_Data = array();
    $slider_one = get_post_meta($sliderID_one, 'sp_wpcp_upload_options', true);
    $slider_one_title = get_post_field('post_title', $sliderID_one);
    $slider_one_imageid = $slider_one['wpcp_gallery'];
    $Slider_one_section_heading = "Art is all around us";

    if (!empty($slider_one_imageid) && !empty($slider_one_title)) {

        $ArrayOfYour_slider_one_imageid = explode(',', $slider_one_imageid);

        foreach ($ArrayOfYour_slider_one_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_one_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slidername' => $slider_one_title,
                'slug' => $FinalNameOfCat,
            );
            array_push($Slider_one_Data, $data);
        }
    }

    $Slider_two = get_post_meta($sliderID_two, 'sp_wpcp_upload_options', true);
    $Slider_two_Title = get_post_field('post_title', $sliderID_two);
    $Slider_two_Data = [];
    $slider_two_imageid = $Slider_two['wpcp_gallery'];
    $Slider_two_section_heading = "Art for the home";

    if (!empty($slider_two_imageid) && !empty($Slider_two_Title)) {

        $ArrayOfYour_slider_two_imageid = explode(',', $slider_two_imageid);

        foreach ($ArrayOfYour_slider_two_imageid as $imageID) {

            $data = array();
            $sliderurl = get_post_field('post_excerpt', $imageID);
            $stringType = isAnchor($sliderurl);

            if (!empty($sliderurl) && $stringType == true) {

                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
                preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
                $title = $matches[1][0];
                $catImages = get_post_field('guid', $imageID);
                $data = array(
                    'section_heading' => $Slider_two_section_heading,
                    'catimage' => $catImages,
                    'title' => $title,
                    'slug' => $FinalNameOfCat,
                    'slidername' => $Slider_two_Title,
                );
                if (count($data) > 0) {
                    array_push($Slider_two_Data, $data);
                }
            }
        }
    }

    $Slider_three = get_post_meta($sliderID_three, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $sliderID_three);
    $Slider_three_data = [];
    $Slider_three_section_heading = "Art as gifts";
    $slider_three_imageid = $Slider_three['wpcp_gallery'];

    if (!empty($slider_three_imageid) && !empty($Slider_three_title)) {

        $ArrayOfYour_slider_three_imageid = explode(',', $slider_three_imageid);

        foreach ($ArrayOfYour_slider_three_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_three_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Slider_three_title,
            );
            array_push($Slider_three_data, $data);
        }
    }
    if ($baby_and_child_gifts_Section_one && $Slider_one_Data && $Slider_two_Data && $Slider_three_data) {
        $response['status'] = 'success';
        $response['section_one'] = $baby_and_child_gifts_Section_one;
        $response['slider_one'] = $Slider_one_Data;
        $response['slider_two'] = $Slider_two_Data;
        $response['slider_three'] = $Slider_three_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/gifts-occasions/', array(
        'methods' => 'POST',
        'callback' => 'gifts_occasions'
            )
    );
});

function gifts_occasions() {

    $home_page_gifts_occasions_slider_one = 204827;
    $home_page_gifts_occasions_slider_two = 239548; //1549;
    $home_page_gifts_occasions_three = 204794;
    $home_page_gifts_occasions_four = 3552;

    $heading = 'Gifts & Occasions';
    $description = 'Discover our beautiful handmade gifts and become the most thoughtful gift giver in an instant.Personalised, bespoke, with a great range of ideas for all ages.All made in Britain with care and attention to detail by our team of talented artisan makers';

    $first_image = get_site_url() . '/wp-content/uploads/2020/09/1-11.jpg';
    $first_link = 'for-him';
    $first_title = 'Gifts for Him';

    $second_image = get_site_url() . '/wp-content/uploads/2020/06/make-up-jars-dragonfly-tbch.jpg';
    $second_link = 'gifts-for-her';
    $second_title = 'Gifts for Her';

    $third_image = get_site_url() . '/wp-content/uploads/2020/06/IMG_0361.jpg';
    $third_link = 'gifts-for-children';
    $third_title = 'Gifts for Children';
    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);

    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $baby_and_child_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge,
    );
    $Slider_one_Data = array();
    $slider_one = get_post_meta($home_page_gifts_occasions_slider_one, 'sp_wpcp_upload_options', true);
    $slider_one_title = get_post_field('post_title', $home_page_gifts_occasions_slider_one);
    $slider_one_imageid = $slider_one['wpcp_gallery'];
    $Slider_one_section_heading = "Great Gift Ideas";

    if (!empty($slider_one_imageid) && !empty($slider_one_title)) {

        $ArrayOfYour_slider_one_imageid = explode(',', $slider_one_imageid);

        foreach ($ArrayOfYour_slider_one_imageid as $imageID) {
            $data = array();
            $sliderurl = get_post_field('post_excerpt', $imageID);

            if ($sliderurl) {
                $pattern = '/"[a-zA-Z0-9.\/\-\?\&]*"/';
                preg_match($pattern, $sliderurl, $matches);
                $stringinlink = strval($matches[0]);
                $trimmedProduct_category = str_replace("/product-category/", '', $stringinlink);
                $trimmedSubCat = str_replace("cards-stationery/", '', $trimmedProduct_category);
                $trimmedart = str_replace("art/", '', $trimmedSubCat);
                $trimmedfasion_jewellery = str_replace("fashion-jewellery/", '', $trimmedart);
                $trimmedGiftsByInterest = str_replace("gifts-by-interest/", '', $trimmedfasion_jewellery);
                $trimmedGifts = str_replace("gifts/", '', $trimmedGiftsByInterest);
                $trimmedRecipient = str_replace("by-recipient/", '', $trimmedGifts);
                $trimmedChildren = str_replace("gifts-for-children/", '', $trimmedRecipient);
                $trimmedClothingType = str_replace("clothing-type/", '', $trimmedChildren);
                $FinalNameOfCat = str_replace('/', '', $trimmedClothingType);
                $slug = trim($FinalNameOfCat, '"');
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];

            if (!empty($title) && !empty($slug)) {
                $catImages = get_post_field('guid', $imageID);
                $data = array(
                    'section_heading' => $Slider_one_section_heading,
                    'catimage' => $catImages,
                    'title' => $title,
                    'slug' => $slug,
                    'slidername' => $slider_one_title,
                );
            }
            if (count($data) > 0) {
                array_push($Slider_one_Data, $data);
            }
        }// end foreach
    }
    $Slider_two = get_post_meta($home_page_gifts_occasions_slider_two, 'sp_wpcp_upload_options', true);
    $Slider_two_Title = get_post_field('post_title', $home_page_gifts_occasions_slider_two);
    $Slider_two_Data = [];
    $slider_two_imageid = $Slider_two['wpcp_gallery'];
    $Slider_two_section_heading = "For Loved Ones";

    if (!empty($slider_two_imageid) && !empty($Slider_two_Title)) {

        $ArrayOfYour_slider_two_imageid = explode(',', $slider_two_imageid);

        foreach ($ArrayOfYour_slider_two_imageid as $imageID) {
            $data = array();
            $sliderurl = get_post_field('post_excerpt', $imageID);

            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];

            if (!empty($title) && !empty($FinalNameOfCat)) {
                $catImages = get_post_field('guid', $imageID);
                $data = array(
                    'section_heading' => $Slider_two_section_heading,
                    'catimage' => $catImages,
                    'title' => $title,
                    'slug' => $FinalNameOfCat,
                    'slidername' => $Slider_two_Title,
                );
            }
            if (count($data) > 0) {
                array_push($Slider_two_Data, $data);
            }
        }// end foreach
    }

    $Slider_three = get_post_meta($home_page_gifts_occasions_three, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $home_page_gifts_occasions_three);
    $Slider_three_data = [];

    $Slider_three_section_heading = "Personalised Gifts";

    $slider_three_imageid = $Slider_three['wpcp_gallery'];

    if (!empty($slider_three_imageid) && !empty($Slider_three_title)) {

        $ArrayOfYour_slider_three_imageid = explode(',', $slider_three_imageid);

        foreach ($ArrayOfYour_slider_three_imageid as $imageID) {
            $data = array();
            $sliderurl = get_post_field('post_excerpt', $imageID);

            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];

            if (!empty($title) && !empty($FinalNameOfCat)) {
                $catImages = get_post_field('guid', $imageID);
                $data = array(
                    'section_heading' => $Slider_three_section_heading,
                    'catimage' => $catImages,
                    'title' => $title,
                    'slug' => $FinalNameOfCat,
                    'slidername' => $Slider_three_title,
                );
                if (count($data) > 0) {
                    array_push($Slider_three_data, $data);
                }
            }
        }// end foreach
    }
    $Slider_four = get_post_meta($home_page_gifts_occasions_four, 'sp_wpcp_upload_options', true);
    $Slider_four_title = get_post_field('post_title', $home_page_gifts_occasions_four);
    $Slider_four_data = [];
    $Slider_four_section_heading = "New In";
    if (!empty($Slider_four) && !empty($Slider_four_title)) {

        foreach ($Slider_four['wpcp_specific_product'] as $key => $valueid) {
            global $product;
            $data = array();
            $_product = wc_get_product($valueid);
            $title = get_the_title($valueid);
            $regular_price = get_post_meta($valueid, '_regular_price', true);
            $sale_price = get_post_meta($valueid, '_sale_price', true);
            $url = get_permalink($valueid);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($valueid), 'single-post-thumbnail');

            $data = array(
                'id' => $valueid,
                'link' => $url,
                'catimage' => $productimage[0],
                'title' => $title,
                'regular_price' => $regular_price,
                'sale_price' => $sale_price,
                'slider_title' => $Slider_four_title,
                'section_heading' => $Slider_four_section_heading,
            );
            if (count($data) > 0) {
                array_push($Slider_four_data, $data);
            }
        }
    }
    if ($baby_and_child_gifts_Section_one && $Slider_one_Data && $Slider_two_Data && $Slider_three_data && $Slider_four_data) {
        $response['status'] = 'success';
        $response['section_one'] = $baby_and_child_gifts_Section_one;
        $response['slider_one'] = $Slider_one_Data;
        $response['slider_two'] = $Slider_two_Data;
        $response['slider_three'] = $Slider_three_data;
        $response['slider_four'] = $Slider_four_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    die;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/letterbox-gifts/', array(
        'methods' => 'POST',
        'callback' => 'letterbox_gifts'
            )
    );
});

function letterbox_gifts() {

    $home_page_letterbox_slider_one = 84447;
    $home_page_letterbox_slider_two = 84451;
    $home_page_letterbox_slider_three = 6407;

    $heading = 'Letter Box Gift Ideas';
    $description = 'If you need to know that your purchased item will just pop through the letterbox then this is the place to be.Search through the fabulous ideas by our artisan sellers and put a smile on someone’s face.Think of it like sending a hug in a box.Here is some inspiration or you can shop the whole collection here!';

    $letterbox_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
    );
    $Slider_one_Data = array();
    $slider_one = get_post_meta($home_page_letterbox_slider_one, 'sp_wpcp_upload_options', true);
    $slider_one_title = get_post_field('post_title', $home_page_letterbox_slider_one);
    $slider_one_imageid = $slider_one['wpcp_gallery'];
    $Slider_one_section_heading = "Send a Smile";

    if (!empty($slider_one) && !empty($slider_one_title)) {

        $slider_one_term_cat = $slider_one['wpcp_taxonomy_terms'][0];
        $slider_one_term_cat = get_term($slider_one_term_cat, 'product_cat');
        //print_r($slider_one_term_cat->term_id); die;
        $slider_one_arr = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            //'posts_per_page' => 10,
            'orderby' => 'post_date',
            'order' => 'ASC',
            //'product_cat' => $slider_one_term_cat->slug,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                    'terms' => $slider_one_term_cat->term_id,
                    'operator' => 'IN' // Possible values are 'IN', 
                )
            ),
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'total_sales',
                    'value' => '0',
                    'compare' => '='
                )
            ),
        );
        $loop = new WP_Query($slider_one_arr);
        //echo "<pre>"; print_r($loop); die;
        while ($loop->have_posts()) : $loop->the_post();
            $data = array();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $Slider_one_title,
                'section_heading' => $Slider_one_section_heading,
            );
            if (count($data) > 0) {
                array_push($Slider_one_Data, $data);
            }
        endwhile;
        wp_reset_query();
    }

    $Slider_two = get_post_meta($home_page_letterbox_slider_two, 'sp_wpcp_upload_options', true);
    $Slider_two_Title = get_post_field('post_title', $home_page_letterbox_slider_two);
    $Slider_two_Data = [];
    $Slider_two_section_heading = "Just Because Cards";

    if (!empty($Slider_two) && !empty($Slider_two_Title)) {

        $valueid = $Slider_two['wpcp_taxonomy_terms'][0];
        $term = get_term($valueid, 'product_cat');
        $Slider_two_arr = array(
            'post_type' => 'product',
            'orderby' => 'post_date',
            'order' => 'ASC',
            'product_cat' => $term->slug,
        );
        $loop = new WP_Query($Slider_two_arr);
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $Slider_four_title,
                'section_heading' => $Slider_two_section_heading,
            );
            array_push($Slider_two_Data, $data);
        endwhile;
        wp_reset_query();
    }
    $Slider_three = get_post_meta($home_page_letterbox_slider_three, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $home_page_letterbox_slider_three);
    $Slider_three_data = [];
    $Slider_three_section_heading = "Small but beautiful gifts";

    if (!empty($Slider_three) && !empty($Slider_three_title)) {

        $Slider_three_term_cat = $Slider_three['wpcp_taxonomy_terms'][0];
        $Slider_three_term_cat = get_term($Slider_three_term_cat, 'product_cat');
        $Slider_three_arr = array(
            'post_type' => 'product',
            'posts_per_page' => 10,
            'product_cat' => $Slider_three_term_cat,
        );
        $loop = new WP_Query($Slider_three_arr);
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'catimage' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
                'slider_title' => $Slider_three_title,
                'section_heading' => $Slider_three_section_heading,
            );
            array_push($Slider_three_data, $data);
        endwhile;
        wp_reset_query();
    }
    if ($letterbox_gifts_Section_one && $Slider_one_Data && $Slider_two_Data && $Slider_three_data) {
        $response['status'] = 'success';
        $response['section_one'] = $letterbox_gifts_Section_one;
        $response['slider_one'] = $Slider_one_Data;
        $response['slider_two'] = $Slider_two_Data;
        $response['slider_three'] = $Slider_three_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/christmas/', array(
        'methods' => 'POST',
        'callback' => 'christmas'
            )
    );
});

function christmas() {

    $slider_one_id = 201681;
    $slider_two_id = 176456;
    $slider_three_id = 176600;

    $heading = 'Christmas Gifts, Cards and Decorations';
    $description = 'Christmas gifts are so important to get right. Christmas comes but once a year, so go on – make the most of it with gorgeous handmade designs to make the most wonderful time of the year that bit more special. Thoughtfully chosen gifts and a beautifully decorated home can make loved ones feel cherished. Looking for something unique for your mum? A special Christmas stocking to be passed down the generations or some beautiful handcrafted decorations? You are in the right place! Why not add a teeny tiny something to your basket for yourself this Christmas! Whatever you choose may your Christmas be blessed with joy and cheer!';

    $first_image = get_site_url() . '/wp-content/uploads/2020/09/shutterstock_351220193-1.jpg';
    $first_link = 'for-him';
    $first_title = 'GIFTS FOR HIM';

    $second_image = get_site_url() . '/wp-content/uploads/2020/06/make-up-jars-dragonfly-tbch.jpg';
    $second_link = 'gifts-for-her';
    $second_title = 'GIFTS FOR HER';

    $third_image = get_site_url() . '/wp-content/uploads/2020/09/shutterstock_520231807-1.jpg';
    $third_link = 'gifts-for-children';
    $third_title = 'GIFTS FOR CHILDREN';

    $Section_one_array_one[] = array("title" => $first_title, "image" => $first_image, "slug" => $first_link);
    $Section_one_array_two[] = array("title" => $second_title, "image" => $second_image, "slug" => $second_link);
    $Section_one_array_three[] = array("title" => $third_title, "image" => $third_image, "slug" => $third_link);
    $Section_one_array_merge = array_merge($Section_one_array_one, $Section_one_array_two, $Section_one_array_three);

    $letterbox_gifts_Section_one = array(
        'heading' => $heading,
        'description' => $description,
        'subheading' => $Section_one_array_merge
    );
    $Slider_one_Data = array();
    $slider_one = get_post_meta($slider_one_id, 'sp_wpcp_upload_options', true);
    $slider_one_title = get_post_field('post_title', $slider_one_id);
    $slider_one_imageid = $slider_one['wpcp_gallery'];
    $Slider_one_section_heading = "Gifts Galore";

    if (!empty($slider_one_imageid) && !empty($slider_one_title)) {

        $ArrayOfYour_slider_one_imageid = explode(',', $slider_one_imageid);

        foreach ($ArrayOfYour_slider_one_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $pattern = '/"[a-zA-Z0-9.\/\-\?\&]*"/';
                preg_match($pattern, $sliderurl, $matches);
                $stringinlink = strval($matches[0]);
                $slug = trim($stringinlink, '"');
                $trimmedProduct_gifts = str_replace("/product-category/gifts/", '', $slug);
                $trimmedProduct_category = str_replace("/product-category/", '', $trimmedProduct_gifts);
                $trimmedSomeBrightIdeas = str_replace("gifts/some-bright-ideas/", '', $trimmedProduct_category);
                $trimmedHomeInteriorsGifts = str_replace("home-interiors-gifts/", '', $trimmedSomeBrightIdeas);
                $trimmedSomeByRecipient = str_replace("by-recipient/", '', $trimmedHomeInteriorsGifts);
                $trimmedSomeBrightideas = str_replace("some-bright-ideas/", '', $trimmedSomeByRecipient);
                $trimmedHomeInteriorsGiftByRoom = str_replace("home-interiors-gifthome-by-room/", '', $trimmedSomeBrightideas);
                $FinalNameOfCat = str_replace("/", '', $trimmedHomeInteriorsGiftByRoom);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_one_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $slider_one_title,
            );
            array_push($Slider_one_Data, $data);
        }
    }
    $Slider_two = get_post_meta($slider_two_id, 'sp_wpcp_upload_options', true);
    $Slider_two_Title = get_post_field('post_title', $slider_two_id);
    $Slider_two_Data = [];
    $slider_two_imageid = $Slider_two['wpcp_gallery'];
    $Slider_two_section_heading = "Something rather Special";

    if (!empty($slider_two_imageid) && !empty($Slider_two_Title)) {

        $ArrayOfYour_slider_two_imageid = explode(',', $slider_two_imageid);

        foreach ($ArrayOfYour_slider_two_imageid as $imageID) {

            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $RemoveAnchor = new SimpleXMLElement($sliderurl);
                $stringinlink = strval($RemoveAnchor['href']);
                $slug_in_array = explode("/", $stringinlink, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_two_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Slider_two_Title,
            );
            array_push($Slider_two_Data, $data);
        }
    }
    $Slider_three = get_post_meta($slider_three_id, 'sp_wpcp_upload_options', true);
    $Slider_three_title = get_post_field('post_title', $slider_three_id);
    $Slider_three_data = [];
    $Slider_three_section_heading = "Ideas for everyone!";
    $slider_three_imageid = $Slider_three['wpcp_gallery'];

    if (!empty($slider_three_imageid) && !empty($Slider_three_title)) {

        $ArrayOfYour_slider_three_imageid = explode(',', $slider_three_imageid);

        foreach ($ArrayOfYour_slider_three_imageid as $imageID) {
            $sliderurl = get_post_field('post_excerpt', $imageID);
            if ($sliderurl) {
                $pattern = '/"[a-zA-Z0-9.\/\-\?\&]*"/';
                preg_match($pattern, $sliderurl, $matches);
                $stringinlink = strval($matches[0]);
                $slug = trim($stringinlink, '"');
                $slug_in_array = explode("/", $slug, 10);
                $get_existing_values = array_values(array_filter($slug_in_array));
                $FinalNameOfCat = end($get_existing_values);
            }
            preg_match_all('/<a .*?>(.*?)<\/a>/', $sliderurl, $matches);
            $title = $matches[1][0];
            $catImages = get_post_field('guid', $imageID);
            $data = array(
                'section_heading' => $Slider_three_section_heading,
                'catimage' => $catImages,
                'title' => $title,
                'slug' => $FinalNameOfCat,
                'slidername' => $Slider_three_title,
            );
            array_push($Slider_three_data, $data);
        }
    }
    if ($letterbox_gifts_Section_one && $Slider_one_Data && $Slider_two_Data && $Slider_three_data) {
        $response['status'] = 'success';
        $response['section_one'] = $letterbox_gifts_Section_one;
        $response['slider_one'] = $Slider_one_Data;
        $response['slider_two'] = $Slider_two_Data;
        $response['slider_three'] = $Slider_three_data;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/pages/category-products/', array(
        'methods' => 'GET',
        'callback' => 'category_products'
            )
    );
});

function category_products($request) {

    $cat = $_REQUEST['cat'];
    $page = ($_REQUEST['page'] ? $_REQUEST['page'] : 1);
    $Products_arr = [];
    $get_orderby = $_REQUEST['orderby'];

    switch ($get_orderby) {
        case 'popularity' :
            $orderby = 'total_sales';
            $order = 'DESC';
            break;
        case 'rating' :
            $orderby = '_wc_average_rating';
            $order = 'DESC';
            break;
        case 'date' :
            $orderby = 'post_date';
            $order = 'DESC';
            break;
        case 'price' :
            $orderby = '_price';
            $order = 'ASC';
            break;
        case 'price-desc' :
            $orderby = '_price';
            $order = 'DESC';
            break;
        default :
            $orderby = 'menu_order title';
            $order = 'ASC';
            break;
    }
    if (!empty($cat)) {

        $autumn_home = array(
            'posts_per_page' => 10,
            /* 'tax_query'	 => array(
              'relation'	 => 'AND',
              array(
              'taxonomy' 	=> 'product_cat',
              'field' 	=> 'slug',
              'terms' 	=> $cat
              )
              ), */
            'product_cat' => $cat,
            'post_type' => 'product',
            'orderby' => $orderby,
            'order' => $order,
            'paged' => $page
        );
        $loop = new WP_Query($autumn_home);
        $total = 0;
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $productimage = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
            $data = array(
                'id' => get_the_ID(),
                'link' => get_permalink(),
                'img' => $productimage[0],
                'title' => get_the_title(),
                'regular_price' => $product->get_regular_price(),
                'sale_price' => $product->get_sale_price(),
                'get_price' => $product->get_price(),
            );
            $total += 1;
            array_push($Products_arr, $data);
        endwhile;
    }
    if ($Products_arr) {
        $response['status'] = 'success';
        $response['products'] = $Products_arr;
    } else {
        $response['status'] = 'fail';
        $response['message'] = "something is wrong";
    }
    return $response;
    exit;
}

// WooCommerce: Allow editing of all orders regardless of status
//add_filter ( 'wc_order_is_editable', 'majemedia_add_order_statuses_to_editable' );
//function majemedia_add_order_statuses_to_editable () {
//return TRUE;
//}

/*
 * Create a column. And maybe remove some of the default ones
 * @param array $columns Array of all user table columns {column ID} => {column Name} 
 */
add_filter('manage_users_columns', 'rudr_modify_user_table');

function rudr_modify_user_table($columns) {

    // unset( $columns['posts'] ); // maybe you would like to remove default columns
    $columns['registration_date'] = 'Registration date'; // add new
    return $columns;
}

/*
 * Fill our new column with the registration dates of the users
 * @param string $row_output text/HTML output of a table cell
 * @param string $column_id_attr column ID
 * @param int $user user ID (in fact - table row ID)
 */
add_filter('manage_users_custom_column', 'rudr_modify_user_table_row', 10, 3);

function rudr_modify_user_table_row($row_output, $column_id_attr, $user) {

    $date_format = 'j M, Y H:i';
    switch ($column_id_attr) {
        case 'registration_date' :
            return date($date_format, strtotime(get_the_author_meta('registered', $user)));
            break;
        default:
    }
    return $row_output;
}

/*
 * Make our "Registration date" column sortable
 * @param array $columns Array of all user sortable columns {column ID} => {orderby GET-param} 
 */
add_filter('manage_users_sortable_columns', 'rudr_make_registered_column_sortable');

function rudr_make_registered_column_sortable($columns) {
    return wp_parse_args(array('registration_date' => 'registered'), $columns);
}

///////////////////Filter for Auth Request //////////////
add_filter('jwt_auth_whitelist', function ( $endpoints ) {
    return array(
        '/wp-json/wp/v2/*',
        '/wp-json/yoast/v1/*',
        '/wp-json/wpcom/v2/*',
        '/wp-json/wp/v1/*',
        '/wp-json/jetpack/v4/*',
        '/wp-json/wcfmmp/v1/*',
        '/wp-json/sow/v1/*',
    );
});
/* * ******************* Register ********************* */
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'register', array(
        'methods' => 'POST',
        'callback' => 'create_users'
            )
    );
});

function create_users($data) {
    $response['status'] = 'error';
    //$parts = explode("@", $data['email']);
    $login_name = $parts[0];
    $user_id = wp_insert_user(array(
        'user_login' => $data['username'],
        'user_pass' => $data['password'],
        'user_email' => $data['email']
    ));
    if (!is_wp_error($user_id)) {

        $user_data = get_userdata($user_id);
        $url = site_url() . "/wp-json/jwt-auth/v1/token";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=" . $data['username'] . "&password=" . $data['password']);
        $output = json_decode(curl_exec($ch));
        curl_close($ch);

        $response['status'] = 'success';
        $response['data'] = $output;
        $response['message'] = "User registered successfully";
    } else {
        $response['data'] = [];
        $response['message'] = $user_id->get_error_message();
    }
    return new WP_REST_Response($response);
    exit;
}

/////////////////////// Login API //////////////////////////////////////////
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'user/login', array(
        'methods' => 'POST',
        'callback' => 'login'
    ));
});

function login($data) {

    $username = sanitize_user($data['username']);
    $password = trim($data['password']);
    $url = site_url() . "/wp-json/jwt-auth/v1/token";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/x-www-form-urlencoded"));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=" . $username . "&password=" . $password);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if (curl_errno($ch)) {
        return curl_error($ch);
    }
    $output = json_decode(curl_exec($ch));
    curl_close($ch);

    if (isset($output->data->id)) {
        update_user_meta($output->data->id, "fcm_token", $data['fcm_token']);
        update_user_meta($output->data->id, "device_type", $data['device_type']);
    }
    if ($output->code == 'invalid_email' || $output->code == 'invalid_username') {
        $res['status'] = 'error';
        $res['message'] = 'Try again or check your email/username';
        return new WP_REST_Response($res);
        exit;
    }
    if ($output->code == 'incorrect_password') {
        $res['status'] = 'error';
        $res['message'] = 'The password you entered is incorrect';
        return new WP_REST_Response($res);
        exit;
    }
    $output->status = 'success';
    return $output;
}

/* ============ FORGOT PASSWORD ============ */
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'forgot/password', array(
        'methods' => 'POST',
        'callback' => 'forgot_password'
    ));
});

function forgot_password($data) {
    $login = $data['email'];
    if (empty($login)) {
        $json = array('status' => 'error', 'message' => 'Please enter registered email');
        return $json;
        exit;
    }
    $userdata = get_user_by('email', $login);
    if (empty($userdata)) {
        $userdata = get_user_by('login', $login);
    }
    if (empty($userdata)) {
        $json = array('status' => 'error', 'message' => 'Email not found');
        return $json;
        exit;
    }
    $username = $userdata->user_login;
    $user = new WP_User(intval($userdata->ID));
    $key = get_password_reset_key($user);
    $admin_email = get_option('admin_email');
    $reset_link = site_url() . '/my-account/lost-password/?key=' . $key . '&id=' . $userdata->ID;
    $mail['subject'] = 'Forgot Password';
    $message = '<html>
          <head>
          	<title>Password Reset</title>
			</head>
			<body style="margin:0;">
			<div style="width:700px; margin:0 auto;background:#f7f7f7;padding:20px 0;">
			<p style="text-align:center; margin-bottom:40px;"><img src="https://thebritishcrafthouse.co.uk/wp-content/uploads/2019/07/The-British-Craft-House-full-CMYK-teal-thick.png" alt="Square-Logo-300dpi" style="width:165px;" border="0"></p>
			<div class="main_sec" style="background: #fff;
		    border: 1px solid #fff;
		    margin: 10px 22px;">
        	<p style="background: #4ab6b6;
			    padding: 32px 35px;
			    color: #fff;
			    font-size: 30px;
			    font-family: sans-serif;
			    margin-top: 0;">Password Reset Request</p>
			    <p style="color: #717171; font-family: sans-serif; margin: 25px 35px;">Hi ' . $username . ',</p>
				<p style="color: #717171; font-family: sans-serif; margin: 25px 35px; line-height: 30px;">Someone has requested a new password for the following account on The British Craft House:</p>
				<p style="color: #717171; font-family: sans-serif; margin: 25px 35px;">Username: ' . $username . '</p>
				<p style="color: #717171; font-family: sans-serif; margin: 25px 35px;">If you did not make this request, just ignore this email. If you would like to proceed:</p>
				<p style="color: #4ab6b6; font-family: sans-serif; margin: 25px 35px;"><a href="' . esc_url($reset_link) . '" title="' . __('Reset your password link : ', 'my_slug') . '" style="color: #4ab6b6;">Click here to reset your password</a></p>
			</div>
			<p style="color: #909090; font-family: sans-serif; text-align: center; font-size: 13px; margin-top: 25px;">The British Craft House</p>
			</div>
          </body>
        </html>';
    $headers = "Content-Type: text/html; charset=UTF-8";
    //$headers .= 'From: Purple Office <'.$admin_email.'>'; 
    wp_mail($login, $mail['subject'], stripslashes($message), $headers);
    $json = array('status' => 'success', 'message' => 'Password reset link has been sent to your registered email');
    return $json;
    exit;
}

/* add_filter( 'woocommerce_api_product_response', 'wc_api_add_custom_data_to_product', 10, 2 );
  function wc_api_add_custom_data_to_product( $product_data, $product ) {
  print_r($product_data); die;
  // retrieve a custom field and add it to API response
  $product_data['vendor_id'] = get_post_field( 'post_author', $product->id);
  $product_data['vendor_name'] = get_the_author_meta( 'display_name', $product_data['vendor_id']);

  return $product_data;
  } */
add_filter('woocommerce_rest_prepare_product', 'wc_rest_api_add_custom_data_to_product', 90, 2);

function wc_rest_api_add_custom_data_to_product($response, $product) {
    $response->data['vendor_id'] = $product->post_author;
    $response->data['vendor_name'] = get_the_author_meta('display_name', $product->post_author);
    return $response;
}

////////////// Hook, To add add extra data in woocommerce product api ///////////////
add_filter('woocommerce_rest_prepare_product_object', 'get_vendor_product_new', 20, 3);

function get_vendor_product_new($response, $object, $request) {
    if (empty($response->data))
        return $response;
    //$vendor_id = $object->post_author;
    $response->data['shipping_details'] = getProductShipping($object->id, 2);
    return $response;
}

////////////// END Hook, To add add extra data in woocommerce product api ///////////////

function getProductShipping($product_id, $quantity) {

    $productID = $product_id;
    $product_quantity = $quantity;
    global $woocommerce, $wpdb;
    $shipping_rule_item_cost = 0;
    $shipping_rule_cost = 0;
    $shipping_country = $woocommerce->customer->get_shipping_country();
    //foreach ( $items as $cart_item ) {
    $country_code_query = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->prefix}woocommerce_per_product_shipping_rules WHERE product_id = " . $productID . " ORDER BY rule_order;", $productID), OBJECT);

    if (!empty($country_code_query)) {
        foreach ($country_code_query as $key => $value) {
            $country_codes = $value->rule_country;
            $country_codes = explode(',', $country_codes);
            if (in_array($shipping_country, $country_codes)) {
                $available_shipping_methods[] = $value->rule_postcode;
            }
            if ($product_quantity > 1) {
                $shipping_rule_item_cost = (float) $value->rule_item_cost * (int) $product_quantity;
            }
            $shipping_rule_cost = (float) $value->rule_cost;
        }
    }
    //}
    $available_shipping_methods = array_unique($available_shipping_methods);

    if (!empty($available_shipping_methods)) {

        foreach ($available_shipping_methods as $key => $value) {
            $method_data = get_option('woocommerce_per_product_' . $value . '_settings');
            $method_title = $method_data['title'];

            $item = new WC_Order_Item_Shipping();
            $item->set_method_title($method_title);
            $item->set_method_id($value); //set an existing Shipping method rate ID
            //$item->set_total( $shipping_lines[0]['total'] ); // (optional)
            //$item->calculate_taxes($calculate_tax_for);
            //$order->add_item( $item );

            $shipping_methods = array(
                'value' => $value,
                'method_title' => $method_title,
                'method_data' => $method_data,
                'item' => $item,
                'shipping_rule_item_cost' => $shipping_rule_item_cost,
                'shipping_rule_cost' => $shipping_rule_cost
            );
        }
        return $shipping_methods;
    } else {
        return 'No shipping method found';
    }
}

//////CURL function///////////
function curlRequest($method, $data = false, $url) {

    $curl = curl_init();
    $secretKey = STRIPE_SECRET_KEY;
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer " . $secretKey
        ),
    ));
    $curl_response = curl_exec($curl);
    curl_close($curl);
    return $curl_response;
}

////// Stripe Payment for woocommerce Order //////
add_action('rest_api_init', 'stripePayment');

function stripePayment() {
    register_rest_route('/wp/v2/', 'stripe_payment', array(
        'methods' => 'POST',
        'callback' => 'stripe_payment_for_order',
    ));
}

function stripe_payment_for_order($request = null) {

    global $wpdb;
    $auth = apache_request_headers();
    $authtoken = $auth['Authorization'];
    $vaildation = validateToken($authtoken);
    $secretKey = STRIPE_SECRET_KEY;

    if (isset($vaildation->id)) {
        $response = array();
        $parameters = $request->get_params();
        $amount = sanitize_text_field($parameters['amount']);
        $stripe_card_token = sanitize_text_field($parameters['stripe_card_token']);
        $order_data = $parameters['order_data'];
        $user_id = $parameters['customer_id'];
        $country = $order_data['shipping_address']['country'];
        $error = new WP_Error();

        if ($country != 'United Kingdom') {
            $error->add(404, __("The item trying to be purchased cannot be dispatched to the selected country " . $country . "."), array('status' => 400));
            return $error;
        }
        if (empty($stripe_card_token)) {
            $error->add(404, __("Payment Token 'stripe card token' is required."), array('status' => 400));
            return $error;
        }
        if (empty($amount)) {
            $error->add(404, __("Payment Token 'amount' is required."), array('status' => 400));
            return $error;
        }
        $transferPost = "description=" . $vaildation->email;
        $url = 'https://api.stripe.com/v1/customers';
        $transfer_response = curlRequest('POST', $transferPost, $url); /////Call curl function to generate stripe Customer id
        if (isset(json_decode($transfer_response)->error)) {
            $response['status'] = "error";
            $response['message'] = json_decode($transfer_response)->error->message;
            return $response;
        }
        $customer_id = json_decode($transfer_response)->id;
        $transferPost1 = "source=" . $stripe_card_token;
        $url1 = "https://api.stripe.com/v1/customers/" . $customer_id . "/sources";
        $transfer_response = curlRequest('POST', $transferPost1, $url1); /////Call curl function to Attach Customer with card token 
        if (isset(json_decode($transfer_response)->error)) {
            $response['status'] = "error";
            $response['message'] = json_decode($transfer_response)->error->message;
            return $response;
        }
        $card_data = json_decode($transfer_response);
        $card_id = $card_data->id;
        $description = "woocommerce order";
        $stripe_total_price = $amount * 100;
        $post_data = "amount=" . $stripe_total_price . "&currency=usd&customer=" . $customer_id . "&source=" . $card_id;
        $url2 = "https://api.stripe.com/v1/charges";
        $curl_response = curlRequest('POST', $post_data, $url2); //Call curl Function

        if (isset(json_decode($curl_response)->error)) {
            $response['status'] = 'error';
            $response['message'] = json_decode($curl_response)->error->message;
            return $response;
        } elseif (isset(json_decode($curl_response)->status) && json_decode($curl_response)->status == "succeeded") {

            if (!empty($order_data) && is_array($order_data) || is_object($order_data)) {

                $address_new = $order_data['billing_address'];
                $shipping_address = $order_data['shipping_address'];
                $line_items = $order_data['line_items'];
                $shipping_lines = $order_data['shipping_lines'];
                //$user_id = $vaildation->id;
                // Now we create the order
                $order = wc_create_order(array('customer_id' => $user_id));
                for ($i = 0; $i < count($line_items); $i++) {
                    $product_id = $line_items[$i]['product_id'];
                    $product_qty = $line_items[$i]['quantity'];
                    $order->add_product(get_product($product_id), $product_qty);
                }
                //$order->add_product( get_product(225929), $line_items[0]['quantity']);
                // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php

                $order->set_address($address_new, 'billing');
                $order->set_address($shipping_address, 'shipping');
                $order->set_payment_method('stripe');
                $order->set_payment_method_title('Credit Card');
                $order->set_transaction_id(json_decode($curl_response)->id);
                $order->set_date_paid(time());

                //$order->add_shipping($shipping_lines);
                // Get a new instance of the WC_Order_Item_Shipping Object
                // Get the customer country code
                $country_code = $order->get_shipping_country();

                // Set the array for tax calculations
                $calculate_tax_for = array(
                    'country' => $country_code,
                    'state' => '', // Can be set (optional)
                    'postcode' => '', // Can be set (optional)
                    'city' => '', // Can be set (optional)
                );
                //$item = new WC_Order_Item_Shipping();
                //$item->set_method_title( $shipping_lines[0]['method_title'] );
                //$item->set_method_id( $shipping_lines[0]['method_id'] );//set an existing Shipping method rate ID
                //$item->set_total( $shipping_lines[0]['total'] ); // (optional)
                //$item->calculate_taxes($calculate_tax_for);
                //$order->add_item( $item );
                $order->calculate_totals();
                $order_id = trim(str_replace('#', '', $order->get_order_number()));

                $my_post = array(
                    'ID' => $order_id,
                    'post_status' => 'wc-completed',
                );
                wp_update_post($my_post);
                //$order->save();
                $response['status'] = 'success';
                $response['message'] = 'Order placed successfully';
                $response['data']['order_id'] = $order_id;
                $response['data'] = $product_id . "df" . $product_qty;
                return $response;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Order details are missing Please check!';
                return $response;
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'There is some error, please contact admin';
            return $response;
        }
    } else {
        $response['status'] = 'error';
        $response['data'] = $vaildation;
        return $response;
    }
}

//////////validate Auth token///////////////////
function validateToken($token) {

    $url = site_url() . "/wp-json/jwt-auth/v1/token/validate";
    $ch = curl_init();
    $headers = array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization:' . $token,
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, true);
    $output = json_decode(curl_exec($ch));

    if ($output->statusCode == 200) { //if validate success get user details from token
        $newurl = site_url() . "/wp-json/wp/v2/users/me";
        $chs = curl_init();
        $header = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization:' . $token,
        );
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_URL, $newurl);
        curl_setopt($chs, CURLOPT_HTTPHEADER, $header);
        curl_setopt($chs, CURLOPT_POST, true);
        if (curl_errno($chs)) {
            return curl_error($chs);
        }
        $res = json_decode(curl_exec($chs));
        return $res;
    } else {
        return $output;
    }
}

//function wcfm_custom_product_manage_fields_pricing( $pricing_fields, $product_id, $product_type ) {
//if( isset( $pricing_fields['sale_price'] ) ) {
//	unset( $pricing_fields['sale_price'] );
//}
//return $pricing_fields;
//}
/**
 * Get Navigation Menu API
 * Transform a navigational menu to it's tree structure
 *
 * @uses  buildTree()
 * @uses  wp_get_nav_menu_items()
 *
 * @param  String     'Menu name' OR $menud_id 
 * @return Array|null $tree 
 */
function get_my_menu() {
    $items = wp_get_nav_menu_items('Main Nav');
    $data = nav_menu_object_tree($items);
    return $data;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'menu', array(
        'methods' => 'GET',
        'callback' => 'get_my_menu',
    ));
});

/**
 * "Build a tree from a flat array in PHP"
 *
 *
 */
function buildTree(array &$elements, $parentId = 0) {
    $branch = array();
    foreach ($elements as &$element) {
        if ($element->menu_item_parent == $parentId) {
            $children = buildTree($elements, $element->ID);
            if ($children)
                $element->wpse_children = $children;
            $branch[$element->ID] = $element;
            unset($element);
        }
    }
    return $branch;
}

function nav_menu_object_tree($nav_menu_items_array) {

    foreach ($nav_menu_items_array as $key => $value) {
        $value->children = array();
        $nav_menu_items_array[$key] = $value;
    }

    $nav_menu_levels = array();
    $index = 0;
    if (!empty($nav_menu_items_array))
        do {
            if ($index == 0) {
                foreach ($nav_menu_items_array as $key => $obj) {
                    if ($obj->menu_item_parent == 0) {
                        $nav_menu_levels[$index][] = $obj;
                        unset($nav_menu_items_array[$key]);
                    }
                }
            } else {
                foreach ($nav_menu_items_array as $key => $obj) {
                    if (in_array($obj->menu_item_parent, $last_level_ids)) {
                        $nav_menu_levels[$index][] = $obj;
                        unset($nav_menu_items_array[$key]);
                    }
                }
            }
            $last_level_ids = wp_list_pluck($nav_menu_levels[$index], 'db_id');
            $index++;
        } while (!empty($nav_menu_items_array));

    $nav_menu_levels_reverse = array_reverse($nav_menu_levels);

    $nav_menu_tree_build = array();
    $index = 0;
    if (!empty($nav_menu_levels_reverse))
        do {
            if (count($nav_menu_levels_reverse) == 1) {
                $nav_menu_tree_build = $nav_menu_levels_reverse;
            }
            $current_level = array_shift($nav_menu_levels_reverse);
            if (isset($nav_menu_levels_reverse[$index])) {
                $next_level = $nav_menu_levels_reverse[$index];
                foreach ($next_level as $nkey => $nval) {
                    foreach ($current_level as $ckey => $cval) {
                        if ($nval->db_id == $cval->menu_item_parent) {
                            $explode_url = explode('product-category/', $cval->url);
                            //echo "<pre>"; print_r($explode_url); 
                            $site = rtrim($explode_url[1], "/");
                            $cval->slug = $site;
                            //echo "<pre>"; print_r($cval);
                            $nval->children[] = $cval;
                            if (empty($nval->children)) {
                                $cval->slug = end($explode_url);
                            }
                        }
                    }
                }
            }
        } while (!empty($nav_menu_levels_reverse));
    $nav_menu_object_tree = $nav_menu_tree_build[0];
    return $nav_menu_object_tree;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'search_vendor', array(
        'methods' => 'GET',
        'callback' => 'serachVendor',
    ));
});

function serachVendor($request) {

    $parameters = $request->get_params();
    $search_string = sanitize_text_field($parameters['search_string']);
    $user_query = new WP_User_Query(array('meta_key' => 'store_name', 'meta_value' => $search_string, 'meta_compare' => 'LIKE'));
    $search_result = [];

    for ($i = 0; $i < count($user_query->results); $i++) {

        $store_id = $user_query->results[$i]->data->ID;
        $user_info = get_user_meta($store_id);

        if (isset($user_info['wcfm_subscription_status'][0]) && $user_info['wcfm_subscription_status'][0] == 'active') {

            $store_user = wcfmmp_get_store($store_id);
            $store_info = $store_user->get_shop_info();
            $gravatar = $store_user->get_avatar();
            $banner_type = $store_user->get_list_banner_type();
            if ($banner_type == 'video') {
                $banner_video = $store_user->get_list_banner_video();
            } else {
                $banner = $store_user->get_list_banner();
                if (!$banner) {
                    $banner = isset($WCFMmp->wcfmmp_marketplace_options['store_list_default_banner']) ? $WCFMmp->wcfmmp_marketplace_options['store_list_default_banner'] : $WCFMmp->plugin_url . 'assets/images/default_banner.jpg';
                    $banner = apply_filters('wcfmmp_list_store_default_bannar', $banner);
                }
            }
            $store_name = isset($store_info['store_name']) ? esc_html($store_info['store_name']) : __('N/A', 'wc-multivendor-marketplace');
            $store_name = apply_filters('wcfmmp_store_title', $store_name, $store_id);
            $store_url = wcfmmp_get_store_url($store_id);
            $store_address = $store_user->get_address_string();
            $store_description = $store_user->get_shop_description();
            $search_result[] = array(
                "vendor_id" => $store_id,
                "vendor_display_name" => $user_info['store_name'][0],
                "vendor_shop_name" => $user_info['store_name'][0],
                "vendor_shop_logo" => $gravatar,
                "mobile_banner" => $banner,
                "vendor_description" => isset($user_info['_store_description'][0]) ? $user_info['_store_description'][0] : null
            );
        }
    }
    return $search_result;
}

//Hide categories from WordPress category widget
function exclude_widget_categories($args) {
    $user = wp_get_current_user();
    $blocked_user_roles = array("customer");
    if (!is_user_logged_in() || is_user_logged_in() && count(array_intersect($blocked_user_roles, $user->roles)) > 0) {
        $exclude = "44802,894";
        $args["exclude"] = $exclude;
        return $args;
    }
}

add_filter("widget_categories_args", "exclude_widget_categories");

function isAnchor($string) {
    return preg_match("/^\<a.*\>.*\<\/a\>/", $string, $m) != 0;
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_store_vendor', array(
        'methods' => 'GET',
        'callback' => 'get_store_vendors'
            )
    );
});

function my_sort($a, $b) {
    if ($a == $b)
        return 0;
    return ($a < $b) ? -1 : 1;
}

function get_store_vendors($request) {
    //echo $_SERVER['HTTP_X_FORWARDED_FOR']; die;
    global $WCFM;
    $_POST["controller"] = 'wcfm-vendors';
    $_POST['length'] = !empty($request['per_page']) ? intval($request['per_page']) : 100;
    $_POST['start'] = !empty($request['page']) ? ( intval($request['page']) - 1 ) * $_POST['length'] : 0;
    $_POST['filter_date_form'] = !empty($request['after']) ? $request['after'] : '';
    $_POST['filter_date_to'] = !empty($request['before']) ? $request['before'] : '';
    $_POST['orderby'] = 'ASC';
    $queries_data = array();
    parse_str($_SERVER['QUERY_STRING'], $queries_data);

    $_POST['search_data'] = array();
    foreach ($queries_data as $query_key => $query_value) {
        if (in_array($query_key, apply_filters('wcfmmp_vendor_list_exclude_search_keys', array('v', 'search_term', 'wcfmmp_store_search', 'wcfmmp_store_category', 'wcfmmp_radius_addr', 'wcfmmp_radius_lat', 'wcfmmp_radius_lng', 'wcfmmp_radius_range', 'excludes', 'orderby', 'lang'))))
            $_POST['search_data'][$query_key] = $query_value;
    }

    define('WCFM_REST_API_CALL', TRUE);
    $WCFM->init();
    $wcfm_vendors_array = array();
    $wcfm_vendors_json_arr = array();
    $response = array();
    $wcfm_vendors_array = $WCFM->ajax->wcfm_ajax_controller();

    if ($_SERVER['HTTP_X_FORWARDED_FOR'] == '122.160.97.221') {
        //echo $dfgd = count($wcfm_vendors_array);
        echo "<pre>";
        print_r($wcfm_vendors_array);
        die;
    }
    //print_r($wcfm_vendors_array);
    // return rest_ensure_response( $wcfm_vendors_array );
    if (!empty($wcfm_vendors_array)) {
        $index = 0;
        foreach ($wcfm_vendors_array as $wcfm_vendors_id => $wcfm_vendors_name) {
            $response[$index] = get_formatted_item_data($wcfm_vendors_id, $wcfm_vendors_name);
            $index ++;
        }
        $response = apply_filters("wcfmapi_rest_prepare_store_vendors_objects", $response, $request);
        $response_final = new WP_REST_Response($response, 200);
        $response_final->header('Cache-Control', 'no-cache');
        //usort($response_final->data,"my_sort");
        // if($_SERVER['HTTP_X_FORWARDED_FOR'] == '122.160.97.221'){
        // echo "<pre>"; print_r($response_final->data); die;
        // }
        return $response_final;
        //return rest_ensure_response( apply_filters( "wcfmapi_rest_prepare_store_vendors_objects", $response, $request ) );
    } else {
        $response_final = new WP_REST_Response($response, 200);
        $response_final->header('Cache-Control', 'no-cache');
        return $response_final;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_store_vendor_products', array(
        'methods' => 'GET',
        'callback' => 'get_store_vendor_products'
    ));
});

function get_store_vendor_products($request) {
    $query_args = prepare_objects_query($request);
    $query = new WP_Query();
    $result = $query->query($query_args);
    $data = array();
    //$objects = array_map( array( $request['id'], 'get_object' ), $result );
    $data_objects = array();
    foreach ($objects as $object) {
        $data = prepare_data_for_response($object, $request);
        $data_objects[] = prepare_response_for_collection($data);
    }
    $response_final = new WP_REST_Response($data_objects);
    $response_final->header('Cache-Control', 'no-cache');
    // $response = rest_ensure_response( $data_objects );
    $response_final = format_collection_response($response_final, $request, $query->found_posts);
    return $response_final;
}

######## Get Temp Auth Token #######################
add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_temp_auth_token', array(
        'methods' => 'GET',
        'callback' => 'requestForTempAuthToken'
    ));
});

function requestForTempAuthToken($request) {

    $tempToken = $request['token'];
    if (empty($tempToken)) {
        $tempToken = tempUserAuthToken();
        $res['token'] = $tempToken;
        $res['status'] = 'success';
        return $res;
    } else {
        $vaildation = validateToken("Bearer " . $tempToken);
        if (!isset($vaildation->id)) {
            $tempToken = tempUserAuthToken();
            $res['token'] = $tempToken;
            $res['status'] = 'success';
            return $res;
        } else {
            $res['token'] = $tempToken;
            $res['status'] = 'success';
            return $res;
        }
    }
}

function tempUserAuthToken() {
    $url = site_url() . "/wp-json/jwt-auth/v1/token";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/x-www-form-urlencoded"));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=tempauthuser@yopmail.com&password=tempauth@123");
    $output = json_decode(curl_exec($ch));
    curl_close($ch);
    if ($output->success == true) {
        return $output->data->token;
    } else {
        return false;
    }
}

/* function my_rest_prepare_product( $data, $post, $request ) {
  return $post->ID;
  $_data = $data->data;
  $comments =  get_comments_number($post->ID);
  }
  add_filter( 'rest_prepare_product', 'my_rest_prepare_product', 12, 3 ); */

/* add_filter('woocommerce_rest_prepare_product_object', 'so54387226_custom_data', 10, 3);

  function so54387226_custom_data($response, $object, $request) {
  if (empty($response->data))
  return $response;
  $id = $object->get_id(); //it will fetch product id
  $response->data['booking_meta_data'] = $id;
  return $response;
  } */

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_per_product_shipping', array(
        'methods' => 'POST',
        'callback' => 'getPerProductShippings'
    ));
});

function getPerProductShippings($request) {

    $productID = $request['product_id'];
    $product_quantity = $request['quantity'];
    global $woocommerce, $wpdb;
    $shipping_rule_item_cost = 0;
    $shipping_rule_cost = 0;
    $shipping_country = $woocommerce->customer->get_shipping_country();
    //foreach ( $items as $cart_item ) {
    $country_code_query = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->prefix}woocommerce_per_product_shipping_rules WHERE product_id = " . $productID . " ORDER BY rule_order;", $productID), OBJECT);
    if (!empty($country_code_query)) {
        foreach ($country_code_query as $key => $value) {
            $country_codes = $value->rule_country;
            $country_codes = explode(',', $country_codes);
            if (in_array($shipping_country, $country_codes)) {
                $available_shipping_methods[] = $value->rule_postcode;
            }
            if ($product_quantity > 1) {
                $shipping_rule_item_cost = (float) $value->rule_item_cost * (int) $product_quantity;
            }
            $shipping_rule_cost = (float) $value->rule_cost;
        }
    }
    //}
    $available_shipping_methods = array_unique($available_shipping_methods);

    if (!empty($available_shipping_methods)) {

        foreach ($available_shipping_methods as $key => $value) {
            $method_data = get_option('woocommerce_per_product_' . $value . '_settings');
            $method_title = $method_data['title'];
            $item = new WC_Order_Item_Shipping();
            $item->set_method_title($method_title);
            $item->set_method_id($value); //set an existing Shipping method rate ID
            //$item->set_total( $shipping_lines[0]['total'] ); // (optional)
            //$item->calculate_taxes($calculate_tax_for);
            //$order->add_item( $item );
            $shipping_methods = array(
                'value' => $value,
                'method_title' => $method_title,
                'method_data' => $method_data,
                'item' => $item,
                'shipping_rule_item_cost' => $shipping_rule_item_cost,
                'shipping_rule_cost' => $shipping_rule_cost
            );
        }
        return wp_send_json(['status' => 'success', 'data' => $shipping_methods]);
    } else {
        return wp_send_json(['status' => 'success', 'message' => 'No shipping method found"']);
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'calculate_per_product_shipping_cost', array(
        'methods' => 'GET',
        'callback' => 'calculate_per_product_shipping_cost'
    ));
});
/**
 * Calculate the per product shipping cost if enabled for the product.
 *
 * @param array $product_data The product data form the package array.
 * @param array $package Shipping package array.
 *
 * @return float|bool
 */
/* function calculate_per_product_shipping_cost() {

  global $woocommerce;

  $customerZipCode =  75098;
  $zipResultArr = csd_check_zip_and_state($customerZipCode);
  $package =  $woocommerce->cart->get_shipping_packages();
  $package[0]['destination']['state'] = $zipResultArr['state'];
  $package[0]['destination']['postcode'] = $customerZipCode ;
  $package[0]['destination']['city'] = $zipResultArr['city'];
  $package[0]['destination']['address'] = '';
  $package[0]['destination']['address_2'] = '';
  print_r($package);}
  /*$product_data = array(
  'variation_id' => '',
  'product_id' => 67582,
  'quantity' =>2,

  );

  $rule               = false;
  $item_shipping_cost = 0;

  if ( $product_data['variation_id'] ) {
  $rule = woocommerce_per_product_shipping_get_matching_rule( $product_data['variation_id'], $package );
  }

  if ( false === $rule ) {
  $rule = woocommerce_per_product_shipping_get_matching_rule( $product_data['product_id'], $package );
  }

  if ( $rule ) {
  $item_shipping_cost += (float) $rule->rule_item_cost * (int) $product_data['quantity'];
  $item_shipping_cost += (float) $rule->rule_cost;
  } elseif ( '0' === $this->cost || $this->cost > 0 ) {
  // Use default shipping cost.
  $item_shipping_cost += (float) $this->cost * (int) $product_data['quantity'];
  } else {
  // NO default and nothing found - abort.
  return false;
  }

  // Fee.
  $item_shipping_cost += $this->get_fee( (float) $this->fee, $item_shipping_cost ) * (int) $product_data['quantity'];

  return $item_shipping_cost;
  } */

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', 'get_store_vendors_list', array(
        'methods' => 'GET',
        'callback' => 'get_store_vendors_list'
    ));
});

function get_store_vendors_list($request) {

    global $WCFM;
    $_POST["controller"] = 'wcfm-vendors';
    $_POST['length'] = !empty($request['per_page']) ? intval($request['per_page']) : 100;
    $_POST['filter_date_form'] = !empty($request['after']) ? $request['after'] : '';
    $_POST['filter_date_to'] = !empty($request['before']) ? $request['before'] : '';
    $_POST['orderby'] = 'name';
    $_POST['order'] = 'ASC';
    $queries_data = array();
    parse_str($_SERVER['QUERY_STRING'], $queries_data);
    define('WCFM_REST_API_CALL', TRUE);
    $WCFM->init();
    $wcfm_vendors_array = array();
    $wcfm_vendors_json_arr = array();
    $response = array();
    $index = 0;
    for ($i = 0; $i < 10; $i++) {

        $_POST['start'] = $i * $_POST['length'];
        $wcfm_vendors_array = $WCFM->ajax->wcfm_ajax_controller();

        if (count($wcfm_vendors_array) > 0) {
            //$wcfm_vendors_json_arr = array_merge($wcfm_vendors_json_arr, $wcfm_vendors_array);
            foreach ($wcfm_vendors_array as $wcfm_vendors_id => $wcfm_vendors_name) {
                $res = get_vendor_formatted_item_data($wcfm_vendors_id, $wcfm_vendors_name);
                if ($res != null) {
                    $response[$index] = $res;
                    $index ++;
                }
            }
        }
    }
    if (!empty($response)) {
        $shop_name = array();
        foreach ($response as $key => $row) {
            $shop_name[$key] = $row['vendor_shop_name'];
        }
        array_multisort($shop_name, SORT_ASC, $response);
        $response_final['status'] = 'success';
        $response_final['data'] = $response;
        return $response_final;
    } else {
        $response_final['status'] = 'success';
        $response_final['message'] = 'No shop found';
        return $response_final;
    }
}

function wcfm_custom_product_manage_fields_advanced($wcmp_advanced_fields) {
    if (isset($wcmp_advanced_fields['enable_reviews'])) {
        $wcmp_advanced_fields['enable_reviews']['custom_attributes'] = array('required' => 1);
    }
    return $wcmp_advanced_fields;
}

add_filter('wcfm_product_manage_fields_advanced', 'wcfm_custom_product_manage_fields_advanced', 50, 2);

// Redirect return to shop to shop page
//add_filter( 'woocommerce_return_to_shop_redirect', function( $redirect_url ) {
//$vendor_id = 0;
//foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
//	$cart_product_id = $cart_item['product_id'];
//$cart_product = get_post( $cart_product_id );
//$cart_product_author = $cart_product->post_author;
//if( function_exists( 'wcfm_is_vendor' ) && wcfm_is_vendor( $cart_product_author ) ) {
//	$vendor_id = $cart_product_author;
//	break;
//}
//}	
//if( $vendor_id ) {
//	$redirect_url = wcfmmp_get_store_url( $vendor_id );
//}
//return $redirect_url;
//}, 100 );
// Code for Viraj script goes here
//add_action('wp_head', 'wfcm_select2_dd_script');
//function wfcm_select2_dd_script() {
//echo "<script>jQuery(document).ready(function() {
// jQuery('#dropdown_product_cat').select2();
//});</script>";
//}
//add_action('wp_loaded', 'wfcm_select2_dd_script', 999);

function wfcm_select2_dd_scriptoff() {
    ?>
    <script>
        jQuery(document).ready(function () {
            jQuery('#dropdown_product_cat').select2();
        });
    </script>
    <?php
}

// =============================================================
function wfcm_select2_dd_script() {

    wp_enqueue_script('child_myscripts', get_stylesheet_directory_uri() . '/assets/js/myscripts.js', array('jquery'));
}

add_action('wp_enqueue_scripts', 'wfcm_select2_dd_script');
