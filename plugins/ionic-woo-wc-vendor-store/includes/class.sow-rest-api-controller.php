<?php
if (!class_exists('SOW_REST_API_Controller')) {
    class SOW_REST_API_Controller
    {
        public $version = 1;

        public function __construct()
        {
            add_action('rest_api_init', array($this, 'register_routes'));
            add_filter('authenticate',        array($this, 'authenticate'), 10, 3);
            add_filter('determine_current_user',    array($this, 'determine_current_user'), 20);
            add_filter('woocommerce_rest_prepare_product', array($this, 'get_vendor_product'), 10, 3);
            add_filter('woocommerce_rest_prepare_shop_order', array($this, 'get_bank_info'), 10, 3);
            add_action('woocommerce_api_create_order', array($this, 'after_woocommerce_api_create_order'), 10, 2);
            add_action('woocommerce_api_edit_order', array($this, 'after_woocommerce_api_edit_order'), 10, 2);

            add_filter('woocommerce_api_order_response', array($this, 'add_order_response'), 10, 3);
            require_once('des.php');
			require_once('wcv_vendors.php');
        }

        public function register_routes()
        {
            register_rest_route('sow/v1', '/user_login', array(
                'methods' => 'GET',
                'callback' => array($this, 'user_login')
            ));

            register_rest_route('sow/v1', '/home_slider', array(
                'methods' => 'GET',
                'callback' => array($this, 'get_home_slider')
            ));

            register_rest_route('sow/v1', '/shipping_method', array(
                'methods' => 'GET',
                'callback' => array($this, 'get_shipping_method')
            ));

            register_rest_route('sow/v1', '/get_vendors', array(
                'methods' => 'GET',
                'callback' => array($this, 'get_vendors')
            ));


            register_rest_route('sow/v1', '/get_vendor_details/(?P<vid>\d+)', array(
                'methods' => 'GET',
                'callback' => array($this, 'get_vendor_details')
            ));

            register_rest_route('sow/v1', '/search_vendors', array(
                'methods' => 'GET',
                'callback' => array($this, 'search_vendors')
            ));

            register_rest_route( 'sow/v1', '/regist_user', array(
				'methods' => 'GET',
				'callback' =>array($this,'regist_user')
			) );
        }

        public function regist_user($request){
            $iv = substr(md5(get_option('sow_rest_api_secret')), 0, 8);
            $key = md5(get_option('sow_rest_api_secret'));
            $pass = SowDes::decrypt($request['password'], $key, 'DES-CBC', $iv);
            $user_id = username_exists( $request['user_name'] );
            if ( !$user_id and email_exists($request['user_email']) == false ) {
                $user_id = wp_create_user( $request['user_name'], $pass, $request['user_email'] );
                wp_update_user( array( 'ID' => $user_id, 'first_name' => $request['first_name'],'last_name' => $request['last_name'],
                    'role' => 'customer') );
                return array(
							'id'=>$user_id,
							'email'=>$request['user_email'],
							'first_name'=>$request['first_name'],
							'last_name'=>$request['last_name'],
							'username'=>$request['last_name'],
							'nick_name'=>$request['last_name']
							);
            } else {
                return false;
            }
        }

        public function search_vendors($request)
        {
            $user_array = get_users(array(
                'role' => 'vendor',
                'meta_query' => array(
                    'relation' => 'AND',

                    array(
                        'key'     => 'pv_shop_name',
                        'value'   => $request['key_word'],
                        'compare' => 'like'
                    )
                )
            ));
            $resp_user_array = array();
            foreach ($user_array as $user) {
                array_push($resp_user_array, array(
                    'id' => $user->ID,
                    'shop_name' => WCV_Vendors::get_vendor_shop_name($user->ID),
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'avatar_url' => get_user_meta($user->ID, 'sow_user_avatar', true)
                ));
            }
            return $resp_user_array;
        }

        public function after_woocommerce_api_edit_order($order_id, $order)
        {
            if ($order->get_payment_method() == 'bacs') {
                $wc_getway_bacs = new WC_Gateway_BACS();
                $order_data['bank_info'] = $wc_getway_bacs->account_details;
            }
        }

        function determine_current_user($input_user)
        {
            $api_request = (defined('XMLRPC_REQUEST') && XMLRPC_REQUEST) || (defined('REST_REQUEST') && REST_REQUEST);
            if ($api_request) {
                return 1;
            }
            return $input_user;
        }

        public function authenticate($input_user, $username, $password)
        {
            $api_request = (defined('XMLRPC_REQUEST') && XMLRPC_REQUEST) || (defined('REST_REQUEST') && REST_REQUEST);
            if ($api_request) {
                return 1;
            }
            return $input_user;
        }


        public function add_order_response($order_data, $order)
        {
            if ($order->get_payment_method() == 'bacs') {
                $wc_getway_bacs = new WC_Gateway_BACS();
                $order_data['bank_info'] = $wc_getway_bacs->account_details;
            }
            return $order_data;
        }


        public function get_vendors()
        {
            $user_array = get_users(array(
                'role' => 'vendor'
            ));
            $resp_user_array = array();
            foreach ($user_array as $user) {
                    array_push($resp_user_array, array(
                        'id' => $user->ID,
                        'shop_name' => WCV_Vendors::get_vendor_shop_name($user->ID),
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'avatar_url' => get_avatar_url($user->ID, array('size' => 32))
                    ));
                }
            return $resp_user_array;
        }

        public function get_vendor_details($request)
        {
            $args = array(
                'post_type'   => 'product',
                'author'      => $request['vid'],
                'post_status' => 'publish'
            );
            if (isset($request['per_page'])) {
                $args['posts_per_page'] = $request['per_page'];
            }
            if (isset($request['page'])) {
                $args['paged'] = $request['page'];
            }

            $vendor_products = get_posts($args);

            $shop_name = WCV_Vendors::get_vendor_shop_name($request['vid']);
            $shop_desc = get_user_meta($request['vid'], 'pv_shop_description', true);

            $result = array();
            $result['shop_name'] = $shop_name;
            $result['shop_desc'] = $shop_desc;
            $result['shop_logo'] = get_avatar_url($request['vid'], array('size' => 32));
            $result['products'] = array();


            foreach ($vendor_products as $_product) {
                $vendor_product = wc_get_product($_product->ID);

                array_push(
                    $result['products'],
                    array(
                        'id' => $_product->ID,
                        'name' => $vendor_product->get_post_data()->post_title,
                        'price' => $vendor_product->get_price(),
                        'image' => $this->get_images($vendor_product)[0]
                    )
                );
            }

            return $result;
        }

        protected function get_images($product)
        {

            $images = array();
            $attachment_ids = array();
            if (has_post_thumbnail($product->id)) {
                $attachment_ids[] = get_post_thumbnail_id($product->id);
            }
            $attachment_ids = array_merge($attachment_ids, $product->get_gallery_attachment_ids());

            foreach ($attachment_ids as $position => $attachment_id) {
                $attachment_post = get_post($attachment_id);
                if (is_null($attachment_post)) {
                    continue;
                }
                $attachment = wp_get_attachment_image_src($attachment_id, 'full');
                if (!is_array($attachment)) {
                    continue;
                }
                $images[] = array(
                    'url'            => current($attachment),
                    'position'      => (int)$position,
                );
            }
            if (count($images) == 0) {
                $images[0] = array(
                    'url'            => wc_placeholder_img_src(),
                    'position'      => 0,
                );
            }
            return $images;
        }

        public function get_vendor_product($response, $post, $request)
        {
            if (isset($request['is_vendor']) && $request['is_vendor'] == true) {
                $vendor_id = $request['vendor_id'];
                $shop_name = WCV_Vendors::get_vendor_shop_name($vendor_id);
                $shop_desc = get_user_meta($vendor_id, 'pv_shop_description', true);

                $response->data['shop_name'] = $shop_name;
                $response->data['shop_desc'] = $shop_desc;
            } else if (isset($request['id']) && WCV_Vendors::is_vendor($post->post_author)) {
                $response->data['vendor_id'] = $post->post_author;
                $shop_name = WCV_Vendors::get_vendor_shop_name($post->post_author);
                $shop_desc = get_user_meta($post->post_author, 'pv_shop_description', true);

                $response->data['shop_name'] = $shop_name;
                $response->data['shop_desc'] = $shop_desc;
            }
            return $response;
        }

        public function get_bank_info($response, $post, $request)
        {
            $order = wc_get_order($post->ID);
            if ($order->get_payment_method() == 'bacs') {
                $wc_getway_bacs = new WC_Gateway_BACS();
                $response->data['bank_info'] = $wc_getway_bacs->account_details;
            }
            return $response;
        }

        public function after_woocommerce_api_create_order($order_id, $request)
        {
            $order = wc_get_order($order_id);
            if ($request['payment_details']['method_id'] == 'bacs') {
                $order = wc_get_order($order_id);
                $order->update_status('wc-on-hold', 'Awaiting BACS payment.');
            } else if ($request['payment_details']['method_id'] == 'cod') {
                $order = wc_get_order($order_id);
                $order->update_status('wc-processing', 'Payment to be made upon delivery.');
            }
            if ($order->is_paid()) {
                WCV_Vendors::create_child_orders($order_id);
            }
        }

        public function user_login($request)
        {
            if (!empty($request['password'])) {
                $iv = substr(md5(get_option('sow_rest_api_secret')), 0, 8);
                $key = md5(get_option('sow_rest_api_secret'));
                $pass = SowDes::decrypt($request['password'], $key, 'DES-CBC', $iv);
                $test_user = wp_authenticate($request['email'], $pass);

                if ($test_user == null || is_wp_error($test_user)) {
                    return 'error';
                } else {
                    if (in_array('customer', $test_user->roles)) {
                        return array(
                            'id' => $test_user->ID,
                            'email' => $test_user->user_email,
                            'first_name' => $test_user->first_name,
                            'last_name' => $test_user->last_name,
                            'username' => $test_user->user_login,
                            'nick_name' => $test_user->user_nicename
                        );
                    }
                }
            }
            return 'error';
        }

        public function get_home_slider($request)
        {
            $result = get_option('sow_rest_api_slider');
            if ($result) {
                return $result;
            }
            return array();
        }

        public function get_shipping_method()
        {
            $result = get_option('sow_rest_api_shipping');
            if ($result) {
                return $result;
            }
            return array();
        }

        private function sow_decrypt($decrypt_str)
        {
            $iv = substr(md5(get_option('sow_rest_api_secret')), 0, 16);
            $key = md5(get_option('sow_rest_api_secret'));
            $data = base64_decode($decrypt_str);
            $result = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
            return rtrim($result, "\x00..\x1F");
        }
    }
}
