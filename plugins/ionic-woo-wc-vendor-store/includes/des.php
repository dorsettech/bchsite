<?php

class SowDes
{

    /**
     * @author TechLee
     * @param  string $input
     * @param  string $key
     * @param  string $method
     * @param  string $iv
     * @return string
     */
    public static function encrypt($input, $key, $method = 'DES-ECB', $iv = null)
    {
        $iv = $iv ? $iv : self::createIv();
        return base64_encode(openssl_encrypt($input, $method, $key, OPENSSL_RAW_DATA, $method == 'DES-ECB' ? '' : $iv));
    }

    /**
     * @author TechLee
     * @param  string $input
     * @param  string $key
     * @param  string $method
     * @param  string $iv
     * @return string
     */
    public static function decrypt($input, $key, $method = 'DES-ECB', $iv = null)
    {
        $iv = $iv ? $iv : self::createIv();
        return openssl_decrypt(base64_decode($input), $method, $key, OPENSSL_RAW_DATA, $method == 'DES-ECB' ? '' : $iv);
    }

    /**
     * IV = new byte[8];
     * @author TechLee
     * @return [type] [description]
     */
    public static function createIv()
    {
        return self::hexToStr("0000000000000000");
    }

    public static function hexToStr($hex)
    {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
        }
        return $string;
    }
}