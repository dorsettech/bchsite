jQuery(document).ready(function ($) {
    let ajax_url = product_params.ajax_url;
    let pps_nonce = product_params.pps_nonce;
    $('.custom-wc-shipping-method, .hb-country-multiselect').select2();
    $(document).on("change", ".custom-wc-shipping-method", function (e) {
        e.preventDefault();
        let pps_method = $(this).val();
        $.ajax({
            type: 'POST',
            url: ajax_url,
            data: {
                'action': 'select_custom_pps_shipping_method',
                'pps_method': pps_method,
                'security': pps_nonce
            },
            success: function (data) {
                // console.log('selected method', data);
                $("[name='update_cart']").removeAttr("disabled");
                $("[name='update_cart']").trigger("click");
            }
        });
    });
    $(document.body).on('updated_cart_totals', function () {
        $('.cart-shipping-options').find('span.select2-container').remove();
        $('.custom-wc-shipping-method').select2();
    });
    // $(document).on('click', 'span.add_multi_input_block.multi_input_block_manupulate.wcfmfa.fa-plus-circle, .multi_input_block_manupulate.remove_multi_input_block.wcfmfa.fa-times-circle ', function(){
    $(document).on('click', '#wcfm_products_manage_form_shipping_expander span.add_multi_input_block.multi_input_block_manupulate.wcfmfa.fa-plus-circle', function () {
        var selectList = jQuery('select.hb-country-multiselect');
        console.log(selectList[selectList.length - 1]);
        //selectList[selectList.length-1].value = [];
        selectList[selectList.length - 1].name = selectList[selectList.length - 1].name + '[]';
        $('.hb-country-multiselect').select2();
    })

    $(document).on('click', '.duplicate_multi_input_block.fa-clone', function () {
        var selectList = jQuery('select.hb-country-multiselect');
        console.log(selectList[selectList.length - 1]);
        //selectList[selectList.length-1].value = [];
        selectList[selectList.length - 1].name = selectList[selectList.length - 1].name + '[]';
        $('.hb-country-multiselect').select2();
    })

    $(document).on("click", ".select2-results__group", function () {
        let current_country_dropdown = $(this).closest('.select2-results__options').attr('id');
        let country_dropdown_id = current_country_dropdown.replace('select2-', '');
        country_dropdown_id = country_dropdown_id.replace('-results', '');
        //console.log(current_country_dropdown);
        let groupName = $(this).html();
        let options = $('#' + country_dropdown_id + ' option');
        $.each(options, function (key, value) {

            if ($(value)[0].parentElement.label.indexOf(groupName) >= 0) {
                $(value).prop("selected", "selected");
            }

        });

        $('#' + country_dropdown_id).trigger("change");
        $(".hb-country-multiselect").select2('close');

    });
});