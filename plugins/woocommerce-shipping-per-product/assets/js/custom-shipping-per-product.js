jQuery(document).ready(function($){
	var recalculate_orders = function() {
		var $orders = $('[name^="per_product_order"]');
		$orders.each( function( index, el ) {
			$( el ).val( index );
		} );
	};
	var product_id = product_params.current_product_id;
	$(document).on("click", ".duplicate-item", function(e){
		e.preventDefault();
		let $tr    = $(this).closest('tr.ui-sortable-handle');
		let $clone = $tr.clone();
		let count_row = $('.per_product_shipping_rules tbody.ui-sortable tr').length;
		// count_row++;

		$clone.find('td.sort input').attr({'name':'per_product_order['+product_id+'][new][]','value':count_row});

		$clone.find('td.country select').attr('name', 'per_product_country['+product_id+'][new]['+count_row+'][]');
		$clone.find('td.country select').attr('id', 'countries-ruleid-'+count_row);
		$clone.find('td.state input').attr('name', 'per_product_state['+product_id+'][new][]');
		$clone.find('td.postcode select').attr('name', 'per_product_postcode['+product_id+'][new][]');
		$clone.find('td.cost input').attr('name', 'per_product_cost['+product_id+'][new][]');
		$clone.find('td.item_cost input').attr('name', 'per_product_item_cost['+product_id+'][new][]');

		$clone.find('td.country span.select2-container').remove();
		$clone.find('td.country select').select2();
		$tr.after($clone);
		recalculate_orders();
	});
	/*
	$('.countries').select({
	}).on('select2:select', function(e) {
		var items= $(this).val();
		console.log(items);
	});
	*/

	$(document).on("click", ".select2-results__group", function(){
		let current_country_dropdown = $(this).closest('.select2-results__options').attr('id');
		let country_dropdown_id = current_country_dropdown.split('-').slice(-2, -1)[0];
		console.log(country_dropdown_id);
		let groupName = $(this).html();
		let options = $('#countries-ruleid-'+country_dropdown_id+' option');
	    // console.log(options);
	    $.each(options, function(key, value){

			if($(value)[0].parentElement.label.indexOf(groupName) >= 0){
				$(value).prop("selected","selected");
			}

		});

		$('#countries-ruleid-'+country_dropdown_id).trigger("change");
		$(".countries").select2('close');

	});

});
