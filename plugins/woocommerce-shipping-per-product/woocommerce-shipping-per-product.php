<?php
/**
 * Plugin Name: WooCommerce Shipping Per Product v2
 * Plugin URI: https://woocommerce.com/products/per-product-shipping/
 * Description: Per product shipping allows you to define different shipping costs for products, based on customer location. These costs can be added to other shipping methods (requires WC 2.0), or used as a standalone shipping method.
 * Version: 2.3.3
 * Author: WooCommerce - Dorset Tech 1
 * Author URI: https://woocommerce.com
 * Requires at least: 3.3
 * Tested up to: 5.1
 * WC requires at least: 2.6
 * WC tested up to: 3.7
 *
 * Copyright: © 2019 WooCommerce
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * Woo: 18590:ba16bebba1d74992efc398d575bf269e
 *
 * @package WC_Shipping_Per_Product
 */
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Required functions
 */
if (!function_exists('woothemes_queue_update')) {
    require_once 'woo-includes/woo-functions.php';
}

/**
 * Plugin updates
 */
woothemes_queue_update(plugin_basename(__FILE__), 'ba16bebba1d74992efc398d575bf269e', '18590');

if (is_woocommerce_active()) {

    /**
     * Wrapper class to init the plugin.
     *
     * @since 1.0.0
     */
    class WC_Shipping_Per_Product_Init {

        /**
         * Constructor.
         */
        public function __construct() {
            define('PER_PRODUCT_SHIPPING_VERSION', '2.3.3');
            define('PER_PRODUCT_SHIPPING_FILE', __FILE__);

            if (is_admin()) {
                include_once 'includes/class-wc-shipping-per-product-admin.php';
                new WC_Shipping_Per_Product_Admin($this);
            }

            include_once 'includes/functions-wc-shipping-per-product.php';

            register_activation_hook(__FILE__, array($this, 'install'));

            add_action('init', array($this, 'load_plugin_textdomain'));
            add_action('woocommerce_shipping_init', array($this, 'load_shipping_method'));
            add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'plugin_action_links'));
            add_filter('plugin_row_meta', array($this, 'plugin_row_meta'), 10, 2);
            add_action('admin_init', array($this, 'register_importer'));
            add_action('woocommerce_loaded', array($this, 'load_post_wc_class'));

            add_action('woocommerce_thankyou', array($this, 'clear_custom_woocommerce_session'));
            add_action('woocommerce_checkout_update_order_meta', array($this, 'update_custom_woocommerce_meta'), 10, 2);
            add_filter('woocommerce_order_shipping_to_display', array($this, 'hb_filter_shipping_value'), 10, 2);
            add_filter('woocommerce_order_get_items', array($this, 'hb_custom_order_get_items'), 10, 3);
            add_action('woocommerce_admin_order_totals_after_tax', array($this, 'hb_custom_admin_order_totals_after_tax'), 10, 1);
            add_action('woocommerce_cart_totals_before_shipping', array($this, 'display_cart_custom_shipping_method'), 20);
            add_action('wp_footer', array($this, 'unset_cart_custom_shipping_method'));
            add_action('wp_enqueue_scripts', array($this, 'regsiter_scripts'));
            add_action('wp_ajax_select_custom_pps_shipping_method', array($this, 'select_custom_pps_shipping_method'));
            add_action('wp_ajax_nopriv_select_custom_pps_shipping_method', array($this, 'select_custom_pps_shipping_method'));

            add_filter('site_transient_update_plugins', array($this, 'hb_remove_update_notifications'));
        }

        /**
         * Add custom shipping options row in cart total section.
         */
        public function display_cart_custom_shipping_method() {
            $pps_custom_shipping_method_row = WC()->session->get('pps_custom_shipping_method_row');
            if ($pps_custom_shipping_method_row != 'added') {
                $pps_selected_shipping_method = WC()->session->get('pps_selected_shipping_method');
                global $woocommerce, $wpdb;
                $items = $woocommerce->cart->get_cart();
                $shipping_country = $woocommerce->customer->get_shipping_country();
                $available_shipping_methods = array();
                foreach ($items as $cart_item) {
                    $country_code_query = $wpdb->get_results($wpdb->prepare("SELECT rule_country, rule_postcode FROM {$wpdb->prefix}woocommerce_per_product_shipping_rules WHERE product_id = " . $cart_item['product_id'] . " ORDER BY rule_order;", $cart_item['product_id']), OBJECT);
                    if (!empty($country_code_query)) {
                        foreach ($country_code_query as $key => $value) {
                            $country_codes = $value->rule_country;
                            $country_codes = explode(',', $country_codes);
                            if (in_array($shipping_country, $country_codes)) {
                                $available_shipping_methods[] = $value->rule_postcode;
                            }
                        }
                    }
                }
                $available_shipping_methods = array_unique($available_shipping_methods);
                ?>
                <tr class="cart-shipping-options"> 
                    <th><?php _e('Shipping Options', 'woocommerce'); ?></th>
                    <td>
                        <select name="per_product_shipping_method" class="custom-wc-shipping-method">
                            <?php
                            // $wc_zone = new WC_Shipping_Zone(6);
                            // $wc_shipping = $wc_zone->get_shipping_methods();
                            // echo "<pre>";print_r($wc_shipping);echo "</pre>";
                            if (!empty($available_shipping_methods)) {
                                echo '<option value="">Select Option</option>';
                                foreach ($available_shipping_methods as $key => $value) {
                                    $method_data = get_option('woocommerce_per_product_' . $value . '_settings');
                                    $method_title = $method_data['title'];
                                    $selected = ($value == $pps_selected_shipping_method) ? 'selected' : '';
                                    echo '<option value="' . $value . '" ' . $selected . '>' . $method_title . '</option>';
                                }
                            } else {
                                echo '<option value="">No shipping method found</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <?php
                WC()->session->set('pps_custom_shipping_method_row', 'added');
            }
        }

        /**
         * Add custom script on frontend footer for update cart on change shipping options.
         */
        public function unset_cart_custom_shipping_method() {
            WC()->session->set('pps_custom_shipping_method_row', '');

            wp_register_script('custom-shipping-per-product-frontend', plugins_url('assets/js/custom-shipping-per-product-frontend.js', PER_PRODUCT_SHIPPING_FILE), array('jquery', 'tbch_select2_410.min_js'), FALSE, true);
            wp_enqueue_script('custom-shipping-per-product-frontend');
            wp_localize_script('custom-shipping-per-product-frontend', 'product_params', array(
                'pps_nonce' => wp_create_nonce("pps-nonce-field"),
                'ajax_url' => admin_url('admin-ajax.php'),
            ));
        }

        public function regsiter_scripts() {

            wp_register_script('custom-shipping-per-product-frontend', plugins_url('assets/js/custom-shipping-per-product-frontend.js', PER_PRODUCT_SHIPPING_FILE), array('jquery', 'tbch_select2_410.min_js'), FALSE, true);
            wp_enqueue_script('custom-shipping-per-product-frontend');
            wp_localize_script('custom-shipping-per-product-frontend', 'product_params', array(
                'pps_nonce' => wp_create_nonce("pps-nonce-field"),
                'ajax_url' => admin_url('admin-ajax.php'),
            ));
        }

        /**
         * Ajax callback function for update cart on change shipping options.
         */
        public function select_custom_pps_shipping_method() {
            //check_ajax_referer( 'pps-nonce-field', 'security' );

            $pps_method = $_POST['pps_method'];
            WC()->session->set('pps_selected_shipping_method', $pps_method);
            $pps_selected_shipping_method = WC()->session->get('pps_selected_shipping_method');

            wc_clear_notices();
            global $woocommerce, $wpdb;
            $customer_country = $woocommerce->customer->get_country();
            $items = $woocommerce->cart->get_cart();
            //echo "<pre>"; print_r($items); die;
            foreach ($items as $cart_item) {
                $country_code_array = array();
                $country_code_query = $wpdb->get_results($wpdb->prepare("SELECT rule_country FROM {$wpdb->prefix}woocommerce_per_product_shipping_rules WHERE product_id = " . $cart_item['product_id'] . " AND rule_postcode = '" . $pps_method . "' ORDER BY rule_order;", $cart_item['product_id']), OBJECT);
                if (!empty($country_code_query)) {
                    foreach ($country_code_query as $key => $value) {
                        $country_code_array[] = $value->rule_country;
                    }
                }
                $country_code_array = implode(',', $country_code_array);
                $country_code_array = explode(',', $country_code_array);
                $country_code_array = array_unique($country_code_array);
                // echo $customer_country.'==';
                // echo '<pre>';print_r($country_code_array);echo "</pre>";

                if (!in_array($customer_country, $country_code_array)) {
                    wc_add_notice(__("Select shipping option below"), 'error');
                }
            }

            die();
        }

        /**
         * Clear woocommerce custom session variables after placed order.
         */
        public function clear_custom_woocommerce_session($order_id) {
            WC()->session->__unset('pps_selected_shipping_method');
            WC()->session->__unset('pps_custom_shipping_method_row');
            WC()->session->__unset('pps_shipping_charges');
        }

        /**
         * Update woocommerce custom shipping values after placed order.
         */
        public function update_custom_woocommerce_meta($order_id, $posted) {
            $pps_shipping_charges = WC()->session->get('pps_shipping_charges');
            $pps_shipping_method = WC()->session->get('pps_selected_shipping_method');
            update_post_meta($order_id, 'pps_shipping_charges', $pps_shipping_charges);
            update_post_meta($order_id, 'pps_shipping_method', $pps_shipping_method);

            $order = new WC_Order($order_id);
            foreach ($order->get_items('shipping') as $item_id => $item_obj) {
                wc_update_order_item_meta($item_id, 'instance_id', $pps_shipping_method);
            }
        }

        /**
         * Display woocommerce custom shipping values with breakdown on Email & Thankyou page.
         */
        public function hb_filter_shipping_value($shipping, $order) {
            $order_id = $order->get_id();
            $pps_shipping_charges = get_post_meta($order_id, 'pps_shipping_charges', true);
            if (!empty($pps_shipping_charges)) {
                $items = $order->get_items();

                $pps_shipping_cost_html = '';
                $pps_shipping_cost = $pps_shipping_charges['shipping_charges'];
                $single_item_line_cost = $pps_shipping_charges['single_item_line_cost'];
                $single_item_item_cost = $pps_shipping_charges['single_item_item_cost'];

                foreach ($items as $item_id => $item_data) {
                    $item_quantity = $item_data->get_quantity();
                    if (!empty($pps_shipping_cost) && count($items) > 1) {
                        foreach ($pps_shipping_cost as $shipping_cost) {
                            $pps_shipping_cost_html .= wc_price($shipping_cost);
                            $pps_shipping_cost_html .= ( $shipping_cost != end($pps_shipping_cost) ) ? ' + ' : ' = ';
                        }
                    } elseif (!empty($single_item_line_cost) && count($items) == 1 && $item_quantity > 1) {
                        $pps_shipping_cost_html .= wc_price($single_item_line_cost[0]);
                    }
                    if ($single_item_item_cost[0] != 0 && count($items) == 1 && $item_quantity > 1) {
                        $pps_shipping_cost_html .= ' + ' . wc_price($single_item_item_cost[0]) . ' = ';
                    }
                    $pps_shipping_total_cost = array_sum($pps_shipping_charges['shipping_charges']);
                    $pps_shipping_cost_html .= wc_price($pps_shipping_total_cost);
                    break;
                }
                $pps_shipping_method = get_post_meta($order_id, 'pps_shipping_method', true);
                $method_data = get_option('woocommerce_per_product_' . $pps_shipping_method . '_settings');
                if ($method_data['title']) {
                    $pps_shipping_cost_html .= ' <small class="shipped_via">via ' . $method_data['title'] . '</small>';
                }
                $shipping = $pps_shipping_cost_html;
            }
            return $shipping;
        }

        /**
         * Remove woocommerce shipping row in Admin panel.
         */
        public function hb_custom_order_get_items($items, $order, $types) {
            $order_id = $order->get_id();
            $pps_shipping_charges = get_post_meta($order_id, 'pps_shipping_charges', true);
            if (is_admin() && !wp_doing_ajax() && $types == array('shipping') && !empty($pps_shipping_charges)) {
                $items = array();
            }
            return $items;
        }

        /**
         * Display woocommerce custom shipping values with breakdown on Admin panel.
         */
        function hb_custom_admin_order_totals_after_tax($order_id) {
            $pps_shipping_charges = get_post_meta($order_id, 'pps_shipping_charges', true);
            if (!empty($pps_shipping_charges)) {
                $order = wc_get_order($order_id);
                $items = $order->get_items();

                $pps_shipping_cost_html = '';
                $pps_shipping_cost = $pps_shipping_charges['shipping_charges'];
                $single_item_line_cost = $pps_shipping_charges['single_item_line_cost'];
                $single_item_item_cost = $pps_shipping_charges['single_item_item_cost'];

                foreach ($items as $item_id => $item_data) {
                    $item_quantity = $item_data->get_quantity();
                    if (!empty($pps_shipping_cost) && count($items) > 1) {
                        foreach ($pps_shipping_cost as $shipping_cost) {
                            $pps_shipping_cost_html .= wc_price($shipping_cost);
                            $pps_shipping_cost_html .= ( $shipping_cost != end($pps_shipping_cost) ) ? ' + ' : ' = ';
                        }
                    } elseif (!empty($single_item_line_cost) && count($items) == 1 && $item_quantity > 1) {
                        $pps_shipping_cost_html .= wc_price($single_item_line_cost[0]);
                    }
                    if ($single_item_item_cost[0] != 0 && count($items) == 1 && $item_quantity > 1) {
                        $pps_shipping_cost_html .= ' + ' . wc_price($single_item_item_cost[0]) . ' = ';
                    }
                    $pps_shipping_total_cost = array_sum($pps_shipping_charges['shipping_charges']);
                    $pps_shipping_cost_html .= wc_price($pps_shipping_total_cost);
                    break;
                }
                $label = __('Shipping', 'woocommerce');
                $value = $pps_shipping_cost_html;
                if (isset($_GET['post'])) {
                    ?>
                    <tr>
                        <td class="label"><?php echo $label; ?>:</td>
                        <td width="1%"></td>
                        <td class="custom-total"><?php echo $value; ?></td>
                    </tr>
                    <?php
                } else {
                    ?>
                    <tr>
                        <th class="label"><span class="wcfmfa fa-question no_mob img_tip" data-tip="<?php _e('This is the shipping and handling total costs for the order.', 'woocommerce-shipping-per-product'); ?>"></span> <?php echo $label; ?>:</th>
                        <td width="1%"></td>
                        <td class="total"><?php echo $value; ?></td>
                    </tr>
                    <?php
                }
            }
        }

        /**
         * Disable Per Product Shipping plugin update notification.
         */
        public function hb_remove_update_notifications($value) {
            if (isset($value) && is_object($value)) {
                unset($value->response[plugin_basename(__FILE__)]);
            }
            return $value;
        }

        /**
         * Loads any class that needs to check for WC loaded.
         *
         * @since 2.2.13
         */
        public function load_post_wc_class() {
            require_once __DIR__ . '/includes/class-wc-shipping-per-product-privacy.php';
        }

        /**
         * Installer.
         */
        public function install() {
            include_once 'installer.php';
        }

        /**
         * Load shipping method class and related hooks.
         */
        public function load_shipping_method() {
            add_filter('woocommerce_cart_shipping_packages', array($this, 'split_shipping_packages_per_product'));
            add_filter('woocommerce_shipping_methods', array($this, 'register_shipping_method'));
        }

        /**
         * Translation.
         */
        public function load_plugin_textdomain() {
            load_plugin_textdomain('woocommerce-shipping-per-product', false, dirname(plugin_basename(__FILE__)) . '/languages/');
        }

        /**
         * Filter plugin action links.
         *
         * @since 2.2.9
         * @version 2.2.9
         *
         * @param array $links Plugin action links.
         *
         * @return array Plugin action links.
         */
        public function plugin_action_links($links) {
            $plugin_links = array(
                '<a href="' . admin_url('admin.php?page=wc-settings&tab=shipping&section=per_product') . '">' . __('Settings', 'woocommerce-shipping-per-product') . '</a>',
                '<a href="https://docs.woocommerce.com/">' . __('Support', 'woocommerce-shipping-per-product') . '</a>',
                '<a href="https://docs.woocommerce.com/document/per-product-shipping/">' . __('Docs', 'woocommerce-shipping-per-product') . '</a>',
            );
            return array_merge($plugin_links, $links);
        }

        /**
         * Show row meta on the plugin screen.
         *
         * @param array  $links Plugin Row Meta.
         * @param string $file  Plugin Base file.
         *
         * @return array
         */
        public function plugin_row_meta($links, $file) {
            if (plugin_basename(__FILE__) === $file) {
                $row_meta = array(
                    'docs' => '<a href="' . esc_url(apply_filters('woocommerce_per_product_shipping_docs_url', 'http://docs.woothemes.com/document/per-product-shipping/')) . '" title="' . esc_attr(__('View Documentation', 'woocommerce-shipping-per-product')) . '">' . __('Docs', 'woocommerce-shipping-per-product') . '</a>',
                    'support' => '<a href="' . esc_url(apply_filters('woocommerce_per_product_shipping_support_url', 'http://support.woothemes.com/')) . '" title="' . esc_attr(__('Visit Premium Customer Support Forum', 'woocommerce-shipping-per-product')) . '">' . __('Premium Support', 'wc_shipping_per_products') . '</a>',
                );
                return array_merge($links, $row_meta);
            }
            return (array) $links;
        }

        /**
         * Register the importer.
         */
        public function register_importer() {
            if (defined('WP_LOAD_IMPORTERS')) {
                register_importer('woocommerce_per_product_shipping_csv', __('WooCommerce Per-product shipping rates (CSV)', 'woocommerce-shipping-per-product'), __('Import <strong>per-product shipping rates</strong> to your store via a csv file.', 'woocommerce-shipping-per-product'), array($this, 'importer'));
            }
        }

        /**
         * Load the importer.
         */
        public function importer() {
            require_once ABSPATH . 'wp-admin/includes/import.php';

            if (!class_exists('WP_Importer')) {
                $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
                if (file_exists($class_wp_importer)) {
                    require $class_wp_importer;
                }
            }

            include_once 'includes/class-wc-shipping-per-product-importer.php';

            $importer = new WC_Shipping_Per_Product_Importer();
            $importer->dispatch();
        }

        /**
         * Register the shipping method.
         *
         * @param array $methods Shipping methods.
         *
         * @return array Shipping methods.
         */
        public function register_shipping_method($methods) {
            include_once 'includes/class-wc-shipping-per-product.php';
            $methods['per_product'] = 'WC_Shipping_Per_Product';

            // For backwards compatibility with 2.2.x installations we load the legacy shipping method if it's enabled.
            $use_legacy = $this->use_legacy_shipping_method();

            if (false !== $use_legacy) {
                // In this case the legacy shipping method is not enabled but some products may be using the deprecated _per_product_shipping_add_to_all option which will be checked in this filter.
                add_filter('woocommerce_package_rates', array($this, 'adjust_package_rates'), 10, 2);
            }

            if (true === $use_legacy) {
                include_once 'includes/class-wc-shipping-legacy-per-product.php';
                $methods['legacy_per_product'] = 'WC_Shipping_Legacy_Per_Product';
            }

            return $methods;
        }

        /**
         * Check if we should be using the legacy shipping method.
         *
         * @return bool|null
         */
        public function use_legacy_shipping_method() {

            // Don't use legacy if a new method has been added to a zone.
            $data_store = WC_Data_Store::load('shipping-zone');
            $raw_zones = $data_store->get_zones();
            $raw_zones[] = (object) array('zone_id' => 0); // Wee need to add zone 0 which is ' Locations not covered by your other zones'.
            // We have to use raw data or an infinite loop will occur.
            foreach ($raw_zones as $raw_zone) {
                $raw_methods = $data_store->get_methods($raw_zone->zone_id, true);
                foreach ($raw_methods as $raw_method) {
                    if ('per_product' === $raw_method->method_id) {
                        return false;
                    }
                }
            }

            $options = get_option('woocommerce_per_product_settings');
            if ($options && isset($options['enabled']) && 'yes' === $options['enabled']) {
                return true;
            }

            return null; // Return null because we may still need to adjust woocommerce_package_rates.
        }

        /**
         * Adjust package rates.
         *
         * @param array $rates   Rates.
         * @param array $package Package.
         *
         * @return array
         */
        public function adjust_package_rates($rates, $package) {
            $_tax = new WC_Tax();
            if ($rates) {
                foreach ($rates as $rate_id => $rate) {
                    // Skip free shipping.
                    if (0 === (int) $rate->cost && apply_filters('woocommerce_per_product_shipping_skip_free_method_' . $rate->method_id, true)) {
                        continue;
                    }
                    // Skip self.
                    if ('legacy_per_product' === $rate->method_id) {
                        continue;
                    }
                    if (count($package['contents']) > 0) {
                        foreach ($package['contents'] as $item_id => $values) {
                            if ($values['quantity'] > 0) {
                                if ($values['data']->needs_shipping()) {
                                    $item_shipping_cost = 0;
                                    $rule = false;
                                    if ($values['variation_id']) {
                                        $rule = woocommerce_per_product_shipping_get_matching_rule($values['variation_id'], $package, false);
                                    }
                                    if (false === $rule) {
                                        $rule = woocommerce_per_product_shipping_get_matching_rule($values['product_id'], $package, false);
                                    }
                                    if (empty($rule)) {
                                        continue;
                                    }
                                    $item_shipping_cost += (float) $rule->rule_item_cost * (int) $values['quantity'];
                                    $item_shipping_cost += (float) $rule->rule_cost;
                                    $rate->cost += $item_shipping_cost;
                                    $rate_options = get_option('woocommerce_' . $rate->get_method_id() . '_' . $rate->get_instance_id() . '_settings', true);
                                    if (isset($rate_options['tax_status']) && 'taxable' === $rate_options['tax_status']) {
                                        $tax_rates = $_tax->get_shipping_tax_rates($values['data']->get_tax_class());
                                        $item_taxes = $_tax->calc_shipping_tax($item_shipping_cost, $tax_rates);
                                        $taxes = array();
                                        // Sum the item taxes.
                                        foreach (array_keys($rate->taxes + $item_taxes) as $key) {
                                            $taxes[$key] = ( isset($item_taxes[$key]) ? $item_taxes[$key] : 0 ) + ( isset($rate->taxes[$key]) ? $rate->taxes[$key] : 0 );
                                        }
                                        $rate->set_taxes($taxes);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $rates;
        }

        /**
         * Splits products with per product shipping enabled into separate packages.
         *
         * @param array $packages Packages to maybe split.
         * @return array
         */
        public function split_shipping_packages_per_product(array $packages) {

            $new_packages = array();
            foreach ($packages as $package_id => $package) {

                /** @var $per_product_shipping_method WC_Shipping_Per_Product */
                $per_product_shipping_method = null;

                $zone = WC_Shipping_Zones::get_zone_matching_package($package);

                foreach ($zone->get_shipping_methods(true) as $method) {
                    if ($method instanceof WC_Shipping_Per_Product) {
                        $per_product_shipping_method = $method;
                        break;
                    }
                };

                if (null === $per_product_shipping_method) {
                    continue; // Per Product shipping not enabled for the zone this package is going to.
                }

                $new_package = array();

                foreach ($package['contents'] as $item_id => $values) {

                    if ($per_product_shipping_method->is_per_product_shipping_product($values, $package)) {
                        // Item shipping is calculated per-product, split it off and make new package.
                        $new_package['contents'][$item_id] = $values;
                        unset($packages[$package_id]['contents'][$item_id]);
                    }
                }

                if (!empty($new_package)) {
                    // Recalculate totals.
                    $new_package['contents_cost'] = array_sum(wp_list_pluck($new_package['contents'], 'line_total'));
                    $package['contents_cost'] = array_sum(wp_list_pluck($package['contents'], 'line_total'));

                    // Copy field from original package.
                    $new_package['applied_coupons'] = $package['applied_coupons'];
                    $new_package['user'] = $package['user'];
                    $new_package['destination'] = $package['destination'];
                    $new_package['cart_subtotal'] = $package['cart_subtotal'];
                    $new_package['ship_via'] = apply_filters('woocommerce_per_product_shipping_ship_via', array(WC_Shipping_Per_Product::METHOD_ID, 'free_shipping', 'local_pickup'));

                    $new_packages[] = $new_package;

                    if (empty($packages[$package_id]['contents'])) {
                        unset($packages[$package_id]); // remove empty packages.
                    }
                }
            }

            return array_merge($new_packages, $packages);
        }

    }

    new WC_Shipping_Per_Product_Init();
}

