jQuery(function ($) {
  $('.wooatm-help').click(function(){
    $('#contextual-help-link').click();
  });
  
  $('#tab-panel-wooatm_help input').click(function(){ 
    $(this).select();
  });
    
  function createIconpicker() {
    var iconPicker = $('.wooatm-icon-picker').fontIconPicker({
                theme: 'fip-bootstrap'
            }),
        icomoon_json_icons = [],
        icomoon_json_search = [];
        
        // Get the JSON file
        $.ajax({
          url: wooatm.icons_path,
          type: 'GET',
          dataType: 'json'
        })
        .done(function(response) {
          // Get the class prefix
          var classPrefix = response.preferences.fontPref.prefix;

          $.each(response.icons, function(i, v) {
            // Set the source
            icomoon_json_icons.push(classPrefix + v.properties.name);

            // Create and set the search source
            if (v.icon && v.icon.tags && v.icon.tags.length) {
              icomoon_json_search.push(v.properties.name + ' ' + v.icon.tags.join(' '));
            } else {
              icomoon_json_search.push(v.properties.name);
            }
          });

          setTimeout(function() {
            // Set new fonts
            iconPicker.setIcons(icomoon_json_icons, icomoon_json_search);
          }, 1000);
        })
        .fail(function() {
          // Show error message and enable
          alert('Failed to load the icons, Please check file permission.');
        });
    }
  createIconpicker();
});
