jQuery(function ($) {
  $('#wooatm-fields').on( 'click', '.wooatm-hndle,.wooatm-handlediv', function(){
    $(this).parent().find('.wooatm-inside').slideToggle();
    $(this).parent().toggleClass('wooatm-opened');
  });
  
  $('#wooatm-fields').on( 'blur', '.wooatm-title', function(){
    var textval = $(this).val() == '' ? 'Enter tab/accordion title' : $(this).val();
    $(this).parents('li').find('.wooatm-hndle').text(textval);
  });


  $( '#wooatm_add' ).click(function(){
    var id = $('.wooatm-title').length + 1;
    var ed_id = 'wooatm_editor_'+id;
    $('.wooatm-add-new img').show();
    
    $.ajax({
      url: ajaxurl,
      type: 'POST',
      dataType: 'html',
      data: { editor: ed_id, id: id, action: 'wooatm_editor' }
    }).done(function ( response ) {
      $('#wooatm-fields').append(response);
      tinyMCE.init(tinyMCEPreInit.mceInit[ed_id]);
      tinyMCEPreInit.qtInit[ed_id] = JSON.parse( JSON.stringify(tinyMCEPreInit.qtInit[ ed_id ] ) );
      tinyMCEPreInit.qtInit[ed_id].id = ed_id;

      setTimeout(function(){
        new QTags(tinyMCEPreInit.qtInit[ ed_id ]);
            QTags._buttonsInit();
            switchEditors.go( ed_id, 'html' );
      }, 1000);
      $('.wooatm-add-new img').hide();
    });
  });
  
  $( '#wooatm-fields' ).on( 'click', '.wooatm-remove', function(){
    if( confirm('Do you really want to delete this tab/accordion?' ) )
      $(this).parent().slideUp('slow',function(){ $(this).remove(); });
  });

  jQuery( "#wooatm-fields" ).sortable({
    cursor: "move",
    scroll: false,
    start:function( event, ui ){
      tinymce.remove();
    },
    stop:function( event, ui ){
      jQuery('#wooatm-fields textarea').each(function(){
        ed_id = jQuery(this).attr('id');
        switchEditors.go( ed_id, tinyMCEPreInit.mceInit[ed_id].mode );
      });
    },
    handle: ".wooatm-hndle",
    placeholder: "wooatm-sort-placeholder",
    opacity: '0.8'
  });
});
