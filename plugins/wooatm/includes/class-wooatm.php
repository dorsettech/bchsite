<?php

class Woo_Atm {

  /**
  * Bootstraps the class and hooks required actions & filters.
  *
  */

  private $options;

  public function __construct() {

    $this->options = get_option( 'wooatm' );

    //Check if woocommerce plugin is installed.
    add_action( 'admin_notices', array( $this, 'check_required_plugins' ) );

    //Add setting link for the admin settings
    add_filter( "plugin_action_links_".WOOATM_BASE, array( $this, 'wooatm_settings_link' ) );

    //Add backend settings
    add_filter( 'woocommerce_get_settings_pages', array( $this, 'wooatm_settings_class' ) );

    //return if wooatm is not enabled
    if( $this->options['enabled'] != 'yes' ) return;

    //Add css and js files for the tabs
    add_action( 'wp_enqueue_scripts',  array( $this, 'wooatm_enque_scripts' ) );

    //Replace default tabs with woocommerce tabs
    add_action( 'plugins_loaded', array( $this, 'add_remove_tabs' ), 10 );

    // //Add wooatm custom tabs
    add_filter( 'woocommerce_product_tabs', array( $this, 'wooatm_add_tabs' ), 98 );

    // //Add wooatm global tabs
    add_action( 'wp_ajax_wooatm_global_tab_editor', array( $this, 'wooatm_global_tab_editor' ) );
  }


  /**
  *
  * Add necessary js and css files for the popup
  *
  */
  public function wooatm_enque_scripts() {
    //Add the js and css files only on the product single page.
    if( !is_product() ) return; 
    
    $css  = "li.resp-tab-item,h2.resp-accordion{ border-color:{$this->options['border_color']};color:{$this->options['title_color']};background:{$this->options['bg']};font-size:{$this->options['title_font']}px; }";
    $css  .= "body .resp-tab-active,.resp-vtabs li.resp-tab-active{ color:{$this->options['title_active']};background:{$this->options['active_bg']}; }";
    $css  .= ".resp-vtabs li.resp-tab-item,.resp-vtabs .resp-tabs-container,.tab1 .resp-tabs-list li,.resp-vtabs li.resp-tab-active,.resp-tab-content,.resp-easy-accordion .resp-tab-content,h2.resp-accordion:first-child,.resp-easy-accordion .resp-tab-content:last-child{ border-color:{$this->options['border_color']}; }li.resp-tab-item{ margin-right:{$this->options['gap_tab']}px; }body .resp-tabs-container, .resp-tab-content{ color:{$this->options['text_color']};background:{$this->options['content_bg']}; }";
    $lh = '';
    
    if( $this->options['title_font'] > 16 ) $lh = 'line-height:' . ( $this->options['title_font'] +  $this->options['acc_icon_size'] + 5 ).'px;';
      $css  .= ".resp-arrow{ {$lh}font-size:{$this->options['acc_icon_size']}px;float:{$this->options['icon_pos']};color:{$this->options['arrow_color']}; }";
      $css  .= ".resp-tab-active .resp-arrow{ color:{$this->options['arrow_active']};}.resp-vtabs li.resp-tab-item{ margin-bottom:{$this->options['gap_tab']}px;}";
      $css  .= "ul.resp-tabs-list{ text-align:{$this->options['tab_align']};}";
      if( $this->options['gap'] > 0 )
        $css  .= "h2.resp-accordion{ margin-top:{$this->options['gap']}px;border-top: 1px solid {$this->options['border_color']}; }";
      if( $this->options['tab_style'] == 'tab2' )
        $css  .= ".tab2 .resp-tabs-list{border-color:{$this->options['active_bg']};}.tab2 li.resp-tab-active{border-color:{$this->options['bg']};}";
      if( $this->options['acc_style'] == 'style2' )
        $css  .= "h2.resp-tab-active{color:{$this->options['title_color']};}.style2 .resp-tabs-container{background:{$this->options['content_bg']}!important;}.wootitle{border-color:{$this->options['border_color']};}.resp-arrow{text-align:{$this->options['icon_pos']};}.style2 h2.resp-accordion:hover{background:{$this->options['active_bg']}!important;color:{$this->options['title_active']};}";

      $breakpoint_width = $this->options['accordion_breakpoint'];

      if( !empty($breakpoint_width) && $breakpoint_width > 0 ):
      ?>
      <style type="text/css">
        @media only screen and (max-width: <?php echo $breakpoint_width;?>px) {
          h2.resp-accordion { display: block !important; }
          ul.resp-tabs-list { display: none; }
        }
      </style>
      <?php
      endif;

    //Add responsive tabs css 
    wp_enqueue_style( 'wooatm_icons', plugins_url( 'assets/icons/style.css', WOOATM_FILE ), '1.0' );
    wp_enqueue_style( 'wooatm-responsivetab', plugins_url( 'assets/css/wooatm-tabs.css', WOOATM_FILE ) );
    wp_enqueue_script( 'wooatm-tabs', plugins_url( 'assets/js/wooatm-tabs.js', WOOATM_FILE ) , array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('wooatm-custom', plugins_url( 'assets/js/custom.js', WOOATM_FILE ), array( 'jquery', 'wooatm-tabs' ), '1.0.0', true );
      
    wp_localize_script('wooatm-custom',  'wooatm', array(
      'style'                 => $this->options['style'],
      'openicon'              => $this->options['acc_icon'],
      'closeicon'             => $this->options['acc_active'],
      'accordions_show_like'  => $this->options['accordions_show_like']
    ));


    wp_localize_script('wooatm-tabs', 'wooatm', 
      array(
        'accordions_show_like' => $this->options['accordions_show_like'],
      ));
    wp_add_inline_style( 'wooatm-responsivetab', $css );
  }

  /**
  *
  * Check if woocommerce is installed and activated and if not
  * activated then deactivate woocommerce mailchimp discount.
  *
  */
  public function check_required_plugins() {
    
    //Check if woocommerce is installed and activated
    if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) { ?>
      <div id="message" class="error">
        <p>WooCommerce Accordions and Tabs Manager requires <a href="http://www.woothemes.com/woocommerce/" target="_blank">WooCommerce</a> to be activated in order to work. Please install and activate <a href="<?php echo admin_url('/plugin-install.php?tab=search&amp;type=term&amp;s=WooCommerce'); ?>" target="">WooCommerce</a> first.</p>
      </div>
      <?php
        deactivate_plugins( '/wooatm/wooatm.php' );
    }

  }

  /**
  * Add new link for the settings under plugin links
  *
  * @param array   $links an array of existing links.
  * @return array of links  along with wooatm settings link.
  *
  */
  public function wooatm_settings_link($links) {
    $settings_link = '<a href="'.admin_url('admin.php?page=wc-settings&tab=wooatm').'">Settings</a>'; 
        array_unshift( $links, $settings_link ); 
        return $links; 
  }

  /**
  * Add new admin setting page for wooatm settings.
  *
  * @param array   $settings an array of existing setting pages.
  * @return array of setting pages along with wooatm settings page.
  *
  */
  public function wooatm_settings_class( $settings ) {
    $settings[] = include 'class-wc-settings-wooatm.php';
    return $settings;
  }

  /**
  * Replace default woocommerce tabs with wooatm tabs/accordions.
  *
  * @param void
  *
  */
  public function add_remove_tabs(){
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
      
    if ( isset($this->options['position']) && $this->options['position'] == 'after_product_description') {
      add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'wooatm_output_tabs'), 10 );
    }
    else{
      add_action( 'woocommerce_after_single_product_summary', array( $this, 'wooatm_output_tabs'), 10 );
    }
  }


  /**
  * Output wooatm tabs/accordions.
  *
  * @param void
  *
  */
  public function wooatm_output_tabs(){
    include_once( WOOATM_PATH . '/includes/accordion-tabs.php' );
  }


  /**
  * Add our custom tabs with woocommerce default tabs
  *
  * @param array $tabs an array of all the existing tabs.
  * @return array $tabs an array with custom tabs and default woocommerce tabs
  *
  */
  public function wooatm_add_tabs( $tabs ){
    global $post;
    $wooatm_fields = get_post_meta( $post->ID, 'wooatm_fields', true );
    $global_tabs = get_option('wooatm_global_tabs');
    $tab_positions = get_option('wooatm_tab_position');
    $options = get_option('wooatm');

    if( !empty($tab_positions) ) {
      $tabs_array = array();
      foreach( $tab_positions as $key => $val ) {
        array_push($tabs_array, $key);
      }
    }

    if( $options['enable_global_tabs'] == 'yes' ) :
      if( is_array($global_tabs) ) {
        $i = 1;
        $key = array_search('Global', $tabs_array);
        $tab_level = (intval($key) + intval(1)) * intval(100) ;
        
        foreach( $global_tabs as $global_tab ) {
          $priority = $tab_level + $i;
          $tabs[ 'wooatm_global_tab_' . $i ] = array(
                      'title'    => do_shortcode( $global_tab['title'] ),
                      'priority' => $priority,
                      'callback' => array( $this, 'wooatm_tab_content' ),
                      'content'  => stripcslashes($global_tab['content'])
                  );
          $i++;
        }
      }
    endif;

        
    if( is_array( $wooatm_fields ) ) {
      $count = 1;
      $key = array_search('Custom Tab', $tabs_array);
      $tab_level = (intval($key) + intval(1)) * intval(100) ;
      
      foreach( $wooatm_fields as $field ){
        $priority = $tab_level + $count;
        $tabs[ 'wooatm_tab_' . $count ] = array(
                'title'    => do_shortcode( $field['title'] ),
                'priority' => $priority,
                'callback' => array( $this, 'wooatm_tab_content' ),
                'content'  => $field['content']
            );
          $count++;
      }
    }

    if (($key = array_search('Global', $tabs_array)) !== false) {
      unset($tabs_array[$key]);
    }

    if (($key = array_search('Custom Tab', $tabs_array)) !== false) {
          unset($tabs_array[$key]);
        }

    foreach( $tabs_array as $key => $val  ) {
      $tab_level = (intval($key) + intval(1)) * intval(100) ;
      $tab_name = strtolower($val);
      $tab_name = str_replace(' ', '_', $tab_name);
      $tabs[$tab_name]['priority'] = $tab_level;
    }
    return $tabs;
  }

  /**
  * WooCommmerce tab content callback
  *
  * @param string $key the id for the tab
  * @param array $tab currently processing tab
  * @return content for the current tab 
  *
  */
  public function wooatm_tab_content( $key, $tab ){
    $content = apply_filters( 'the_content', $tab['content'] );
    echo $content;
  }

  public function wooatm_global_tab_content( $key, $tab ){
    $content = apply_filters( 'the_content', $tab['content'] );
    echo $content;
  }    

  public function wooatm_global_tab_editor() { 
    $id = $_POST['editor'];
    $count = $_POST['id'];
  ?>
  <li>
    <div class="wooatm-handlediv" title="Click to toggle"><br></div>
    <div class="wooatm-remove" title="Remove"><br></div>
    <h3 class="wooatm-hndle"><span>Enter Tab/Accordion title</span></h3>
    <div class="wooatm-inside">
      <input type="text" class="wooatm-title" placeholder="Enter Tab/Accordion title here" name="wooatm[<?php echo $count; ?>][title]">
        <?php 
        wp_editor( '', $id, array( 'textarea_name' => 'wooatm['.$count.'][content]' ) ); 
        $qt_settings = '{}';
        
        if ( !empty( $this->quicktags_settings ) ) {
          $qt_settings = json_encode( $this->quicktags_settings );
          $qt_settings = "'$id':{$qt_settings},";
          $qt_settings = '{' . trim($qt_settings, ',') . '}';
        }

        $tinymce_settings = '{}';
        if ( !empty( $this->mce_settings ) ){
          $tinymce_settings = $this->_parse_init( $this->mce_settings );
            $tinymce_settings = "'$id':{$tinymce_settings},";
            $tinymce_settings = '{' . trim($tinymce_settings, ',') . '}';
        }
        ?>
    </div>
  </li>
  
  <script type="text/javascript">
    tinyMCEPreInit.mceInit = jQuery.extend( tinyMCEPreInit.mceInit, <?php echo $tinymce_settings; ?>);
    tinyMCEPreInit.qtInit = jQuery.extend( tinyMCEPreInit.qtInit, <?php echo $qt_settings; ?>);
  </script>
  <?php exit;
  }
 
}