<?php
/**
 * Single Product accordion/tabs
 *
 * @author  MagniGenie
 * @package WooAtm
 * @version 1.4
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$options = get_option( 'wooatm' );
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs )  ) : ?>

  <div id="wooatm-tabs-container" class="woocommerce-tabs wc-tabs-wrapper wooatm-tabs <?php echo $options['acc_style'] . ' ' . $options['tab_style'];?>">
    <ul class="resp-tabs-list">
      <?php 
        foreach ( $tabs as $key => $tab ) :
          if( array_key_exists('title', $tab) ) :
      ?>
        <li class="<?php echo esc_attr( $key ); ?>_tab">
          <?php 
            echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); 
          ?>
        </li>
      <?php 
          endif;
        endforeach; 
      ?>
    </ul>
    <div class="resp-tabs-container">
      <?php 
        foreach ( $tabs as $key => $tab ) : 
          if( array_key_exists('title', $tab) ) :
          ?>
        <div id="tab-<?php echo esc_attr( $key ); ?>">
          <?php call_user_func( $tab['callback'], $key, $tab ); ?>
        </div>
      <?php 
          endif;
        endforeach; 
      ?>
    </div>
  </div>

<?php endif; ?>