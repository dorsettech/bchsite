<?php

class Woo_Atm_Fields {

    /**
     * Bootstraps the class and hooks required actions & filters.
     *
     */
    protected $mce_settings = null;
    protected $quicktags_settings = null;

    public function __construct() {
        add_action( 'admin_enqueue_scripts', array( $this, 'add_admin_scripts' ) );
        $options = get_option( 'wooatm' );
        if( isset( $options['wooatm_fields'] ) && $options['wooatm_fields'] == 'yes' ){
            add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_before_init' ) );
            add_filter( 'quicktags_settings', array( $this, 'quicktags_settings' ) );
            add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
            add_action( 'save_post', array( $this, 'save_postmeta' ) );
            add_action( 'wp_ajax_wooatm_editor', array( $this, 'wooatm_editor' ) );
            add_action( 'wp_ajax_global_wooatm_editor', array( $this, 'global_wooatm_editor' ) );
        }

    }

    /*
    *
    *
    */
    public function tiny_mce_before_init( $settings ){
        $this->mce_settings = $settings;
        return $settings;
    }

    public function quicktags_settings( $settings ){
        $this->quicktags_settings = $settings;
        return $settings;
    }

    function add_metabox() {
        add_meta_box( 'wooatm', 'WOOATM Custom Tabs', array( $this, 'metabox_callback' ), 'product', 'normal', 'high' );
    }

    public function _parse_init( $init ) {
        $options = '';

        foreach ( $init as $k => $v ) {
            if ( is_bool($v) ) {
                $val = $v ? 'true' : 'false';
                $options .= $k . ':' . $val . ',';
                continue;
            } elseif ( !empty($v) && is_string($v) && ( ('{' == $v{0} && '}' == $v{strlen($v) - 1}) || ('[' == $v{0} && ']' == $v{strlen($v) - 1}) || preg_match('/^\(?function ?\(/', $v) ) ) {
                $options .= $k . ':' . $v . ',';
                continue;
            }
            $options .= $k . ':"' . $v . '",';
        }

        return '{' . trim( $options, ' ,' ) . '}';
    }

    function metabox_callback( $post ) {
        $wooatm_fields = get_post_meta( $post->ID, 'wooatm_fields', true );
        wp_nonce_field( 'wooatm_nonce', 'wooatm_nonce_field' );
        ?>
        <div class="wooatm-container">
            <ul id="wooatm-fields">
                <?php 
                    if( is_array( $wooatm_fields ) ):
                        $count = 1;
                        foreach( $wooatm_fields as $field ) :
                 ?>
                    <li>
                        <div class="wooatm-handlediv" title="Click to toggle"><br></div>
                        <div class="wooatm-remove" title="Remove"><br></div>
                        <h3 class="wooatm-hndle"><span><?php echo $field['title']; ?></span></h3>
                        <div class="wooatm-inside">
                            <input type="text" class="wooatm-title" value="<?php echo $field['title']; ?>" placeholder="Enter Tab/Accordion title here" name="wooatm[<?php echo $count; ?>][title]">
                            <?php wp_editor( $field['content'], 'wooatm_editor_'.$count, array( 'textarea_name' => 'wooatm[' . $count . '][content]', 'textarea_rows' => 6 ) ); ?>
                        </div>
                    </li>
                <?php $count++; endforeach; endif; ?>
            </ul>
            <div class="wooatm-add-new">
                <input type="button" id="wooatm_add" class="button button-primary button-large" value="Add New" accesskey="n">
                <img src="<?php echo admin_url( 'images/spinner.gif' );?>" alt="loading" class="wooatm-loading" />
            </div>
        </div>
        <?php
    }

    function save_postmeta( $post_id ) {
        // Bail if we're doing an auto save
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
        
        // if our nonce isn't there, or we can't verify it, bail
        if( !isset( $_POST['wooatm_nonce_field'] ) || !wp_verify_nonce( $_POST['wooatm_nonce_field'], 'wooatm_nonce' ) ) return;
        
        // if our current user can't edit this post, bail
        if( !current_user_can( 'edit_post' ) ) return;
        
        // Probably a good idea to make sure your data is set
        $wooatm_fields = isset( $_POST['wooatm'] ) ? $_POST['wooatm'] : '';
        update_post_meta( $post_id, 'wooatm_fields', $wooatm_fields );
    }

    public function add_admin_scripts( $hook ){
        if( get_post_type() == 'product' ){
            wp_enqueue_style( 'wooatm_admin', plugins_url( 'assets/css/admin.css', WOOATM_FILE )  );
            wp_enqueue_script( 'wooatm_admin', plugins_url( 'assets/js/admin.js', WOOATM_FILE ), array( 'jquery-ui-sortable' ) );
        }
        else if( $hook == 'woocommerce_page_wc-settings' && !empty( $_GET['tab'] ) && $_GET['tab'] == 'wooatm' ){
            wp_enqueue_media();
            wp_enqueue_script( 'quicktags' );
            wp_enqueue_style( 'wooatm_icons', plugins_url( 'assets/icons/style.css', WOOATM_FILE ), '1.0' );
            wp_enqueue_style( 'wooatm_admin_style', plugins_url( 'assets/css/wooatm-admin.css', WOOATM_FILE ), '1.0' );
            wp_enqueue_style( 'wooatm_iconpicker', plugins_url( 'assets/css/jquery.fonticonpicker.min.css', WOOATM_FILE ), array(),  '1.0' );
            wp_enqueue_script( 'wooatm-icon-picker', plugins_url( 'assets/js/jquery.fonticonpicker.min.js', WOOATM_FILE ), array( 'jquery' ), '1.0' );
            wp_enqueue_script( 'wooatm_admin', plugins_url( 'assets/js/admin-settings.js', WOOATM_FILE ), array( 'wooatm-icon-picker' ) );
            $icons_path = array( 'icons_path' => plugins_url( 'assets/icons/selection.json', WOOATM_FILE ) );
            wp_localize_script( 'wooatm_admin', 'wooatm' , $icons_path );
        }
    }

    public function wooatm_editor(){
        $id = $_POST['editor'];
        $count = $_POST['id'];
    ?>
        <li>
            <div class="wooatm-handlediv" title="Click to toggle"><br></div>
            <div class="wooatm-remove" title="Remove"><br></div>
            <h3 class="wooatm-hndle"><span>Enter Tab/Accordion title</span></h3>
            <div class="wooatm-inside">
                <input type="text" class="wooatm-title" placeholder="Enter Tab/Accordion title here" name="wooatm[<?php echo $count; ?>][title]">
                <?php wp_editor( '', $id, array( 'textarea_name' => 'wooatm['.$count.'][content]' ) ); ?>
                <?php
                    $qt_settings = '{}';
                    if ( !empty( $this->quicktags_settings ) ) {
                        $qt_settings = json_encode( $this->quicktags_settings );
                        $qt_settings = "'$id':{$qt_settings},";
                        $qt_settings = '{' . trim($qt_settings, ',') . '}';
                    }

                    $tinymce_settings = '{}';
                    if ( !empty( $this->mce_settings ) ){
                        $tinymce_settings = $this->_parse_init( $this->mce_settings );
                        $tinymce_settings = "'$id':{$tinymce_settings},";
                        $tinymce_settings = '{' . trim($tinymce_settings, ',') . '}';
                    }
                ?>
            </div>
        </li>
        <script type="text/javascript">
            tinyMCEPreInit.mceInit = jQuery.extend( tinyMCEPreInit.mceInit, <?php echo $tinymce_settings; ?>);
            tinyMCEPreInit.qtInit = jQuery.extend( tinyMCEPreInit.qtInit, <?php echo $qt_settings; ?>);
        </script>
    <?php exit; 
    }

    public function global_wooatm_editor(){
        $id = $_POST['editor'];
        $count = $_POST['id'];
    ?>
        <li>
            <div class="wooatm-handlediv" title="Click to toggle"><br></div>
            <div class="wooatm-remove" title="Remove"><br></div>
            <h3 class="wooatm-hndle"><span>Enter Tab/Accordion title</span></h3>
            <div class="wooatm-inside">
                <input type="text" class="wooatm-title" placeholder="Enter Tab/Accordion title here" name="wooatm[global_tabs][<?php echo $count; ?>][title]">
                <?php wp_editor( '', $id, array( 'textarea_name' => 'wooatm[global_tabs]['.$count.'][content]' ) ); ?>
                <?php
                    $qt_settings = '{}';
                    if ( !empty( $this->quicktags_settings ) ) {
                        $qt_settings = json_encode( $this->quicktags_settings );
                        $qt_settings = "'$id':{$qt_settings},";
                        $qt_settings = '{' . trim($qt_settings, ',') . '}';
                    }

                    $tinymce_settings = '{}';
                    if ( !empty( $this->mce_settings ) ){
                        $tinymce_settings = $this->_parse_init( $this->mce_settings );
                        $tinymce_settings = "'$id':{$tinymce_settings},";
                        $tinymce_settings = '{' . trim($tinymce_settings, ',') . '}';
                    }
                ?>
            </div>
        </li>
        <script type="text/javascript">
            tinyMCEPreInit.mceInit = jQuery.extend( tinyMCEPreInit.mceInit, <?php echo $tinymce_settings; ?>);
            tinyMCEPreInit.qtInit = jQuery.extend( tinyMCEPreInit.qtInit, <?php echo $qt_settings; ?>);
        </script>
    <?php exit; 
    }    
}
new Woo_Atm_Fields;