<?php
/**
 * WooCommerce Accordions and Tab Manager
 *
 * @author 		Magnigenie
 * @category 	Admin
 * @version     1.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if (  class_exists( 'WC_Settings_Page' ) ) :

/**
 * WC_Settings_Accounts
 */
class WC_Settings_Woo_Atm extends WC_Settings_Page {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id    = 'wooatm';
		$this->label = __( 'WOOATM', 'wooatm' );
		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
		add_action( 'woocommerce_settings_' . $this->id, array( $this, 'output' ) );
		add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );
		add_action( 'woocommerce_admin_field_sorting', array( $this, 'wooatm_sorting' ) );

		add_filter( 'woocommerce_admin_settings_sanitize_option_wooatm_sorting', array( $this, 'wooatm_save_sorting' ), 10, 3 );

		add_action( 'admin_footer', array( $this, 'wooatm_add_scripts') );
		add_action( 'woocommerce_admin_field_global_tabs', array( $this, 'wooatm_global_tabs' ) );
		add_action( 'woocommerce_admin_settings_sanitize_option_wooatm_global_tabs', array( $this, 'wooatm_save_global_tabs' ), 10, 3);
		add_action( 'admin_enqueue_scripts', array($this, 'wooatm_add_media_scripts') );
	}


	public function wooatm_add_media_scripts() {
		wp_enqueue_media();
		wp_enqueue_style( 'wooatm-admin-stylesheet', plugins_url( 'assets/css/wooatm-admin.css', WOOATM_FILE ));
	}


	/**
	 * Get settings array
	 *
	 * @return array
	 */
	public function get_settings() {

		return apply_filters( 'woocommerce_' . $this->id . '_settings', array(

			array(	'title' => __( 'WooCommerce Accordions & Tabs Manager Settings', 'wooatm' ), 'type' => 'title','desc' => '', 'id' => 'wooatm_title' ),
        		array(
					'title'			=> __( 'Enable', 'wooatm' ),
					'desc' 			=> __( 'Enable Accordions & Tabs Manager.', 'wooatm' ),
					'type' 			=> 'checkbox',
					'id'				=> 'wooatm[enabled]',
					'default' 	=> 'no'											
				),
        		array(
					'title' 		=> __( 'Enable WOOATM Custom Tabs', 'wooatm' ),
					'desc' 			=> __( 'Allows you to add your own custom tabs', 'wooatm' ),
					'type' 			=> 'checkbox',
					'id'				=> 'wooatm[wooatm_fields]',
					'default' 	=> 'yes'											
				),
       			array(
					'title' 		=> __( 'WoocCommerce Tabs Design ', 'wooatm' ),
					'desc' 			=> '',
					'type' 			=> 'radio',
					'options'		=>	array(  'default' => 'Horizontal Tabs', 'vertical' => 'Vertical Tabs', 'accordion' => 'Accordion' ),
					'id'				=> 'wooatm[style]',
					'default' 	=> 'default'											
				),
                array(
					'title' 		=> __( 'WoocCommerce Tabs Position ', 'wooatm' ),
					'desc' 			=> '',
					'type' 			=> 'radio',
					'options'		=>	array(  'default' => 'After Product Summery', 'after_product_description' => 'After Product Description'),
					'id'				=> 'wooatm[position]',
					'default' 	=> 'default'											
        		),
       			array(
					'title' 		=> __( 'Gap between tabs(px)', 'wooatm' ),
					'desc' 			=> __( 'Gap between 2 tab elements.', 'wooatm' ),
					'type' 			=> 'number',
					'id'				=> 'wooatm[gap_tab]',
					'default' 	=> '3',
					'css' 			=> 'width: 75px;',										
				),
        		array(
					'title' 		=> __( 'Gap between accordions(px)', 'wooatm' ),
					'desc' 			=> __( 'Gap between 2 accordion elements.', 'wooatm' ),
					'type' 			=> 'number',
					'id'				=> 'wooatm[gap]',
					'default' 	=> '5',
					'css' 			=> 'width: 75px;',										
				),
				array(
					'title' 	  => __( 'Accordion/Tab title font size(px)', 'wooatm' ),
					'id' 		  	=> 'wooatm[title_font]',
					'type' 		  => 'number',
					'default'	  => '13',
					'css' 		  => 'width: 75px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion/Tab background color', 'wooatm' ),
					'id' 		  	=> 'wooatm[bg]',
					'type' 		  => 'color',
					'default'	  => '#c1c1c1',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion/Tab active background', 'wooatm' ),
					'id' 		  	=> 'wooatm[active_bg]',
					'type' 		  => 'color',
					'default'	  => '#fff',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion/Tab title color', 'wooatm' ),
					'id' 		  	=> 'wooatm[title_color]',
					'type' 		  => 'color',
					'default'	  => '#2e3865',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion/Tab active title color', 'wooatm' ),
					'id' 		  	=> 'wooatm[title_active]',
					'type' 		  => 'color',
					'default'	  => '#2e3865',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Content text color', 'wooatm' ),
					'id' 		  	=> 'wooatm[text_color]',
					'type' 		  => 'color',
					'default'	  => '#000000',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Content background color', 'wooatm' ),
					'id' 		  	=> 'wooatm[content_bg]',
					'type' 		  => 'color',
					'default'	  => '#ffffff',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion/Tab border color', 'wooatm' ),
					'id' 		  	=> 'wooatm[border_color]',
					'type' 		  => 'color',
					'default'	  => '#c1c1c1',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion style', 'wooatm' ),
					'id' 		  	=> 'wooatm[acc_style]',
					'type' 		  => 'select',
					'default'	  => 'Choose your preffered accordion style',
					'options' 	=> array( 'default' => 'Style 1', 'style1' => 'Style 2', 'style2' 	=> 'Style 3' ),
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Tab style', 'wooatm' ),
					'id' 		  	=> 'wooatm[tab_style]',
					'type' 		  => 'select',
					'default'	  => 'Choose your preffered tab style',
					'options' 	=> array( 'default' => 'Style 1', 'tab1' => 'Style 2', 'tab2' => 'Style 3' ),
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Tabs alignment', 'wooatm' ),
					'id' 		  	=> 'wooatm[tab_align]',
					'type' 		  => 'select',
					'default'	  => 'left',
					'options'	  => array( 'left' => 'Left', 'center' => 'Center', 'right'=> 'Right'),
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion icon position', 'wooatm' ),
					'id' 		  	=> 'wooatm[icon_pos]',
					'type' 		  => 'select',
					'default'	  => 'right',
					'options'	  => array( 'left' => 'Left', 'right'=> 'Right'),
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion icon', 'wooatm' ),
					'id' 		  	=> 'wooatm[acc_icon]',
					'type' 		  => 'text',
					'default'	  => 'wooatm-square-plus',
					'class'		  => 'wooatm-icon-picker',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion active icon', 'wooatm' ),
					'id' 		  	=> 'wooatm[acc_active]',
					'type' 		  => 'text',
					'default'	  => 'wooatm-square-minus',
					'class'		  => 'wooatm-icon-picker',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion icon font size(px)', 'wooatm' ),
					'id' 		  	=> 'wooatm[acc_icon_size]',
					'type' 		  => 'number',
					'default'	  => '20',
					'css' 		  => 'width: 55px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion arrow color', 'wooatm' ),
					'id' 		  	=> 'wooatm[arrow_color]',
					'type' 		  => 'color',
					'default'	  => '#000000',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Accordion active arrow color', 'wooatm' ),
					'id' 		  	=> 'wooatm[arrow_active]',
					'type' 		  => 'color',
					'default'	  => '#000000',
					'css' 		  => 'width: 125px;',
					'desc_tip'	=>  true
				),
				array(
					'title' 	  => __( 'Keep accordions like when page loads', 'wooatm' ),
					'id' 		  	=> 'wooatm[accordions_show_like]',
					'type' 		  => 'select',
					'default'	  => 'Keep all closed',
					'options'	  => array( 'all_closed' => 'Keep all closed', 'first_open' => 'Keep only first expanded', 'multi_expanded' => 'Keep all expanded'),
					'desc_tip'	=>  true
				),				
				array(
					'title' 	  => __( 'Tabs to accordion break point width in px', 'wooatm' ),
					'id' 		  	=> 'wooatm[accordion_breakpoint]',
					'type' 		  => 'number',
					'default'	  => '768',
					'css' 		  => 'width: 100px;',
					'desc'			=> 'This will be the width from where the tabs will be convert into accordions',
					'desc_tip'	=>  true
				),								
				array(
					'title' 	  => __( 'Enable global tabs for products', 'wooatm' ),
					'id' 		  	=> 'wooatm[enable_global_tabs]',
					'type' 		  => 'checkbox',
					'class'     => 'enable_global_tabs', 
					'default' 	=> 'no'
				),
        		array(
					'title' 		=> __( 'Sort tabs', 'wooatm' ),
					'desc' 			=> __( 'Sort tabs for the product.', 'wooatm' ),
					'type' 			=> 'sorting',
					'id'				=> 'wooatm_sorting',
				),
        		array(
					'title' 		=> __( 'Global Tabs', 'wooatm' ),
					'desc' 			=> __( 'Global tabs would be shown for each product.', 'wooatm' ),
					'type' 			=> 'global_tabs',
					'id'				=> 'wooatm_global_tabs',
				),																					
				array( 'type' => 'sectionend', 'id' => 'simple_wooatm_options'),

		)); // End pages settings
	}





	public function wooatm_sorting( $value) {
		$options = get_option( 'wooatm' );
		?>
		<tr valign="top" class="tabs-sorting-wrap">
			<th scope="row" class="titledesc">
				<label for="tabs_sorting">Tabs Sorting</label>
			</th>
			<td class="forminp global-tabs">
				<ul id="wooatm-soratble">
					<?php
					$tab_postions = get_option('wooatm_tab_position');
					if( empty($tab_postions) ) :	
					?>
						<li><span class="button-primary button-large"><input type="hidden" 
						name="wooatm[sorting][Custom Tab]">Custom Tabs</span></li>
						<li><span class="button-primary button-large"><input type="hidden" 
						name="wooatm[sorting][Additional Information]">Additional Information</span></li>
						<li><span class="button-primary button-large"><input type="hidden" 
						name="wooatm[sorting][Description]">Description</span></li>
						<li><span class="button-primary button-large"><input type="hidden" 
						name="wooatm[sorting][Reviews]">Reviews</span></li>
						<li class="global-tabs" style="display: none;"><span class="button-primary button-large"><input type="hidden" name="wooatm[sorting][Global]">Global</span></li>
					<?php
						else :
					?>
					<?php
						
						foreach( $tab_postions as $key => $tab_postion ) :
							if( $key == 'Global' )
								$class = 'global-tabs';
							else
								$class = '';
					?>
						<li class="<?php echo $class; ?>"><span class="button-primary button-large"><input type="hidden" name="wooatm[sorting][<?php echo $key; ?>]"><?php echo $key; ?></span></li>
					<?php
						endforeach;
					endif;
					?>
					</ul>
				</td>
			</tr>
		<?php
	}

	public function wooatm_global_tabs( $value) {
		$options = get_option( 'wooatm' );
		?>
		<tr valign="top" class="tabs-sorting-wrap global-tabs">
			<th scope="row" class="titledesc">
				<label for="tabs_sorting">Global Tabs</label>
			</th>
			<td class="forminp global-tabs">
				<ul id="wooatm-global-tabs-list">
				<?php
					$global_tabs = get_option('wooatm_global_tabs');
					if( is_array($global_tabs) ) :
						$count = 1;
						foreach( $global_tabs as $tab ) :
							?>
 					<li>
          	<div class="wooatm-handlediv" title="Click to toggle"><br></div>
            <div class="wooatm-remove" title="Remove"><br></div>
            <h3 class="wooatm-hndle"><span><?php echo stripcslashes($tab['title']); ?></span></h3>
            <div class="wooatm-inside">
            	<input type="text" class="wooatm-title" value="<?php echo stripcslashes($tab['title']); ?>" placeholder="Enter Tab/Accordion title here" name="wooatm[global_tabs][<?php echo $count; ?>][title]">
              <?php wp_editor( stripcslashes($tab['content']), 'wooatm_editor_'.$count, array( 'textarea_name' => 'wooatm[global_tabs][' . $count . '][content]', 'textarea_rows' => 6 ) ); ?>
                        </div>
                    </li>
							<?php
						$count++; endforeach; endif;
					
				?>	
				</ul>
				<input type="button" id="wooatm_add_global_tabs" class="button button-primary button-large" value="Add New" accesskey="n">				
				<?php
				/*
				?>
        <img src="<?php echo admin_url( 'images/spinner.gif' );?>" alt="loading" class="wooatm-global-tabs-loading" style="position: relative; top: 5px; left: 3px;" />
        <?php
					*/
        ?>
			</td>
		</tr>
		<?php
	}

	public function wooatm_save_sorting($value, $option, $raw_value) {
		$options = $_POST['wooatm'];
		$tab_positions = $options['sorting'];
		delete_option( 'wooatm_tab_position' );
		update_option( 'wooatm_tab_position' , $tab_positions  );
	}

	public function wooatm_save_global_tabs($value, $option, $raw_value) {
		$options = $_POST['wooatm'];
		delete_option( 'wooatm_global_tabs' );
		if( array_key_exists('global_tabs', $options) ) {
			$global_tabs = $options['global_tabs'];
			update_option( 'wooatm_global_tabs' , $global_tabs  );
		}
	}

	public function wooatm_add_scripts() {
		?>
		<script type="text/javascript">
			jQuery(function($) {
				toggle_global_tabs();

				$('body').on('click', 'input.wooatm.new-global-tab', function() {
					var Selected = $(this);
					var ParentHtml = '<div class="tabs-title-div"><input type="text" placeholder="Global Tab Title" class="wooatm_global_tab_title" style="width: 40%; margin-bottom: 15px; margin-top: 0px; min-height: 33px;" name="wooatm[global_tab_title][][title]" data-name="wooatm[global_tab_title][][title]"><input type="button"  class="button button-large wooatm remove-row" value="Remove Tab"></div>';
					$(ParentHtml).appendTo('div.tab-title-group');
					GetRowsLength = $('div.tab-title-group').find('.tabs-title-div').length;
					var currentName = $('div.tab-title-group').find('.tabs-title-div input.wooatm_global_tab_title').attr("data-name");
				});

				$('body').on('click', '.wooatm.remove-row', function() {
					$(this).closest('div.tabs-title-div').remove();
				});

				$('body').on('click', '.enable_global_tabs', function() {
					toggle_global_tabs();
				});

				function toggle_global_tabs() {
					if( $(".enable_global_tabs").is(':checked') ) {
						$('ul#wooatm-soratble').find('li.global-tabs').show();
						$('tr.tabs-sorting-wrap.global-tabs').show();
					}
					else {
						$('ul#wooatm-soratble').find('li.global-tabs').hide();
						$('tr.tabs-sorting-wrap.global-tabs').hide();
					}
				}	

				$('#wooatm-soratble').sortable();
				$( "#wooatm-soratble" ).disableSelection();

		    $( '#wooatm_add_global_tabs' ).click(function(){
		        var id = $('#wooatm-global-tabs-list li').length + 1;
		        var ed_id = 'wooatm_global_tab_'+id;
		        $('.wooatm-add-new img').show();
		        $.ajax({
		          url: ajaxurl,
		          type: 'POST',
		          dataType: 'html',
		          data: { editor: ed_id, id: id, action: 'global_wooatm_editor' }
		        }).done(function ( response ) {
		            $('#wooatm-global-tabs-list').append(response);
		            tinyMCE.init(tinyMCEPreInit.mceInit[ed_id]);
		            tinyMCEPreInit.qtInit[ed_id] = JSON.parse( JSON.stringify(tinyMCEPreInit.qtInit[ ed_id ] ) );
		            tinyMCEPreInit.qtInit[ed_id].id = ed_id;
		            setTimeout(function(){
		                new QTags(tinyMCEPreInit.qtInit[ ed_id ]);
		                QTags._buttonsInit();
		                switchEditors.go( ed_id, 'html' );
		            }, 1000);
		        });
		    });

		    $( '#wooatm-global-tabs-list' ).on( 'click', '.wooatm-remove', function(){
        	if( confirm('Do you really want to delete this tab/accordion?' ) )
            $(this).parent().slideUp('slow',function(){ $(this).remove(); });
    		});

    		jQuery( "#wooatm-global-tabs-list" ).sortable({
        	cursor: "move",
        	scroll: false,
        	start:function( event, ui ){
          	tinymce.remove();
        	},
        	stop:function( event, ui ){
          	jQuery('#wooatm-global-tabs-list textarea').each(function(){
            	ed_id = jQuery(this).attr('id');
              switchEditors.go( ed_id, tinyMCEPreInit.mceInit[ed_id].mode );
            });
        	},
        	handle: ".wooatm-hndle",
        	placeholder: "wooatm-sort-placeholder",
        	opacity: '0.8'
    		});

        $('#wooatm-global-tabs-list').on( 'click', '.wooatm-hndle,.wooatm-handlediv', function(){
        	$(this).parent().find('.wooatm-inside').slideToggle();
        	$(this).parent().toggleClass('wooatm-opened');
    		});

	    	$('#wooatm-global-tabs-list').on( 'blur', '.wooatm-title', function(){
	        var textval = $(this).val() == '' ? 'Enter tab/accordion title' : $(this).val();
	        $(this).parents('li').find('.wooatm-hndle').text(textval);
	    	});
			});
		</script>
		<?php
	}
}
return new WC_Settings_Woo_Atm();

endif;