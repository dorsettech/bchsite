<?php

/*
  Plugin Name: Google Analytics for Vendors
  Description: Google Analytics for Vendors
  Author: Dorset
  Version: 1.0
 */
defined('ABSPATH') || exit;

if (!defined('gav_PLUGIN_URL')) {
    define('gav_PLUGIN_URL', plugin_dir_url(__FILE__));
}
if (!defined('gav_PLUGIN_DIR')) {
    define('gav_PLUGIN_DIR', plugin_dir_path(__FILE__));
}


require_once "class.php";

function wpdocs_theme_name_scripts() {
    wp_enqueue_style('tbch_select2_410.min_style', gav_PLUGIN_URL . 'assets/select2_410.min.css');
    wp_enqueue_script('tbch_select2_410.min_js', gav_PLUGIN_URL . 'assets/select2_410.min.js');

    wp_enqueue_style('canvas-jquery-ui-style', gav_PLUGIN_URL . 'assets/jquery-ui.css');
    wp_enqueue_script('canvas-jquery-ui-js', gav_PLUGIN_URL . 'assets/jquery-ui.js');

//    wp_enqueue_script('canvas-graph-js', gav_PLUGIN_URL . 'assets/canvas-graph.js');
//    wp_enqueue_script('canvas-graph-js', gav_PLUGIN_URL . 'assets/canvasjs.min.js');
    wp_enqueue_script('canvas-graph-js', gav_PLUGIN_URL . 'assets/jquery.canvasjs.min.js');
//    wp_enqueue_script('my_custom_script', gav_PLUGIN_URL . 'assets/scripts.js');
}

add_action('wp_enqueue_scripts', 'wpdocs_theme_name_scripts', 99);
add_action('admin_menu', 'google_analytics_vendor');

function google_analytics_vendor() {
    add_menu_page('Google Analytics Vendor', 'Google Analytics Vendor', 'manage_options', 'google-analytics-vendor', 'index');
}

function index() {
    if (is_user_logged_in()) {
        $curuser = wp_get_current_user();
        $cur_username = $curuser->user_login;
        //echo $cur_username;
        $obj = new GA;
        $obj->OutputData($cur_username);
    }
}

function customshordcode() {
    if (is_user_logged_in()) {
        $curuser = wp_get_current_user();
        $cur_username = $curuser->user_login;
        //echo $cur_username;
        $date_data = [];
        if (isset($_GET['range'])) {
            $GetRange = $_GET['range'];
            if ($GetRange == 'custom') {
                if (isset($_GET['start_date'])) {
                    $date_data['start_date'] = $_GET['start_date'];
                }

                if (isset($_GET['end_date'])) {
                    $date_data['end_date'] = $_GET['end_date'];
                }
            } elseif ($GetRange == '7day') {
                $date_data['start_date'] = date('Y-m-d', strtotime('-7 days'));
                $date_data['end_date'] = date('Y-m-d');
            } elseif ($GetRange == 'month') {
                $date_str = date('d') - 1;
                $date_data['start_date'] = date('Y-m-d', strtotime("-$date_str days"));
                $date_data['end_date'] = date('Y-m-d');
            } elseif ($GetRange == 'last_month') {
                $date_str_e = date('d');
                $date_data['start_date'] = date('Y-m-d', strtotime('first day of last month'));
                $date_data['end_date'] = date('Y-m-d', strtotime("-$date_str_e days"));
            } elseif ($GetRange == 'year') {
                $date_data['start_date'] = date('Y-m-d', strtotime('-365 days'));
                $date_data['end_date'] = date('Y-m-d');
            }
        } else {
            $date_data['start_date'] = date('Y-m-d', strtotime('-7 days'));
            $date_data['end_date'] = date('Y-m-d');
        }
        $obj = new GA;
        $obj->OutputData($cur_username, $date_data);
    }
}

add_action('wcfm_admin_report_sales_by_date_after', 'customshordcode');
add_action('wcfm_wcfmmarketplace_report_sales_by_date_after', 'customshordcode'); // working 
add_action('wcfm_wcmarketplace_report_sales_by_date_after', 'customshordcode');
add_action('wcfm_wcpvendors_report_sales_by_date_after', 'customshordcode');
add_action('wcfm_wcvendors_report_sales_by_date_after', 'customshordcode');

//add_shortcode('codeprintResults', 'customshordcode');
