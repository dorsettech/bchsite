<?php
// Load the Google API PHP Client Library.

require_once __DIR__ . '/vendor/autoload.php';

class GA {

    function initializeAnalytics() {
        // Creates and returns the Analytics Reporting service object.
        // Use the developers console and download your service account
        // credentials in JSON format. Place them in this directory or
        // change the key file location if necessary.
//        $KEY_FILE_LOCATION = __DIR__ . '/the-british-craft-house-e4928f1e86db.json';
        $KEY_FILE_LOCATION = __DIR__ . '/tbch-analytics-bf1e83f645bd.json';
        //$KEY_FILE_LOCATION = __DIR__ . '/tbch-analytics-bf1e83f645bd.json';
        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("Project");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new Google_Service_Analytics($client);

        return $analytics;
    }

    function getFirstProfileId($analytics) {
        // Get the user's first view (profile) ID.
        // Get the list of accounts for the authorized user.
        $accounts = $analytics->management_accounts->listManagementAccounts();
        $response = [];
        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();
            // Get the list of properties for the authorized user.
            $properties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);
            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();
                // Get the list of views (profiles) for the authorized user.
                $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstPropertyId);
                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();
                    // Return the first view (profile) ID.
                    return $items[0]->getId();
                } else {
                    $response['error'] = 'No views (profiles) found for this user.';
//                    throw new Exception('No views (profiles) found for this user.');
                }
            } else {
                $response['error'] = 'No properties found for this user.';
//                throw new Exception('No properties found for this user.');
            }
        } else {
            $response['error'] = 'No accounts found for this user.';
//            throw new Exception('No accounts found for this user.');
        }
        return $response;
    }

    function getFirstProfileIDs($analytics) {
        // Get the user's first view (profile) ID.
        // Get the list of accounts for the authorized user.
        $response = [];
        $accounts = $analytics->management_accounts->listManagementAccounts();
        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();

            $firstAccountId = $items[0]->getId();

            // Get the list of properties for the authorized user.
            $properties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();

                // Get the list of views (profiles) for the authorized user.
                $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstPropertyId);

                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();
                    $acc_ids = [];
                    $data_ids = array(
                        'AccountID' => $firstAccountId,
                        'PropertyID' => $firstPropertyId,
                        'ProfileID' => $items[0]->getId(),
                    );
                    // Return the first view (profile) ID.
                    return $data_ids;
                } else {
                    $response['error'] = 'No views (profiles) found for this user.';
//                    throw new Exception('No views (profiles) found for this user.');
                }
            } else {
                $response['error'] = 'No properties found for this user.';
//                throw new Exception('No properties found for this user.');
            }
        } else {
            $response['error'] = 'No accounts found for this user.';
//            throw new Exception('No accounts found for this user.');
        }
    }

    function OrganicResults($analytics, $profileId, $start_date, $end_date, $author) {
        // Calls the Core Reporting API and queries for the number of sessions
        // for the last seven days.
        $optParams = array(//OPTINAL SETTINGS
            'dimensions' => 'ga:dimension1,ga:pagePath,ga:city,ga:date',
            'filters' => "ga:dimension1==$author",
            'metrics' => 'ga:pageviews',
        );
        // Create Dimension Filter.
        return $analytics->data_ga->get(
                        'ga:' . $profileId, $start_date, $end_date, 'ga:pageviews', $optParams
        );
    }

    function getProductTitleBySlug($get_slug) {
        if (!$get_slug) {
            return FALSE;
        }
        $slug = "";
        if (strpos($slug, 'product&p=')) {
            $slug = explode('&p=', $slug);
            $pro_id = (int) $slug[1];
            $slug = get_permalink($pro_id);
            if ($slug) {
                $slug = explode('product/', $slug);
                $slug = $slug = str_replace('/', '', $slug[1]);
            }
        } elseif (strpos($get_slug, '?fbclid')) {
            $exp_slug = explode('?fbclid', $get_slug);
            $slug = $exp_slug[0];
        } elseif (strpos($get_slug, '?unapproved')) {
            $exp_slug = explode('?unapproved', $get_slug);
            $slug = $exp_slug[0];
        } elseif (strpos($get_slug, '?epik')) {
            $exp_slug = explode('?epik', $get_slug);
            $slug = $exp_slug[0];
        } elseif (strpos($get_slug, '?add-to-cart')) {
            $exp_slug = explode('?add-to-cart', $get_slug);
            $slug = $exp_slug[0];
        } elseif (strpos($get_slug, '?')) {
            $exp_slug = explode('?', $get_slug);
            $slug = $exp_slug[0];
        }

        if (empty($slug)) {
            $slug = $get_slug;
        }
        global $wpdb;
        $wppost_table = $wpdb->prefix . 'posts';
        $get_post_title = $wpdb->get_results("SELECT ID, post_title FROM `$wppost_table` WHERE post_name = '$slug'");
        $product_title = [];
        if ($get_post_title) {
            $product_title = array(
                'id' => $get_post_title[0]->ID,
                'title' => $get_post_title[0]->post_title
            );
        }
        if (!empty($get_post_title)) {
            return $product_title;
        } else {
            return $slug;
        }
        return FALSE;
    }

    function shorten_text($text, $max_length = 140, $cut_off = '...', $keep_word = false) {
        if (strlen($text) <= $max_length) {
            return $text;
        }

        if (strlen($text) > $max_length) {
            if ($keep_word) {
                $text = substr($text, 0, $max_length + 1);

                if ($last_space = strrpos($text, ' ')) {
                    $text = substr($text, 0, $last_space);
                    $text = rtrim($text);
                    $text .= $cut_off;
                }
            } else {
                $text = substr($text, 0, $max_length);
                $text = rtrim($text);
                $text .= $cut_off;
            }
        }

        return $text;
    }

    function printResults($data1) {
        wp_enqueue_script('my_custom_script', gav_PLUGIN_URL . 'assets/scripts.js');

        $limit_record = 10;
        if (isset($_GET['rec_limit'])) {
            $limit_record = $_GET['rec_limit'];
        }
        $fsort = 'ASC';
        $fSort = 'ASC';
        if (isset($_GET['sort'])) {
            $fSort = $_GET['sort'];
            if ($_GET['sort'] == 'ASC') {
                $fsort = 'DESC';
            } else {
                $fsort = 'ASC';
            }
        }
        $ftype = '';
        $s_asc_icon = gav_PLUGIN_URL . 'assets/sort-down.svg';
        $s_desc_icon = gav_PLUGIN_URL . 'assets/sort-up.svg';
        $s_n_icon = gav_PLUGIN_URL . 'assets/sort-n.svg';
        $filter_icon = gav_PLUGIN_URL . 'assets/filter.svg';
        if (isset($_GET['type'])) {
            $ftype = $_GET['type'];
        }
        ?>
        <style type="text/css">
            .styled-table {
                border-collapse: collapse;
                margin: 50px auto !important;
                font-size: 0.9em;
                font-family: sans-serif;
                min-width: 400px;
                /*box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);*/
                padding: 20px 5px;
                width: 100% !important;
                border-top: 1px solid #e3e3e3 !important;
                border-bottom: 1px solid #e3e3e3 !important;
            }

            .styled-table td a {
                text-decoration: none !important;
                color: #000;
            }

            .styled-table tr:hover td {
                background: #dfdede !important;
            }

            .styled-table .active-row td a {
                color: #17a2b8;
            }

            .styled-table thead tr {
                background-color: #009879;
                color: #ffffff;
                text-align: left;
            }

            .styled-table tbody td {
                font-size: 12px;
                line-height: 1.3em;
            }

            .styled-table th,
            .styled-table td {
                padding: 12px 15px;
            }

            .styled-table tbody tr {
                border-bottom: 1px solid #dddddd;
            }

            .styled-table tbody tr:nth-of-type(even) {
                background-color: #f3f3f3;
            }

            .styled-table tbody tr:last-of-type {
                border-bottom: 2px solid #009879;
            }

            .styled-table tbody tr.active-row {
                font-weight: bold;
                color: #17a2b8;
            }

            .canvasjs-chart-credit,
            .canvasjs-chart-toolbar {
                display: none !important;
            }

            .bch_ga_custom_filters_form ul {
                list-style: none;
                margin: 0;
            }

            .bch_ga_custom_filters_form ul li {
                display: inline-block;
                padding: 5px 10px;
                vertical-align: text-top;
            }

            .bch_analytics_report_sort {
                display: inline !important;
            }

            .bch_analytics_report_sort_btn {
                padding: 0px !important;
                color: #17a2b8 !important;
                background: transparent !important;
                font-size: 16px !important;
            }

            .bch_analytics_report_sort_btn svg {
                width: 10px;
                height: auto;
                margin-bottom: -2px;
            }

            .bch_table_header th {
                line-height: 1.3;
                font-size: 12px !important;
                font-weight: 800 !important;
            }

            .bch_ga_custom_filters_form .bch_filter_button {
                padding: 2px 20px;
                border-radius: 3px;
                height: 25px;
                font-size: 16px;
                vertical-align: middle;
                background: #17a2b8;
                border: 1px solid #17a2b8;
            }

            .bch_ga_custom_filters_form .bch_filter_button:hover {
                color: #17a2b8;
                background: transparent;
            }

            .bch_ga_custom_filters_form .bch_filter_button svg {
                width: 12px;
            }
            /*
            #chartContainer{
        position: relative;
        }
        #chartContainer:after{
        content: '';
        width: 100px;
        height: 65px;
        position: absolute;
        background: #fff !important;
        bottom: 0;
        left: 0;
        }
            */
            /*            #wcfm_report_details .wcfm-collapse-content{
                            padding-bottom: 0px !important;
                        }
                        #wcfm_report_details .wcfm-collapse-content .wcfm-container{
                            
                        }*/
        </style>
        <div id="container" style="display: table;width:100%;">
            <!-- GetdataPointsCallback -->
            <div class="wcfm-container" style="width:98%; margin:0px auto 10px auto;padding: 30px 0;">
                <div id="chartContainer" style="height: 550px;width: 96%;margin: auto;border: 1px solid #e3e3e3;"></div>
            </div>
            <div class="wcfm-container" style="width:100%;">

                <table class="styled-table" style="width: 100%;">
                    <thead>
                        <tr>
                            <th colspan="9">
                                <div class="bch_ga_custom_filters_form">
                                    <form method="GET" action="">

                                        <ul>
                                            <li>
                                                <?php
                                                if (isset($_GET)) {
                                                    foreach ($_GET as $key => $value) {
                                                        if (isset($value)) {
                                                            if ($key !== 'rec_limit') {
                                                                echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                            </li>
                                            <li>
                                                <label for="bch_rec_limit">Limit Record: </label>
                                                <select name="rec_limit" id="bch_rec_limit">
                                                    <option value="10" <?php echo $limit_record == '10' ? 'selected' : ''; ?>>10 Records</option>
                                                    <option value="25" <?php echo $limit_record == '25' ? 'selected' : ''; ?>>25 Records</option>
                                                    <option value="50" <?php echo $limit_record == '50' ? 'selected' : ''; ?>>50 Records</option>
                                                    <option value="100" <?php echo $limit_record == '100' ? 'selected' : ''; ?>>100 Records</option>
                                                    <option value="all" <?php echo $limit_record == 'all' ? 'selected' : ''; ?>>All</option>
                                                </select>
                                            </li>
                                            <li>
                                                <button type="submit" name="submit" class="bch_filter_button">
                                                    <?php echo file_get_contents($filter_icon); ?> Filter
                                                </button>
                                                <!--<input type="submit" name="submit" value="Filter" class="bch_filter_button">-->
                                            </li>
                                        </ul>
                                    </form>
                                    <?php
                                    if ($data1) {
                                        echo '<div class="bch_total_record_view"><b>Total Records are: </b><span>' . count($data1) . '</span></div>';
                                        ?>

                                        <?php
                                    }
                                    ?>
                                </div>

                            </th>
                        </tr>
                        <tr class="bch_table_header">
                            <th style="width: 5%;">Sr#
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'num' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="num" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'num') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                            <th style="width: 30%; word-break: break-all;">Product Name
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'pro_title' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="pro_title" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'pro_title') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                            <th style="width: 10%;">Total View
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'page_view' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="page_view" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'page_view') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                            <th style="width: 10%;">View Date
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'date' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="date" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'date') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                            <th style="width: 10%;">From
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'location' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="location" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'location') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                            <th style="width: 10%;">Conversion Rate
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'c_rate' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="c_rate" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'c_rate') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                            <th style="width: 10%;">New/Return User
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'user' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="user" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'user') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                            <th style="width: 20%;">Source
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'source' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="source" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'source') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                            <th style="width: 20%;">Bounce Rate
                                <form method="GET" action="" class="bch_analytics_report_sort">
                                    <?php
                                    if (isset($_GET)) {
                                        foreach ($_GET as $key => $value) {
                                            if (isset($value)) {
                                                if ($key !== 'sort' || $key !== 'type') {
                                                    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="hidden" value="<?php echo $ftype == 'bounce_rate' ? $fsort : 'ASC'; ?>" name="sort">
                                    <input type="hidden" value="bounce_rate" name="type">
                                    <button type="submit" class="bch_analytics_report_sort_btn">
                                        <?php
                                        if ($ftype == 'bounce_rate') {
                                            if ($fsort == 'DESC') {
                                                echo file_get_contents($s_asc_icon);
                                            } else {
                                                echo file_get_contents($s_desc_icon);
                                            }
                                        } else {
                                            echo file_get_contents($s_n_icon);
                                        }
                                        ?>
                                    </button>
                                </form>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $linegraphs_data = [];
                        $graph_data = [];
                        ?>
                        <?php
                        $number = 1;
                        $main_data_arr = [];
                        if ($data1) {
                            $data_graph = [];
                            $analytics_data = [];
                            foreach ($data1 as $key => $value) {

                                $slug = str_replace('/product/', '', $value['product']);
                                $slug = str_replace('/', '', $slug);
                                $get_prod_title = '';
                                if ($slug) {
                                    if ($this->getProductTitleBySlug($slug)) {
                                        $get_prod_title = $this->getProductTitleBySlug($slug);
                                    }
                                }
                                if (!empty($get_prod_title)) {
                                    if ($number <= $limit_record || $limit_record == 'all') {
                                        $date_stamp = strtotime($value['date']);
                                        $date_stamp_ms = $date_stamp * 1000;

                                        $data_graph[$value['product']][$number] = array(
                                            'num' => $number,
                                            'date' => $date_stamp_ms,
                                            'product' => $get_prod_title,
                                            'page_views' => $value['page_views']
                                        );
                                        $por_url = site_url() . $value['product'];

                                        if (isset($get_prod_title['title'])) {
                                            $pro_title = $get_prod_title['title'];
                                        } else {
                                            $pro_title = $slug;
                                        }
                                        $page_views = $value['page_views'];
                                        $view_date = date('d-m-Y', $date_stamp);
                                        $view_location = $value['city'];
                                        $conversion_rate = $value['goalConversionRateAll'];
                                        $user_lev = $value['newUsers'] > 0 ? 'New' : 'Old';
                                        $view_source = $value['source'];
                                        $bounce_rate = $value['bounce_rate'];
                                        $bounce_rate_g = 0;
//                                        if ($value['entrances'] > 0 && $value['entrances'] <= 100) {
                                        if ($value['entrances']) {
                                            $bounce_rate_g = 100 / $value['entrances'];
                                            $bounce_rate_g = number_format($bounce_rate_g, 0);
                                        }
                                        $get_c_rates = 0.0;
                                        if (isset($get_prod_title['id'])) {
                                            $product_id = $get_prod_title['id'];
                                            $get_purchases = $this->getOrderPurchases($product_id, $page_views, $date_stamp);
                                            if (!empty($get_purchases['c_rate'])) {
                                                $get_c_rates = $get_purchases['c_rate'];
                                            }
                                        }

                                        $analytics_data[] = array(
                                            'num' => $number,
                                            'pro_url' => $por_url,
                                            'pro_title' => $pro_title,
                                            'page_view' => $page_views,
                                            'date' => $view_date,
                                            'location' => $view_location,
                                            'c_rate' => $get_c_rates,
                                            'user' => $user_lev,
                                            'source' => $view_source,
                                            'bounce_rate' => $bounce_rate_g,
                                        );
                                        $number++;
                                    }
                                }
                            }
                            if (!empty($analytics_data)) {
                                if ($fSort == "ASC") {
                                    usort($analytics_data, function ($a, $b) {
                                        $fType = 'num';
                                        if (isset($_GET['type'])) {
                                            $fType = $_GET['type'];
                                        }
                                        return $a[$fType] > $b[$fType];
                                    });
                                } elseif ($fSort == "DESC") {
                                    usort($analytics_data, function ($a, $b) {
                                        $fType = 'num';
                                        if (isset($_GET['type'])) {
                                            $fType = $_GET['type'];
                                        }
                                        return $a[$fType] < $b[$fType];
                                    });
                                } else {
                                    usort($analytics_data, function ($a, $b) {
                                        return $a['num'] > $b['num'];
                                    });
                                }

                                $number2 = 1;
                                foreach ($analytics_data as $data_row) {
                                    ?>
                                    <tr class="<?php echo ($number2 % 2 == 0) ? 'active-row' : ''; ?>">
                                        <td><?php echo $data_row['num']; ?></td>
                                        <td style="word-break: break-all;">
                                            <a href="<?php echo $data_row['pro_url']; ?>"><?php echo $data_row['pro_title']; ?></a>
                                        </td>
                                        <td><?php echo $data_row['page_view']; ?></td>
                                        <td><?php echo $data_row['date']; ?></td>
                                        <td><?php echo $data_row['location']; ?></td>
                                        <td><?php echo $data_row['c_rate']; ?>%</td>
                                        <td><?php echo $data_row['user']; ?></td>
                                        <td><?php echo $data_row['source']; ?></td>
                                        <td><?php echo $data_row['bounce_rate']; ?>%</td>

                                    </tr>
                                    <?php
                                    $number2++;
                                }
                            }

                            if ($data_graph) {
                                $ic = 1;
                                foreach ($data_graph as $pro => $data) {
                                    //                                if ($pro !== '/product/sea-heart-stained-glass-suncatcher-blue-or-sunset-sky/') {
                                    //                                    continue;
                                    //                                }
                                    $dataPoints1 = [];
                                    usort($data, function ($a, $b) {
                                        return $a['date'] > $b['date'];
                                    });
                                    $anc_title = '';
                                    $add_with_date = [];
                                    foreach ($data as $key => $value) {
                                        $add_with_date[$value['date']]['date'] = $value['date'];
                                        $add_with_date[$value['date']]['page_view'] += $value['page_views'];

                                        if (isset($value['product']['title'])) {
                                            $anc_title_ = $this->shorten_text($value['product']['title'], 10, '...');
                                        } else {
                                            $anc_title_ = $this->shorten_text($value['product'], 10, '...');
                                        }
                                        $add_with_date[$value['date']]['anc_title'] = $anc_title_;
                                    }
                                    foreach ($add_with_date as $num_val => $values) {
                                        $dataPoints1[] = array(
                                            'x' => $values['date'],
                                            'y' => (string) $values['page_view'],
                                        );
                                        $anc_title = $values['anc_title'];
                                    }
                                    $main_data_arr[] = array(
                                        //                                    'type' => 'spline',
                                        'type' => 'line',
                                        'neckHeight' => 60,
                                        'name' => $anc_title,
                                        'showInLegend' => false,
                                        'xValueType' => 'dateTime',
                                        'dataPoints' => $dataPoints1
                                    );
                                }
                            }
                        } else {
                            echo '<tr><td colspan="9">No results found</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>


            <script>
                function GetdataPointsCallback() {
                    var GetdataPoints = <?php echo json_encode($main_data_arr, JSON_NUMERIC_CHECK); ?>;
                    return GetdataPoints;
                }
            </script>
        </div>

        <?php
    }

    function getOrderPurchases($product_id, $page_views, $date) {
        if (!$product_id && !$page_views && !$date) {
            return FALSE;
        }

        $order_status = [
            'wc-completed'
        ];
//        $date = strtotime('2020-04-23 15:05:06');

        $gen_start_lap = strtotime('today', $date);
        $gen_end_lap = strtotime('tomorrow', $gen_start_lap) - 1;
        $time_lap_start = date('Y-m-d H:i:s', $gen_start_lap);
        $time_lap_end = date('Y-m-d H:i:s', $gen_end_lap);

        global $wpdb;
        $results = $wpdb->get_results("
            SELECT order_items.order_id, order_item_meta.meta_value product_id, order_items.order_item_id, posts.post_date
            FROM {$wpdb->prefix}woocommerce_order_items as order_items
            LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
            LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
            WHERE posts.post_type = 'shop_order'
            AND order_items.order_item_type = 'line_item'
            AND order_item_meta.meta_key = '_product_id'
            AND order_item_meta.meta_value = '$product_id'
            AND post_date BETWEEN '$time_lap_start' AND '$time_lap_end' 
            ");
//AND post_date BETWEEN '$time_lap_start' AND '$time_lap_end' 

        $return = [];
        if ($results) {

            foreach ($results as $result) {
                $order_time = strtotime($result->post_date);
                $get_qtys = $wpdb->get_results("SELECT meta_Value qty FROM `wp_woocommerce_order_itemmeta` WHERE meta_key = '_qty' AND `order_item_id` = " . $result->order_item_id);
                $product_qty = 0;
                if ($get_qtys) {
                    foreach ($get_qtys as $get_qty) {
                        $pro_qty = (int) $get_qty->qty;
                        $product_qty += $pro_qty;
                    }
                }
                $c_local_rate = ($product_qty / $page_views) * 100;
                $c_local_rate = number_format($c_local_rate, 2);
                $return = [
                    'order_id' => $result->order_id,
                    'order_item_id' => $result->order_item_id,
                    'product_id' => $result->product_id,
                    'order_date' => $result->post_date,
                    'product_qty' => $product_qty,
                    'page_views' => $page_views,
                    'c_rate' => $c_local_rate ? $c_local_rate : 0.00,
                ];
            }
        }
        if ($return) {
            return $return;
        } else {
            return FALSE;
        }
    }

    /**
     * Note: This code assumes you have an authorized Analytics service object.
     * See the Goal Developer Guide for details.
     * Retrieves all goals for all views (profiles) for the authorized user, using a
     * wildcard '~all' as the view (profile) ID.
     * Requests goals for a single view (profile).
     */
    function GetAllGoalIDs($analytics, $profile) {

        try {
            $goals = $analytics->management_goals->listManagementGoals($profile['AccountID'], $profile['PropertyID'], $profile['ProfileID']);
            $data = [];
            foreach ($goals->getItems() as $goal) {
                $data[] = $goal->getId();
            }
            if (!empty($data)) {
                return $data;
            } else {
                return 'empty';
            }
        } catch (apiServiceException $e) {
            print 'There was an Analytics API service error '
                    . $e->getCode() . ':' . $e->getMessage();
        } catch (apiException $e) {
            print 'There was a general API error '
                    . $e->getCode() . ':' . $e->getMessage();
        }
    }

    /**
     * Note: This code assumes you have an authorized Analytics service object.
     * See the Goal Developer Guide for details.
     * This request gets an existing goal.
     */
    function getGoalByID($analytics, $profile, $goal_id) {
        if (!$goal_id) {
            return 'empty goal id';
        }
        try {
            $goal = $analytics->management_goals->get($profile['AccountID'], $profile['PropertyID'], $profile['ProfileID'], $goal_id);
            return $goal;
        } catch (apiServiceException $e) {
            print 'There was an Analytics API service error '
                    . $e->getCode() . ':' . $e->getMessage();
        } catch (apiException $e) {
            print 'There was a general API error '
                    . $e->getCode() . ':' . $e->getMessage();
        }
    }

    function GetresultWithPath($analytics, $profileId, $start_date, $end_date, $author) {
        // Calls the Core Reporting API and queries for the number of sessions
        // for the last seven days.
        $array_dim = array(
            'ga:dimension1',
            'ga:pagePath',
            'ga:city',
            'ga:date',
            'ga:source',
        );

        $array_metrics = array(
            'ga:pageviews',
            'ga:users',
            'ga:newUsers',
            'ga:sessions',
//            'ga:goalConversionRateAll',
//            'ga:percentNewSessions',
//            'ga:bounces',
//            'ga:bounceRate',
//            'ga:avgSessionDuration',
//            'ga:entrances',
        );

        $optParams = array(//OPTINAL SETTINGS
            'dimensions' => implode(',', $array_dim),
            'filters' => "ga:dimension1==$author",
            'metrics' => implode(',', $array_metrics),
        );

        $datam = 'ga:pageviews';
        $get_data = $analytics->data_ga->get('ga:' . $profileId, $start_date, $end_date, $datam, $optParams);



        if ($get_data->totalResults == 0) {
            $optParams = array(//OPTINAL SETTINGS
                'dimensions' => implode(',', $array_dim),
                'filters' => "ga:dimension2==$author",
                'metrics' => implode(',', $array_metrics),
            );

            $datam = 'ga:pageviews';
            $get_data = $analytics->data_ga->get('ga:' . $profileId, $start_date, $end_date, $datam, $optParams);
        }
        /**/
        $retData = [];
        if ($get_data->totalResults > 0) {
            if ($get_data->rows) {
                foreach ($get_data as $w_key => $w_row) {
                    $retData[] = array(
                        'vendor_name'           => isset( $w_row[0] ) ? $w_row[0] : '',
                        'product'               => isset( $w_row[1] ) ? $w_row[1] : '',
                        'city'                  => isset( $w_row[2] ) ? $w_row[2] : '',
                        'date'                  => isset( $w_row[3] ) ? $w_row[3] : '',
                        'source'                => isset( $w_row[4] ) ? $w_row[4] : '',
                        'page_views'            => isset( $w_row[5] ) ? $w_row[5] : '',
                        'users'                 => isset( $w_row[6] ) ? $w_row[6] : '',
                        'newUsers'              => isset( $w_row[7] ) ? $w_row[7] : '',
                        'sessions'              => isset( $w_row[8] ) ? $w_row[8] : '',
                        'goalConversionRateAll' => isset( $w_row[9] ) ? $w_row[9] : '',
                        'percentNewSessions'    => isset( $w_row[10] ) ? $w_row[10] : '',
                        'bounce'                => isset( $w_row[11] ) ? $w_row[11] : '',
                        'bounce_rate'           => isset( $w_row[12] ) ? $w_row[12] : '',
                        'avgSessionDuration'    => isset( $w_row[13] ) ? $w_row[13] : '',
                        'entrances'             => isset( $w_row[14] ) ? $w_row[14] : '',
                    );
                }
            }
        }
        return $retData;
    }

    function GetresultWithAds($analytics, $profileId, $start_date, $end_date, $author) {
        // Calls the Core Reporting API and queries for the number of sessions
        // for the last seven days.
        $array_dim = array(
            'ga:dimension1',
            'ga:source',
        );

        $array_metrics = array(
            'ga:pageviews',
            'ga:users',
            'ga:newUsers',
            'ga:percentNewSessions',
            'ga:goalConversionRateAll',
            'ga:costPerTransaction',
            'ga:costPerGoalConversion',
            'ga:costPerConversion',
        );

        $optParams = array(//OPTINAL SETTINGS
            'dimensions' => implode(',', $array_dim),
            'filters' => "ga:dimension1==$author",
            'metrics' => implode(',', $array_metrics),
        );

        $datam = 'ga:pageviews';
        $get_data = $analytics->data_ga->get('ga:' . $profileId, $start_date, $end_date, $datam, $optParams);
        $retData = [];
        if ($get_data->totalResults > 0) {
            if ($get_data->rows) {
                foreach ($get_data as $w_key => $w_row) {
                    $retData[] = array(
                        'vendor_name' => $w_row[0],
                        'source' => $w_row[1],
                        'page_views' => $w_row[2],
                        'users' => $w_row[3],
                        'newUsers' => $w_row[4],
                        'percentNewSessions' => $w_row[5],
                        'goalConversionRateAll' => $w_row[6],
                        'costPerTransaction' => $w_row[7],
                        'costPerGoalConversion' => $w_row[8],
                        'costPerConversion' => $w_row[9],
                    );
                }
            }
        }
        return $retData;
    }

    function OutputData($author, $dates = array()) {
//        ini_set('display_errors', '1');
//        ini_set('display_startup_errors', '1');
//        error_reporting(E_ALL);
        $start_date = date('Y-m-d', strtotime('today'));
        if (isset($dates['start_date'])) {
            $start_date = $dates['start_date'];
        }
        $end_date = date('Y-m-d', strtotime('today'));
        if (isset($dates['end_date'])) {
            $end_date = $dates['end_date'];
        }

        $analytics = $this->initializeAnalytics();

        $profileID = $this->getFirstProfileId($analytics);

//        $profile = $this->getFirstProfileIDs($analytics);
//        $goals = $this->GetAllGoalIDs($analytics, $profile);
//        $author = 'Joysofglass';
//        $author = 'Bumblebee Glass';
//        $author = 'Jen Shelton';
//        $author = 'Sixpenny Studio';
//        $author = 'SurreyWoodsmiths';
//       
        if (isset($profileID['error'])) {
            echo $profileID['error'];
        } else {
//            $organic = [];
//            $organic = $this->OrganicResults($analytics, $profileID, $dates['start_date'], $dates['end_date'], $author);
//
//            $get_api_Data_2 = [];
//            $get_api_Data_2 = $this->GetresultWithAds($analytics, $profileID, $start_date, $end_date, $author);

            $get_api_Data_1 = [];
            $get_api_Data_1 = $this->GetresultWithPath($analytics, $profileID, $start_date, $end_date, $author);

            $this->printResults($get_api_Data_1);
        }

//
        //        $access_users = [
        //            18,
        //            21,
        //            2152
        //        ];
        //        $current_user = get_current_user_id();
        //        if (in_array($current_user, $access_users)) {
        //            $filter_dates = array(
        //                'start_date' => $start_date,
        //                'end_date' => $end_date
        //            );
        //            $this->printResults($get_api_Data_1);
        //        }
    }

}
