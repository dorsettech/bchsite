(function($){

    "use strict";

    $(document).on( 'change', '.xwc--package-qty', function(e) {
        if ( e.target && e.target.matches('.xwc--package-qty') ) {
            set_quantity($(e.target).closest('form'),$(e.target).val());
        }
    });

    function set_quantity(form, qty) {
        if ( form.length>0 ) {
            form.find('input[name="quantity"]').val(qty);
        }
    }

    $('.xwc--package-qty').each( function(i,o) {
        var form = $(this).closest('form');

        if ( $(this).val() !== 1 ) {
            set_quantity(form, $(this).val());
        }

        if ( $(this).hasClass('xwc--package-hide') ) {
            form.find('input[name="quantity"]').css("height", 0).css("position", "absolute").css("left", -9999);
        }
    } );

    if ( $('body').hasClass('woocommerce-cart-form-no-qty') ) {
        $('.woocommerce-cart-form').find('.qty').prop( "disabled", true );
    }

})(jQuery);