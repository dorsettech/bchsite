(function($){
    "use strict";

	$(document).on( 'svx-package_manager-save', function(e,f) {

		var r = {};

		svx.skips.push('_wcmn_default');
		svx.skips.push('_wcmn_default_qty');

        r.default = {};

		r.default.preset = u(svx.settings['_wcmn_default'].val)?svx.settings['_wcmn_default'].val:false;
		r.default.qty = u(svx.settings['_wcmn_default_qty'].val)?svx.settings['_wcmn_default_qty'].val:false;

		r.product_tag = {};
		r.product_cat = {};

		var c=0;
		$.each( svx.settings['_wcmn_tags'].val, function(i,g) {
			r.product_tag[c] = {
				name:u(g.name)?g.name:'Not set',
				term:u(g.term),
				qty:u(g.qty)?g.qty:false,
                preset:u(g.preset)?g.preset:false,
                order:c
			}
			c++;
		} );

		var c=0;
		$.each( svx.settings['_wcmn_categories'].val, function(i,g) {
			r.product_cat[u(g.term)] = {
				name:u(g.name)?g.name:'Not set',
				term:u(g.term),
                qty:u(g.qty)?g.qty:false,
                preset:u(g.preset)?g.preset:false,
				order:c
			}
			c++;
		} );

		f.save_val = r;

	} );

	$(document).on( 'svx-package_manager-load', function(e,f) {

		svx.settings['_wcmn_default'].val = u(f.val.default)&&u(f.val.default.preset)!==false?f.val.default.preset:'';
		svx.settings['_wcmn_default_qty'].val = u(f.val.default)&&u(f.val.default.qty)!==false?f.val.default.qty:'';

		svx.settings['_wcmn_tags'].val = [];
		svx.settings['_wcmn_categories'].val = [];

		var c=0;
		$.each( f.val.product_tag, function(i,g) {
			svx.settings['_wcmn_tags'].val[c] = {
				name:u(g.name)!==false?g.name:'Not set',
				term:u(g.term)!==false?g.term:i,
                qty:u(g.qty)!==false?g.qty:g,
                preset:u(g.preset)?g.preset:false,
                order:c
			}
			c++;
		} );

		c=0;
		$.each( f.val.product_cat, function(i,g) {
			svx.settings['_wcmn_categories'].val[typeof g.order!=='undefined'?g.order:c] = {
				name:u(g.name)!==false?g.name:g,
				term:u(g.term)!==false?g.term:i,
                qty:u(g.qty)!==false?g.qty:g,
                preset:u(g.preset)?g.preset:false,
                order:c
			}
			c++;
		} );

	} );
    
	function u(e) {
		return typeof e == 'undefined' ? false : e;
	}

})(jQuery);