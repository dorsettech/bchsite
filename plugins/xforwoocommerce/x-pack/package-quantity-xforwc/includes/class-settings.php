<?php

	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

	class XforWC_PackageQuantity_Settings {

		public static $plugin;

		public static function init() {

			self::$plugin = array(
				'name' => 'Package Quantity Discount for WooCommerce',
				'xforwc' => 'Package Quantity Discount',
				'slug' => 'package-quantity-xforwc',
				'label' => 'package_quantity_xforwc',
				'image' => XforWC_PackageQuantity()->plugin_url() . '/assets/images/package-quantity-xforwc.png',
				'path' => 'package-quantity-xforwc/package-quantity-xforwc',
				'version' => XforWC_PackageQuantity::$version,
			);

			if ( isset( $_GET['page'], $_GET['tab'] ) && ( $_GET['page'] == 'wc-settings' ) && $_GET['tab'] == 'package_quantity_xforwc' ) {
				add_filter( 'svx_plugins_settings', array( 'XforWC_PackageQuantity_Settings', 'get_settings' ), 50 );
			}

			if ( function_exists( 'XforWC' ) ) {
				add_filter( 'xforwc_settings', array( 'XforWC_PackageQuantity_Settings', 'xforwc' ), 9999999903 );
				add_filter( 'xforwc_svx_get_package_quantity_xforwc', array( 'XforWC_PackageQuantity_Settings', '_get_settings_xforwc' ) );
			}

			add_filter( 'svx_plugins', array( 'XforWC_PackageQuantity_Settings', 'add_plugin' ), 0 );

			add_action( 'admin_enqueue_scripts', array( 'XforWC_PackageQuantity_Settings', 'scripts' ) );

		}

		public static function xforwc( $settings ) {
			$settings['plugins'][] = self::$plugin;

			return $settings;
		}

		public static function add_plugin( $plugins ) {
			$plugins[self::$plugin['label']] = array(
				'slug' => self::$plugin['label'],
				'name' => self::$plugin['xforwc']
			);

			return $plugins;
		}

		public static function _get_settings_xforwc() {
			$settings = self::get_settings( array() );
			return $settings[self::$plugin['label']];
		}

		public static function scripts( $hook ) {

			if ( in_array( $hook, array( 'post.php', 'post-new.php', 'woocommerce_page_wc-settings' ) ) ) {
				$init = true;
			}

			if ( $hook == 'woocommerce_page_wc-settings' && isset( $_GET['page'], $_GET['tab'] ) && $_GET['page'] == 'wc-settings' && $_GET['tab'] == 'package_quantity_xforwc' ) {
				$init = true;
			}

			if ( isset( $_GET['page']) && $_GET['page'] == 'xforwoocommerce' ) {
				$init = true;
			}

			if ( !isset( $init ) ) {
				return false;
			}

			wp_register_script( 'package-quantity-xforwc-js', XforWC_PackageQuantity()->plugin_url() . '/assets/js/admin.js', array( 'jquery' ), XforWC_PackageQuantity()->version(), true );
            wp_enqueue_script( 'package-quantity-xforwc-js' );
		}

		public static function get_settings( $plugins ) {

			$plugins[self::$plugin['label']] = array(
				'slug' => self::$plugin['label'],
				'name' => esc_html( function_exists( 'XforWC' ) ? self::$plugin['xforwc'] : self::$plugin['name'] ),
				'desc' => esc_html( function_exists( 'XforWC' ) ? self::$plugin['name'] . ' v' . self::$plugin['version'] : esc_html__( 'Settings page for', 'xforwoocommerce' ) . ' ' . self::$plugin['name'] ),
				'link' => 'https://xforwoocommerce.com/store/package-quantity/',
				'ref' => array(
					'name' => esc_html__( 'Visit XforWooCommerce.com', 'xforwoocommerce' ),
					'url' => 'https://xforwoocommerce.com',
				),
				'doc' => array(
					'name' => esc_html__( 'Get help', 'xforwoocommerce' ),
					'url' => 'https://help.xforwoocommerce.com',
				),
				'sections' => array(
					'dashboard' => array(
						'name' => esc_html__( 'Dashboard', 'xforwoocommerce' ),
						'desc' => esc_html__( 'Dashboard Overview', 'xforwoocommerce' ),
					),
					'package_quantity' => array(
						'name' => esc_html__( 'Quantity Discount', 'xforwoocommerce' ),
						'desc' => esc_html__( 'Package Quantity Discounts', 'xforwoocommerce' ),
					),
					'package_templates' => array(
						'name' => esc_html__( 'Package Templates', 'xforwoocommerce' ),
						'desc' => esc_html__( 'Package Templates Overview', 'xforwoocommerce' ),
					),
					'manager' => array(
						'name' => esc_html__( 'Manager', 'xforwoocommerce' ),
						'desc' => esc_html__( 'Manager Overview', 'xforwoocommerce' ),
					),
					'general' => array(
						'name' => esc_html__( 'General', 'xforwoocommerce' ),
						'desc' => esc_html__( 'General Overview', 'xforwoocommerce' ),
					),
				),
				'settings' => array(

					'wcmn_dashboard' => array(
						'type' => 'html',
						'id' => 'wcmn_dashboard',
                        'desc' => '	
                            <img src="' . XforWC_PackageQuantity()->plugin_url() . '/assets/images/package-quantity-box-xforwc.png" class="svx-dashboard-image" />
                            <h3><span class="dashicons dashicons-store"></span> XforWooCommerce</h3>
                            <p>' . esc_html__( 'Visit XforWooCommerce.com store, demos and knowledge base.', 'xforwoocommerce' ) . '</p>
                            <p><a href="https://xforwoocommerce.com" class="xforwc-button-primary x-color" target="_blank">XforWooCommerce.com</a></p>

                            <br /><hr />

                            <h3><span class="dashicons dashicons-admin-tools"></span> ' . esc_html__( 'Help Center', 'xforwoocommerce' ) . '</h3>
                            <p>' . esc_html__( 'Need support? Visit the Help Center.', 'xforwoocommerce' ) . '</p>
                            <p><a href="https://help.xforwoocommerce.com" class="xforwc-button-primary red" target="_blank">XforWooCommerce.com HELP</a></p>
                            
                            <br /><hr />

                            <h3><span class="dashicons dashicons-update"></span> ' . esc_html__( 'Automatic Updates', 'xforwoocommerce' ) . '</h3>
                            <p>' . esc_html__( 'Get automatic updates, by downloading and installing the Envato Market plugin.', 'xforwoocommerce' ) . '</p>
                            <p><a href="https://envato.com/market-plugin/" class="svx-button" target="_blank">Envato Market Plugin</a></p>
                            
                            <br />',
						'section' => 'dashboard',
					),

					'wcmn_utility' => array(
						'name' => esc_html__( 'Plugin Options', 'xforwoocommerce' ),
						'type' => 'utility',
						'id' => 'wcmn_utility',
						'desc' => esc_html__( 'Quick export/import, backup and restore, or just reset your optons here', 'xforwoocommerce' ),
						'section' => 'dashboard',
					),

					'package_quantity' => array(
						'name' => esc_html__( 'Quantity Discount', 'xforwoocommerce' ),
						'type' => 'list',
						'id'   => 'package_quantity',
						'desc' => esc_html__( 'Add Quantity Discount', 'xforwoocommerce' ),
						'autoload' => false,
						'section' => 'package_quantity',
						'title' => esc_html__( 'Name', 'xforwoocommerce' ),
						'options' => 'list',
						'default' => array(),
						'settings' => array(
							'name' => array(
								'name' => esc_html__( 'Name', 'xforwoocommerce' ),
								'type' => 'text',
								'id' => 'name',
								'desc' => esc_html__( 'Enter quantity discount name', 'xforwoocommerce' ),
								'default' => '',
							),
							'options' => array(
								'name' => esc_html__( 'Add Options', 'xforwoocommerce' ),
								'type' => 'list',
								'id' => 'options',
								'desc' => esc_html__( 'Add quantity discount options', 'xforwoocommerce' ),
								'default' => '',
								'options' => 'list',
								'settings' => array(
									'name' => array(
										'name' => esc_html__( 'Name', 'xforwoocommerce' ),
										'type' => 'text',
										'id' => 'name',
										'desc' => esc_html__( 'Enter option name', 'xforwoocommerce' ),
										'default' => '',
									),
									'number' => array(
										'name' => esc_html__( 'Number of Items', 'xforwoocommerce' ),
										'type' => 'number',
										'id' => 'number',
										'desc' => esc_html__( 'Enter number of items', 'xforwoocommerce' ),
										'default' => '',
									),
									'discount_percent' => array(
										'name' => esc_html__( 'Discount Per Cent', 'xforwoocommerce' ),
										'type' => 'number',
										'id' => 'discount_percent',
										'desc' => esc_html__( 'Enter Discount per cent', 'xforwoocommerce' ),
										'default' => '',
									),
									'discount_price' => array(
										'name' => esc_html__( 'Exact Discount Price', 'xforwoocommerce' ),
										'type' => 'number',
										'id' => 'discount_price',
										'desc' => esc_html__( 'Enter exact Discount price', 'xforwoocommerce' ),
										'default' => '',
									),
								),
							),
						),
					),

					'package_templates' => array(
						'name' => esc_html__( 'Package Templates', 'xforwoocommerce' ),
						'type' => 'list',
						'id'   => 'package_templates',
						'desc' => esc_html__( 'Add Package Templates', 'xforwoocommerce' ),
						'autoload' => false,
						'section' => 'package_templates',
						'title' => esc_html__( 'Name', 'xforwoocommerce' ),
						'options' => 'list',
						'default' => array(),
						'settings' => array(
							'name' => array(
								'name' => esc_html__( 'Name', 'xforwoocommerce' ),
								'type' => 'text',
								'id' => 'name',
								'desc' => esc_html__( 'Enter package template name', 'xforwoocommerce' ),
								'default' => '',
							),
							'options' => array(
								'name' => esc_html__( 'Add Options', 'xforwoocommerce' ),
								'type' => 'list',
								'id' => 'options',
								'desc' => esc_html__( 'Add package template options', 'xforwoocommerce' ),
								'default' => '',
								'options' => 'list',
								'settings' => array(
									'name' => array(
										'name' => esc_html__( 'Name', 'xforwoocommerce' ),
										'type' => 'text',
										'id' => 'name',
										'desc' => esc_html__( 'Enter option name', 'xforwoocommerce' ),
										'default' => '',
									),
									'number' => array(
										'name' => esc_html__( 'Package Quantity', 'xforwoocommerce' ),
										'type' => 'number',
										'id' => 'number',
										'desc' => esc_html__( 'Enter package quantity', 'xforwoocommerce' ),
										'default' => '',
									),
								),
							),
						),
					),

					'package_manager' => array(
						'name' => esc_html__( 'Manager', 'xforwoocommerce' ),
						'type' => 'hidden',
						'id'   => 'package_manager',
						'desc' => esc_html__( 'Set packages', 'xforwoocommerce' ),
						'autoload' => false,
						'section' => 'hidden',
					),

					'_wcmn_default_qty' => array(
						'name' => esc_html__( 'Default Quantity Template', 'xforwoocommerce' ),
						'type' => 'select',
						'id'   => '_wcmn_default_qty',
						'desc' => esc_html__( 'Set default quantity template', 'xforwoocommerce' ),
						'autoload' => false,
						'section' => 'manager',
						'options' => 'read:package_quantity',
						'class' => 'svx-selectize',
					),

					'_wcmn_default' => array(
						'name' => esc_html__( 'Default Package Template', 'xforwoocommerce' ),
						'type' => 'select',
						'id'   => '_wcmn_default',
						'desc' => esc_html__( 'Set default package template', 'xforwoocommerce' ),
						'autoload' => false,
						'section' => 'manager',
						'options' => 'read:package_templates',
						'class' => 'svx-selectize',
					),

					'_wcmn_tags' => array(
						'name' => esc_html__( 'Tags', 'xforwoocommerce' ),
						'type' => 'list',
						'id'   => '_wcmn_tags',
						'desc' => esc_html__( 'Add tag package template', 'xforwoocommerce' ),
						'autoload' => false,
						'section' => 'manager',
						'title' => esc_html__( 'Name', 'xforwoocommerce' ),
						'options' => 'list',
						'default' => array(),
						'settings' => array(
							'name' => array(
								'name' => esc_html__( 'Name', 'xforwoocommerce' ),
								'type' => 'text',
								'id' => 'name',
								'desc' => esc_html__( 'Enter override name', 'xforwoocommerce' ),
								'default' => '',
							),
							'term' => array(
								'name' => esc_html__( 'Term', 'xforwoocommerce' ),
								'type' => 'select',
								'id'   => 'term',
								'desc' => esc_html__( 'Set override term', 'xforwoocommerce' ),
								'options' => 'ajax:taxonomy:product_tag:has_none',
								'default' => '',
								'class' => 'svx-selectize',
							),
							'qty' => array(
								'name' => esc_html__( 'Quantity Template', 'xforwoocommerce' ),
								'type' => 'select',
								'id'   => 'qty',
								'desc' => esc_html__( 'Set default quantity template', 'xforwoocommerce' ),
								'options' => 'read:package_quantity',
								'class' => 'svx-selectize',
							),
							'preset' => array(
								'name' => esc_html__( 'Package Template', 'xforwoocommerce' ),
								'type' => 'select',
								'id'   => 'preset',
								'desc' => esc_html__( 'Set override package template', 'xforwoocommerce' ),
								'options' => 'read:package_templates',
								'class' => 'svx-selectize',
							),
						),
					),

					'_wcmn_categories' => array(
						'name' => esc_html__( 'Category', 'xforwoocommerce' ),
						'type' => 'list',
						'id'   => '_wcmn_categories',
						'desc' => esc_html__( 'Add category package template', 'xforwoocommerce' ),
						'autoload' => false,
						'section' => 'manager',
						'title' => esc_html__( 'Name', 'xforwoocommerce' ),
						'options' => 'list',
						'default' => array(),
						'settings' => array(
							'name' => array(
								'name' => esc_html__( 'Name', 'xforwoocommerce' ),
								'type' => 'text',
								'id' => 'name',
								'desc' => esc_html__( 'Enter override name', 'xforwoocommerce' ),
								'default' => '',
							),
							'term' => array(
								'name' => esc_html__( 'Term', 'xforwoocommerce' ),
								'type' => 'select',
								'id'   => 'term',
								'desc' => esc_html__( 'Set override term', 'xforwoocommerce' ),
								'section' => 'manager',
								'options' => 'ajax:taxonomy:product_cat:has_none',
								'default' => '',
								'class' => 'svx-selectize',
							),
							'qty' => array(
								'name' => esc_html__( 'Quantity Template', 'xforwoocommerce' ),
								'type' => 'select',
								'id'   => 'qty',
								'desc' => esc_html__( 'Set default quantity template', 'xforwoocommerce' ),
								'options' => 'read:package_quantity',
								'class' => 'svx-selectize',
							),
							'preset' => array(
								'name' => esc_html__( 'Package Template', 'xforwoocommerce' ),
								'type' => 'select',
								'id'   => 'preset',
								'desc' => esc_html__( 'Set override package template', 'xforwoocommerce' ),
								'section' => 'manager',
								'options' => 'read:package_templates',
								'class' => 'svx-selectize',
							),
						),
					),

					'hide_qty' => array(
						'name' => esc_html__( 'Hide WooCommerce Quantity', 'xforwoocommerce' ),
						'type' => 'checkbox',
						'desc' => esc_html__( 'Hide WooCommerce quantity field. Make sure you have default pacakge template set', 'xforwoocommerce' ),
						'id'   => 'hide_qty',
						'autoload' => false,
						'default' => 'no',
						'section' => 'general'
					),

					'hide_one' => array(
						'name' => esc_html__( 'Hide the One Option', 'xforwoocommerce' ),
						'type' => 'checkbox',
						'desc' => esc_html__( 'Hide package templates one option. If hidden first quantity value will be set by default', 'xforwoocommerce' ),
						'id'   => 'hide_one',
						'autoload' => false,
						'default' => 'no',
						'section' => 'general'
					),

					'hide_qty_checkout' => array(
						'name' => esc_html__( 'Hide Quantity at Checkout', 'xforwoocommerce' ),
						'type' => 'checkbox',
						'desc' => esc_html__( 'Hide quantity options on Checkout page', 'xforwoocommerce' ),
						'id'   => 'hide_qty_checkout',
						'autoload' => false,
						'default' => 'no',
						'section' => 'general'
					),

				)
			);

			return SevenVX()->_do_options( $plugins, self::$plugin['label'] );
		}

	}

	XforWC_PackageQuantity_Settings::init();
