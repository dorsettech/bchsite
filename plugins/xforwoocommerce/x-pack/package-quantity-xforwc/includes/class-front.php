<?php

	if ( !defined( 'ABSPATH' ) ) {
		exit;
	}

	class XforWC_PackageQuantity_Frontend {

        protected static $_instance = null;
        
        public $options = array();

		public static function instance() {

			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
            }
            
            return self::$_instance;
            
		}

		function __construct() {
            add_action( 'woocommerce_after_add_to_cart_quantity', array( $this, 'add_packages_selectbox' ) );

            add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );

            add_filter( 'woocommerce_add_cart_item', array( $this, 'add_customs_item' ), 10, 3 );
            add_filter( 'woocommerce_get_cart_item_from_session', array( $this, 'add_customs_from_session' ), 10, 3 );

            add_filter( 'body_class', array( $this, 'add_class' ) );
            
            add_filter( 'xforwc__add_meta_information_used', array( $this, 'info' ) );
		}

		function info( $val ) {
			return array_merge( $val, array( 'Package Quantity Discount for WooCommerce' ) );
		}

        function scripts() {
			wp_register_script( 'package-quantity-xforwc-js', XforWC_PackageQuantity()->plugin_url() . '/assets/js/scripts.js', array( 'jquery' ), XforWC_PackageQuantity()->version(), true );
            wp_enqueue_script( 'package-quantity-xforwc-js' );
        }

        function add_packages_selectbox() {
            $this->_get_options();

            global $product;

            if ( empty( $product ) ) {
                return;
            }

            $_get_template = $this->_get_manager( $product->get_id() );

            if ( empty( $_get_template['preset'] ) ) {
                return;
            }

            $this->_get_selectbox( $_get_template );
        }

        function _get_selectbox_option( $option, $qty ) {
?>
            <option class="xwc--package-qty-option" value="<?php echo intval( $option['number'] ); ?>" data-discount-percent="<?php echo floatval( isset( $option['discount_percent'] ) ? $option['discount_percent'] : 0 ); ?>" data-discount-price="<?php echo floatval( isset( $option['discount_price'] ) ? $option['discount_price'] : 0 ); ?>">
                <?php echo esc_html( $option['name'] . ' ' . $this->_get_name_qty( $option['number'], $qty ) ); ?>
            </option>
<?php
        }

        function _get_name_qty( $n, $qty ) {
            $opt = $this->_get_qty_opt( $n, $qty );
            if ( floatval( $opt['discount_percent'] ) > 0 ) {
                global $product;

                return $n . 'X' . strip_tags( wc_price( $product->get_price() * ( 1 - floatval( $opt['discount_percent'] )/100 ) ) );
            }
            else if ( floatval( $opt['discount_price'] ) > 0 ) {
                return $n . 'X' . strip_tags( wc_price( $opt['discount_price'] ) );
            }
        }

        function _get_adjust_price( $id, $opt ) {
            if ( floatval( $opt['discount_percent'] ) > 0 ) {
                $product = wc_get_product( $id );                    

                return $product->get_price() * ( 1 - floatval( $opt['discount_percent'] )/100 );
            }
            else if ( floatval( $opt['discount_price'] ) > 0 ) {
                return $opt['discount_price'];
            }
        }

        function _get_qty_opt( $n, $qty ) {
            $qtyOpt = 0;

            foreach( $qty['options'] as $q ) {
                if ( intval( $n ) >= intval( $q['number'] ) ) {
                    $qtyOpt = $q;
                }
            }

            return $qtyOpt;
        }

        function _get_selectbox( $_get_template ) {
?>
            <select id="<?php echo uniqid('xwc--package-qty-'); ?>" class="xwc--package-qty<?php echo SevenVXGet()->get_option( 'hide_qty', 'package_quantity_xforwc', false ) == 'yes' ? ' xwc--package-hide' : ''; ?>">
<?php
                $this->_get_selectbox_option_one();
                foreach( $_get_template['preset']['options'] as $option ) {
                    $this->_get_selectbox_option( $option, $_get_template['qty'] );
                }
?>
            </select>
<?php
        }

        function _get_selectbox_option_one() {
            global $product;

            if ( SevenVXGet()->get_option( 'hide_one', 'package_quantity_xforwc', false ) !== 'yes' ) {
?>
                <option class="xwc--package-qty-option" value="1">
                    <?php echo esc_html( '1X' . strip_tags( wc_price( $product->get_price() ) ) ); ?>
                </option>
<?php
            }
        }

        function _get_options() {
            if ( empty( $this->options ) ) {
                $this->options['package_manager'] = SevenVXGet()->get_option( 'package_manager', 'package_quantity_xforwc', false );
                $this->options['package_quantity'] = SevenVXGet()->get_option( 'package_quantity', 'package_quantity_xforwc', false );
                $this->options['package_templates'] = SevenVXGet()->get_option( 'package_templates', 'package_quantity_xforwc', false );
            }
        }

		function _get_template( $template ) {
            if ( empty( $this->options['package_templates'] ) ) {
				return false;
            }

            if ( !is_array( $this->options['package_templates'] ) ) {
				return false;
            }

            foreach( $this->options['package_templates'] as $templateItem ) {
                if ( $template == sanitize_title( $templateItem['name'] ) ) {
                    return $templateItem;
                }
            }

            return false;
        }

		function _get_template_qty( $qtyName ) {
            if ( empty( $this->options['package_quantity'] ) ) {
				return false;
            }

            if ( !is_array( $this->options['package_quantity'] ) ) {
				return false;
            }

            foreach( $this->options['package_quantity'] as $qtyItem ) {
                if ( $qtyName == sanitize_title( $qtyItem['name'] ) ) {
                    return $qtyItem;
                }
            }

            return false;
        }

		function _get_manager( $id ) {
			if ( empty( $this->options['package_manager'] ) ) {
				$this->_get_options();
            }

            if ( empty( $this->options['package_manager'] ) ) {
				return false;
            }

            if ( empty( $id ) ) {
                return false;
            }

			$over = $this->options['package_manager'];

            if ( isset( $over['product_tag'] ) && is_array( $over['product_tag'] ) ) {
				foreach( $over['product_tag'] as $k => $v ) {
					$v = is_array( $v ) ? $v : array( 'term' => $k, 'preset' => $v );
					if ( !empty( $v['term'] ) && has_term( $v['term'], 'product_tag', $id ) ) {
						return array(
                            'preset' => $this->_get_template( $v['preset'] ),
                            'qty' => $this->_get_template_qty( $v['qty'] ),
                        );
					}
				}
			}

			if ( isset( $over['product_cat'] ) && is_array( $over['product_cat'] ) ) {

				$term_ids = wp_get_post_terms( $id, 'product_cat', array( 'fields' => 'ids' ) );

				if ( $term_ids && !is_wp_error( $term_ids ) ) {
					$term_parents = get_ancestors( $term_ids[0], 'product_cat' );

					$checks = array( $term_ids[0] );
					if ( !empty( $term_parents ) ) {
						$checks = array_merge( $checks, $term_parents );
					}

					foreach( $checks as $check ) {
						if ( array_key_exists( $check, $over['product_cat'] ) ) {
							return array(
                                'preset' => $this->_get_template( $over['product_cat'][$check]['preset'] ),
                                'qty' => $this->_get_template_qty( $over['product_cat'][$check]['qty'] ),
                            );
						}
					}
				}
			}
			
			if ( isset( $over['default'] ) && $over['default'] !== '' ) {
				return array(
                    'preset' => $this->_get_template( $over['default']['preset'] ),
                    'qty' => $this->_get_template_qty( $over['default']['qty'] ),
                );
            }

            return false;
        }

		function add_customs_from_session( $session_data, $values, $key ) {
			$session_data = $this->add_customs_item( $session_data, $key );

			return $session_data;
		}

		function add_customs_item( $product_data, $cart_item_key ) {
            $options = $this->_get_manager( $product_data['product_id'] );

            if ( empty( $options['qty'] ) ) {
                return $product_data;
            }

            $price = $this->_get_qty_opt( $product_data['quantity'], $options['qty'] );

            if ( $price > 0 ) {
                $this->set_price( $product_data, $this->_get_adjust_price( $product_data['product_id'], $price ) );
            }

			return $product_data;

		}

		function set_price( $product_data, $price ) {
			if ( method_exists( $product_data['data'], 'set_price' ) ) {
				$product_data['data']->set_price( floatval( $price ) );
			}
			else {
				$product_data['data']->adjust_price( floatval( $price ) );
			}
		}

        function add_class( $classes ) {
            if ( SevenVXGet()->get_option( 'hide_qty_checkout', 'package_quantity_xforwc', false ) == 'yes' ) {
                $classes[] = 'woocommerce-cart-form-no-qty';
            }
            return $classes;
        }

    }

    add_action( 'init', array( 'XforWC_PackageQuantity_Frontend', 'instance' ) );

	if ( !function_exists( 'xforwc__add_meta_information' ) ) {
		function xforwc__add_meta_information_action() {
			echo '<meta name="generator" content="XforWooCommerce.com - ' . esc_attr( implode( ' - ', apply_filters( 'xforwc__add_meta_information_used', array() ) ) ) . '"/>';
		}
		function xforwc__add_meta_information() {
			add_action( 'wp_head', 'xforwc__add_meta_information_action', 99 );
		}
		xforwc__add_meta_information();
	}
