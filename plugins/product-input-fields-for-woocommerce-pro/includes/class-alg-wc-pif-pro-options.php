<?php
/**
 * Product Input Fields for WooCommerce Pro - Options
 *
 * @version 1.1.7
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Alg_WC_PIF_Pro_Options' ) ) {

	class Alg_WC_PIF_Pro_Options {

		/**
		 * Gets products input fields options
		 *
		 * @version 1.1.7
		 * @since   1.1.3
		 * @todo    [dev] add info about Pro version's additional options to free version (i.e. "Converts characters to uppercase...", "Allows multiple options to be selected, if using the select type", "Multi Checkbox")
		 */
		public static function get_settings( $settings ) {

			//========= General Section ============
			$general_section_end = wp_list_filter( $settings, array(
				'id'   => 'general_options',
				'type' => 'sectionend',
			), 'AND' );

			if ( is_array( $general_section_end ) && ! empty( $general_section_end ) ) {
				reset( $general_section_end );
				$general_section_end_index = key( $general_section_end );

				$new_settings = array(
					array(
						'title'   => __( 'Uppercase', 'product-input-fields-for-woocommerce' ),
						'desc'    => __( 'Converts characters to uppercase version, when possible', 'product-input-fields-for-woocommerce' ),
						'id'      => 'to_uppercase',
						'default' => 'no',
						'type'    => 'checkbox',
					),
				);

				array_splice( $settings, $general_section_end_index, 0, $new_settings );
			}

			//========= Select radio option  ============
			$radio_section_end = wp_list_filter( $settings, array(
				'id'   => 'type_select_radio_options',
				'type' => 'sectionend',
			), 'AND' );

			if ( is_array( $radio_section_end ) && ! empty( $radio_section_end ) ) {
				reset( $radio_section_end );
				$radio_section_end_index = key( $radio_section_end );

				$new_settings = array(
					array(
						'title'    => __( 'Multiple', 'product-input-fields-for-woocommerce' ),
						'desc'     => __( 'Allows multiple options to be selected, if using the <strong>select</strong> type', 'product-input-fields-for-woocommerce' ),
						'desc_tip' => __( 'Only works with select type', 'product-input-fields-for-woocommerce' ),
						'id'       => 'multiple',
						'default'  => 'no',
						'type'     => 'checkbox',
					),
				);

				array_splice( $settings, $radio_section_end_index, 0, $new_settings );
			}

			// ========== Multiple title =============
			$multiple_title = wp_list_filter( $settings, array(
				'id'   => 'type_select_radio_options',
				'type' => 'title',
			), 'AND' );
			if ( is_array( $multiple_title ) && ! empty( $multiple_title ) ) {
				reset( $multiple_title );
				$multiple_title_index = key( $multiple_title );

				$fill_only_on_type_message               = __( 'Fill this only if <strong>%s</strong> type is selected.', 'product-input-fields-for-woocommerce' );
				$settings[ $multiple_title_index ]['title'] = __( 'Select/Radio/Multi Checkbox Type Options', 'product-input-fields-for-woocommerce' );
				$settings[ $multiple_title_index ]['desc']  = sprintf( $fill_only_on_type_message, __( 'Select/Radio/Multi Checkbox', 'product-input-fields-for-woocommerce' ) );
			}

			//========= Type option  ============
			$type_option = wp_list_filter( $settings, array(
				'id'   => 'type',
				'type' => 'select',
			), 'AND' );
			$first_key = key( $type_option );
			$type_option[ $first_key ]['options']['multicheck'] = __( 'Multi Checkbox', 'product-input-fields-for-woocommerce' );
			$settings[ $first_key ] = $type_option[ $first_key ];
			return $settings;
		}
	}
}