<?php
/**
 * Product Input Fields for WooCommerce Pro - Functions
 *
 * @version 1.1.7
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Alg_WC_PIF_Pro_Functions' ) ) {

	class Alg_WC_PIF_Pro_Functions {

		/**
		 * Handles Uppercase input fields
		 *
		 * @version 1.1.3
		 * @since   1.1.3
		 */
		public static function handle_uppercase_input_field( $field_html, $field_type, $field_options ) {
			if ( $field_options['to_uppercase'] !== 'yes' ) {
				return $field_html;
			}
			$field_html = preg_replace( '/(<*\b[^><]*)>/i', '$1 data-uppercase="true" oninput="this.value = this.value.toUpperCase()">', $field_html );

			return $field_html;
		}

		/**
		 * Handles Multiple attribute
		 *
		 * @version 1.1.7
		 * @since   1.1.7
		 */
		public static function handle_multiple_input_field( $field_html, $field_type, $field_options, $_value, $field_name, $class, $required, $style ) {
			if (
				( $field_type != 'select' && $field_type != 'multicheck' )
			) {
				return $field_html;
			}

			if (
				$field_type == 'select' &&
				$field_options['multiple'] == 'yes'
			) {
				// Add multiple attribute
				$field_html = preg_replace( '/(<select[^><]*)>/i', '$1 multiple="multiple">', $field_html );

				// Add [] on name attribute
				$field_html = preg_replace( '/(name="(.*?))"/i', '$1[]"', $field_html );
			} elseif ( $field_type == 'multicheck' ) {
				$select_options_raw  = $field_options['type_select_options'];
				$select_options      = alg_get_select_options( $select_options_raw, false );
				$select_options_html = '';
				if ( ! empty( $select_options ) ) {
					reset( $select_options );
					foreach ( $select_options as $option_key => $option_text ) {
						$_value              = ! is_array( $_value ) ? array() : $_value;
						$checked_str         = in_array( $option_key, $_value ) ? 'checked' : '';
						$select_options_html .= '<input' . $class . $style . ' type="checkbox" value="' . esc_attr( $option_key ) .
						                        '" name="' . $field_name . '[]" id="' . $field_name . '_' . esc_attr( $option_key ) . '"' . $checked_str . ' />';
						$select_options_html .= '<label for="' . $field_name . '_' . esc_attr( $option_key ) .
						                        '">' . $option_text . '</label><br>';
					}
				}
				$field_html = $select_options_html;
			}

			return $field_html;
		}
	}
}
