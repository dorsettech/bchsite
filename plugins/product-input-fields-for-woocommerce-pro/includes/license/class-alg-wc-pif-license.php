<?php 
/**
 * Product Input Fields for WooCommerce Pro - License
 *
 * @version 1.3.1
 * @since   1.3.1
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Alg_WC_PIF_Pro_License' ) ) {

	class Alg_WC_PIF_Pro_License {

		static function pif_edd_handle_license() {
			if ( isset( $_GET[ 'license' ] ) && $_GET[ 'license' ] === 'pif_activate' ) {
				self::pif_activate_license();
			} elseif ( isset( $_GET[ 'license' ] ) && $_GET[ 'license' ] === 'pif_deactivate' ) {
				self::pif_deactivate_license();
			}
		}

		static function pif_activate_license() {
			// run a quick security check
			/*if ( ! check_admin_referer( 'edd_sample_nonce', 'edd_sample_nonce' ) )
				return; // get out if we didn't click the Activate button*/
			// retrieve the license from the database
			$license = trim( get_option( 'alg_wc_pif_edd_license_key_pif' ) );
			// data to send in our API request
			$api_params = array(
					'edd_action'=> 'activate_license',
					'license'   => $license,
					'item_name' => urlencode( EDD_PIF_ITEM_NAME ) // the name of our product in EDD
			);
			// Call the custom API.
			$response = wp_remote_get( add_query_arg( $api_params, EDD_PIF_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );
			// make sure the response came back okay
			if ( is_wp_error( $response ) )
				return false;
			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			// $license_data->license will be either "active" or "inactive"
			update_option( 'edd_license_key_pif_status', $license_data->license );
		}

		static function pif_deactivate_license() {
			// run a quick security check
			/*if ( ! check_admin_referer( 'edd_sample_nonce', 'edd_sample_nonce' ) )
				return; // get out if we didn't click the Activate button*/
			// retrieve the license from the database
			$license = trim( get_option( 'alg_wc_pif_edd_license_key_pif' ) );
			// data to send in our API request
			$api_params = array(
					'edd_action'=> 'deactivate_license',
					'license'   => $license,
					'item_name' => urlencode( EDD_PIF_ITEM_NAME ) // the name of our product in EDD
			);
			// Call the custom API.
			$response = wp_remote_get( add_query_arg( $api_params, EDD_PIF_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );
			// make sure the response came back okay
			if ( is_wp_error( $response ) )
				return false;
			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			// $license_data->license will be either "deactivated" or "failed"
			if ( $license_data->license == 'deactivated' )
				delete_option( 'edd_license_key_pif_status' );
		}

		static function check_for_updates() {

			if( ! class_exists( 'EDD_PIF_Plugin_Updater' ) ) {
				
				// load our custom updater if it doesn't already exist
				$vendorDir = dirname(dirname(__FILE__));
				$baseDir = dirname($vendorDir);
				include( $baseDir . '/plugin-updates/EDD_PIF_Plugin_Updater.php' );

			}
			/**
			 * Retrieve our license key from the DB
			 */ 
			$license_key = trim( get_option( 'alg_wc_pif_edd_license_key_pif' ) );

			/**
			 * Setup the updater
			 */
			$plugin_path = dirname ( dirname ( untrailingslashit( plugin_dir_path ( __FILE__ ) ) ) ) . '/product-input-fields-for-woocommerce-pro.php';

			$edd_updater = new EDD_PIF_Plugin_Updater( EDD_PIF_STORE_URL, $plugin_path , array(
				'version'   => Alg_WC_PIF_Pro::$version,    // current version number
				'license'   => $license_key,      // license key (used get_option above to retrieve from DB)
				'item_name' => EDD_PIF_ITEM_NAME, // name of this plugin
				'author'    => 'Ashok Rane'       // author of this plugin
				)
			);
		}
	}
}