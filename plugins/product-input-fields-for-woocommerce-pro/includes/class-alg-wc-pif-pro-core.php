<?php
/**
 * Product Input Fields for WooCommerce Pro - Core Class
 *
 * @version 1.1.7
 * @since   1.1.3
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'Alg_WC_PIF_Pro_Core' ) ) {

	class Alg_WC_PIF_Pro_Core {

		public function __construct() {
			add_filter( 'alg_product_input_fields_options', array( 'Alg_WC_PIF_Pro_Options', 'get_settings' ) );
			add_filter( 'alg_wc_pif_field_html', array( 'Alg_WC_PIF_Pro_Functions', 'handle_uppercase_input_field' ), 10, 3 );
			add_filter( 'alg_wc_pif_field_html', array( 'Alg_WC_PIF_Pro_Functions', 'handle_multiple_input_field' ), 10, 8 );
		}

	}
}
