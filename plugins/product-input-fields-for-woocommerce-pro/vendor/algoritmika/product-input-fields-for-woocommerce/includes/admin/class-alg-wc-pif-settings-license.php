<?php
/**
 * Product Input Fields for WooCommerce - License Settings
 *
 * @version 1.1.8
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Alg_WC_PIF_Settings_License' ) ) :

class Alg_WC_PIF_Settings_License extends Alg_WC_PIF_Settings_Section {

	/**
	 * Constructor.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function __construct() {
		$this->id   = 'license';
		$this->desc = __( 'License', 'product-input-fields-for-woocommerce' );
		parent::__construct();
	}

	/**
	 * get_section_settings.
	 *
	 * @version 1.1.8
	 * @since   1.0.0
	 * @todo    (later) major reset settings - including all global and *local* input fields
	 * @todo    (later) add dashboard and move all options (except dashboard) to another settings section(s)
	 * @todo    (later) global required_message, max_size_message, wrong_file_type_message (maybe with replaceable %title%) (and add desc_tip - used when not required by JS enabled / outside the add to cart button form and per field message not set (i.e. leave blank to use default)) - validate_product_input_fields_on_add_to_cart()
	 * @todo    (maybe) frontend_position - clean up
	 */
	function get_section_settings() {
		$license = get_option( 'alg_wc_pif_edd_license_key_pif' );
		$status  = get_option( 'edd_license_key_pif_status' );
		$current_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		$link = '';
		if ( false !== $license ) {
			if( $status !== false && $status == 'valid' ) {
				$link = '<span style="color:green;">active</span>' .
				wp_nonce_field( 'edd_sample_nonce' , 'edd_sample_nonce' ) . 
				'<a href="' . $current_link . '&license=pif_deactivate" class="button-secondary" name="edd_pif_license_deactivate">Deactivate<a/>';
			} else {
				$link = wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ) . 
				'<a href="' . $current_link . '&license=pif_activate" class="button-secondary" name="edd_pif_license_activate">Activate<a/>';
			}
		}

		$settings = array(
			array(
				'title'     => __( 'Plugin License Options', 'product-input-fields-for-woocommerce' ),
				'type'      => 'title',
				'id'        => 'alg_wc_pif_license_options',
			),
			array(
				'title'     => __( 'License Key	', 'product-input-fields-for-woocommerce' ),
				'desc'      => __( 'Enter your license key.', 'product-input-fields-for-woocommerce' ),
				'id'        => 'edd_license_key_pif',
				'default'   => '',
				'type'      => 'text',
			),
			array(
				'title'     => __( 'Activate License', 'product-input-fields-for-woocommerce' ),
				'desc'      => __( $link, 'product-input-fields-for-woocommerce' ),
				'id'        => 'edd_license_pif_hidden_button',
				'default'   => '',
				'type'      => 'text',
				'css'       => 'display:none;',
			),
			array(
				'type'      => 'sectionend',
				'id'        => 'alg_wc_pif_license_options',
			),
		);
		
		return $settings;
	}

}

endif;

return new Alg_WC_PIF_Settings_License();
