<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Alg_WC_PIF_Pro_Core' => $baseDir . '/includes/class-alg-wc-pif-pro-core.php',
    'Alg_WC_PIF_Pro_Functions' => $baseDir . '/includes/class-alg-wc-pif-pro-functions.php',
    'Alg_WC_PIF_Pro_Options' => $baseDir . '/includes/class-alg-wc-pif-pro-options.php',
    'Alg_WC_PIF_Pro_License_Page' => $baseDir . '/includes/license/class-alg-wc-pif-license.php',
    'Alg_WC_PIF_Pro_License' => $baseDir . '/includes/license/alg-wc-pif-pro-license-page/class-alg-wc-pif-license-page.php',
    
);
