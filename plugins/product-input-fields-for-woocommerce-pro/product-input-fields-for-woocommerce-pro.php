<?php
/*
Plugin Name: Product Input Fields for WooCommerce Pro
Plugin URI: https://www.tychesoftwares.com/store/premium-plugins/product-input-fields-for-woocommerce/
Description: Add custom frontend input fields to WooCommerce products.
Version: 1.3.2
Author: Tyche Softwares
Author URI: https://www.tychesoftwares.com
Text Domain: product-input-fields-for-woocommerce
Domain Path: /langs
Copyright: © 2019 Algoritmika Ltd.
WC tested up to: 3.5.7
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Check if WooCommerce is active
$plugin = 'woocommerce/woocommerce.php';
if (
	! in_array( $plugin, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ) ) &&
	! ( is_multisite() && array_key_exists( $plugin, get_site_option( 'active_sitewide_plugins', array() ) ) )
) {
	return;
}

// Constants
if ( ! defined( 'ALG_WC_PIF_PRO_VERSION' ) ) {
	define( 'ALG_WC_PIF_PRO_VERSION', '1.3.2' );
}
if ( ! defined( 'ALG_WC_PIF_PRO_ID' ) ) {
	define( 'ALG_WC_PIF_PRO_ID',      'alg_wc_pif_pro' );
}

if ( ! function_exists( 'get_wc_pif_pro_option' ) ) {
	/**
	 * get_wc_pif_pro_option.
	 *
	 * @version 1.1.3
	 * @since   1.1.3
	 */
	function get_wc_pif_pro_option( $option, $default = false ) {
		return get_option( ALG_WC_PIF_PRO_ID . '_' . $option, $default );
	}
}

if ( ! class_exists( 'Alg_WC_PIF_Pro' ) ) :

/**
 * Main Alg_WC_PIF_Pro Class
 *
 * @class   Alg_WC_PIF_Pro
 * @version 1.1.3
 * @since   1.1.3
 */
final class Alg_WC_PIF_Pro {

	public static $version = '1.3.2';


	/**
	 * @var   Alg_WC_PIF_Pro The single instance of the class
	 * @since 1.1.3
	 */
	protected static $_instance = null;

	/**
	 * Main Alg_WC_PIF_Pro Instance
	 *
	 * Ensures only one instance of Alg_WC_PIF_Pro is loaded or can be loaded.
	 *
	 * @version 1.1.3
	 * @since   1.1.3
	 * @static
	 * @return  Alg_WC_PIF_Pro - Main instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Initializes
	 *
	 * @version 1.1.4
	 * @since   1.1.4
	 * @access  public
	 */
	public function init(){
		// Set up localisation
		$this->handle_localization();

		// Include required files
		$this->includes();

		add_action( 'alg_get_plugins_list', array( $this, 'pif_remove_plugin_name' ), PHP_INT_MAX );

		if ( is_admin() ) {
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'action_links' ) );
		}

		new Alg_WC_PIF_Pro_Core();
	}

	/**
	 * Localization
	 *
	 * @version 1.1.3
	 * @since   1.1.3
	 */
	private function handle_localization() {
		$domain = 'product-input-fields-for-woocommerce';
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );
		$loaded = load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . 'plugins' . '/' . $domain . '-pro' . '/' . $domain . '-' . $locale . '.mo' );
		if ( $loaded ) {
			return $loaded;
		} else {
			load_plugin_textdomain( $domain, false, dirname( plugin_basename( __FILE__ ) ) . '/langs/' );
		}
	}

	/**
	 * alg_wc_product_input_fields_pro_filter.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 * @todo    (maybe) rethink max=100
	 */
	function alg_wc_product_input_fields_filter( $value, $type, $product_id = 0 ) {
		switch ( $type ) {
			case 'settings':
				return '';
			case 'settings_array':
				return array( 'min' => '1', 'max' => '100' );
			case 'all_products_total_fields':
				return get_wc_pif_option( 'global_total_number', 1 );
			case 'per_product_total_fields_default':
				return get_wc_pif_option( 'local_total_number_default', 1 );
			case 'per_product_total_fields':
				return get_post_meta( $product_id, '_' . ALG_WC_PIF_ID . '_' . 'local_total_number', true );
		}
		return $value;
	}

	/**
	 * Show action links on the plugin screen.
	 *
	 * @version 1.1.3
	 * @since   1.0.0
	 * @param   mixed $links
	 * @return  array
	 */
	function action_links( $links ) {
		$custom_links = array();
		$custom_links[] = '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=alg_wc_pif' ) . '">' . __( 'Settings', 'woocommerce' ) . '</a>';
		return array_merge( $custom_links, $links );
	}

	/**
	 * Include required core files used in admin and on the frontend.
	 *
	 * @version 1.1.3
	 * @since   1.0.0
	 */
	function includes() {
		$this->settings = array();

		if ( is_admin() && get_wc_pif_pro_option( 'version', '' ) !== ALG_WC_PIF_PRO_VERSION ) {
			foreach ( $this->settings as $section ) {
				foreach ( $section->get_settings() as $value ) {
					if ( isset( $value['default'] ) && isset( $value['id'] ) ) {
						$autoload = isset( $value['autoload'] ) ? (bool) $value['autoload'] : true;
						add_option( $value['id'], $value['default'], '', $autoload );
					}
				}
			}
			update_option( ALG_WC_PIF_PRO_ID . '_' . 'version', ALG_WC_PIF_PRO_VERSION );
		}

		$this->define_constants();

		if ( isset( $_GET['license'] ) ) {
			add_action( 'admin_init', array( 'Alg_WC_PIF_Pro_License', 'pif_edd_handle_license' ) );
		}

		Alg_WC_PIF_Pro_License::check_for_updates();
	}	

	/**
	 * Get the plugin url.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 * @return  string
	 */
	function plugin_url() {
		return untrailingslashit( plugin_dir_url( __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 * @return  string
	 */
	function plugin_path() {
		return untrailingslashit( plugin_dir_path( __FILE__ ) );
	}

	private function define_constants() {
		define( 'EDD_PIF_STORE_URL', 'https://www.tychesoftwares.com/' );
		define( 'EDD_PIF_ITEM_NAME', 'Product Input Fields for WooCommerce' );
	}

	

	public function pif_remove_plugin_name(){

		$plugin_list = get_option( 'alg_wpcodefactory_helper_plugins' );

		if ( $plugin_list != '' ) {
			$plugin_list = array_diff( $plugin_list, array( 'product-input-fields-for-woocommerce-pro' ) );
			update_option( 'alg_wpcodefactory_helper_plugins', $plugin_list );
		}
	}

}

endif;

if ( ! function_exists( 'alg_wc_product_input_fields_pro' ) ) {
	/**
	 * Returns the main instance of Alg_WC_PIF_Pro to prevent the need to use globals.
	 *
	 * @version 1.1.3
	 * @since   1.1.3
	 * @return  Alg_WC_PIF_Pro
	 */
	function alg_wc_product_input_fields_pro() {
		return Alg_WC_PIF_Pro::instance();
	}
}

$plugin = alg_wc_product_input_fields_pro();
add_filter( 'alg_wc_product_input_fields', array( $plugin, 'alg_wc_product_input_fields_filter' ), PHP_INT_MAX, 3 );
require_once "vendor/autoload.php";
$plugin->init();
