=== Product Input Fields for WooCommerce Pro ===
Contributors: algoritmika, anbinder, karzin, tychesoftwares
Tags: woocommerce, product input fields
Requires at least: 4.4
Tested up to: 5.1
Stable tag: 1.3.2
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Add custom frontend input fields to WooCommerce products.

== Description ==

**Product Input Fields for WooCommerce** plugin lets you add custom input fields to WooCommerce product's frontend for customer to fill before adding product to cart.

Input fields can be added **globally** (i.e. for all products) or on **per product** basis.

You can choose numerous different **types** for fields:

* Text
* Textarea
* Number
* Checkbox
* Color
* File
* Datepicker
* Weekpicker
* Timepicker
* Select
* Radio
* Password
* Country
* Email
* Phone
* Search
* URL
* Range

Each type comes with specific **options** you can set for each field.

Additionally you can set fields **HTML template** and much more.

= Feedback =
* We are open to your suggestions and feedback. Thank you for using or trying out one of our plugins!

== Installation ==

1. Upload the entire plugin folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the "Plugins" menu in WordPress.
3. Start by visiting plugin settings at "WooCommerce > Settings > Product Input Fields".

== Screenshots ==

1. Frontend options.
2. Email options.
3. Setting number of global (i.e. for all products) product input fields.
4. Setting global (i.e. for all products) product input field options.
5. Setting local (i.e. on per product basis) product input field options.

== Changelog ==
= 1.3.2 - 10/04/2019 = 

* Fix - Update notification was not coming for the plugin when updated from version 1.3 to 1.3.1. This is fixed now. 

= 1.3.1 - 10/04/2019 = 
* Enhancement - The plugin is made compatible with the WPML plugin. The static strings and Dynamic strings can now be translated into different languages from the plugin. 
* Enhancement - A new page is added which will be shown when the plugin is installed and activated for the first time. This page allows users to activate the license key  for future updates. 
* Fix - Data was not getting deleted from the database when the plugin is uninstalled. This is fixed now. 

= 1.3 = 
* This is minor update to the plugin. This update just has changes pointing to the new server for automatic updates. 

= 1.2.5 - 23/01/2019 =
* Dev - Plugin URI updated.
* Dev - Admin settings restyled and descriptions updated.
* Dev - Code clean up.

= 1.2.4 - 26/10/2018 =
* Add compatibility with Advanced Order Export For WooCommerce plugin

= 1.2.3 - 09/10/2018 =
* Display fields on PDF Invoices & Packing Slips plugin

= 1.2.2 - 19/09/2018 =
* Add 'Load Datepicker Style' option
* Add 'Load Timepicker Style' option
* Update Timepicker JS
* Turn off autocomplete on timepicker, datepicker and weekpicker
* Fix Datepicker, Timepicker and Weekpicker style

= 1.2.1 - 18/09/2018 =
* Add color input compatibility with Opera and Safari

= 1.2.0 - 17/09/2018 =
* Add color section on admin input fields options
* Add option to allow typing or pasting the color manually
* Update WC tested up to

= 1.1.9 - 10/08/2018 =
* Fix maxlength attribute on textarea

= 1.1.8 - 01/08/2018 =
* Fix PHP warnings
* Fix file uploading when using multiple file inputs
* Add "Smart Textarea" option, showing only the textarea excerpt on frontend and hovering it will make it display the full content

= 1.1.7 - 12/07/2018 =
* Add multi select option for select field
* Add multi checkbox option
* Check if order and input fields exist before trying to delete file uploads
* Improve help link for pattern attribute
* Improve input sanitizing
* Display multiple array value as comma separated string

= 1.1.6 - 18/05/2018 =
* Remove slashes from the values

= 1.1.5 - 02/05/2018 =
* Remove check for pro version

= 1.1.4 - 29/04/2018 =
* Fix empty setting section on admin settings

= 1.1.3 - 28/04/2018 =
* Add composer
* Add new option to convert characters to uppercase version, when possible

= 1.1.2 - 18/04/2018 =
* Dev - "WC tested up to" added to plugin header.

= 1.1.1 - 30/10/2017 =
* Dev - WooCommerce v3.2 compatibility - Admin settings - `select` type options fixed.
* Dev - WooCommerce v3.0 compatibility - "woocommerce_add_order_item_meta hook uses out of date data structures and function is deprecated..." notice fixed.
* Fix - `add_product_input_fields_to_order_item_meta()` - Checking if product input fields values exist (fixes notice in log).
* Dev - Saving settings array as main class property.

= 1.1.0 - 15/06/2017 =
* Dev - WooCommerce 3.x.x compatibility - `output_custom_input_fields_in_admin_order()` - Using `meta_exists()` and `get_meta()` functions to access order items meta data.
* Dev - WooCommerce 3.x.x compatibility - `alg_get_frontend_product_input_fields()` - Product ID (using `get_id()` function instead of accessing `id` object property directly).
* Dev - Core - `add_files_to_email_attachments()` - Additional validation added.
* Tweak - Plugin link updated from <a href="https://coder.fm">https://coder.fm</a> to <a href="https://wpcodefactory.com">https://wpcodefactory.com</a>.

= 1.0.1 - 28/03/2017 =
* Dev - Language (POT) file added.
* Dev - readme.txt updated (screenshots added etc.).
* Tweak - http replaced with https in links to coder.fm.

= 1.0.0 - 28/03/2017 =
* Initial Release.

== Upgrade Notice ==

= 1.2.4 =
* Add compatibility with Advanced Order Export For WooCommerce plugin