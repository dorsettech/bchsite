<?php
global $WCFM, $wp_query;

?>

<div class="collapse wcfm-collapse" id="wcfm_build_listing">
	
	<div class="wcfm-page-headig">
		<span class="wcfmfa fa fa-cubes"></span>
		<span class="wcfm-page-heading-text"><?php _e( 'Shop Featured Product', 'wcfm-custom-menus' ); ?></span>
		<?php do_action( 'wcfm_page_heading' ); ?>
	</div>
	<div class="wcfm-collapse-content">
		<div id="wcfm_page_load"></div>
		<?php do_action( 'before_wcfm_build' ); ?>
		
		<div class="wcfm-container wcfm-top-element-container">
			<h2><?php _e('Shop Featured Product', 'wcfm-custom-menus' ); ?></h2>
			<div class="wcfm-clearfix"></div>
	  </div>
	  <div class="wcfm-clearfix"></div><br />
		

		<div class="wcfm-container">
			<div id="wcfm_build_listing_expander" class="wcfm-content">

				<!---- Add Content Here ---->
				<form id="wcfm_manage_shop_cat" method="post">
					<?php $nonce = wp_create_nonce( 'shop_manage_nonce' ); ?>
					<input type="hidden" name="shop_manage_nonce" value="<?php echo $nonce ?>" />
					<input type="hidden" name="wcfm_action" value="shop_manage" />
					<?php

						$product_categories   = get_terms( 'product_cat', 'orderby=name&hide_empty=0&parent=0' );

						$args = array(
							'hide_empty' => false,
							'meta_query' => array(
								array(
									'key'		=> 'vendor_id',
									'value'		=> get_current_user_id(),
								)
							),
							'taxonomy'  => 'shop_category',
						);
						$product_categories = get_terms( $args );

						$product_section_cat = get_user_meta( get_current_user_id(), 'product_section_cat', true );

						// Added 28-10-2020
						// Get and set for saved selected dropdown value
						if (isset( $product_section_cat ) && !empty( $product_section_cat )) {
							$product_section_cat = explode( ',', $product_section_cat );
						} else {
							$product_section_cat = array();
						}

						// Load Added Featured Shop Category
						$products_array = array();
						if ( !empty($product_categories) && is_array($product_categories)) {
							foreach ($product_categories as $product_category) {
								$products_array[$product_category->term_id] = $product_category->name;
							}
						} else {
							$products_array = array('-- Empty Shop Category --');
						}

						// Old
						// $products_array = array();
						// if($product_section_cat != '') {
						// 	$product_section_cat = explode(",", $product_section_cat);
						// 	// $product_section_cat = explode(",", '');
						// 	if( !empty( $product_section_cat ) ) {
						// 		foreach( $product_section_cat as $upsell_id ) {
						// 			$products_array[$upsell_id] = get_term( absint($upsell_id), 'shop_category' )->name;
						// 		}
						// 	}
						// } else {
						// 	$products_array = array('-- Empty Shop Category --');
						// }
						// if ( get_current_user_id() == 3990 ) {
						// 	ini_set('display_errors', 1);
						// 	ini_set('display_startup_errors', 1);
						// 	error_reporting(E_ALL);
						// 	echo "<pre>";
						// 	print_r($products_array);
						// 	echo "</pre>";
						// }
						$WCFM->wcfm_fields->wcfm_generate_form_field( array(
									"dropdown_product_cat" => array(
										'type' => 'select',
										'label' => 'Select featured Shop Category' ,
										'name' => 'product_section_cat',
										'class' => 'wcfm-select wcfm_ele simple variable external grouped booking',
										'label_class' => 'wcfm_title',
										'options' => $products_array,
										'value'	=> $product_section_cat,
										'custom_attributes' => array( 'taxonomy' => 'shop_category', 'parent' => '0' ),
										'attributes' => array(  'multiple' => 'multiple', 'style' => 'width: 300px;' ) 
									)
								)
							);


					?>
					<input type="submit" name="submit_shop_section" value="Save" id="wcfm_manage_shop_submit_button" class="wcfm_submit_button test" />

				</form>


				<!-- play end here -->
				<div class="wcfm-clearfix"></div>
			</div>
			<div class="wcfm-clearfix"></div>
		</div>
	
		<div class="wcfm-clearfix"></div>
		<?php
		do_action( 'after_wcfm_build' );
		?>
	</div>
</div>