<?php
global $WCFM, $wp_query;
	$args = array(
		'hide_empty' => false, // also retrieve terms which are not used yet
		'meta_query' => array(
			array(
				'key'		=> 'vendor_id',
				'value'		=> get_current_user_id(),
			)
		),
		'taxonomy'  => 'shop_category',
	);
	$terms = get_terms( $args );
?>

<div class="collapse wcfm-collapse" id="wcfm_build_listing">
	
	<div class="wcfm-page-headig">
		<span class="wcfmfa fa fa-cubes"></span>
		<span class="wcfm-page-heading-text"><?php _e( 'Shop Category Management', 'wcfm-custom-menus' ); ?></span>
		<?php do_action( 'wcfm_page_heading' ); ?>
	</div>
	<div class="wcfm-collapse-content">
		<div id="wcfm_page_load"></div>
		<?php do_action( 'before_wcfm_build' ); ?>
		
		<div class="wcfm-container wcfm-top-element-container">
			<h2><?php _e('Shop Category Management', 'wcfm-custom-menus' ); ?></h2>
			<?php
			echo '<a id="add_new_product_dashboard" class="add_new_wcfm_ele_dashboard text_tip" href="'.get_wcfm_edit_shop_category_url().'" data-tip="' . __('Add New Product', 'wc-frontend-manager') . '"><span class="wcfmfa fa-cube"></span><span class="text">' . __( 'Add New', 'wc-frontend-manager') . '</span></a>';
			?>
			<div class="wcfm-clearfix"></div>
	  </div>
	  <div class="wcfm-clearfix"></div><br />
		

		<div class="wcfm-container">
			<div id="wcfm_build_listing_expander" class="wcfm-content">

				<!---- Add Content Here ----->
				<table id="wcfm-products" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th><?php _e( 'Name', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Description', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Actions', 'wc-frontend-manager' ); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($terms as $key => $category) { ?>
								<tr>
									<td><?php echo $category->term_id; ?></td>
									<td><?php echo $category->name; ?></td>
									<td><?php echo $category->description; ?></td>
									<td>
										<a class="wcfm-action-icon" href="<?php echo get_wcfm_edit_shop_category_url($category->term_id, $category); ?>"><span class="wcfmfa fa-edit text_tip" data-tip="Edit" data-hasqtip="98"></span></a>		
									</td>
								</tr>
							<?php }
						?>
					</tbody>
					<tfoot>
						<tr>
							<th>#</th>
							<th><?php _e( 'Name', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Description', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Actions', 'wc-frontend-manager' ); ?></th>
						</tr>
					</tfoot>
				</table>


				<!-- play end here -->
				<div class="wcfm-clearfix"></div>
			</div>
			<div class="wcfm-clearfix"></div>
		</div>
	
		<div class="wcfm-clearfix"></div>
		<?php
		do_action( 'after_wcfm_build' );
		?>
	</div>
</div>