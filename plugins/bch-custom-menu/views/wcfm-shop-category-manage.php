<?php
global $WCFM, $wp_query, $wp;
$shop_category_repo = '';

$category_id = $category_name = $category_description = '';


if( isset( $wp->query_vars['wcfm-bch-shop-category-manage'] ) ) {

	if( empty( $wp->query_vars['wcfm-bch-shop-category-manage'] ) ) {

	} else {

		$shop_cat_id = $wp->query_vars['wcfm-bch-shop-category-manage'];
		$shop_category_repo = get_term($shop_cat_id, 'shop_category');

		$cat_vendor_id = get_term_meta($shop_category_repo->term_id, 'vendor_id', true);
		if($cat_vendor_id == get_current_user_id()) {
			//all pass
			//editable page
			$category_id = $shop_category_repo->term_id;
			$category_name = $shop_category_repo->name;
			$category_description = $shop_category_repo->description;
		} else {
			//redirect to add shop category
			$url = get_wcfm_edit_shop_category_url();
			wp_redirect($url);
			exit;
		}
	}
}

?>

<div class="collapse wcfm-collapse" id="wcfm_build_listing">

	<div class="wcfm-page-headig">
		<span class="wcfmfa fa fa-cubes"></span>
		<span class="wcfm-page-heading-text"><?php _e( 'Manage Shop Category', 'wcfm-custom-menus' ); ?></span>
		<?php do_action( 'wcfm_page_heading' ); ?>
	</div>
	<div class="wcfm-collapse-content">
		<div id="wcfm_page_load"></div>
		<?php do_action( 'before_wcfm_build' ); ?>
		
		<div class="wcfm-container wcfm-top-element-container">
			<h2><?php _e('Manage Shop Category', 'wcfm-custom-menus' ); ?></h2>
			<div class="wcfm-clearfix"></div>
	  </div>
	  <div class="wcfm-clearfix"></div><br />


		<div class="wcfm-container">
			<div id="wcfm_build_listing_expander" class="wcfm-content">

				<!---- Add Content Here ----->
				<form id="wcfm_edit_shop_cat" method="post">
					<?php $nonce = wp_create_nonce( 'edit_shop_cat' ); ?>
					<input type="hidden" name="edit_shop_cat" value="<?php echo $nonce ?>" />
					<input type="hidden" name="wcfm_action" value="edit_shop_cat" />
					<input type="hidden" name="shop_category_id" value="<?php echo $category_id; ?>" />
					<?php


					$WCFM->wcfm_fields->wcfm_generate_form_field(
						array(
							"category_name" => array(
								'label' => __( 'Category Name', 'wc-multivendor-marketplace'),
								'name' => 'category_name',
								'type' => 'text',
								'class' => 'wcfm-text',
								'label_class' => 'wcfm_title',
								'value' => $category_name
							),
							"category_description" => array(
								'label' => __('Description', 'wc-frontend-manager'),
								'type' => 'wpeditor',
								'name'	=> 'category_description',
								'class' => 'wcfm-textarea wcfm_ele wcfm_full_ele simple variable external grouped booking ' . $rich_editor,
								'label_class' => 'wcfm_title wcfm_full_ele wcfm_wpeditor',
								'rows' => 10,
								'value' => $category_description
							),
						),
					);

					?>
					<input type="submit" name="submit_shop_category" value="Save" id="wcfm_manage_shop_category_submit_button" class="wcfm_submit_button" />
				</form>

				<!-- play end here -->
				<div class="wcfm-clearfix"></div>
			</div>
			<div class="wcfm-clearfix"></div>
		</div>
	
		<div class="wcfm-clearfix"></div>
		<?php
		do_action( 'after_wcfm_build' );
		?>
	</div>
</div>