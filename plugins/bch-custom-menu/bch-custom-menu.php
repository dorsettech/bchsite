<?php

/**
 * Plugin Name: BCH Shop Section Main
 * Plugin URI: http://wclovers.com
 * Description: BCH Shop Section.
 * Author: Dorset Tech
 * Version: 2.0.0
 * Author URI: http://wclovers.com
 *
 * Text Domain: wcfm-custom-menus
 * Domain Path: /lang/
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 3.2.0
 *
 */
if (!defined('ABSPATH')){
    exit; // Exit if accessed directly
}
// if(!class_exists('WCFM')) return; // Exit if WCFM not installed

/**
 * WCFM - Custom Menus Query Var
 */
function wcfmcsm_query_vars($query_vars) {
    $wcfm_modified_endpoints = (array) get_option('wcfm_endpoints');

    $query_custom_menus_vars = array(
        'wcfm-bch-shop-section' => !empty($wcfm_modified_endpoints['wcfm-bch-shop-section']) ? $wcfm_modified_endpoints['wcfm-bch-shop-section'] : 'bch-shop-section',
        'wcfm-bch-shop-category' => !empty($wcfm_modified_endpoints['wcfm-bch-shop-category']) ? $wcfm_modified_endpoints['wcfm-bch-shop-category'] : 'bch-shop-category',
        'wcfm-bch-shop-category-manage' => !empty($wcfm_modified_endpoints['wcfm-bch-shop-category-manage']) ? $wcfm_modified_endpoints['wcfm-bch-shop-category-manage'] : 'bch-shop-category-manage',
    );

    $query_vars = array_merge($query_vars, $query_custom_menus_vars);

    return $query_vars;
}

add_filter('wcfm_query_vars', 'wcfmcsm_query_vars', 51);

/**
 * WCFM - Custom Menus End Point Title
 */
function wcfmcsm_endpoint_title($title, $endpoint) {
    global $wp;
    switch ($endpoint) {
        case 'wcfm-bch-shop-section' :
            $title = __('Shop Featured Product', 'wcfm-custom-menus');
            break;
        case 'wcfm-bch-shop-category' :
            $title = __('Manage Shop Category', 'wcfm-custom-menus');
            break;
        case 'wcfm-bch-shop-category-manage' :
            $title = __('Manage Shop Category', 'wcfm-custom-menus');
            break;
    }

    return $title;
}

add_filter('wcfm_endpoint_title', 'wcfmcsm_endpoint_title', 51, 2);

/**
 * WCFM - Custom Menus Endpoint Intialize
 */
function wcfmcsm_init() {
    global $WCFM_Query;

    // Intialize WCFM End points
    $WCFM_Query->init_query_vars();
    $WCFM_Query->add_endpoints();

    if (!get_option('wcfm_updated_end_point_cms')) {
        // Flush rules after endpoint update
        flush_rewrite_rules();
        update_option('wcfm_updated_end_point_cms', 1);
    }
}

add_action('init', 'wcfmcsm_init', 51);

/**
 * WCFM - Custom Menus Endpoiint Edit
 */
function wcfm_custom_menus_endpoints_slug($endpoints) {

    $custom_menus_endpoints = array(
        'wcfm-bch-shop-section' => 'bch-shop-section',
        'wcfm-bch-shop-category' => 'bch-shop-category',
        'wcfm-bch-shop-category-manage' => 'bch-shop-category-manage',
    );

    $endpoints = array_merge($endpoints, $custom_menus_endpoints);

    return $endpoints;
}

add_filter('wcfm_endpoints_slug', 'wcfm_custom_menus_endpoints_slug');

if (!function_exists('get_wcfm_custom_menus_url')) {

    function get_wcfm_custom_menus_url($endpoint) {
        global $WCFM;
        $wcfm_page = get_wcfm_page();
        $wcfm_custom_menus_url = wcfm_get_endpoint_url($endpoint, '', $wcfm_page);
        return $wcfm_custom_menus_url;
    }

}

/**
 * WCFM - Custom Menus
 */
function wcfmcsm_wcfm_menus($menus) {
    global $WCFM;

    // if(get_current_user_id() == '4070') {
    $custom_menus = array(
        'wcfm-bch-shop-section' => array(
            'label' => __('Manage Shop Section', 'wcfm-custom-menus'),
            'url' => get_wcfm_custom_menus_url('wcfm-bch-shop-section'),
            'icon' => 'cubes',
            'priority' => 999998
        ),
        'wcfm-bch-shop-category' => array(
            'label' => __('Shop Category', 'wcfm-custom-menus'),
            'url' => get_wcfm_custom_menus_url('wcfm-bch-shop-category'),
            'icon' => 'cubes',
            'priority' => 999
        ),
    );

    $menus = array_merge($menus, $custom_menus);
    // }
    return $menus;
}

add_filter('wcfm_menus', 'wcfmcsm_wcfm_menus', 91);

/**
 *  WCFM - Custom Menus Views
 */
function wcfm_csm_load_views($end_point) {
    global $WCFM, $WCFMu;
    $plugin_path = trailingslashit(dirname(__FILE__));
    // echo 'endpoint'.$end_point;
    switch ($end_point) {
        case 'wcfm-bch-shop-section':
            require_once( $plugin_path . 'views/wcfm-views-build.php' );
            break;
        case 'wcfm-bch-shop-category':
            require_once( $plugin_path . 'views/wcfm-shop-category-list.php' );
            break;
        case 'wcfm-bch-shop-category-manage':
            require_once( $plugin_path . 'views/wcfm-shop-category-manage.php' );
            break;
    }
}

add_action('wcfm_load_views', 'wcfm_csm_load_views', 51);
add_action('before_wcfm_load_views', 'wcfm_csm_load_views', 51);

// Custom Load WCFM Scripts
function wcfm_csm_load_scripts($end_point) {
    global $WCFM;
    $plugin_url = trailingslashit(plugins_url('', __FILE__));

    switch ($end_point) {
        case 'wcfm-bch-shop-section':
            wp_enqueue_script('shop_manage_select', $plugin_url . 'js/select2.min.js', array('jquery'));

            wp_enqueue_script('shop_manage', $plugin_url . 'js/shop_manage.js', array('jquery'));
            wp_localize_script('shop_manage', 'bths_ajax_scripts', array(
                'ajaxurl' => admin_url('admin-ajax.php'),
            ));
            break;
    }
}

add_action('wcfm_load_scripts', 'wcfm_csm_load_scripts');
add_action('after_wcfm_load_scripts', 'wcfm_csm_load_scripts');

// Custom Load WCFM Styles
function wcfm_csm_load_styles($end_point) {
    global $WCFM, $WCFMu;
    $plugin_url = trailingslashit(plugins_url('', __FILE__));

    switch ($end_point) {
        case 'wcfm-bch-shop-section':
            wp_enqueue_style('wcfmu_build_css_select', $plugin_url . 'css/select2.min.css', array(), $WCFM->version);
            wp_enqueue_style('wcfmu_build_css', $plugin_url . 'css/wcfm-style-build.css', array(), $WCFM->version);

            break;
    }
}

add_action('wcfm_load_styles', 'wcfm_csm_load_styles');
add_action('after_wcfm_load_styles', 'wcfm_csm_load_styles');



//save shop featured category
add_action('init', 'product_company_form_submission');

function product_company_form_submission() {
    if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['wcfm_action']) && $_POST['wcfm_action'] == "shop_manage") {
        $verify = wp_verify_nonce($_POST['shop_manage_nonce'], 'shop_manage_nonce');
        if ($verify) {
            if (wcfm_is_vendor() || current_user_can('administrator')) {
                $vendor_id = get_current_user_id();
                if (isset($_POST['product_section_cat']) && sizeof($_POST['product_section_cat']) > 0) {
                    $product_section_cat = implode(",", $_POST['product_section_cat']);
                    update_user_meta($vendor_id, 'product_section_cat', $product_section_cat);
                } else {
                    update_user_meta($vendor_id, 'product_section_cat', '');
                }
            }
        }
    }
}

require_once( plugin_dir_path(__FILE__) . '/helpers/bch-helper.php');


//save shop category
add_action('init', 'bch_handle_vendor_shop_category');

function bch_handle_vendor_shop_category() {
    if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['wcfm_action']) && $_POST['wcfm_action'] == "edit_shop_cat") {

        // echo "----------British-----------";
        // if (4070 == get_current_user_id()) {
        // 	echo "----------British-----------".get_current_user_id();
        // 	$store_name = get_user_meta( get_current_user_id(), 'store_name', true );
        // 	echo sanitize_title($store_name);
        // exit;
        // }
        $verify = wp_verify_nonce($_POST['edit_shop_cat'], 'edit_shop_cat');
        if ($verify) {
            if (wcfm_is_vendor() || current_user_can('administrator')) {
                $vendor_id = get_current_user_id();
                $term_id = '';
                if (isset($_POST['shop_category_id']) && $_POST['shop_category_id'] != '' && $_POST['shop_category_id'] != '0') {
                    //edit/update
                    $tax_data = wp_update_term(
                            $_POST['shop_category_id'], 'shop_category', array(
                        'description' => $_POST['category_description'],
                        'name' => $_POST['category_name'],
                            )
                    );
                    $term_id = (int) $tax_data->term_id;
                    update_term_meta($term_id, 'vendor_id', $vendor_id);
                } else {
                    //create
                    $store_name = get_user_meta($vendor_id, 'store_name', true);
                    if ($store_name == '') {
                        $user_repo = get_user_by('ID', $vendor_id);
                        $store_name = $user_repo->user_login;
                    }


                   $taxDataObj = new WPInsertTerm( 
                         $_POST['category_name'], // the term 
                            'shop_category', // the taxonomy
                            array(
                                'description' => $_POST['category_description'],
                                'slug' => sanitize_title($_POST['category_name']) . '-' . sanitize_title($store_name), //'-'.sanitize_title($vendor_id),
                            )
                    );
                    $new_tax_data = $taxDataObj->addTerm();
                    // if (get_current_user_id() == 3990 ) {
                    //     print "<pre>";
                    //     print_r($new_tax_data);
                    //     print_r($new_tax_data['term_id']);
                    //     print "</pre>";
                    //     exit;
                    // }
                    // $new_tax_data = wp_insert_term(
                    //         $_POST['category_name'], // the term 
                    //         'shop_category', // the taxonomy
                    //         array(
                    //             'description' => $_POST['category_description'],
                    //             'slug' => $new_slug,
                    //     sanitize_title($_POST['category_name']) . '-' . sanitize_title($store_name), //'-'.sanitize_title($vendor_id),
                    //         )
                    // );
                    
                    $term_id = (int) $new_tax_data['term_id'];
                    update_term_meta($term_id, 'vendor_id', $vendor_id);
                    $url = get_wcfm_edit_shop_category_url($term_id);
                    wp_redirect($url);
                    exit;
                }
                
                
            }
        }
    }
}



// =========
class WPInsertTerm {
    /**
     * Arguments to hold defaults
     * @since 1.0.0
     * @var array
    */
    protected $defaults = [
        'alias_of' => '', 
        'description' => '', 
        'parent' => 0, 
        'slug' => ''
    ];

    /**
     * Arguments set by user
     * @since 1.0.0
     * @var array
    */
    protected $args = [];

    /**
     * Term to insert
     * @since 1.0.0
     * @var string
    */
    protected $term = null;

    /**
     * Taxonomy the term should belong to
     * @since 1.0.0
     * @var string
    */
    protected $taxonomy = null;

    /**
     * Constructor
     *
     * @param string $term = null
     * @param string $taxonomy = null
     * @param array  $args = []
     * @since 1.0.0
     */     
    public function __construct( $term = null, $taxonomy = null, $args = [] ) {
        $this->term     = $term;
        $this->taxonomy = $taxonomy;
        if ( is_array( $args ) ) {
            $this->args = array_merge( $this->defaults, $args );
        } else { 
            $this->args = $this->defaults;
        }
    }

    /**
     * Public method wpdb()
     *
     * Returns the global wpdb class
     *
     * @since 1.0.0
     * @return $wpdb
     */
    public function wpdb() {
        global $wpdb;

        return $wpdb;
    }

    /**
     * Private method validateVersion()
     *
     * Validate the current WordPress version
     *
     * @since 1.0.0
     * @return $validateVersion
     */
    private function validateVersion() {
        global $wp_version;

        $validateVersion = false;

        if ( '4.4' > $wp_version ) {
            throw new InvalidArgumentException( 
                sprintf(
                    __( 'Your WordpPress version is too old. A minimum version of WordPress 4.4 is expected. Please upgrade' ),
                    __METHOD__
                )
            );
        }

        return $validateVersion = true;
    }

    /**
     * Private method validateTaxonomy()
     *
     * Validate the $taxonomy value
     *
     * @since 1.0.0
     * @return $validateTaxonomy
     */
    private function validateTaxonomy() {
        $validateTaxonomy = filter_var( $this->taxonomy, FILTER_SANITIZE_STRING );
        // Check if taxonomy is valid
        if ( !taxonomy_exists( $validateTaxonomy )  ) {
            throw new InvalidArgumentException( 
                sprintf(
                    __( 'Your taxonomy does not exists, please add a valid taxonomy' ),
                    __METHOD__
                )
            );
        }

        return $validateTaxonomy;
    }

    /**
     * Private method validateTerm()
     *
     * Validate the $term value
     *
     * @since 1.0.0
     * @return $validateTerm
     */
    private function validateTerm() {
        /**
         * Filter a term before it is sanitized and inserted into the database.
         *
         * @since 1.0.0
         *
         * @param string $term     The term to add or update.
         * @param string $taxonomy Taxonomy slug.
         */
        $validateTerm = apply_filters( 'pre_insert_term', $this->term, $this->validateTaxonomy() );     

        // Check if the term is not empty
        if ( empty( $validateTerm ) ) {
            throw new InvalidArgumentException( 
                sprintf(
                    __( '$term should not be empty, please add a valid value' ),
                    __METHOD__
                )
            );
        }

        // Check if term is a valid integer if integer is passed
        if (    is_int( $validateTerm )
             && 0 == $validateTerm
        ){
            throw new InvalidArgumentException( 
                sprintf(
                    __('Invalid term id supplied, please asdd a valid value'),
                    __METHOD__
                )
            );
        }

        // Term is not empty, sanitize the term and trim any white spaces
        $validateTerm = filter_var( trim( $validateTerm ), FILTER_SANITIZE_STRING );
        if ( empty( $validateTerm ) ){
            throw new InvalidArgumentException( 
                sprintf(
                    __( 'Invalid term supplied, please asdd a valid term name' ),
                    __METHOD__
                )
            );
        }

        return $validateTerm;
    }

    /**
     * Private method parentExist()
     *
     * Validate if the parent term exist if passed
     *
     * @since 1.0.0
     * @return $parentexist
     */
    private function parentExist() {
        $parentExist = $this->args['parent'];

        if (    $parentExist > 0
             && !term_exists( (int) $parentExist ) 
        ) {
            throw new InvalidArgumentException( 
                sprintf(
                    __( 'Invalid parent ID supplied, no term exists with parent ID passed. Please add a valid parent ID' ),
                    __METHOD__
                )
            );
        }

        return $parentExist;
    }

    /**
     * Private method sanitizeTerm()
     *
     * Sanitize the term to insert
     *
     * @since 1.0.0
     * @return $sanitizeTerm
     */
    private function sanitizeTerm() {
        $taxonomy              = $this->validateTaxonomy();
        $arguments             = $this->args;

        $arguments['taxonomy'] = $taxonomy;
        $arguments['name']     = $this->validateTerm();
        $arguments['parent']   = $this->parentExist();

        // Santize the term 
        $arguments = sanitize_term( $arguments, $taxonomy, 'db' );

        // Unslash name and description fields and cast parent to integer
        $arguments['name']        = wp_unslash( $arguments['name'] );
        $arguments['description'] = wp_unslash( $arguments['description'] );
        $arguments['parent']      = (int) $arguments['parent'];

        return (object) $arguments;
    }

    /**
     * Private method slug()
     *
     * Get or create a slug if no slug is set
     *
     * @since 1.0.0
     * @return $slug
     */
    private function slug() {
        $term = $this->sanitizeTerm();
        $new_slug = $term->slug;
        if ( !$new_slug ) {
            $slug = sanitize_title( $term->name );
        } else {
            $slug = $new_slug;
        }

        return $slug;
    }

    /**
     * Public method addTerm()
     *
     * Add the term to db
     *
     * @since 1.0.0
     */
    public function addTerm() {
        $wpdb       = $this->wpdb();
        $term       = $this->sanitizeTerm();
        $taxonomy   = $term->taxonomy;
        $name       = $term->name;
        $parent     = $term->parent;
        $description     = $term->description;
        $term_group = $term->term_group;

        $term_group = 0;

        if ( $term->alias_of ) {
            $alias = get_term_by( 
                'slug', 
                $term->alias_of, 
                $term->taxonomy 
            );
            if ( !empty( $alias->term_group ) ) {
                // The alias we want is already in a group, so let's use that one.
                $term_group = $alias->term_group;
            } elseif ( ! empty( $alias->term_id ) ) {
                /*
                 * The alias is not in a group, so we create a new one
                 * and add the alias to it.
                 */
                $term_group = $wpdb->get_var(
                    "SELECT MAX(term_group) 
                    FROM $wpdb->terms"
                ) + 1;

                wp_update_term( 
                    $alias->term_id, 
                    $this->args['taxonomy'], 
                    [
                        'term_group' => $term_group,
                    ] 
                );
            }
        }

        $slug = wp_unique_term_slug( 
            $this->slug(), 
            $term
        );

        if ( false === $wpdb->insert( $wpdb->terms, compact( 'name', 'slug', 'term_group' ) ) ) {
            return new WP_Error( 'db_insert_error', __( 'Could not insert term into the database' ), $wpdb->last_error );
        }

        $term_id = (int) $wpdb->insert_id;

        // Seems unreachable, However, Is used in the case that a term name is provided, which sanitizes to an empty string.
        if ( empty( $slug ) ) {
            $slug = sanitize_title( 
                $slug, 
                $term_id 
            );

            /** This action is documented in wp-includes/taxonomy.php */
            do_action( 'edit_terms', $term_id, $taxonomy );
            $wpdb->update( $wpdb->terms, compact( 'slug' ), compact( 'term_id' ) );

            /** This action is documented in wp-includes/taxonomy.php */
            do_action( 'edited_terms', $term_id, $taxonomy );
        }

        $tt_id = $wpdb->get_var( 
            $wpdb->prepare( "
                SELECT tt.term_taxonomy_id 
                FROM $wpdb->term_taxonomy AS tt 
                INNER JOIN $wpdb->terms AS t 
                ON tt.term_id = t.term_id 
                WHERE tt.taxonomy = %s 
                AND t.term_id = %d
            ", 
            $taxonomy, 
            $term_id 
            ) 
        );

        if ( !empty($tt_id) ) {
            return [
                'term_id'          => $term_id, 
                'term_taxonomy_id' => $tt_id
            ];
        }

        $wpdb->insert( 
            $wpdb->term_taxonomy, 
            compact( 'term_id', 'taxonomy', 'description', 'parent') + ['count' => 0] 
        );
        $tt_id = (int) $wpdb->insert_id;


        /**
         * Fires immediately after a new term is created, before the term cache is cleaned.
         *
         * @since 2.3.0
         *
         * @param int    $term_id  Term ID.
         * @param int    $tt_id    Term taxonomy ID.
         * @param string $taxonomy Taxonomy slug.
         */
        do_action( "create_term", $term_id, $tt_id, $taxonomy );

        /**
         * Fires after a new term is created for a specific taxonomy.
         *
         * The dynamic portion of the hook name, `$taxonomy`, refers
         * to the slug of the taxonomy the term was created for.
         *
         * @since 2.3.0
         *
         * @param int $term_id Term ID.
         * @param int $tt_id   Term taxonomy ID.
         */
        do_action( "create_$taxonomy", $term_id, $tt_id );

        /**
         * Filter the term ID after a new term is created.
         *
         * @since 2.3.0
         *
         * @param int $term_id Term ID.
         * @param int $tt_id   Taxonomy term ID.
         */
        $term_id = apply_filters( 'term_id_filter', $term_id, $tt_id );

        clean_term_cache($term_id, $taxonomy);

        /**
         * Fires after a new term is created, and after the term cache has been cleaned.
         *
         * @since 2.3.0
         *
         * @param int    $term_id  Term ID.
         * @param int    $tt_id    Term taxonomy ID.
         * @param string $taxonomy Taxonomy slug.
         */
        do_action( 'created_term', $term_id, $tt_id, $taxonomy );

        /**
         * Fires after a new term in a specific taxonomy is created, and after the term
         * cache has been cleaned.
         *
         * The dynamic portion of the hook name, `$taxonomy`, refers to the taxonomy slug.
         *
         * @since 2.3.0
         *
         * @param int $term_id Term ID.
         * @param int $tt_id   Term taxonomy ID.
         */
        do_action( "created_$taxonomy", $term_id, $tt_id );
        // exit('ssjdkfsdkjfsdkjfdskjfdkjsfkj');
        return [
            'term_id'          => $term_id, 
            'term_taxonomy_id' => $tt_id
        ];      
    }
}