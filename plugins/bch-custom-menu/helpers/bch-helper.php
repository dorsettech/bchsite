<?php
//register shop category
function bch_register_shop_category() {

	$labels = [
		"name" => __( "Shop Categories", "twentyseventeen" ),
		"singular_name" => __( "Shop Category", "twentyseventeen" ),
	];

	$args = [
		"label" => __( "Shop Categories", "twentyseventeen" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'shop_category', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "shop_category",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "shop_category", [ "product" ], $args );

}
add_action( 'init', 'bch_register_shop_category' );

//get endpoint url for shop category
if(!function_exists('get_wcfm_edit_shop_category_url')) {
	function get_wcfm_edit_shop_category_url( $cat_id = '', $the_cat = array(), $language_code = '' ) {
		global $WCFM;
		$wcfm_page = get_wcfm_page( $language_code );
		$wcfm_edit_product_url = wcfm_get_endpoint_url( 'wcfm-bch-shop-category-manage', $cat_id, $wcfm_page );
		return $wcfm_edit_product_url;
	}
}

add_filter( 'wcfm_is_allow_custom_taxonomy', '__return_false' );


//add shop category to add/edit product
add_action('after_wcfm_products_manage_taxonomies', 'bch_add_shop_category_to_sidebar');
function bch_add_shop_category_to_sidebar($product_id) {
	// if(get_current_user_id() == 4070) {
		global $WCFM;
		$taxonomy = "shop_category";
		$args = array(
			'hide_empty' => false,
			'meta_query' => array(
				array(
					'key'		=> 'vendor_id',
					'value'		=> get_current_user_id(),
				)
			),
			'taxonomy'  => 'shop_category',
		);
		$shop_categories = get_terms( $args );
		$productshopcategories = array();

		$pshop_categories = get_the_terms( $product_id, 'shop_category' );
		if( !empty($pshop_categories) ) {
			foreach($pshop_categories as $pkey => $spcategory) {
				$productshopcategories[] = $spcategory->term_id;
			}
		} else {
			$productshopcategories = array();
		}

		//echo new field goes here' ?>
		<div class="wcfm_product_manager_cats_checklist_fields">
		<p class="wcfm_title wcfm_full_ele"><strong><?php echo 'Shop Category'; ?></strong></p><label class="screen-reader-text" for="shop_category"><?php echo 'Shop Category'; ?></label>

			<ul id="product_cats_checklist" class="product_taxonomy_checklist product_taxonomy_checklist_product_cat wcfm_ele simple variable external grouped booking">
				<?php
					if ( $shop_categories ) {

						foreach ( $shop_categories as $cat ) {
							echo '<li class="product_cats_checklist_item checklist_item_' . esc_attr( $cat->term_id ) . '" data-item="' . esc_attr( $cat->term_id ) . '">';

							echo '<input type="checkbox" class="wcfm-checkbox checklist_type_' . $taxonomy . '" name="shop_category[]" value="' . esc_attr( $cat->term_id ) . '"' . checked( in_array( $cat->term_id, $productshopcategories ), true, false ) . '/><span>' . __( esc_html( $cat->name ), 'wc-frontend-manager' ) . '</span>';
						}

					}
				?>
			</ul>
		</div>
	<?php //}
}


//save shop category on add/edit product
add_action ('after_wcfm_products_manage_meta_save', 'bch_save_custom_shop_category', 10, 2);
function bch_save_custom_shop_category($product_id, $form_data ) {

	// if(get_current_user_id() == 4070) {

		if(isset($form_data['shop_category']) && sizeof($form_data['shop_category']) > 0 ) {

			$term_taxonomy_ids = wp_set_post_terms( $product_id, $form_data['shop_category'], 'shop_category' );
		} else {

			//remove //detach
			wp_delete_object_term_relationships( $product_id, 'shop_category' );
		}

	// }

}

// if custom taxonomy search for shop section filter by vendor query
add_filter( 'wcfm_json_search_taxonomies', 'bch_shop_section_taxonomy_filter', 10 , 2 );
function bch_shop_section_taxonomy_filter($term_list, $taxonomy) {
	$matched_filter = array();
	if($taxonomy == "shop_category") {
		$term = wc_clean( empty( $term ) ? sanitize_text_field( $_REQUEST['term'] ) : $term );

		$args = array(
			'hide_empty' => false,
			'search'     => $term, 
			'meta_query' => array(
				array(
					'key'		=> 'vendor_id',
					'value'		=> get_current_user_id(),
				)
			),
			'taxonomy'  => 'shop_category',
		);
		$shop_categories = get_terms( $args );

		if( !empty( $shop_categories ) ) {
			foreach ( $shop_categories as $term ) {
				$matched_filter[esc_attr($term->term_id)] = esc_html($term->name);
			}
		}
		$term_list = $matched_filter;
	}

	return $term_list;
}
