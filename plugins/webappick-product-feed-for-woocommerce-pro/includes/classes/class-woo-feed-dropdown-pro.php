<?php
/**
 * The file that defines the merchants attributes dropdown
 *
 * A class definition that includes attributes dropdown and functions used across the admin area.
 *
 * @link       https://webappick.com/
 * @since      1.0.0
 *
 * @package    Woo_Feed
 * @subpackage Woo_Feed/includes
 * @author     Ohidul Islam <wahid@webappick.com>
 */

class Woo_Feed_Dropdown_Pro extends Woo_Feed_Dropdown {

	public $cats = [];

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Dropdown of Merchant List
	 *
	 * @param string $selected
	 *
	 * @return string
	 */
	public function merchantsDropdown( $selected = '' ) {
		$options = $this->get_cached_dropdown( 'merchantsDropdown', $selected );
		if ( false === $options ) {
			$attributes = new Woo_Feed_Merchant_Pro();
			$options = $this->cache_dropdown( 'merchantsDropdown', $attributes->merchants(), $selected );
		}
		return $options;
	}

	/**
	 * Get Active Languages for current site.
	 *
	 * @param string $selected
	 *
	 * @return string
	 */
	public function getActiveLanguages( $selected = '' ) {
		$options = $this->get_cached_dropdown( 'getActiveLanguages', $selected );
		if ( false === $options ) {
			$languages = [];
			if ( class_exists( 'SitePress' ) ) {
				$get_languages = apply_filters( 'wpml_active_languages', null, 'orderby=id&order=desc' );
				if ( ! empty( $get_languages ) ) {
					foreach ( $get_languages as $key => $language ) {
						$languages[ $key ] = $language['translated_name'];
					}
				}
			}

			// when polylang plugin is activated
			if ( defined( 'POLYLANG_BASENAME' ) || function_exists( 'PLL' ) ) {
				// polylang language names
				$poly_languages_names = pll_languages_list( ['fields' => 'name']);

				// polylang language locales
				$poly_languages_slugs = pll_languages_list( ['fields' => 'slug']);

				// polylang language lists
				$get_languages = array_combine( $poly_languages_slugs, $poly_languages_names );

				if ( ! empty( $get_languages ) ) {
					$languages = [];
					foreach ( $get_languages as $key => $value ) {
						$languages[ $key ] = $value;
					}
				}
			}


			//when translatepress is activated
			if ( is_plugin_active( 'translatepress-multilingual/index.php' ) ) {
				if ( class_exists( 'TRP_Translate_Press' ) ) {
					$tr_press_languages = trp_get_languages( 'default' );

					if ( ! empty( $tr_press_languages ) ) {
						foreach ( $tr_press_languages as $key => $value ) {
							$languages[ $key ] = $value;
						}
					}
				}
			}

			//language dropdown
			$options = $this->cache_dropdown( 'getActiveLanguages', $languages, $selected, __( 'Select Language', 'woo-feed' ) );

		}

		return $options;
	}

    /**
     * Get Active Currency
     * @param string $selected
     *
     * @return false|mixed|string
     * @since 3.3.2
     */
	public function getActiveCurrencies( $selected = '' ) {
		$options = $this->get_cached_dropdown( 'getActiveCurrencies', $selected );
		if ( false === $options ) {
			global $woocommerce_wpml;
			if ( class_exists( 'SitePress' ) && class_exists( 'woocommerce_wpml' ) && isset( $woocommerce_wpml->multi_currency->currencies ) ) {
				$get_currencies = $woocommerce_wpml->multi_currency->currencies;
				if ( ! empty( $get_currencies ) ) {
					$currencies = [];
					foreach ( $get_currencies as $key => $currency ) {
						$currencies[ $key ] = $key;
					}
					$options = $this->cache_dropdown( 'getActiveCurrencies', $currencies, $selected, __( 'Select Currency', 'woo-feed' ) );
				}
			} elseif ( class_exists( 'WC_Aelia_CurrencySwitcher' ) ) {
				$base_currency  = get_woocommerce_currency();
				$get_currencies = apply_filters( 'wc_aelia_cs_enabled_currencies', $base_currency );

				// Fixed warning with Alia Currency Plugin's initial settings when activated.
				if ( ! empty( $get_currencies ) ) {

					if ( is_array( $get_currencies ) ) {
						$currencies = [];
						foreach ( $get_currencies as $currency ) {
							$currencies[ $currency ] = $currency;
						}
					} elseif ( gettype( $get_currencies ) === 'string' ) {
						$currencies = [
							$get_currencies => $get_currencies,
                        ];
					} else {
						$currencies = [];
					}


					$options = $this->cache_dropdown( 'getActiveCurrencies', $currencies, $selected, __( 'Select Currency', 'woo-feed' ) );
				}
			} elseif ( class_exists( 'WOOCS' ) ) {
				global $WOOCS;
				$get_currencies = $WOOCS->get_currencies();
				if ( ! empty( $get_currencies ) ) {
					$currencies = [];
					foreach ( $get_currencies as $key => $currency ) {
						$currencies[ $key ] = $key;
					}
					$options = $this->cache_dropdown( 'getActiveCurrencies', $currencies, $selected, __( 'Select Currency', 'woo-feed' ) );
				}
			} elseif ( is_plugin_active( 'currency-switcher-woocommerce/currency-switcher-woocommerce.php' ) ) {

				if ( function_exists( 'alg_get_enabled_currencies' ) ) {
					$currencies = alg_get_enabled_currencies();
					$currencies = array_combine( $currencies, $currencies );

					$options = $this->cache_dropdown( 'getActiveCurrencies', $currencies, $selected, __( 'Select Currency', 'woo-feed' ) );
				}
			}
		}

		return $options;
	}

	/**
	 * Get All Product Categories
	 *
	 * @param int $parent Category Parent ID.
	 *
	 * @return array
	 */
	public function get_categories( $parent = 0 ) {

		$args = [
			'taxonomy'     => 'product_cat',
			'parent'       => $parent,
			'orderby'      => 'term_group',
			'show_count'   => 1,
			'pad_counts'   => 1,
			'hierarchical' => 1,
			'title_li'     => '',
			'hide_empty'   => 0,
        ];

		$categories = get_categories( $args );
		if ( ! empty( $categories ) ) {
			foreach ( $categories as $cat ) {
				$this->cats[ $cat->slug ] = $cat->name;
				$this->get_categories( $cat->term_id );
			}
		}

		return $this->cats;
	}

	/**
	 * Get All Product Category
	 *
	 * @return array
	 */
	public function categories() {
		$categories = woo_feed_get_cached_data( 'woo_feed_dropdown_product_categories' );
		if ( false === $categories ) {
			$categories = $this->get_categories();
			woo_feed_set_cache_data( 'woo_feed_dropdown_product_categories', $categories );
		}

		return (array) $categories;
	}

	/**
	 * Get WooCommerce Vendor List for multi-vendor shop
	 *
	 * @return false|WP_User[]|array
	 */
	public function getAllVendors() {
		$users = woo_feed_get_cached_data( 'woo_feed_dropdown_product_vendors' );
		if ( false === $users ) {
			$users = [];
			$vendor_role = woo_feed_get_multi_vendor_user_role();
			if ( ! empty( $vendor_role ) ) {
				/**
				 * Filter Get Vendor (User) Query Args
				 *
				 * @param array $args
				 */
				$args = apply_filters( 'woo_feed_get_vendors_args', ['role' => $vendor_role]);
				if ( is_array( $args ) && ! empty( $args ) ) {
					$users = get_users( $args );
				}
			}
			woo_feed_set_cache_data( 'woo_feed_dropdown_product_vendors', $users );
		}

		return apply_filters( 'woo_feed_product_vendors', $users );
	}

	// Product DropDowns.

	/**
	 * Get All Custom Attributes
	 *
	 * @return array
	 */
	protected function getProductMetaKeys() {
		$info = woo_feed_get_cached_data( 'woo_feed_dropdown_meta_keys' );
		if ( false === $info ) {
			global $wpdb;
			$info = [];
			// Load the main attributes.

			$default_exclude_keys = [
				// WP internals.
				'_edit_lock',
				'_wp_old_slug',
				'_edit_last',
				'_wp_old_date',
				// WC internals.
				'_downloadable_files',
				'_sku',
				'_weight',
				'_width',
				'_height',
				'_length',
				'_file_path',
				'_file_paths',
				'_default_attributes',
				'_product_attributes',
				'_children',
				'_variation_description',
				// ignore variation description, engine will get child product description from WC CRUD WC_Product::get_description().
				// Plugin Data.
				'_wpcom_is_markdown',
				// JetPack Meta.
				'_yith_wcpb_bundle_data',
				// Yith product bundle data.
				'_et_builder_version',
				// Divi builder data.
				'_vc_post_settings',
				// Visual Composer (WP Bakery) data.
				'_enable_sidebar',
				'frs_woo_product_tabs',
				// WooCommerce Custom Product Tabs http://www.skyverge.com/.
            ];

			/**
			 * Exclude meta keys from dropdown
			 *
			 * @param array $exclude meta keys to exclude.
			 * @param array $default_exclude_keys Exclude keys by default.
			 */
			$user_exclude = apply_filters( 'woo_feed_dropdown_exclude_meta_keys', null, $default_exclude_keys );

			if ( is_array( $user_exclude ) && ! empty( $user_exclude ) ) {
				$user_exclude         = esc_sql( $user_exclude );
				$default_exclude_keys = array_merge( $default_exclude_keys, $user_exclude );
			}

			$default_exclude_keys = array_map( 'esc_sql', $default_exclude_keys );
			$exclude_keys         = '\'' . implode( '\', \'', $default_exclude_keys ) . '\'';

			$default_exclude_key_patterns = [
				'%_et_pb_%', // Divi builder data
				'attribute_%', // Exclude product attributes from meta list
				'_yoast_wpseo_%', // Yoast SEO Data
				'_acf-%', // ACF duplicate fields
				'_aioseop_%', // All In One SEO Pack Data
				'_oembed%', // exclude oEmbed cache meta
				'_wpml_%', // wpml metas
				'_oh_add_script_%', // SOGO Add Script to Individual Pages Header Footer.
            ];

			/**
			 * Exclude meta key patterns from dropdown
			 *
			 * @param array $exclude meta keys to exclude.
			 * @param array $default_exclude_key_patterns Exclude keys by default.
			 */
			$user_exclude_patterns = apply_filters( 'woo_feed_dropdown_exclude_meta_keys_pattern', null, $default_exclude_key_patterns );
			if ( is_array( $user_exclude_patterns ) && ! empty( $user_exclude_patterns ) ) {
				$default_exclude_key_patterns = array_merge( $default_exclude_key_patterns, $user_exclude_patterns );
			}
			$exclude_key_patterns = '';
			foreach ( $default_exclude_key_patterns as $pattern ) {
				$exclude_key_patterns .= $wpdb->prepare( ' AND meta_key NOT LIKE %s', $pattern );
			}

			$sql = "SELECT DISTINCT( meta_key ) FROM $wpdb->postmeta WHERE 1=1 AND post_id IN ( SELECT ID FROM $wpdb->posts WHERE post_type = 'product' OR post_type = 'product_variation' ) AND ( meta_key NOT IN ( $exclude_keys ) $exclude_key_patterns )";

			// sql escaped, cached
			$data = $wpdb->get_results( $sql ); // phpcs:ignore

			if ( count( $data ) ) {
				foreach ( $data as $value ) {
					//TODO Remove ACF Fields
					$info[ Woo_Feed_Products_v3_Pro::POST_META_PREFIX . $value->meta_key ] = $value->meta_key;
				}
			}
			woo_feed_set_cache_data( 'woo_feed_dropdown_meta_keys', $info );
		}

		return (array) $info;
	}

	/**
	 * Get Attribute Mappings
	 *
	 * @return array
	 */
	protected function getCustomMappedAttributes() {
		global $wpdb;
		// Load Custom Category Mapped Attributes
		$info = [];
		// query cached and escaped
		$data = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->options WHERE option_name LIKE %s;", Woo_Feed_Products_v3_Pro::PRODUCT_ATTRIBUTE_MAPPING_PREFIX . '%' ) );  // phpcs:ignore
		if ( count( $data ) ) {
			foreach ( $data as $value ) {
				$opts                        = maybe_unserialize( $value->option_value );
				$info[ $value->option_name ] = is_array( $opts ) && isset( $opts['name'] ) ? $opts['name'] : str_replace( Woo_Feed_Products_v3_Pro::PRODUCT_ATTRIBUTE_MAPPING_PREFIX, '', $value->option_name );
			}
		}

		return (array) $info;
	}

	/**
	 * Get Dynamic Attribute List
	 *
	 * @return array
	 */
	protected function dynamicAttributes() {
		global $wpdb;

		// Load Custom Category Mapped Attributes
		$info = array();
		// query escaped and cached
		$data = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->options WHERE option_name LIKE %s;", Woo_Feed_Products_v3_Pro::PRODUCT_DYNAMIC_ATTRIBUTE_PREFIX . '%' ) ); // phpcs:ignore
		if ( count( $data ) ) {
			foreach ( $data as $key => $value ) {
				$opts                        = maybe_unserialize( $value->option_value );
				$opts                        = maybe_unserialize( $opts );
				$info[ $value->option_name ] = is_array( $opts ) && isset( $opts['wfDAttributeName'] ) ? $opts['wfDAttributeName'] : str_replace(
					'wf_dattribute_',
					'',
					$value->option_name
				);
			}
		}

		return (array) $info;
	}

	/**
	 * Product Attributes
	 *
	 * @return array
	 */
	protected function get_product_attributes() {

		$attributes = parent::get_product_attributes();

        if ( woo_feed_is_multi_vendor() ) {
            $multi_vendor=[
                'vendor_store_name'=> esc_html__( 'Vendor Store Name', 'woo-feed' )
            ];
            $attributes['--95']  = esc_html__( 'Multi Vendor', 'woo-feed' );
            $attributes          += $multi_vendor;
            $attributes['---95'] = '';
        }

		$_dynamic_attributes = $this->dynamicAttributes();
		if ( ! empty( $_dynamic_attributes ) && is_array( $_dynamic_attributes ) ) {
			$attributes['--96']  = esc_html__( 'Dynamic Attributes', 'woo-feed' );
			$attributes          += $_dynamic_attributes;
			$attributes['---96'] = '';
		}

		$_attribute_mappings = $this->getCustomMappedAttributes();
		if ( ! empty( $_attribute_mappings ) && is_array( $_attribute_mappings ) ) {
			$attributes['--97']  = esc_html__( 'Attribute Mappings', 'woo-feed' );
			$attributes          += $_attribute_mappings;
			$attributes['---97'] = '';
		}

		$_acf_fields = woo_feed_get_acf_field_list();
		if ( ! empty( $_acf_fields ) && is_array( $_acf_fields ) ) {
			$attributes['--98']  = esc_html__( 'Advance Custom Fields (ACF)', 'woo-feed' );
			$attributes          += $_acf_fields;
			$attributes['---98'] = '';
		}

		$_meta_keys = $this->getProductMetaKeys();
		if ( ! empty( $_meta_keys ) && is_array( $_meta_keys ) ) {

			unset( $attributes['--55'], $attributes['---55'] );

			$attributes['--99']  = esc_html__( 'Custom Fields & Post Metas', 'woo-feed' );
			$attributes          += $_meta_keys;
			$attributes['---99'] = '';
		}

		// Extra Attributes.
		$extra_attributes = apply_filters( 'woo_feed_dropdown_product_extra_attributes', null );
		if ( is_array( $extra_attributes ) && ! empty( $extra_attributes ) ) {
			$attributes['--100']  = esc_html__( 'Extra Attributes', 'woo-feed' );
			$attributes           += [];
			$attributes['---100'] = '';
		}

        // WPML attribute
        if( class_exists( 'SitePress' ) ){

            $attributes = woo_feed_insert_after_key( 'id', $attributes, 'parent_id', esc_html__( 'Parent Product ID (WPML)', 'woo-feed' ) );

        }

		return apply_filters( 'woo_feed_product_attribute_dropdown', $attributes );
	}

	/**
	 * Read txt file which contains google taxonomy list
	 *
	 * @param string $selected
	 *
	 * @return string
	 */
	public function googleTaxonomy( $selected = '' ) {

	    // Get All Google Taxonomies
		$fileName = plugin_dir_path( __FILE__ )
            . '..' . DIRECTORY_SEPARATOR
            . '..' . DIRECTORY_SEPARATOR
            . 'libs' . DIRECTORY_SEPARATOR
            . 'webappick-product-feed-for-woocommerce' . DIRECTORY_SEPARATOR
            . 'admin' . DIRECTORY_SEPARATOR
            . 'partials'. DIRECTORY_SEPARATOR
            . 'templates' . DIRECTORY_SEPARATOR
            . 'taxonomies' . DIRECTORY_SEPARATOR
            . 'google_taxonomy.txt';

		$customTaxonomyFile = fopen( $fileName, 'r' ); // phpcs:ignore
		$str                = '';
		if ( ! empty( $selected ) ) {
			$selected = trim( $selected );
			if ( ! is_numeric( $selected ) ) {
				$selected = html_entity_decode( $selected );
			} else {
				$selected = (int) $selected;
			}
		}
		if ( $customTaxonomyFile ) {
			// First line contains metadata, ignore it
			fgets( $customTaxonomyFile ); // phpcs:ignore
			while ( $line = fgets( $customTaxonomyFile ) ) { // phpcs:ignore
				list( $catId, $cat ) = explode( '-', $line );
				$catId = (int) trim( $catId );
				$cat   = trim( $cat );
                $is_selected = selected( $selected, is_numeric( $selected ) ? $catId : $cat, false );
				$str   .= "<option value='$catId' $is_selected>$cat</option>";
			}
		}
		if ( ! empty( $str ) ) {
			$str = '<option></option>' . $str;
		}

		return $str;
	}

	/**
	 * Read txt file which contains google taxonomy list
	 *
	 * @return array
	 */
	public function googleTaxonomyArray() {
		// Get All Google Taxonomies
		$fileName           = WOO_FEED_FREE_ADMIN_PATH . '/partials/templates/taxonomies/google_taxonomy.txt';
		$customTaxonomyFile = fopen( $fileName, 'r' );  // phpcs:ignore
		$taxonomy           = [];
		if ( $customTaxonomyFile ) {
			// First line contains metadata, ignore it
			fgets( $customTaxonomyFile );  // phpcs:ignore
			while ( $line = fgets( $customTaxonomyFile ) ) {  // phpcs:ignore
				list( $catId, $cat ) = explode( '-', $line );
				$taxonomy[] = [
					'value' => absint( trim( $catId ) ),
					'text'  => trim( $cat ),
                ];
			}
		}

		return array_filter( $taxonomy );
	}
}
